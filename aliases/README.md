# Aliases

## Content

- [Applications](./application-aliases.sh) › Application aliases
- [Common](./common-aliases.sh) › Common aliases
- [Directory](./directory-aliases.sh) › Directory aliases
- [Enhanced](./enhanced-aliases.sh) › Enhanced aliases
- [GIT](./git-aliases.sh) › Some `git` aliases

<!-- * [Application Aliases](./application-aliases.sh) - Application Aliases
* [Common Aliases](./common-aliases.sh) - Common Aliases
* [Directory Aliases](./directory-aliases.sh) - Directory Aliases
* [Enhanced Aliases](./enhanced-aliases.sh) - Enhanced Aliases
* [``GIT`` Aliases](./git-aliases.sh) - GIT Aliases -->

<!-- > ⚙️ -->

## Application Aliases

| ALIAS       | COMMAND                                   | DESCRIPTION                                                                   |
| ----------- | ----------------------------------------- | ----------------------------------------------------------------------------- |
| v           | `nvim`                                    | NeoVim's command                                                              |
| p           | `pandoc`                                  | The Pandoc command                                                            |
| m           | `mpv`                                     | The MPV command                                                               |
| r           | `ranger`                                  | The Ranger command                                                            |
| trc         | `transmission-remote-cli`                 | Transmission's remote CLI command                                             |
| trss        | `transmission-rss`                        | Transmission's RSS command                                                    |
| pman        | `sudo pacman`                             | ArchLinux's pacman command                                                    |
| pip2        | `sudo pip2`                               | Python's 2 PIP command                                                        |
| pip3        | `sudo pip3`                               | Python's 3 PIP command                                                        |
| nb          | `newsboat`                                | The NewsBoat command                                                          |
| jp          | `joplin`                                  | The Joplin command                                                            |
| xup         | `xrdb .Xresources`                        | The Xrdb command                                                              |
| libreoffice | `env GTK_THEME=Yaru:light libreoffice`    | The libreoffice command                                                       |
| hh          | `hstr`                                    | The HSTR command                                                              |
| y           | `yarn`                                    | The Yarn command                                                              |
| ya          | `yarn add`                                | Install a package in dependencies (`package.json`)                            |
| yad         | `yarn add --dev`                          | Install a package in devDependencies (`package.json`)                         |
| yap         | `yarn add --peer`                         | Install a package in peerDependencies (`package.json`)                        |
| yb          | `yarn build`                              | Run the build script defined in `package.json`                                |
| ycc         | `yarn cache clean`                        | Clean yarn's global cache of packages                                         |
| yd          | `yarn dev`                                | Run the dev script defined in `package.json`                                  |
| yga         | `yarn global add`                         | Install packages globally on your operating system                            |
| ygls        | `yarn global list`                        | Lists global installed packages                                               |
| ygrm        | `yarn global remove`                      | Remove global installed packages from your OS                                 |
| ygu         | `yarn global upgrade`                     | Upgrade packages installed globally to their latest version                   |
| yh          | `yarn help`                               | Show help for a yarn command                                                  |
| yi          | `yarn init`                               | Interactively creates or updates a package.json file                          |
| yin         | `yarn install`                            | Install dependencies defined in `package.json`                                |
| yln         | `yarn lint`                               | Run the lint script defined in `package.json`                                 |
| yls         | `yarn list`                               | List installed packages                                                       |
| yout        | `yarn outdated`                           | Check for outdated package dependencies                                       |
| yp          | `yarn pack`                               | Create a compressed gzip archive of package dependencies                      |
| yrm         | `yarn remove`                             | Remove installed packages                                                     |
| yrun        | `yarn run`                                | Run a defined package script                                                  |
| ys          | `yarn serve`                              | Start the dev server                                                          |
| yst         | `yarn start`                              | Run the start script defined in `package.json`                                |
| yt          | `yarn test`                               | Run the test script defined in `package.json`                                 |
| ytc         | `yarn test --coverage`                    | Run the test script defined in `package.json` with coverage                   |
| yuc         | `yarn global upgrade && yarn cache clean` | Upgrade global packages and clean yarn's global cache                         |
| yui         | `yarn upgrade-interactive`                | Prompt for which outdated packages to upgrade                                 |
| yuil        | `yarn upgrade-interactive --latest`       | Prompt for which outdated packages to upgrade to the latest available version |
| yup         | `yarn upgrade`                            | Upgrade packages to their latest version                                      |
| yv          | `yarn version`                            | Update the version of your package                                            |
| yw          | `yarn workspace`                          | Run a command within a single workspace.                                      |
| yws         | `yarn workspaces`                         | Run a command within all defined workspaces.                                  |

## Common Aliases

| ALIAS       | COMMAND                                                                                      | DESCRIPTION                                                             |
| ----------- | -------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------- |
| `..`        | `cd ..`                                                                                      | The cd command                                                          |
| `...`       | `cd ../../`                                                                                  | The cd command                                                          |
| `....`      | `cd ../../../`                                                                               | The cd command                                                          |
| trp         | `trash-put`                                                                                  | The TrashCLI put command                                                |
| tre         | `trash-empty`                                                                                | The TrashCLI empty command                                              |
| trl         | `trash-list`                                                                                 | The TrashCLI list command                                               |
| trr         | `trash-restore`                                                                              | The TrashCLI restore command                                            |
| trm         | `trash-rm`                                                                                   | The TrashCLI rm command                                                 |
| ytdl        | `youtube-dl --ignore-errors --output "%(title)s.%(ext)s"`                                    | The YouTube-DL command                                                  |
| ytadl       | `youtube-dl --ignore-errors --output "%(title)s.%(ext)s" --extract-audio --audio-format m4a` | The YouTube-DL command                                                  |
| go          | `richgo`                                                                                     | The Golang command                                                      |
| c           | `"$(history \| tail -2 \| sed "s/  [0-9]*  //;2d")" > /tmp/cmdoutput && cat /tmp/cmdoutput \| xsel -b && notify-send "Terminal" "Ouput Copied"`                              | Copy output of last command                                                           |
| addkey      | `sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys`                              | Adds PGP keys                                                           |
| autokey     | `sudo apt-get update 2> /tmp/keymissing; for key in $(grep "NO_PUBKEY" /tmp/keymissing \| sed "s/.*NO_PUBKEY //"); do echo -e "\nProcessing key: $key"; gpg --keyserver pool.sks-keyservers.net --recv $key && gpg --export --armor $key \| sudo apt-key add -; done`                                                                                        | Auto-grab PGP missing keys                                              |
| srv         | `python3 -m http.server --bind 127.0.0.1`                                                    | Simple static files web server                                          |
| listening   | `lsof -iTCP -sTCP:LISTEN -n -P +c0`                                                          | Print listening services                                                |
| random      | `LC_ALL=C tr -dc A-Za-z0-9 < /dev/urandom \| head -c`                                         | Prints a random alphanumeric characters string with the given length    |

## Directory Aliases

| ALIAS       | COMMAND                                   | DESCRIPTION                          |
| ----------- | ----------------------------------------- | ------------------------------------ |
| Home        | `cd $HOME`                                | Go to user's home directory          |
| Config      | `cd $HOME/.config`                         | Go to user's .config directory       |
| Designs     | `cd $HOME/Designs`                        | Go to user's designs directory       |
| Desktop     | `cd $HOME/Desktop`                        | Go to user's desktop directory       |
| Documents   | `cd $HOME/Documents`                      | Go to user's documents directory     |
| Downloads   | `cd $HOME/Downloads`                      | Go to user's downloads directory     |
| Games       | `cd $HOME/Games`                          | Go to user's games directory         |
| Local       | `cd $HOME/.local`                          | Go to user's .local directory        |
| Music       | `cd $HOME/Music`                          | Go to user's music directory         |
| Pictures    | `cd $HOME/Pictures`                       | Go to user's pictures directory      |
| Projects    | `cd $HOME/Projects`                       | Go to user's projects directory      |
| Public      | `cd $HOME/Public`                         | Go to user's public directory        |
| Repos       | `cd $HOME/Repos`                          | Go to user's repositories directory  |
| Shared      | `cd $HOME/Shared`                         | Go to user's shared directory        |
| Templates   | `cd $HOME/Templates`                      | Go to user's templates directory     |
| Videos      | `cd $HOME/Videos`                         | Go to user's videos directory        |

## Enhanced Aliases

| ALIAS       | COMMAND                                                                                                       | DESCRIPTION                                                  |
| ----------- | ------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| ls          | `exa --icons --group-directories-first` or `exa --group-directories-first` or `lsd -hA --group-dirs first`    | ALIAS `ls` command to `exa` if available, `lsd` otherwise    |
| l           | `ls -lFh`                                                                                                     | The `ls` command                                             |
| la          | `ls -lFh`                                                                                                     | The `ls` command                                             |
| lr          | `ls -tRFh`                                                                                                    | The `ls` command                                             |
| lt          | `ls -ltFh`                                                                                                    | The `ls` command                                             |
| ll          | `ls -l`                                                                                                       | The `ls` command                                             |
| ldot        | `ls -ld .*`                                                                                                   | The `ls` command                                             |
| lS          | `ls -1FSsh`                                                                                                   | The `ls` command                                             |
| lart        | `ls -1Fcart`                                                                                                  | The `ls` command                                             |
| lrt         | `ls -1Fcrt`                                                                                                   | The `ls` command                                             |
| zshrc       | `${=EDITOR} ~/.zshrc`                                                                                         | Quick access to the `~/.zshrc` file                          |
| zshenv      | `${=EDITOR} ~/.zshenv`                                                                                        | Quick access to the `~/.zshenv` file                         |
| bashrc      | `${=EDITOR} ~/.bashrc`                                                                                        | Quick access to the `~/.bashrc` file                         |
| bashenv     | `${=EDITOR} ~/.bashenv`                                                                                       | Quick access to the `~/.bashenv` file                        |
| shrc        | `${=EDITOR} ~/.shrc`                                                                                          | Quick access to the `~/.shrc` file                           |
| grep        | `grep --color`                                                                                                | Enables colouring in `grep` command                          |
| sgrep       | `grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS,.hg,.bzr}`                                                   | The `grep` command                                           |
| t           | `tail -f`                                                                                                     | The `tail` command                                           |
| H           | `\| head`                                                                                                      | The `head` command                                           |
| T           | `\| tail`                                                                                                      | The `tail` command                                           |
| G           | `\| grep`                                                                                                      | The `grep` command                                           |
| L           | `\| less`                                                                                                      | The `less` command                                           |
| M           | `\| most`                                                                                                      | The `most` command                                           |
| LL          | `2>&1 \| less`                                                                                                 | The `less` command                                           |
| CA          | `2>&1 \| cat -A`                                                                                               | The `cat` command                                            |
| NE          | `2> /dev/null`                                                                                                |                                                              |
| NUL         | `> /dev/null 2>&1`                                                                                            |                                                              |
| P           | `2>&1 \| pygmentize -l pytb`                                                                                    |                                                              |
| dud         | `du -d 1 -h`                                                                                                  | The `du` command                                             |
| duf         | `du -sh *`                                                                                                    | The `du` command                                             |
| fd          | `find . -type d -name`                                                                                        | The `find` command                                           |
| ff          | `find . -type f -name`                                                                                        | The `find` command                                           |
| h           | `history`                                                                                                     | The `history` command                                        |
| hgrep       | `fc -El 0 \| grep`                                                                                             | Greps the given pattern from command's history               |
| help        | `man`                                                                                                         | The `man` command                                            |
| psf         | `ps -f`                                                                                                       | The `ps` command with full format                            |
| sortnr      | `LC_ALL=C sort -n -r`                                                                                         | The `sort` command                                           |
| unexport    | `unset`                                                                                                       | The `unset` command                                          |
| rm          | `rm -iv`                                                                                                      | Enables verbosity for the `rm` command                       |
| cp          | `cp -iv`                                                                                                      | Enables verbosity for the `cp` command                       |
| mv          | `mv -iv`                                                                                                      | Enables verbosity for the `mv` command                       |
| tailf       | `tail -f`                                                                                                     | The `tail` command                                           |

## `GIT` Aliases

| ALIAS       | COMMAND                                                                                                                                                                                                                                      | DESCRIPTION          |
| ----------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------- |
| g           | `git`                                                                                                                                                                                                                                        | The `git` command    |
| ga          | `git add`                                                                                                                                                                                                                                    | The `git` command    |
| gac         | `git add . && git commit`                                                                                                                                                                                                                    | The `git` command    |
| gb          | `git branch`                                                                                                                                                                                                                                 | The `git` command    |
| gbd         | `git branch -d`                                                                                                                                                                                                                              | The `git` command    |
| gc          | `git commit`                                                                                                                                                                                                                                 | The `git` command    |
| gch         | `git checkout`                                                                                                                                                                                                                               | The `git` command    |
| gcl         | `clone`                                                                                                                                                                                                                                      | The `git` command    |
| gclr        | `git clone --recurse-submodules`                                                                                                                                                                                                             | The `git` command    |
| gd          | `git diff`                                                                                                                                                                                                                                   | The `git` command    |
| gf          | `git fetch`                                                                                                                                                                                                                                  | The `git` command    |
| gfa         | `git fetch --all`                                                                                                                                                                                                                            | The `git` command    |
| glg         | `git log --graph --abbrev-commit --decorate --format=format:'%C(bold green)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold yellow)(%ar)%C(reset)%C(auto)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all`    | The `git` command    |
| gm          | `git merge`                                                                                                                                                                                                                                  | The `git` command    |
| gnb         | `git checkout -b`                                                                                                                                                                                                                            | The `git` command    |
| gpl         | `git pull`                                                                                                                                                                                                                                   | The `git` command    |
| gplr        | `git pull --rebase`                                                                                                                                                                                                                          | The `git` command    |
| gpom        | `git pull origin master`                                                                                                                                                                                                                     | The `git` command    |
| gpu         | `git push`                                                                                                                                                                                                                                   | The `git` command    |
| gr          | `git remote`                                                                                                                                                                                                                                 | The `git` command    |
| gre         | `git restore`                                                                                                                                                                                                                                | The `git` command    |
| grm         | `git remote`                                                                                                                                                                                                                                 | The `git` command    |
| grs         | `git restore --staged .`                                                                                                                                                                                                                     | The `git` command    |
| gst         | `git status`                                                                                                                                                                                                                                 | The `git` command    |
| gt          | `git ls-tree -r master --name-only`                                                                                                                                                                                                          | The `git` command    |
