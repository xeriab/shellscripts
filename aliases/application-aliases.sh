#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./aliases/application-aliases.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# (hash nvim 1>/dev/null 2>&1) && {
#     alias v='nvim'
#     alias v='nvim $(fzf)'
#     alias vi=nvim
#     alias vimdiff='nvim -d'
#     alias testvim="VIMINIT='let \$MYVIMRC=\"\$HOME/box/testvim/init.vim\" | source \$MYVIMRC' nvim"
# }

# (hash vim 1>/dev/null 2>&1) && {
#     alias v='vim'
#     alias v='vim $(fzf)'
#     alias vi=vim
#     alias vimdiff='vim -d'
#     alias testvim="VIMINIT='let \$MYVIMRC=\"\$HOME/box/testvim/init.vim\" | source \$MYVIMRC' nvim"
# }

# Neo/vim shortcuts --------------------------------------------- v for vim --
if hash nvim 2>/dev/null; then
  alias vim=nvim
  alias vless="nvim -u /usr/share/nvim/runtime/macros/less.vim"
  alias suvim='sudo -E nvim'
  alias oldvim="VIMINIT='let \$MYVIMRC=\"\$XDG_DATA_HOME/nvim/vimrc\" | source \$MYVIMRC' /usr/bin/vim"
else
  alias vless="vim -u /usr/share/vim/vim74/macros/less.vim"
  alias suvim='sudo -E vim'
fi

(hash pandoc 1>/dev/null 2>&1) && {
  alias p='pandoc'
}

(hash mpv 1>/dev/null 2>&1) && {
  alias m='mpv'
}

(hash ranger 1>/dev/null 2>&1) && {
  alias r='ranger'
}

(hash transmission-remote-cli 1>/dev/null 2>&1) && {
  alias trc='transmission-remote-cli'
}

(hash transmission-rss 1>/dev/null 2>&1) && {
  alias trss='transmission-rss'
}

(hash pacman 1>/dev/null 2>&1) && {
  alias pman='sudo pacman'
}

(hash pip2 1>/dev/null 2>&1) && {
  alias pip2='sudo pip2'
}

(hash pip3 1>/dev/null 2>&1) && {
  alias pip='sudo pip3'
}

(hash newsboat 1>/dev/null 2>&1) && {
  alias nb='newsboat'
}

(hash joplin 1>/dev/null 2>&1) && {
  alias jp='joplin'
}

(hash xrdb 1>/dev/null 2>&1) && {
  alias xup='xrdb .Xresources'
}

(hash libreoffice 1>/dev/null 2>&1) && {
  alias libreoffice='env GTK_THEME=Adwaita:light libreoffice'
}

# hh to be alias for hstr
(hash hstr 1>/dev/null 2>&1) && {
  alias hh='hstr'
}

(hash yarn 1>/dev/null 2>&1) && {
  alias y="yarn"
  alias ya="yarn add"
  alias yadd="yarn add --dev"
  alias yap="yarn add --peer"
  alias yb="yarn build"
  alias ycc="yarn cache clean"
  alias yd="yarn dev"
  alias yga="yarn global add"
  alias ygls="yarn global list"
  alias ygrm="yarn global remove"
  alias ygu="yarn global upgrade"
  alias yh="yarn help"
  alias yi="yarn init"
  alias yin="yarn install"
  alias yln="yarn lint"
  alias yls="yarn list"
  alias yout="yarn outdated"
  alias yp="yarn pack"
  alias yrm="yarn remove"
  alias yrun="yarn run"
  alias ys="yarn serve"
  alias yst="yarn start"
  alias yt="yarn test"
  alias ytc="yarn test --coverage"
  alias yuc="yarn global upgrade && yarn cache clean"
  alias yui="yarn upgrade-interactive"
  alias yuil="yarn upgrade-interactive --latest"
  alias yup="yarn upgrade"
  alias yv="yarn version"
  alias yw="yarn workspace"
  alias yws="yarn workspaces"
}

# Maven
(hash mvn 1>/dev/null 2>&1) && {
  alias maven='mvn'
  alias j8mvn='JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64 mvn'
  alias jltsmvn='JAVA_HOME=/opt/programs/jdk/jdk-11.0.8+10 mvn'
  alias j11mvn='JAVA_HOME=/usr/lib/jvm/java-1.11.0-openjdk-amd64 mvn'
  alias j14mvn='JAVA_HOME=/usr/lib/jvm/java-1.14.0-openjdk-amd64 mvn'
  alias jmvn='JAVA_HOME=/usr/lib/jvm/current mvn'
}

(hash bat 1>/dev/null 2>&1) && {
  alias cat='bat --theme TwoDark'
  alias ncat='bat --theme TwoDark'
}

(hash lazydocker 1>/dev/null 2>&1) && {
  alias lzd='lazydocker'
}

(hash mpv 1>/dev/null 2>&1) && {
  alias mpv='mpv --hwdec=vdpau'
}

# Use neovim for vim if present.
[ -x "$(hash nvim)" ] && alias vim="nvim" vimdiff="nvim -d"

# alias antlr4='java -jar $XDG_LOCAL_HOME/lib/antlr-4.9.2-complete.jar'
# alias grun='java org.antlr.v4.gui.TestRig'

# alias run-veloren='env VK_ICD_FILENAMES=/usr/share/vulkan/icd.d/intel_icd.x86_64.json target/debug/veloren-voxygen'
