#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./aliases/common-aliases.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# Carry over aliases to the root account when using sudo
alias sudo='sudo '

# Make watch work with aliases
alias watch='watch '

# `cd` Aliases

# Easier directory navigation
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias .....='cd ../../../../'
alias -- -="cd -"

(hash trash-put 1>/dev/null 2>&1) && {
  alias trp='trash-put'
}

(hash trash-empty 1>/dev/null 2>&1) && {
  alias tre='trash-empty'
}

(hash trash-list 1>/dev/null 2>&1) && {
  alias trl='trash-list'
}

(hash trash-restore 1>/dev/null 2>&1) && {
  alias trr='trash-restore'
}

(hash trash-rm 1>/dev/null 2>&1) && {
  alias trm='trash-rm'
}

# (hash youtube-dl 1>/dev/null 2>&1) && {
#   alias ytadl='youtube-dl --ignore-errors --output "%(title)s.%(ext)s" --extract-audio --audio-format m4a'
#   alias ytdl='youtube-dl --ignore-errors --output "%(title)s.%(ext)s"'
# }

(hash yt-dlp 1>/dev/null 2>&1) && {
  alias ytadl='yt-dlp --ignore-errors --output "%(title)s.%(ext)s" --extract-audio --audio-format m4a'
  alias ytdl='yt-dlp --ignore-errors --output "%(title)s.%(ext)s"'
}

(hash go 1>/dev/null 2>&1) && {
  (hash richgo 1>/dev/null 2>&1) && {
    alias go='richgo'
  }
}

# Copy output of last command
alias c='"$(history | tail -2 | sed "s/  [0-9]*  //;2d")" > /tmp/cmdoutput && cat /tmp/cmdoutput | xsel -b && notify-send "Terminal" "Ouput Copied"'

##################################################
# OpenPGP/GPG pubkeys stuff (for Launchpad / etc.#
##################################################

(hash apt-key 1>/dev/null 2>&1) && {
  ###### add keys
  alias addkey='sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys'
}

# (hash apt-get 1>/dev/null 2>&1) && {
#     ###### autograb missing keys
#     alias autokey='sudo apt-get update 2> /tmp/keymissing; for key in $(grep "NO_PUBKEY" /tmp/keymissing |sed "s/.*NO_PUBKEY //"); do echo -e "\nProcessing key: $key"; gpg --keyserver pool.sks-keyservers.net --recv $key && gpg --export --armor $key | sudo apt-key add -; done'
# }

(hash python3 1>/dev/null 2>&1) && {
  # Simple static files web server:
  # alias srv='python3 -m http.server --bind 127.0.0.1'
  alias srv='python3 -m http.server --bind 0.0.0.0'
}

(hash php 1>/dev/null 2>&1) && {
  # Simple PHP static files web server:
  alias serve='php -S 0.0.0.0:8000 -t .'
}

# (hash bundle 1>/dev/null 2>&1) && {
#     alias jekyll='bundle exec jekyll serve'
# }

# Print listening services:
alias listening='lsof -iTCP -sTCP:LISTEN -n -P +c0'

# Prints a random alphanumeric characters string with the given length:
alias random='LC_ALL=C tr -dc A-Za-z0-9 < /dev/urandom | head -c'

# Shortcuts
alias c=clear
hash colordiff 1>/dev/null 2>&1 && alias diff=colordiff

(hash tre 1>/dev/null 2>&1) && {
  alias tree='tre'
}

alias tree='tree -F --dirsfirst -a -I ".idea|.settings|.project|.env|.vscode|.git|.hg|.svn|__pycache__|.mypy_cache|.pytest_cache|*.egg-info|.sass-cache|.DS_Store|node_modules|vendor|target|build" --ignore-case'

# alias change-java-version="sudo update-alternatives --config java_home && java -version && javac -version"
# alias change-maven-version="sudo update-alternatives --config maven_home && mvn -version"
# sudo update-alternatives --install /usr/lib/jvm/default-java java_home /usr/lib/jvm/default-java 100
# alias dircolors='gdircolors'
