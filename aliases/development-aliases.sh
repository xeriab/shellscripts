#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./aliases/development-aliases.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

(hash php 1>/dev/null 2>&1) && (hash composer 1>/dev/null 2>&1) && {
  alias composer="php -d allow_url_fopen=On -d memory_limit=-1 $(command -v composer)"
}

alias empty-commit='git commit --allow-empty -m "A commit for Testing 💻" && git push'
