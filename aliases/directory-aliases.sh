#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./aliases/directory-aliases.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

alias Home='cd $HOME'
alias Config='cd $HOME/.config'
alias Designs='cd $HOME/Designs'
alias Desktop='cd $HOME/Desktop'
alias Documents='cd $HOME/Documents'
alias Downloads='cd $HOME/Downloads'
alias Games='cd $HOME/Games'
alias Local='cd $HOME/.local'
alias Music='cd $HOME/Music'
alias Pictures='cd $HOME/Pictures'
alias Projects='cd $HOME/Projects'
alias Public='cd $HOME/Public'
alias Repos='cd $HOME/Repos'
alias Shared='cd $HOME/Shared'
alias Templates='cd $HOME/Templates'
alias Videos='cd $HOME/Videos'
