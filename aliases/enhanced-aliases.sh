#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./aliases/enhanced-aliases.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

#
# region LS Command Aliases
#

# # ls, the common ones I use a lot shortened for rapid fire usage
# alias l='ls -lFh'     #size,show type,human readable
# alias la='ls -lAFh'   #long list,show almost all,show type,human readable
# alias lr='ls -tRFh'   #sorted by date,recursive,show type,human readable
# alias lt='ls -ltFh'   #long list,sorted by date,show type,human readable
# alias ll='ls -l'      #long list
# alias ldot='ls -ld .*'
# alias lS='ls -1FSsh'
# alias lart='ls -1Fcart'
# alias lrt='ls -1Fcrt'

# ls, the common ones I use a lot shortened for rapid fire usage
#

[ -n "$TERM" ] && {
  ! [ "$TERM" = "linux" ] || [ "$TERM" = "xterm-256color" ] && {
    if (hash exa 1>/dev/null 2>&1); then
      # alias ls='exa --icons -a --group-directories-first'
      alias ls='exa --icons --group-directories-first'
    elif (hash lsd 1>/dev/null 2>&1); then
      # alias ls='lsd -hA --group-dirs first'
      # alias ls='lsd -A --group-dirs first'
      alias ls='lsd --group-dirs first'
    else
      alias ls='command ls'
    fi
  } || {
    if (hash exa 1>/dev/null 2>&1); then
      # alias ls='exa --icons -a --group-directories-first'
      alias ls='exa --icons --group-directories-first'
    elif (hash lsd 1>/dev/null 2>&1); then
      # alias ls='lsd -hA --group-dirs first'
      # alias ls='lsd -A --group-dirs first'
      alias ls='lsd --group-dirs first'
    else
      alias ls='command ls'
    fi
  }
}

# size,show type,human readable
alias l='ls -lFh'

# long list,show almost all,show type,human readable
alias la='ls -lAFh'

# sorted by date,recursive,show type,human readable
alias lr='ls -tRFh'

# long list,sorted by date,show type,human readable
alias lt='ls -ltFh'

# long list
alias ll='ls -l'
alias ldot='ls -ld .*'
alias lS='ls -1FSsh'

if ! (hash exa 1>/dev/null 2>&1) && ! (hash lsd 1>/dev/null 2>&1); then
  alias lart='ls -1Fcart'
  alias lrt='ls -1Fcrt'
fi

#
# endregion LS Command Aliases
#

# Quick access to the ~/.zshrc file
alias zshrc='${=EDITOR} ~/.zshrc'

# Quick access to the ~/.zshenv file
alias zshenv='${=EDITOR} ~/.zshenv'

# Quick access to the ~/.bashrc file
alias bashrc='${=EDITOR} ~/.bashrc'

# Quick access to the ~/.bashenv file
alias bashenv='${=EDITOR} ~/.bashenv'

# # Quick access to the ~/config/fish/config.fish file
# alias fishrc='${=EDITOR} ~/config/fish/config.fish'

alias grep='grep --color'
alias sgrep='grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS,.hg,.bzr}'

alias t='tail -f'

# Command line head / tail shortcuts
alias H='| head'
alias T='| tail'
alias G='| grep'
alias L="| less"
alias M="| most"
alias LL="2>&1 | less"
alias CA="2>&1 | cat -A"
alias NE="2> /dev/null"
alias NUL="> /dev/null 2>&1"
alias P="2>&1 | pygmentize -l pytb"

alias dud='du -d 1 -h'
alias duf='du -sh *'
alias fd='find . -type d -name'
alias ff='find . -type f -name'

alias h='history'
alias hgrep="fc -El 0 | grep"
alias help='man'
alias psf='ps -f'
alias sortnr='LC_ALL=C sort -n -r'
alias unexport='unset'

alias rm='rm -iv'
alias cp='cp -iv'
alias mv='mv -iv'

alias tailf='tail -f'

if ! (hash tree 1>/dev/null 2>&1); then
  if (hash tre 1>/dev/null 2>&1); then
    alias tree='tre'
  fi
fi

# alias spt='launchspt'
