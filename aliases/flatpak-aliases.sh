#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./aliases/flatpak-aliases.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

alias postman='flatpak run com.getpostman.Postman'
alias kooha='flatpak run io.github.seadve.Kooha'
