#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./aliases/git-aliases.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

alias g='git'
alias ga='git add'
alias gac='git add . && git commit'
alias gb='git branch'
alias gbd='git branch -d'
alias gc='git commit'
alias gch='git checkout'
alias gcl='git clone'
alias gclr='git clone --recurse-submodules'
alias gd='git diff'
alias gf='git fetch'
alias gfa='git fetch --all'
alias glg="git log --graph --abbrev-commit --decorate --format=format:'%C(bold green)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold yellow)(%ar)%C(reset)%C(auto)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all"
alias gm='git merge'
alias gnb='git checkout -b'
alias gpl='git pull'
alias gplr='git pull --rebase'
alias gpom='git pull origin master'
alias gpu='git push'
alias gr='git remote'
alias gre='git restore'
alias grm='git remote'
alias grs='git restore --staged .'
alias gst='git status'
alias gt='git ls-tree -r master --name-only'
