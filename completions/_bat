#compdef bat
#
# shellcheck disable=SC3043
#
# ./completions/_bat
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

#
# Completion script for bat
#
# @author Xeriab Nabil <xeriab@tuta.io>
#

#
# region Completion
#

local arguments

arguments=(
  {-A,--show-all}'[show non-printable characters like space, tab or newline. This option can also be used]'
  {-p,--plain}'[only show plain style, no decorations. This is an alias for --style=plain. When -p]'
  '--pager[).]'
  {-l,--language}'[explicitly set the language for syntax highlighting. The language can be specified as a]'
  {-H,--highlight-line}'[...]'
  '--file-name[...]'
  {-d,--diff}'[only show lines that have been added/removed/modified with respect to the Git index. Use]'
  '--diff-context[to control how much context you want to see.]'
  '--diff-context[include N lines of context around added/removed/modified lines when using --diff.]'
  '--tabs[set the tab width to T spaces. Use a width of 0 to pass tabs through directly]'
  '--wrap[specify the text-wrapping mode (*auto*, never, character). The --terminal-width option]'
  '--terminal-width[explicitly set the width of the terminal instead of determining it automatically. If]'
  {-n,--number}'[only show line numbers, no other decorations. This is an alias for --style=numbers]'
  '--color[specify when to use colored output. The automatic mode only enables colors if an]'
  '--italic-text[specify when to use ANSI sequences for italic text in the output. Possible values:]'
  '--decorations[specify when to use the decorations that have been specified via --style. The]'
  {-f,--force-colorization}'[alias for --decorations=always --color=always. This is useful if the output of bat is]'
  '--paging[specify when to use the pager. To disable the pager, use --paging=never or its]'
  '--pager[determine which pager is used. This option will override the PAGER and BAT_PAGER]'
  {-m,--map-syntax}'[...]'
  '--theme[set the theme for syntax highlighting. Use --list-themes to see all available themes.]'
  '--list-themes[display a list of supported themes for syntax highlighting.]'
  '--style[configure which elements (line numbers, file headers, grid borders, Git modifications]'
  {-r,--line-range}'[...]'
  {-L,--list-languages}'[display a list of supported languages for syntax highlighting.]'
  {-u,--unbuffered}'[this option exists for POSIX-compliance reasons (u is for unbuffered). The output is]'
  {-h,--help}'[print this help message.]'
  {-V,--version}'[show version information.]'
  '*:filename:_files'
)

_arguments -s $arguments

#
# endregion Completion
#

# Local Variables:
# mode: Shell-Script
# sh-indentation: 2
# indent-tabs-mode: nil
# sh-basic-offset: 2
# End:
# vim: ft=zsh sw=2 ts=2 et
