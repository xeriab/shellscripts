#!/usr/bin/env bash
#
# shellcheck disable=SC3043
#
# ./completions/_dotnet.bash
#
# -*- tab-width: 2; encoding: utf-8; mode: bash; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

#
# Completion script for .NET CLI
#
# @author Xeriab Nabil <xeriab@tuta.io>
#

#
# region Completion
#

_dotnet_bash_complete() {
  local word=${COMP_WORDS[COMP_CWORD]}
  local completions

  completions="$(dotnet complete --position "${COMP_POINT}" "${COMP_LINE}" 2>/dev/null)"

  if [ $? -ne 0 ]; then
    completions=""
  fi

  COMPREPLY=($(compgen -W "$completions" -- "$word"))
}

complete -f -F _dotnet_bash_complete dotnet

#
# endregion Completion
#
