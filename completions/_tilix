#compdef tilix
#
# shellcheck disable=SC3043
#
# ./completions/_tilix
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

#
# Completion script for Tilix
#
# @author Xeriab Nabil <xeriab@tuta.io>
#

#
# region Completion
#

local arguments

arguments=(
  {-h,--help}'[show help options]'
  '--help-all[show all help options]'
  '--help-gapplication[show GApplication options]'
  '--help-gtk[show GTK+ Options]'
  {-w,--working-directory}'[set the working directory of the terminal]'
  {-p,--profile}'[set the starting profile]'
  {-t,--title}'[set the title of the new terminal]'
  {-s,--session}'[open the specified session]'
  {-a,--action}'[send an action to current Tilix instance]'
  {-e,--command}'[execute the parameter as a command]'
  '--maximize[maximize the terminal window]'
  '--minimize[minimize the terminal window]'
  '--window-style[override the preferred window style to use, one of: normal, disable-csd, disable-csd-hide-toolbar, borderless]'
  '--full-screen[full-screen the terminal window]'
  '--focus-window[focus the existing window]'
  '--new-process[start additional instance as new process (Not Recommended)]'
  '--geometry[set the window size; for example: 80x24, or 80x24+200+200 (COLSxROWS+X+Y)]'
  {-q,--quake}'[opens a window in quake mode or toggles existing quake mode window visibility]'
  {-v,--version}'[show the Tilix and dependant component versions]'
  '--preferences[show the Tilix preferences dialog directly]'
  {-g,--group}'[group tilix instances into different processes (Experimental, not recommended)]'
  '--display[X display to use]'
  '*:filename:_files'
)

_arguments -s $arguments

#
# endregion Completion
#

# Local Variables:
# mode: Shell-Script
# sh-indentation: 2
# indent-tabs-mode: nil
# sh-basic-offset: 2
# End:
# vim: ft=zsh sw=2 ts=2 et
