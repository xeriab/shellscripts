#!/usr/bin/env zsh
#
# shellcheck disable=SC3043
#
# ./completions/dotnet-completion.plugin.zsh
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

(command -v compdef >/dev/null 2>&1) && {
  compdef _dotnet dotnet
}
