#!/usr/bin/env bash
#
# shellcheck disable=SC3043
#
# ./completions/gradle-tab-completion.bash
#
# -*- tab-width: 2; encoding: utf-8; mode: bash; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

_gradle() {
  # shellcheck disable=SC2039
  local cur=${COMP_WORDS[COMP_CWORD]}

  # shellcheck disable=SC2039
  local gradle_cmd='gradle'

  if [[ -x ./gradlew ]]; then
    gradle_cmd='./gradlew'
  fi

  if [[ -x ../gradlew ]]; then
    gradle_cmd='../gradlew'
  fi

  # shellcheck disable=SC2039
  local commands=''

  # shellcheck disable=SC2039
  local cache_dir="$HOME/.cache/gradle_tabcompletion"

  mkdir -p $cache_dir

  # TODO: include the gradle version in the checksum?  It's kinda slow
  #local gradle_version=$($gradle_cmd --version --quiet --no-color | grep '^Gradle ' | sed 's/Gradle //g')

  local gradle_files_checksum=''

  # top-level gradle file
  if [[ -f build.gradle ]]; then
    # mac
    if [[ -x $(which md5 2>/dev/null) ]]; then
      # shellcheck disable=SC2039
      local all_gradle_files=$(find . -name build.gradle 2>/dev/null)
      gradle_files_checksum=$(md5 -q -s "$(md5 -q $all_gradle_files)")
    else # linux
      gradle_files_checksum=($(find . -name build.gradle | xargs md5sum | md5sum))
    fi
  else # no top-level gradle file
    gradle_files_checksum='no_gradle_files'
  fi

  if [[ -f $cache_dir/$gradle_files_checksum ]]; then # cached! yay!
    oldest_gradle_file=($(find . -type f -name build.gradle -print0 | xargs -0 stat -f "%m %N" | sort -rn | head -1 | cut -f2- -d" "))

    if [ "$oldest_gradle_file" -nt "$cache_dir/$gradle_files_checksum" ]; then
      commands=$($gradle_cmd --no-color --quiet tasks --all | grep ' - ' | awk '{print $1}' | tr '\n' ' ')

      if [[ ! -z $commands ]]; then
        echo $commands >$cache_dir/$gradle_files_checksum
      fi
    else
      # commands=$(cat $cache_dir/$gradle_files_checksum)
      commands=$(cat $cache_dir/$gradle_files_checksum)
      touch echo $cache_dir/$gradle_files_checksum
    fi

  else # not cached! boo-urns!
    commands=$($gradle_cmd --no-color --quiet tasks --all | grep ' - ' | awk '{print $1}' | tr '\n' ' ')

    if [[ ! -z $commands ]]; then
      echo $commands >$cache_dir/$gradle_files_checksum
    fi
  fi

  COMPREPLY=($(compgen -W "$commands" -- $cur))
}

clear_gradle_cache() {
  # shellcheck disable=SC2039
  local cache_dir="$HOME/.cache/gradle_tabcompletion"

  if [[ -d $cache_dir ]]; then
    find $cache_dir -type f -mtime +7 -exec rm -f {} \;
  fi
}

clear_gradle_cache

complete -F _gradle gradle
complete -F _gradle gradlew
complete -F _gradle ./gradlew
