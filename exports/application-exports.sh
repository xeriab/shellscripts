#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./aliases/application-aliases.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

(hash nvim 1>/dev/null 2>&1) && {
  # Preferred editor for local and remote sessions
  if [ -z "$SSH_CONNECTION" ]; then
    export EDITOR='nvim'
  else
    export EDITOR='vim'
  fi
}

(hash vim 1>/dev/null 2>&1) && {
  # Preferred editor for local and remote sessions
  if [ -n "$SSH_CONNECTION" ]; then
    export EDITOR='nvim'
  else
    export EDITOR='vim'
  fi
}

(hash nvim 1>/dev/null 2>&1) && {
  export VISUAL='nvim'
}

(hash vim 1>/dev/null 2>&1) && {
  export VISUAL='vim'
}

if (hash tilix 1>/dev/null 2>&1); then
	export TERMINAL="tilix"
else
	(hash gnome-terminal 1>/dev/null 2>&1) && {
		export TERMINAL="gnome-terminal"
	}
fi

if (hash firefox 1>/dev/null 2>&1); then
	export BROWSER="firefox"
else
	(hash brave-browser 1>/dev/null 2>&1) && {
		export BROWSER="brave-browser"
	}
fi

if (hash zathura 1>/dev/null 2>&1); then
	export READER="zathura"
else
	(hash evince 1>/dev/null 2>&1) && {
		export READER="evince"
	}
fi

if (hash nvim 1>/dev/null 2>&1); then
	export EDITOR="nvim"
	export VISUAL="nvim"
else
	(hash vim 1>/dev/null 2>&1) && {
		export EDITOR="vim"
		export VISUAL="vim"
	}
fi

if (hash celluloid 1>/dev/null 2>&1); then
	export VIDEO="celluloid"
else
	(hash mpv 1>/dev/null 2>&1) && {
		export VIDEO="mpv"
	}
fi

if (hash eog 1>/dev/null 2>&1); then
	(hash eog 1>/dev/null 2>&1) && export IMAGE="eog"
else
	(hash sxiv 1>/dev/null 2>&1) && export IMAGE="sxiv"
fi

# ! (hash pyenv 1>/dev/null 2>&1) && eval "$(pyenv init --path)"
# ! (hash pyenv 1>/dev/null 2>&1) && eval "$(pyenv init)"
