# ⚙️ Functions

| Function            | Description                                                              |
| ------------------- | ------------------------------------------------------------------------ |
| `alert`             | Add an "alert" alias for long running commands like `sleep 10; alert`    |
| `ccat`              | Colourised `cat` output                                                  |
| `cless`             | Colourised `less` output                                                 |
| `ctail`             | Colourised `tail -f` output                                              |
| `error`             | Fake error string                                                        |
| `getsite`           | Download entire web site/web page using `wget`                           |
| `grepcolor`         | Pretend to be busy in office to enjoy a cup of coffee                    |
| `lc`                | `cd` to a directory using `lf` command                                   |
| `mkd`               | Make directory and then `cd` to the created directory                    |
| `reload`            | Reload current shell                                                     |
