#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./functions/common/fuzzpak.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2020-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# Fuzzpak
#
# Run `Flatpak` applications.
#
Fuzzpak() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='fuzzpak'

  # shellcheck disable=SC2034
  TEMP_DIR="/tmp/${Self}-tmp"

  # shellcheck disable=SC2034
  OUT_FILE="${TEMP_DIR}/script.Log"

  # shellcheck disable=SC2034
  local -r Date="(12/12/2020)"

  # shellcheck disable=SC2034
  local -r Version="0.1.0"

  # shellcheck disable=SC2034
  local FunctionDebug=False

  # shellcheck disable=SC2034
  local ExitStatus=0

  # shellcheck disable=SC2034
  local Exitable=False

  # shellcheck disable=SC2034
  local Verbose=False

  # shellcheck disable=SC2034
  local VerboseOptions=

  # shellcheck disable=SC2034
  local FlatpakDir="${FlatpakDir:-"$HOME"/.var/app}"

  # shellcheck disable=SC2034
  local FlatpakAppId=

  # shellcheck disable=SC2034
  local Program=

  # shellcheck disable=SC2034
  local ListInstalled=False

  # shellcheck disable=SC2034 disable=SC2155
  local NoAnsi=False

  # shellcheck disable=SC2034
  local AdditionalArguments=

  # shellcheck disable=SC2034
  local OptionIndex=

  # Usage
  #
  Usage() {
    local Arrow="${Faint}›${Reset} "
    local Seperator="${Faint}${FgBrightBlack}-${Reset}"
    # local DSQ="${Normal}${FgBlack}'${Reset}"
    local LSQ="${Faint}'${Reset}"
    # local Pipe="${Faint}|${Reset}"

    # Enables verbose output (may be supplied multiple times).
    printf "\n"
    printf "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset} ${Bold}${Faint}Run \`Flatpak\` applications.${Reset}\n"
    printf "\n"
    printf "${Faint}Copyright (C) 2021 - ${CURRENT_YEAR} Xeriab Nabil (aka KodeBurner) $(FormatLink "<xeriab@tuta.io>" "<xeriab@tuta.io>")${Reset}\n"
    printf "\n"
    printf "${Bold}Usage:${Reset} ${FgGreen}${Self}${Reset} ${Faint}[${Reset}OPTIONS${Reset}${Faint}]${Reset} ${Faint}<${Reset}program${Reset}${Faint}>${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Available Options${Reset}:\n"
    printf "  ${Faint}-${Reset}${Normal}d${Reset} ${Faint}--${Reset}${Normal}${FgYellow}directory${Reset}  ${Seperator} ${Normal}Location of flatpaks ${Faint}${FgBrightBlack}(${Reset}${Faint}${Bold}Default${Reset}${Faint}${FgBrightBlack}:${Reset} ${LSQ}${FgBrightBlue}${HOME}/.var/app${Reset}${LSQ}${Faint}${FgBrightBlack})${Reset}${Faint}.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}l${Reset} ${Faint}--${Reset}${Normal}${FgYellow}list${Reset}       ${Seperator} ${Normal}List installed Flatpaks.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}v${Reset} ${Faint}--${Reset}${Normal}${FgYellow}verbose${Reset}    ${Seperator} ${Normal}Enable verbose output.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}V${Reset} ${Faint}--${Reset}${Normal}${FgYellow}version${Reset}    ${Seperator} ${Normal}Output version information and exit.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}h${Reset} ${Faint}--${Reset}${Normal}${FgYellow}help${Reset}       ${Seperator} ${Normal}Display this help message and exit.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}i${Reset} ${Faint}--${Reset}${Normal}${FgYellow}info${Reset}       ${Seperator} ${Normal}Display information and exit.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Additional Options${Reset}:\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}ansi${Reset}         ${Seperator} ${Normal}Force ANSI output.${Reset}\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}no-ansi${Reset}      ${Seperator} ${Normal}Disable ANSI output.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Arguments${Reset}:\n"
    printf "  ${Normal}program${Reset}         ${Seperator} ${Normal}Flatpak progrm name.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Examples${Reset}:\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} ${Faint}--${Reset}directory ${LSQ}/var/lib/flatpak/app${LSQ} kooha     ${Faint}# Run Kooha${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Information${Reset}:\n"
    printf "  ${Normal}Program${Reset}: ${DSQ}${Faint}${Program:-Not Specified!}${Reset}${DSQ}\n"
    printf "\n"
  }

  # Info
  #
  Info() {
    local -r Arrow="${Faint}›${Reset} "
    local -r Seperator="${Faint}-${Reset}"
    local -r DSQ="${Normal}${FgBlack}'${Reset}"

    printf "\n"
    printf "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset} ${Bold}${Faint}Run \`Flatpak\` applications.${Reset}\n"
    printf "\n"
    printf "${Faint}Copyright (C) 2021 - ${CURRENT_YEAR} Xeriab Nabil (aka KodeBurner) $(FormatLink "<xeriab@tuta.io>" "<xeriab@tuta.io>")${Reset}\n"
    printf "\n"
    printf "${Bold}${FgWhite}Information${Reset}:\n"
    printf " ${Arrow}${Normal}Program${Reset}: ${DSQ}${Faint}${Program:-Not Specified!}${Reset}${DSQ}\n"
    printf "\n"
  }

  # ArgumentsParser
  #
  ArgumentsParser() {
    # shellcheck disable=SC2034
    local Argument=""

    # shellcheck disable=SC2034
    local VerboseOptionsHolder=""

    # shellcheck disable=SC2034
    local VerboseOpts=""

    # shellcheck disable=SC2034
    local Option=

    # shellcheck disable=SC2034
    local OptionArgument=

    # NeedsArgument
    #
    NeedsArgument() {
      if [ -z "$2" ]; then
        # Log -e "No (argument)s provided for option ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}."
        Log -e "Option ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset} must have an argument."
        return 1
      fi
    }

    [ $# -eq 0 ] && {
      Usage
      ExitStatus=$(($# > 1))
      return $ExitStatus
    }

    while [ $# -gt 0 ]; do
      case $1 in
      #
      # Common
      #

      #
      #
      -- | -)
        Exitable=False
        ExitStatus=0
        shift
        Arguments="${Arguments} ${*}"
        Arguments=$(StrNormalize "$Arguments")
        # Log -d "Arguments: '$Arguments'"
        break
        ;;

      # Help
      #
      -h | --help | -help)
        Usage
        Exitable=True
        ExitStatus=$(($# > 1))
        return $ExitStatus
        ;;

      # Info
      #
      -i | --info | -info)
        Info
        Exitable=True
        ExitStatus=$(($# > 1))
        return $ExitStatus
        ;;

      # Directory
      #
      -d=* | --directory=* | -directory=*)
        Exitable=False
        ExitStatus=0

        [ -n "$1" ] && {
          FlatpakDir="${1#*=}"
          shift
        }
        ;;

      # Directory
      #
      -d | --directory | -directory)
        Exitable=False
        ExitStatus=0

        NeedsArgument "$1" "$2"

        [ -n "$2" ] && {
          FlatpakDir="$2"
          shift 2
        }
        ;;

      # List
      #
      -l | --list | -list)
        Exitable=False
        ExitStatus=0
        ListInstalled=True
        shift
        ;;

      # ANSI
      #
      --ansi | -ansi)
        Exitable=False
        ExitStatus=0
        # NoAnsi=False
        # shellcheck disable=SC2034
        # DISABLE_CONSOLE_OUTPUT_COLORS=False
        # DISABLE_CONSOLE_OUTPUT_STYLES=False
        SetupAnsiColors
        SetupAnsiStyles
        shift
        ;;

      # No ANSI
      #
      --no-ansi | -no-ansi)
        Exitable=False
        ExitStatus=0
        # NoAnsi=True
        # shellcheck disable=SC2034
        # DISABLE_CONSOLE_OUTPUT_COLORS=True
        # DISABLE_CONSOLE_OUTPUT_STYLES=True
        DisableAnsiColors
        DisableAnsiStyles
        shift
        ;;

      # Verbose
      #
      -v | --verbose | -verbose)
        # shellcheck disable=SC2034
        Exitable=False
        FunctionDebug=True
        # Verbose=True
        VerboseOptionsHolder="-v ${VerboseOptionsHolder}"
        # shellcheck disable=SC2034
        VerboseOptions=$VerboseOptionsHolder
        shift
        ;;

      # Version
      #
      -V | --version | -version)
        Log -r "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset}\n"
        Exitable=True
        ExitStatus=0
        return $ExitStatus
        ;;

      #
      # Misc
      #

      #
      #
      --* | -*)
        Log -e "Unrecognized option(s)${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}" 1>&2
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      # Bad short option
      #
      \?)
        Log -e "Unrecognized option(s)${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}" 1>&2
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      #
      #
      *)
        Exitable=False
        ExitStatus=0
        Program="${1:-}"
        [ $# -gt 0 ] && shift
        ;;
      esac
    done

    set -- "${Arguments}"

    # shellcheck disable=SC2034
    AdditionalArguments=$Arguments
  }

  ! (IsInteractive) && {
    Log -e "Cannot run in non-interactive shell." 1>&2
    return 1
  }

  #
  #
  ArgumentsParser "$@"

  [ "$FunctionDebug" = True ] || [ "$FunctionDebug" = 1 ] && Verbose=True

  # ListFlatpaks
  #
  ListFlatpaks() {
    # local Apps=
    # local AppList=
    # local AppIds=
    # local AppNames=

    Log -r "${Faint}${FgCyan}›${Reset} ${Bold}${Faint}${FgWhite}Installed${Reset} ${Normal}${Bold}${FgCyan}Flatpaks${Reset}${Bold}${Faint}${FgWhite}:${Reset}\n"

    command flatpak list

    # Apps=$(command flatpak list --columns=name,application | command sort -u | command head -64)
    # AppNames="$(printf '%s\n' "$Apps" | command awk -F'\t' '{ print $1 }' | command sed -Ere "s~(.*)~'\1'~; s~\s+~#~g;" | ArrayElementEncode)"
    # AppIds="$(printf '%s\n' "$Apps" | command awk -F'\t+' '{ print $2 }')"

    # AppList=$(Array)

    # # AppIds=$(Array "$AppIds")
    # # AppNames=$(Array "$AppNames")
    # # AppIds=$(printf '%s\n' "$AppIds" | ArrayElementDecode)
    # # AppIds=$(command flatpak list --columns=application | command sort -u | command head -64)
    # # AppNames=$(command flatpak list --columns=name | command sort -u | command head -64)

    # # echo "AppNames: '$AppNames'"

    # # printf '%s\n' "$AppNames" | ArrayElementDecode

    # for Each in $(printf '%s\n' "$AppNames" | ArrayElementDecode); do
    #   ! (StrContains -m regex "$Each" "Platform") &&
    #     ! (StrContains -m regex "$Each" "Gtk3theme") &&
    #     ! (StrContains -m regex "$Each" "theme") &&
    #     ! (StrContains -m regex "$Each" "ffmpeg") &&
    #     ! (StrContains -m regex "$Each" "SDK") &&
    #     ! (StrContains -m plain "$Each" "Intel") &&
    #     ! (StrContains -m regex "$Each" "Mesa") &&
    #     ! (StrContains -m regex "$Each" "openh264") &&
    #     ! (StrContains -m regex "$Each" "translations") &&
    #     ! (StrContains -m regex "$Each" "nvidia") &&
    #     ! (StrContains -m regex "$Each" "Locale") && {
    #     AppList=$(ArrayAppend "$AppList" "$(StrReplace -m regex '#' ' ' "$Each")")
    #   }
    # done
    # unset Each

    # printf '%s\n' "$AppList"

    # # for Each in $(printf '%s\n' "$AppIds" | ArrayElementDecode); do
    # #   ! (StrContains -m regex "$Each" "Platform") &&
    # #     ! (StrContains -m regex "$Each" "Gtk3theme") &&
    # #     ! (StrContains -m regex "$Each" "Locale") && {
    # #     printf '%s\n' "$Each"
    # #   }

    # #   # printf '%s\n' "$Each"
    # #   # printf '%s\n' "$AppIds"
    # #   # printf '%s\n' "$AppNames"
    # # done
    # # unset Each

    # # printf '%s\n' "$AppIds"
    # # printf '%s\n' "$AppNames"
  }

  # LaunchApp
  #
  LaunchApp() {
    local App="${1:-}"

    FlatpakAppId="$(command find "${FlatpakDir}" -mindepth 1 \
      -maxdepth 1 -type d -iname "*${App}*" -printf '%f\n')"

    if [ -n "$FlatpakAppId" ]; then
      (hash flatpak 1>/dev/null 2>&1) && {
        [ "$Verbose" = True ] && Log -d "Program:           ${App}"
        [ "$Verbose" = True ] && Log -d "Flatpak Directory: ${FlatpakDir}"
        [ "$Verbose" = True ] && Log -d "Flatpak App ID:    ${FlatpakAppId}"

        Log -r "${Faint}${FgCyan}›${Reset} ${Normal}Launching${Reset} ${Faint}'${Reset}${FgBrightBlue}${App}${Reset}${Faint}'${Reset}${Normal}...${Reset}\n"

        if [ -n "$AdditionalArguments" ]; then
          command flatpak run "${FlatpakAppId}" "$AdditionalArguments"
        else
          command flatpak run "${FlatpakAppId}"
        fi

        [ "$Verbose" = True ] && {
          Log -r "${Faint}${FgCyan}›${Reset} ${Normal}Done launching${Reset} ${Faint}'${Reset}${FgBrightBlue}${FlatpakAppId}${Faint}'${Reset}${Normal}.${Reset}\n"
        }
      }
    else
      Log -e "App ${Faint}'${Reset}${FgBrightWhite}${App}${Reset}${Faint}'${Reset} is not found." >&2
    fi

    unset App
  }

  if [ $# -gt 0 ] && [ "$Exitable" = False ] || [ "$Exitable" = 0 ]; then
    if [ "$ListInstalled" = False ] || [ "$ListInstalled" = 0 ]; then
      if [ -n "$Program" ]; then
        LaunchApp "${Program}"
      fi
    else
      ListFlatpaks
    fi
  fi

  unset Self
  unset TEMP_DIR
  unset OUT_FILE

  return $ExitStatus
}

# region Aliases
#

alias fuzzpak=Fuzzpak

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
