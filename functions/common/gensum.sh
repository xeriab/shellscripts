#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./functions/common/gensum.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2020-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# set -u

# GenSum
#
# Powerful multi file, multi checksum generator.
#
#
GenSum() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='gensum'

  # shellcheck disable=SC2034
  TEMP_DIR="/tmp/${Self}-tmp"

  # shellcheck disable=SC2034
  OUT_FILE=
  # OUT_FILE="${TEMP_DIR}/${Self:-$SCRIPT}.log"

  # SAVEIFS=$IFS

  # IFS=' '

  # shellcheck disable=SC2034
  local -r Date="(12/12/2020)"

  # shellcheck disable=SC2034
  local -r Version="0.1.0"

  # # shellcheck disable=SC2034
  # local Dependencies='
  #   pv
  #   wget
  #   unar
  # '
  # shellcheck disable=SC2034
  local Dependencies=

  # shellcheck disable=SC2034
  local FunctionDebug=False

  # shellcheck disable=SC2034
  local ExitStatus=0

  # shellcheck disable=SC2034
  local Exitable=False

  # shellcheck disable=SC2034
  local Verbose=False

  # shellcheck disable=SC2034
  local VerboseOptions=

  # shellcheck disable=SC2034 disable=SC2155
  local NoAnsi=False

  # shellcheck disable=SC2034
  local ChecksumFile=

  # shellcheck disable=SC2034
  local Archive=

  # shellcheck disable=SC2034
  local Directory=

  # shellcheck disable=SC2034
  local DotFiles=False

  # shellcheck disable=SC2034
  local ShaMode=basic

  # shellcheck disable=SC2034
  local Extract=False

  # shellcheck disable=SC2034
  local UseMd5=True

  # shellcheck disable=SC2034
  local UseCrc=True

  # shellcheck disable=SC2034
  local Text=

  # shellcheck disable=SC2034
  local Checksums=

  # shellcheck disable=SC2034
  local OutputFile=

  # shellcheck disable=SC2034
  local HasParameter=False

  # shellcheck disable=SC2034
  local AdditionalArguments=

  # shellcheck disable=SC2034
  local OptionIndex=0

  Dependencies=$(Array 'pv' 'wget' 'unar')

  # Usage
  #
  Usage() {
    local -r Arrow="${Faint}›${Reset} "
    local -r Seperator="${Faint}-${Reset}"
    local -r DSQ="${Normal}${FgBlack}'${Reset}"
    local -r LSQ="${Faint}'${Reset}"
    local -r Pipe="${Faint}|${Reset}"

    # Enables verbose output (may be supplied multiple times).
    printf "\n"
    printf "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset} ${Bold}${Faint}Powerful multi file, multi checksum generator.${Reset}\n"
    printf "\n"
    printf "${Faint}Copyright (C) 2021 - ${CURRENT_YEAR} Xeriab Nabil (aka KodeBurner) $(FormatLink "<xeriab@tuta.io>" "<xeriab@tuta.io>")${Reset}\n"
    printf "\n"
    printf "${Bold}Usage:${Reset} ${FgGreen}${Self}${Reset} ${Faint}<${Reset}${Bold}OPTIONS${Reset}${Faint}>${Reset} ${Faint}[${Reset}${Bold}ARGUMENTS${Reset}${Faint}]${Reset}${Faint}...${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Available Options${Reset}:\n"
    printf "  ${Faint}-${Reset}${Normal}c${Reset} ${Faint}--${Reset}${Normal}${FgYellow}checksum${Reset}  ${Faint}<${Reset}${FgBrightWhite}file${Reset}${Faint}>${Reset}       ${Seperator} ${Normal}Specifies a file for checksum check.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}d${Reset} ${Faint}--${Reset}${Normal}${FgYellow}directory${Reset} ${Faint}<${Reset}${FgBrightWhite}directory${Reset}${Faint}>${Reset}  ${Seperator} ${Normal}Calculate checksum for files inside a directory.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}H${Reset} ${Faint}--${Reset}${Normal}${FgYellow}hidden${Reset}                 ${Seperator} ${Normal}Include hidden dot files when descending directories.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}k${Reset} ${Faint}--${Reset}${Normal}${FgYellow}crc${Reset}                    ${Seperator} ${Normal}Use CRC sum.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}m${Reset} ${Faint}--${Reset}${Normal}${FgYellow}md5${Reset}                    ${Seperator} ${Normal}Use MD5 checksum.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}o${Reset} ${Faint}--${Reset}${Normal}${FgYellow}output${Reset}    ${Faint}<${Reset}${FgBrightWhite}outfile${Reset}${Faint}>${Reset}    ${Seperator} ${Normal}Write output to the outfile.${Reset}\n"
    # printf "  ${Faint}-${Reset}${Normal}s${Reset} ${Faint}--${Reset}${Normal}${FgYellow}sha${Reset}       ${Faint}[${Reset}${FgBrightWhite}1${Reset}${Pipe}${Reset}${FgBrightWhite}224${Reset}${Pipe}${Reset}${FgBrightWhite}256${Reset}${Pipe}${Reset}${FgBrightWhite}384${Reset}${Pipe}${Reset}${FgBrightWhite}512${Reset}${Pipe}${Reset}${FgBrightWhite}Basic${Reset}${Pipe}${Reset}${FgBrightWhite}All${Reset}${Faint}]${Reset}   ${Seperator} ${Normal}Use SHA1${Pipe}SHA224${Pipe}SHA256${Pipe}SHA384${Pipe}SHA512${Pipe}Basic or All.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}s${Reset} ${Faint}--${Reset}${Normal}${FgYellow}sha${Reset}                    ${Seperator} ${Normal}Use SHA1${Pipe}SHA224${Pipe}SHA256${Pipe}SHA384${Pipe}SHA512${Pipe}Basic or All.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}t${Reset} ${Faint}--${Reset}${Normal}${FgYellow}string${Reset}    ${Faint}<${Reset}${FgBrightWhite}string${Reset}${Faint}>${Reset}     ${Seperator} ${Normal}Calculate checksum for strings instead of files.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}x${Reset} ${Faint}--${Reset}${Normal}${FgYellow}extract${Reset}                ${Seperator} ${Normal}Extract archives if using -d.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}z${Reset} ${Faint}--${Reset}${Normal}${FgYellow}archive${Reset}   ${Faint}<${Reset}${FgBrightWhite}archive${Reset}${Faint}>${Reset}    ${Seperator} ${Normal}Calculate checksum for an archive and its contents.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}v${Reset} ${Faint}--${Reset}${Normal}${FgYellow}verbose${Reset}                ${Seperator} ${Normal}Enable verbose output.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}V${Reset} ${Faint}--${Reset}${Normal}${FgYellow}version${Reset}                ${Seperator} ${Normal}Output version information and exit.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}h${Reset} ${Faint}--${Reset}${Normal}${FgYellow}help${Reset}                   ${Seperator} ${Normal}Display this help message and exit.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}i${Reset} ${Faint}--${Reset}${Normal}${FgYellow}info${Reset}                   ${Seperator} ${Normal}Display information and exit.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Additional Options${Reset}:\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}ansi${Reset}                      ${Seperator} ${Normal}Force ANSI output.${Reset}\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}no-ansi${Reset}                   ${Seperator} ${Normal}Disable ANSI output.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Arguments${Reset}:\n"
    printf "  ${Normal}${FgYellow}directory${Reset}                   ${Seperator} ${Normal}Directory to generate checksum for.${Reset}\n"
    printf "  ${Normal}${FgYellow}file${Reset}                        ${Seperator} ${Normal}File to generate checksum for.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Examples${Reset}:\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} ${Faint}-${Reset}${FgYellow}c${Reset} ${LSQ}/path/to/file${LSQ}  ${Faint}# Generate checksums for the file${Reset}\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} ${Faint}-${Reset}${FgYellow}d${Reset} ${LSQ}.${LSQ}              ${Faint}# Generate checksums for the directory${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Information${Reset}:\n"
    printf "  ${Normal}ChecksumFile${Reset}: ${DSQ}${Faint}${ChecksumFile:-Not Specified!}${Reset}${DSQ}\n"
    printf "  ${Normal}Directory${Reset}:    ${DSQ}${Faint}${Directory:-Not Specified!}${Reset}${DSQ}\n"
    printf "  ${Normal}Use MD5${Reset}:      ${DSQ}${Faint}${UseMd5:-False}${Reset}${DSQ}\n"
    printf "  ${Normal}Use CRC${Reset}:      ${DSQ}${Faint}${UseCrc:-False}${Reset}${DSQ}\n"
    printf "  ${Normal}SHA Mode${Reset}:     ${DSQ}${Faint}${ShaMode:-Not Specified!}${Reset}${DSQ}\n"
    printf "  ${Normal}Dot Files${Reset}:    ${DSQ}${Faint}${DotFiles:-Not Specified!}${Reset}${DSQ}\n"
    printf "  ${Normal}String${Reset}:       ${DSQ}${Faint}${Text:-Not Specified!}${Reset}${DSQ}\n"
    printf "\n"
  }

  # Info
  #
  Info() {
    local -r Arrow="${Faint}›${Reset} "
    local -r Seperator="${Faint}-${Reset}"
    local -r DSQ="${Normal}${FgBlack}'${Reset}"

    printf "\n"
    printf "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset} ${Bold}${Faint}Powerful multi file, multi checksum generator.${Reset}\n"
    printf "\n"
    printf "${Faint}Copyright (C) 2021 - ${CURRENT_YEAR} Xeriab Nabil (aka KodeBurner) $(FormatLink "<xeriab@tuta.io>" "<xeriab@tuta.io>")${Reset}\n"
    printf "\n"
    printf "${Bold}${FgWhite}Information${Reset}:\n"
    printf " ${Arrow}${Normal}ChecksumFile${Reset}: ${DSQ}${Faint}${ChecksumFile:-Not Specified!}${Reset}${DSQ}\n"
    printf " ${Arrow}${Normal}Directory${Reset}:    ${DSQ}${Faint}${Directory:-Not Specified!}${Reset}${DSQ}\n"
    printf " ${Arrow}${Normal}Use MD5${Reset}:      ${DSQ}${Faint}${UseMd5:-False}${Reset}${DSQ}\n"
    printf " ${Arrow}${Normal}Use CRC${Reset}:      ${DSQ}${Faint}${UseCrc:-False}${Reset}${DSQ}\n"
    printf " ${Arrow}${Normal}SHA Mode${Reset}:     ${DSQ}${Faint}${ShaMode:-Not Specified!}${Reset}${DSQ}\n"
    printf " ${Arrow}${Normal}Dot Files${Reset}:    ${DSQ}${Faint}${DotFiles:-Not Specified!}${Reset}${DSQ}\n"
    printf " ${Arrow}${Normal}String${Reset}:       ${DSQ}${Faint}${Text:-Not Specified!}${Reset}${DSQ}\n"
    printf "\n"
  }

  # ArgumentsParser
  #
  ArgumentsParser() {
    # shellcheck disable=SC2034
    local Argument=""

    # shellcheck disable=SC2034
    local VerboseOptionsHolder=""

    # shellcheck disable=SC2034
    local Option=

    # shellcheck disable=SC2034
    local OptionArgument=

    # NeedsArgument
    #
    NeedsArgument() {
      if [ -z "$2" ]; then
        # Log -e "No (argument)s provided for option ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}."
        # Log -e "Option ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset} must have an argument."
        Log -f "Option ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset} must have an argument."
        return 1
      fi
    }

    [ $# -eq 0 ] && {
      Usage
      ExitStatus=$(($# > 1))
      return $ExitStatus
    }

    while [ $# -gt 0 ]; do
      case $1 in
      #
      # Common
      #

      # Hidden
      #
      -H | --hidden | -hidden)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))
        DotFiles=True
        shift
        ;;

      # Extract
      #
      -x | --extract | -extract)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))
        Extract=True
        shift
        ;;

      # MD5
      #
      -m | --md5 | -md5)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))
        UseMd5=True
        unset Text
        shift
        ;;

      # CRC
      #
      -k | --crc | -crc)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))
        UseCrc=True
        shift
        ;;

      # Output
      #
      -o | --output | -output)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))

        UseCrc=True

        NeedsArgument "$1" "$2"

        [ -n "$2" ] && {
          OutputFile="$2"
          shift 2
        }

        if IsFile "$OutputFile" || IsExecutable "$OutputFile"; then
          Log -w "File ${Reset}${Faint}'${Reset}${Bold}${OutputFile}${Reset}${Faint}'${Reset} exists!"
          # Log -r "${Bold}Do you want to append the output on ${1}? [y/N]${Reset}\n"
          # read -r input
          Question 'yes|no' "${Bold}Do you want to append the output on ${Faint}'${Reset}${Bold}${OutputFile}${Reset}${Faint}'${Reset}${Normal}${Bold}?${Reset}" 'Choice'

          # Check input
          # shellcheck disable=SC2154
          case "$Choice" in
          # Yes
          "$NewLine" | 1 | [Yy][Ee][Ss] | [Yy])
            Log -i "${FgBlue}Appending output to ${Reset}${Faint}'${Reset}${Bold}${OutputFile}${Reset}${Faint}'${Reset}"
            OUT_FILE=$OutputFile
            ;;
            # User aborted
          "$EMPTY_STR" | 0 | [Nn][Oo] | [Nn])
            Log -i "${FgYellow}Output to file disabled${Reset}"
            ;;
          # User aborted
          *) ;;
          esac

          unset Choice
        else
          OUT_FILE=$OutputFile
          command touch "$OutputFile"
        fi
        ;;

      # Checksum
      #
      -c=* | --checksum=* | -checksum=*)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))

        [ -n "$1" ] && {
          ChecksumFile="${1#*=}"
          shift 1
        }

        if (IsUrl "$ChecksumFile"); then
          if ! IsDir "$TEMP_DIR"; then
            command mkdir $TEMP_DIR
          fi

          command wget "$ChecksumFile" -O "$TEMP_DIR/${ChecksumFile##*/}" -q --show-progress
          command tput cuu 1
          command tput el

          ChecksumFile="$TEMP_DIR/${ChecksumFile##*/}"
        fi

        (command file "$ChecksumFile" | command grep "ASCII\ text") 1>/dev/null 2>&1
        ExitStatus=$?

        if [ "$ExitStatus" = 0 ]; then
          Checksums=$ChecksumFile
        else
          # Log -e "Argument: ${Faint}'-x${Reset}${Bold}${1}${Reset}${Faint}'${Reset} ${Faint}'${Reset}${Bold}${ChecksumFile}${Reset}${Faint}'${Reset} is not a text file." 1>&2
          Log -e "-x$1: ${Faint}'${Reset}${ChecksumFile}${Faint}'${Reset} is not a text file."
          ExitStatus=0
          return $ExitStatus
        fi
        ;;

      # Checksum
      #
      -c | --checksum | -checksum)
        NeedsArgument "$1" "$2"

        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))

        [ -n "$2" ] && {
          ChecksumFile="$2"
          # shift 2
          shift 2
        }

        if (IsUrl "$ChecksumFile"); then
          if ! IsDir "$TEMP_DIR"; then
            command mkdir $TEMP_DIR
          fi

          command wget "$ChecksumFile" -O "$TEMP_DIR/${ChecksumFile##*/}" -q --show-progress
          command tput cuu 1
          command tput el

          ChecksumFile="$TEMP_DIR/${ChecksumFile##*/}"
        fi

        (command file "$ChecksumFile" | command grep "ASCII\ text") 1>/dev/null 2>&1
        ExitStatus=$?

        if [ "$ExitStatus" = 0 ]; then
          Checksums=$ChecksumFile
        else
          # Log -e "Argument: ${Faint}'-x${Reset}${Bold}${1}${Reset}${Faint}'${Reset} ${Faint}'${Reset}${Bold}${ChecksumFile}${Reset}${Faint}'${Reset} is not a text file." 1>&2
          Log -e "${Faint}-${Reset}x${Option}${Faint}:${Reset} ${Faint}'${Reset}${ChecksumFile}${Faint}'${Reset} is not a text file."
          ExitStatus=0
          return $ExitStatus
        fi
        ;;

      # SHA
      #
      -s=* | --sha=* | -sha=*)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 2))

        [ -n "$1" ] && {
          OptionArgument="${1#*=}"
          shift 1
        }

        UseMd5=False
        UseCrc=False

        case "$OptionArgument" in
        1 | 224 | 256 | 384 | 512 | [Bb]asic | [Aa]ll)
          ShaMode="$OptionArgument"
          ;;
        *)
          Log -e "-s argument is wrong! accepted args: [1|224|256|384|512|Basic|basic|All|all]"
          # Log -e "Argument: ${Faint}'-s${Reset}${Bold}${OptionArgument}${Reset}${Faint}'${Reset} ${Faint}'${Reset}${Bold}${OptionArgument}${Reset}${Faint}'${Reset} is not a text file." 1>&2
          ExitStatus=1
          return $ExitStatus
          ;;
        esac
        ;;

      # SHA
      #
      -s | --sha | -sha)
        NeedsArgument "$1" "$2"

        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 2))

        [ -n "$2" ] && {
          OptionArgument=$2
          shift 2

                  case "$OptionArgument" in
        1 | 224 | 256 | 384 | 512 | [Bb]asic | [Aa]ll)
          ShaMode="$OptionArgument"
          ;;
        *)
          Exitable=True
          ExitStatus=1
          Log -e "-s argument is wrong! accepted args: [1|224|256|384|512|Basic|basic|All|all]"
          # Log -e "Argument: ${Faint}'-s${Reset}${Bold}${OptionArgument}${Reset}${Faint}'${Reset} ${Faint}'${Reset}${Bold}${OptionArgument}${Reset}${Faint}'${Reset} is not a text file." 1>&2
          return $ExitStatus
          ;;
        esac
        }

        UseMd5=False
        UseCrc=False
        ;;

      # String
      #
      -t | --string | -string)
        NeedsArgument "$1" "$2"

        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))

        HasParameter=True

        # Log -d "OPTIND: '$OPTIND'"
        # Log -d "OPTARG: '$OPTARG'"

        # [ -n "$2" ] && {
        #   OptionArgument="$*"
        #   shift
        # }

        local Args=

        shift

        # Args=$(Array "$@")
        Args=$(Array "$*")
        # First="$(printf '%s\n' "$Args" | ArrayNth 0)"
        # Args=$(printf '%s\n' "$Args" | ArrayRemove "$First")

        # if [ "$(ArraySize "$Args")" -gt 1 ]; then
        #   First="$(printf '%s\n' "$Arguments" | ArrayNth 0)"
        #   Args=$(printf '%s\n' "$Args" | ArrayRemove "$First")
        # fi

        for Each in $(printf '%s\n' "$Args" | ArrayElementEncode); do
          Text="$Text $Each"
        done
        unset Each

        if [ -n "$Text" ]; then
          # Log -i "${Bold}String Checksum${Reset}"
          Log -r "${Bold}${FgGreen}›${Reset} ${Bold}String Checksum${Reset}\n"
          # Text=$(printf "$Text" | command sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')
          Text=$(StrTrim "${Text}")
        else
          ExitStatus=1
          Log -e "Empty string!"
          return $ExitStatus
        fi

        break
        ;;

      # Archive
      #
      -z=* | --archive=* | -archive=*)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))

        [ -n "$1" ] && {
          Archive="${1#*=}"
          shift
        }

        if IsArchive "$Archive"; then
          # Log -i "${Bold}Checking archive${Reset} ${Faint}'${Reset}${Bold}${Archive}${Reset}${Faint}'${Reset}"
          Log -r "${Faint}${FgCyan}›${Reset} ${Bold}Checking archive ${Faint}'${Reset}${Bold}${Archive}${Reset}${Faint}'${Reset}\n"
          HasParameter=True
          Extract=True
          ChecksumCascade "$Archive"
        else
          Exitable=True
          ExitStatus=1
          Log -e "${Faint}-${Reset}${FgYellow}z${Reset}${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset} is not an archive file."
          return $ExitStatus
        fi
        ;;

      # Archive
      #
      -z | --archive | -archive)
        NeedsArgument "$1" "$2"

        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))

        [ -n "$2" ] && {
          Archive="$2"
          # shift 2
          shift
        }

        if IsArchive "$Archive"; then
          # Log -i "${Bold}Checking archive${Reset} ${Faint}'${Reset}${Bold}${Archive}${Reset}${Faint}'${Reset}"
          Log -r "${Faint}${FgCyan}›${Reset} ${Bold}Checking archive ${Faint}'${Reset}${Bold}${Archive}${Reset}${Faint}'${Reset}\n"
          HasParameter=True
          Extract=True
          ChecksumCascade "$Archive"
        else
          Exitable=True
          ExitStatus=1
          Log -e "${Faint}-${Reset}${FgYellow}z${Reset}${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset} is not an archive file."
          return $ExitStatus
        fi
        ;;

      # Directory
      #
      -d=* | --directory=* | -directory=*)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))

        [ -n "$1" ] && {
          Directory="${1#*=}"
          shift 1
        }

        if IsDir "$Directory"; then
          # Log -i "${Bold}Checking directory${Reset} ${Faint}'${Reset}${Bold}${Directory}${Reset}${Faint}'${Reset}"
          Log -r "${Faint}${FgCyan}›${Reset} Checking directory ${Faint}'${Reset}${Bold}${Directory}${Reset}${Faint}'${Reset}\n"
          HasParameter=True
          ChecksumCascade "$Directory"
        else
          Log -e "${Faint}-${Reset}${FgYellow}d${Reset}${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset} is not a directory."
          ExitStatus=1
          return $ExitStatus
        fi
        ;;

      # Directory
      #
      -d | --directory | -directory)
        NeedsArgument "$1" "$2"

        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))

        [ -n "$2" ] && {
          Directory="$2"
          shift 2
        }

        if IsDir "$Directory"; then
          # Log -i "${Bold}Checking directory${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}"
          Log -r "${Faint}${FgCyan}›${Reset} Checking directory ${Faint}'${Reset}${Bold}${Directory}${Reset}${Faint}'${Reset}\n"
          HasParameter=True
          ChecksumCascade "$Directory"
        else
          Log -e "${Faint}-${Reset}${FgYellow}d${Reset}${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset} is not a directory."
          ExitStatus=1
          return $ExitStatus
        fi
        ;;

      #
      # Options
      #

      # Help
      #
      -h | --help | -help)
        Exitable=True
        ExitStatus=$(($# > 1))
        OptionIndex=$((OptionIndex + 1))
        Usage
        shift
        return $ExitStatus
        ;;

      # Info
      #
      -i | --info | -info)
        Exitable=True
        ExitStatus=$(($# > 1))
        OptionIndex=$((OptionIndex + 1))
        Info
        shift
        return $ExitStatus
        ;;

      # ANSI
      #
      --ansi | -ansi)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))
        # NoAnsi=False
        # shellcheck disable=SC2034
        # DISABLE_CONSOLE_OUTPUT_COLORS=False
        # DISABLE_CONSOLE_OUTPUT_STYLES=False
        SetupAnsiColors
        SetupAnsiStyles
        shift
        ;;

      # No ANSI
      #
      --no-ansi | -no-ansi)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))
        # NoAnsi=True
        # shellcheck disable=SC2034
        # DISABLE_CONSOLE_OUTPUT_COLORS=True
        # DISABLE_CONSOLE_OUTPUT_STYLES=True
        DisableAnsiColors
        DisableAnsiStyles
        shift
        ;;

      # Verbose
      #
      -v | --verbose | -verbose)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))

        FunctionDebug=True
        # Verbose=True
        VerboseOptionsHolder="-v ${VerboseOptionsHolder}"
        # shellcheck disable=SC2034
        VerboseOptions=$VerboseOptionsHolder
        shift
        ;;

      # Version
      #
      -V | --version | -version)
        Exitable=True
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))
        Log -r "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset}\n"
        shift
        return $ExitStatus
        ;;

      #
      # Misc
      #

      #
      #
      -- | -)
        Exitable=False
        ExitStatus=0
        OptionIndex=$((OptionIndex + 1))
        shift
        Arguments="${Arguments} ${*}"
        Arguments=$(StrNormalize "$Arguments")
        break
        ;;

      #
      #
      --* | -*)
        Log -e "Unrecognized option(s)${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}" 1>&2
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      # Bad short option
      #
      \?)
        Log -e "Unrecognized option(s)${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}" 1>&2
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      #
      #
      :)
        Log -e "${Faint}-${Reset}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset} parameter is mandatory."
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      #
      #
      *)
        Exitable=False
        ExitStatus=0
        [ $# -gt 0 ] && shift
        ;;
      esac
    done

    OptionIndex=$((OptionIndex - 1))

    set -- "${Arguments}"

    # shellcheck disable=SC2034
    AdditionalArguments=$Arguments

    if [ -n "$Text" ]; then
      return
    fi
  }

  # CompareSum
  #
  # Generates a checksum for a file or a string. You can specify the program as
  # first argument. Second argument is the file/string of which we want to
  # generate the digest the third argument is the index of the entry.
  # If necessary compares the generated checksum with a list of checksums given
  # as file.
  #
  CompareSum() {
    # shellcheck disable=SC2034
    Self='gensum'

    local -r Argument="${1:-}"
    # local Pre="${FgBlue}${3:-}) ${Reset}${Bold}${1/sum/}: ${Reset}"
    local Pre=
    local Parameters=
    local TempChecksum=
    local FileName=
    local Algorithm=

    Algorithm=$(StrReplace 'sum' '' "$Argument")
    Algorithm=$(StrToUpper "$Algorithm")
    [ "$Algorithm" = "CK" ] && Algorithm="CRC"

    Pre="${FgBrightBlue}${3:-}${Reset}${Faint})${Reset} ${FgBrightWhite}${Algorithm}${Reset}${Faint}:${Reset} "

    case "$Argument" in
    "cksum")
      Parameters="{ print \$1,\$2 }"
      ;;
    *)
      Parameters="{ print \$1 }"
      ;;
    esac

    if [ -z "$Text" ] || IsEmpty "$Text"; then
      FileName=$(basename "$2")
      # TempChecksum=$(command pv -N "${FileName} ${1/sum/}" "$2" | "$1" | $AWK_COMMAND "$Parameters")
      # TempChecksum=$(command pv -N "${FileName} $(StrReplace 'sum' '' "$Argument")" "$FileName" | $Argument | $AWK_COMMAND "$Parameters")
      TempChecksum=$(
        command pv -N "${FileName} ${Algorithm}" "$2" |
          $Argument | $AWK_COMMAND "$Parameters"
      )

      if [ -z "$OUT_FILE" ]; then
        command tput cuu 1
        command tput el
      fi

      # shellcheck disable=SC2034
      ENABLE_OUTFILE=True

      WriteToFile "${TempChecksum} ${FileName}"

      # shellcheck disable=SC2034
      ENABLE_OUTFILE=False
    else
      local TempChecksum=

      # Hashing the string
      TempChecksum=$(printf '%s' "$Result" | $Argument | $AWK_COMMAND "$Parameters")
      # echo "printf '%s' \"$Result\" | $Argument | $AWK_COMMAND '${Parameters}'"
    fi

    if [ -n "$Checksums" ]; then
      (command grep "$TempChecksum" "$Checksums") 1>/dev/null 2>&1
      ExitStatus=$?

      if [ $ExitStatus = 0 ]; then
        TempChecksum="${FgGreen}${TempChecksum} OK!${Reset}"
      else
        TempChecksum="${FgRed}${TempChecksum} WRONG!${Reset}"
      fi
    else
      TempChecksum="${FgWhite}${TempChecksum}${Reset}"
    fi

    Log -r "${Pre}${TempChecksum}\n"

    unset Pre Parameters TempChecksum FileName
  }

  # Checksum
  #
  # Calculates checksum for given argument. The argument must be a file.
  #
  Checksum() {
    # shellcheck disable=SC2034
    Self='gensum'

    local -r Argument="${1:-}"
    local Result=1

    if [ "$UseMd5" ] && [ "$UseMd5" = True ] || [ "$UseMd5" = 1 ]; then
      CompareSum md5sum "$Argument" "$Result"
      Result=$((Result + 1))
    fi

    if [ -n "$ShaMode" ]; then
      if [ "$ShaMode" = "Basic" ] || [ "$ShaMode" = "basic" ] || [ "$ShaMode" = "1" ] || [ "$ShaMode" = "All" ] || [ "$ShaMode" = "all" ]; then
        CompareSum sha1sum "$Argument" "$Result"
        Result=$((Result + 1))
      fi
      if [ "$ShaMode" = "All" ] || [ "$ShaMode" = "all" ] || [ "$ShaMode" = "224" ]; then
        CompareSum sha224sum "$Argument" "$Result"
        Result=$((Result + 1))
      fi
      if [ "$ShaMode" = "Basic" ] || [ "$ShaMode" = "basic" ] || [ "$ShaMode" = "256" ] || [ "$ShaMode" = "All" ] || [ "$ShaMode" = "all" ]; then
        CompareSum sha256sum "$Argument" "$Result"
        Result=$((Result + 1))
      fi
      if [ "$ShaMode" = "All" ] || [ "$ShaMode" = "all" ] || [ "$ShaMode" = "384" ]; then
        CompareSum sha384sum "$Argument" "$Result"
        Result=$((Result + 1))
      fi
      if [ "$ShaMode" = "All" ] || [ "$ShaMode" = "all" ] || [ "$ShaMode" = "512" ]; then
        CompareSum sha512sum "$Argument" "$Result"
        Result=$((Result + 1))
      fi
    fi

    if [ "$UseCrc" ] && [ "$UseCrc" = True ] || [ "$UseCrc" = 1 ]; then
      CompareSum cksum "$Argument" "$Result"
      Result=$((Result + 1))
    fi
  }

  # ChecksumCascade
  #
  # Recursive function for calculating checksums. It starts checking for
  # archives then for directories and then if the argument is a simple file it
  # will calculate its checksum.
  #
  ChecksumCascade() {
    # shellcheck disable=SC2034
    Self='gensum'

    local -r Argument="${1:-}"

    if [ -z "$Argument" ]; then
      return
    fi

    if [ "$Extract" = True ] || [ "$Extract" = 1 ] && IsArchive "$Argument"; then
      Archive "$Argument"
    elif IsDir "$Argument"; then
      local Files=

      if [ "$DotFiles" ] && [ "$DotFiles" = True ] || [ "$DotFiles" = 1 ]; then
        Files=$(command find "$Argument" -type f -print0)
      else
        Files=$(command find "$Argument" -type f -not -path '*/\.*' -print0)
      fi

      for Each in $(printf '%s\n' "$Files"); do
        ! [ "$Each" = "$(printf '\n')" ] && {
          # echo "Each: '$Each'"
          # Checksum "$Each"
          Log -r "\n${Bold}${Green}›${Reset} Checking file ${Faint}'${Reset}${Bold}${Each}${Reset}${Faint}'${Reset}\n"
          ChecksumCascade "$Each"
        }
      done

      unset Each
      unset Files
    else
      Checksum "$Argument"
    fi
  }

  # Archive
  #
  # Extracts the archive given as argument and calls the recursive checksum
  # function on its contents.
  #
  Archive() {
    # shellcheck disable=SC2034
    Self='gensum'

    local -r Argument="${1:-}"
    local FilePath=

    if IsArchive "$Argument"; then
      # Log -i "Extracting archive ${Faint}'${Reset}${Bold}${Argument}${Reset}${Faint}'${Reset}"
      Log -r "${Faint}${FgCyan}›${Reset} Extracting archive ${Faint}'${Reset}${Bold}${Argument}${Reset}${Faint}'${Reset}\n"

      if ! IsDir "$TEMP_DIR"; then
        command mkdir "$TEMP_DIR"
      fi

      FilePath=$(
        command unar -d -r -o "$TEMP_DIR" "$Argument" |
          command grep -i 'extracted\ to' | $AWK_COMMAND -F \" "{ print \$2 }"
      )

      Checksum "$Argument"

      if [ -n "$FilePath" ]; then
        ChecksumCascade "$FilePath"
      fi
    else
      Log -e "File ${Faint}'${Reset}${Bold}${Argument}${Reset}${Faint}'${Reset} is not an archive."
    fi

    unset FilePath
  }

  ! (IsInteractive) && {
    Log -e "Cannot run in non-interactive shell." 1>&2
    return 1
  }

  # trap 'rm -rf $TEMP_DIR; exit 0' INT TERM EXIT
  trap 'rm -rf $TEMP_DIR;' INT TERM

  #
  #
  ArgumentsParser "$@"

  for Dependency in $(printf '%s\n' "$Dependencies"); do
    CheckFor "$Dependency"
  done

  unset Dependency

  if [ "$MISSING_DEPENDENCY" = True ]; then
    ExitStatus=1
  fi

  # shellcheck disable=SC2034
  Self='gensum'

  local Arguments=
  local Element=
  local Result=

  [ "$FunctionDebug" = True ] || [ "$FunctionDebug" = 1 ] && Verbose=True

  if [ $# -gt 0 ] && [ "$Exitable" = False ] || [ "$Exitable" = 0 ]; then
    Arguments=$(Array "$@")

    # StrSubStr "${*}" 1
    # Log -v "$(StrSubStr "$(echo "$*")" 7)"
    # ArrayDefine 'Args'

    for Index in $(Range 0 "$((OptionIndex - 1))"); do
      # Element="$(printf '%s\n' "$Arguments" | ArrayNth "$Index")"
      # Element="$(printf '%s\n' "$Arguments" | ArrayIndexOf "$Element")"
      Element="$(ArrayGet "$Index" "$Arguments")"
      # printf '%s\n' "$Arguments" | ArrayRemove "$Element"
      # echo "Element: '$Element'"
      Arguments=$(printf '%s\n' "$Arguments" | ArrayRemove "$Element")
    done
    unset Index

    # Arguments=$(Array "$Arguments")

    # echo "ArrayLen: '$(ArrayLen "$Arguments")'"
    # echo "OptionIndex: '$OptionIndex'"

    # printf '%s\n' "$Arguments" | ArrayElementDecode

    if [ -n "$Text" ]; then
      Log -r "${Bold}${Faint}=${FgCyan}›${Reset} ${Faint}'${Reset}${Bold}${Text}${Reset}${Faint}'${Reset}\n"
      Checksum "$Text"
      ExitStatus=0
    else
      Result=0

      # for Each in "$@"; do
      # for Each in $(printf '%s\n' "$Arguments" | ArrayElementEncode); do
      for Each in $Arguments; do
        if [ "$Result" -eq 0 ]; then
          # Log -r "${Bold}Checking given files${Reset}\n"
          Log -r "${Bold}${FgBrightGreen}›${Reset} ${Bold}Checking given files${Reset}\n"
          # Log -r "${Faint}${FgCyan}›${Reset} Checking file ${Faint}'${Reset}${Bold}${Each}${Reset}${Faint}'${Reset}\n"
          Result=$((Result + 1))
        fi

        [ "$HasParameter" = False ] && {
          if IsDir "$Each"; then
            Log -e "${Faint}'${Reset}${Bold}${Each}${Reset}${Faint}'${Reset} is a directory, skipping..."
          elif IsFile "$Each" || IsExecutable "$Each"; then
            Checksum "$Each"
          else
            Log -e "File ${Faint}'${Reset}${Bold}${Each}${Reset}${Faint}'${Reset} doesn't exists!"
          fi
        }
      done

      unset Each
    fi

    if [ "$Result" -eq 0 ] && [ "$HasParameter" = False ]; then
      Log -e "Missing arguments."
      ExitStatus=1
    fi
  fi

  unset Arguments First Result

  (IsDir "${TEMP_DIR}") && AskDeleteTempFiles

  unset Self
  unset TEMP_DIR
  unset OUT_FILE

  return $ExitStatus
}

# region Aliases
#

alias generate-checksum=GenSum

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
