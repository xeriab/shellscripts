#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154 disable=SC2181
#
# ./functions/common/laravel-new-app.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

#
# original artwork by http://www.sanderfocus.nl/#/portfolio/tech-heroes
# converted to shell by #nixers @ irc.unix.chat
#

# LaravelNewApp
#
LaravelNewApp() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='laravel-new-app'

  # shellcheck disable=SC2034
  TEMP_DIR="/tmp/${Self}-tmp"

  # shellcheck disable=SC2034
  OUT_FILE="${TEMP_DIR}/script.Log"

  # shellcheck disable=SC2034
  local -r Date="(12/12/2021)"

  # shellcheck disable=SC2034
  local -r Version="0.1.0"

  local -r AppName="${1:-}"

  local SuProgram=
  local DockerCli=
  local CurrentUser=
  local CurrentDir=

  CurrentUser="${USER}"
  CurrentDir=$(pwd)

  if (hash docker 1>/dev/null 2>&1); then
    DockerCli='docker'
  elif (hash podman 1>/dev/null 2>&1); then
    DockerCli='podman'
  fi

  if (hash sudo 1>/dev/null 2>&1); then
    SuProgram='sudo'
  elif (hash doas 1>/dev/null 2>&1); then
    SuProgram='doas --'
  fi

  command $DockerCli info > /dev/null 2>&1

  # Ensure that Docker is running...
  if [ $? -ne 0 ]; then
    LogWarning "Docker/Podman is not running..."
    Die 1
  fi

  command $DockerCli run --rm \
    -v "$CurrentDir":/opt \
    -w /opt \
    laravelsail/php81-composer:latest \
    bash -c "laravel new $AppName && cd $AppName && php ./artisan sail:install --with=mysql,redis,meilisearch,mailhog,selenium "

  command cd "$AppName" || return

  printf "\n"

  if ($SuProgram -n True > /dev/null 2>&1); then
    $SuProgram chown -R "${CurrentUser}": .
    printf "${Bold}${FgWhite}Get started with:${Normal} cd ${AppName} && ./vendor/bin/sail up"
  else
    printf "${Bold}${FgWhite}Please provide your password so we can make some final adjustments to your application's permissions.${Normal}"
    printf "\n"
    $SuProgram chown -R "${CurrentUser}": .
    printf "\n"
    printf "${Bold}${FgWhite}Thank you! We hope you build something incredible. Dive in with:${Normal} cd ${AppName} && ./vendor/bin/sail up"
  fi

  unset SuProgram DockerCli CurrentUser CurrentDir
  unset Self TEMP_DIR OUT_FILE

  return 0
}

# region Aliases
#

alias laravel-new-app=LaravelNewApp

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
