#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./functions/common/misc.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
# alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s~^\s*[0-9]\+\s*//;s~[;&|]\s*alert$//'\'')"'

# Add an "alert" alias for long running commands.
# Use like so:
#   sleep 10; alert
#
Alert() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='alert'

  # ! (hash notify-send 1>/dev/null 2>&1) && {
  #   LogError "notify-send command not found"
  #   exit 1
  # }

  ProgramExists notify-send

  local title="${1:-Alert}"

  local icon="${2:-}"

  local LastCommand="${3:-}"

  # shellcheck disable=SC2181
  [ $? = 0 ] && icon="terminal" || icon="Error:"

  # last_command=$(command history | command tail -n1 | command sed -e 's~^\s*[0-9]\+\s*//;s~[;&|]\s*alert$//')
  LastCommand=$(command history | command tail -n1 | command sed -e 's~^\s*[0-9]\+\s*//;s~[;&|]\s*alert$//')

  command notify-send --category "presence" \
    --urgency=low \
    --icon "${icon}" \
    "${title}" \
    "${LastCommand}"

  unset Self
}

# View man pages
#
PreviewManPage() {
  # shellcheck disable=SC2124
  local Arguments="$@"

  if [ "$(uname | tr '[:upper:]' '[:lower:]')" = 'darwing' ]; then
    command man -t "$Arguments" | command ps2pdf - - | open -f -a Preview
  else
    command man -t "$Arguments" | command ps2pdf - "/tmp/$Arguments.pdf"
    xdg-open "/tmp/$Arguments.pdf"
  fi
}

# Man page coloring using less
#
# termcap
# ks       make the keypad send commands
# ke       make the keypad send digits
# vb       emit visual bell
# mb       start blink
# md       start bold
# me       turn off bold, blink and underline
# so       start standout (reverse video)
# se       stop standout
# us       start underline
# ue       stop underline
#
Man() {
  export GROFF_NO_SGR=yes

  env \
    LESS_TERMCAP_mb="$(printf "\e[1;31m")" \
    LESS_TERMCAP_md="$(printf "\e[0;35m")" \
    LESS_TERMCAP_me="$(printf "\e[0m")" \
    LESS_TERMCAP_se="$(printf "\e[0m")" \
    LESS_TERMCAP_so="$(printf "\e[1;47;30m")" \
    LESS_TERMCAP_ue="$(printf "\e[0m")" \
    LESS_TERMCAP_us="$(printf "\e[0;34m")" \
    GROFF_NO_SGR=yes \
    LESS='-R -s' \
    command man "$@"
}

# Spellcheck with aspell
#
Spell() {
  printf "$@" | command aspell -a | command grep -Ev "^@|^$"
}

# Show active aria2 downloads with diana
#
da() {
  watch -ctn 3 "(printf '\033[32mGID\t\t Name\t\t\t\t\t\t\t%   Down   Size Speed    Up   S/L Time\033[36m'; \
        diana list| cdiff-so-fancyut -c -112; printf '\033[37m'; diana stats)"
}

# Jump x directories up
#
up() {
  local path=""
  local x=0

  [ "$1" ] || set 1

  while [ "$x" -lt "$1" ]; do
    path="${path}../"
    x=$((x + 1))
  done

  cd "$path" || return
}

# diff-so-fancy with regular files
#
dsf() {
  diff -u "$1" "$2" | diff-so-fancy | less
}

# rga (ripgrep-all) + fzf-tmux
# find-in-file - usage: fif <searchTerm> or fif "string with spaces" or fif "regex"
#
fif() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self="fif"

  if [ $# -lt 1 ]; then
    log "warn" "Needs a string to search for!" >&2
    return 1
  fi

  # shellcheck disable=SC2068,SC2145,SC2027
  file="$(rga --max-count=1 --ignore-case --files-with-matches --no-messages "$@" | fzf-tmux +m --preview="rga --ignore-case --pretty --context 10 '"$@"' {}")" && open "$file"

  unset Self
}

#
#
Touch() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self="touch"

  if [ $# -lt 1 ]; then
    # printf "%b %b\n" "Error:" "first argument should be specified." >&2
    LogError "first argument should be specified."
    return 1
  fi

  local file="${1:-}"
  file="$(realpath "${file}")"

  command touch "${file}"

  log "info" "File ${DIM}'${FINE}${BOLD}${file}${FINE}${DIM}'${FINE}${LIGHT_WHITE} created.${FINE}"

  unset Self
}

# Create and enter directory
#
mkd() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self="mkd"

  if [ $# -lt 1 ]; then
    # printf "%b %b\n" "Error:" "first argument should be specified." >&2
    LogError "first argument should be specified."
    return 1
  fi

  command mkdir -v -p "${1}"

  unset Self

  command cd "${1}" || return
}

# Create and enter directory
mk() {
  command mkdir -v -p "$@" && command cd "$1" || return
}

# # 1: spec, 2:expected, 3:args...
# #
# testopts() {
# 	# shellcheck disable=SC1090 disable=SC1091
# 	. "${XDG_DATA_HOME}/shell/common/lib/getopts.sh"

# 	INDEX=1.0
# 	OPTSTRING="$1"

# 	printf "%s" "$2" >"a"
# 	printf "\n" >"b"

# 	shift 2

# 	while get_opts "$OPTSTRING" "OPT:INDEX" "$@"; do
# 		printf "%s:%s: %s\n" "$INDEX" "$OPT" "$OPTARG" >>"b"
# 	done

# 	diff -u "a" "b"
# }

# Colourized cat output
#
ccat() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self=$(eval basename "$0" .sh)

  if [ $# -lt 1 ]; then
    # printf "%b %b\n" "Error:" "first argument should be specified." >&2
    LogError "first argument should be specified."
    return 1
  fi

  ! (hash glow 1>/dev/null 2>&1) && {
    LogError "'glow' command not found"
    Die 1
  }

  ! (hash highlight 1>/dev/null 2>&1) && {
    LogError "'highlight' command not found"
    Die 1
  }

  case "$1" in
  *.md) glow -s dark "$1" ;;
  *) highlight -O ansi "$1" --force ;;
  esac

  unset Self
}

# Colourized less output
#
cless() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self=$(eval basename "$0" .sh)

  if [ $# -lt 1 ]; then
    # printf "%b %b\n" "Error:" "first argument should be specified." >&2
    LogError "first argument should be specified."
    return 1
  fi

  ! (hash glow 1>/dev/null 2>&1) && {
    LogError "'glow' command not found"
    Die 1
  }

  ! (hash highlight 1>/dev/null 2>&1) && {
    LogError "'highlight' command not found"
    Die 1
  }

  case "$1" in
  *.md) glow -s dark "$1" | less -r ;;
  *) highlight -O ansi "$1" --force | less -r ;;
  esac

  unset Self
}

# Colourized tail output
#
ctail() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self=$(eval basename "$0" .sh)

  if [ $# -lt 1 ]; then
    # printf "%b %b\n" "Error:" "first argument should be specified." >&2
    LogError "first argument should be specified."
    return 1
  fi

  # run as: $ tailc logs/supplier-matching-worker.log Words_to_highlight
  local file="${1:-}"
  local color=

  if [ -n "$2" ]; then
    # shellcheck disable=SC2016
    color='
		# // {print "\033[37m" $0 "\033[39m"}
		{matched=0}
		/DEBUG/     {matched=1; print "\033[1;32m" $0 "\033[0m"}
		/INFO/      {matched=1; print "\033[1;34m" $0 "\033[0m"}
		/WARN/      {matched=1; print "\033[1;33m" $0 "\033[0m"}
		/WARNING/   {matched=1; print "\033[1;33m" $0 "\033[0m"}
		/WTF/       {matched=1; print "\033[1;33m" $0 "\033[0m"}
		/SEVERE/    {matched=1; print "\033[1;31m" $0 "\033[0m"}
		/ERROR/     {matched=1; print "\033[1;31m" $0 "\033[0m"}
		/Error:/    {matched=1; print "\033[1;31m" $0 "\033[0m"}
		/failed/    {matched=1; print "\033[1;31m" $0 "\033[0m"}
		/Caused by/ {matched=1; print "\033[0;96m" $0 "\033[0m"}
		# /('$2')/    {matched=1; print " '$2' ›› \033[1;32m" $0 "\033[0m"}
		/('$2')/    {matched=1; print " › \033[1;32m" $0 "\033[0m"}
		matched==0  {print "\033[0;37m" $0 "\033[39m"}'
  else
    # shellcheck disable=SC2016
    color='
		# // {print "\033[37m" $0 "\033[39m"}
		{matched=0}
		/DEBUG/     {matched=1; print "\033[1;32m" $0 "\033[0m"}
		/INFO/      {matched=1; print "\033[1;34m" $0 "\033[0m"}
		/WARN/      {matched=1; print "\033[1;33m" $0 "\033[0m"}
		/WARNING/   {matched=1; print "\033[1;33m" $0 "\033[0m"}
		/WTF/       {matched=1; print "\033[1;33m" $0 "\033[0m"}
		/SEVERE/    {matched=1; print "\033[1;31m" $0 "\033[0m"}
		/ERROR/     {matched=1; print "\033[1;31m" $0 "\033[0m"}
		/Error:/    {matched=1; print "\033[1;31m" $0 "\033[0m"}
		/failed/    {matched=1; print "\033[1;31m" $0 "\033[0m"}
		/Caused by/ {matched=1; print "\033[0;96m" $0 "\033[0m"}
		matched==0  {print "\033[0;37m" $0 "\033[39m"}'
  fi

  # command tail -5000f "${file}" | $AWK_COMMAND "${color}"
  command tail -n5000 -f "${file}" | $AWK_COMMAND "${color}"

  # Colors
  # 30 - black   34 - blue          40 - black    44 - blue
  # 31 - red     35 - magenta       41 - red      45 - magenta
  # 32 - green   36 - cyan          42 - green    46 - cyan
  # 33 - yellow  37 - white         43 - yellow   47 - white

  unset Self
}

#
#
#

# # '$0:A' gets the absolute path of this file
# SHELL_COLORIZE_PLUGIN_PATH=$0:A

# #
# #
# colorize_check_requirements() {
#   # shellcheck disable=SC1090 disable=SC1091
#   . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

#   # shellcheck disable=SC2034
#   Self="colorize_check_requirements"

#   if [ $# -lt 1 ]; then
#     log "warn" "Needs a string to search for!" >&2
#     return 1
#   fi

#   local available_tools='chroma pygmentize'

#   if [ -z "$SHELL_COLORIZE_TOOL" ]; then
#     if (hash pygmentize); then
#       SHELL_COLORIZE_TOOL='pygmentize'
#     elif (hash chroma); then
#       SHELL_COLORIZE_TOOL='chroma'
#     else
#       printf "Neither 'pygments' nor 'chroma' is installed!" >&2
#       return 1
#     fi
#   fi

#   if [ $(str_contains "$available_tools" "$SHELL_COLORIZE_TOOL") ]; then
#     printf "SHELL_COLORIZE_TOOL '$SHELL_COLORIZE_TOOL' not recognized. Available options are 'pygmentize' and 'chroma'." >&2
#     return 1
#   elif (hash "$SHELL_COLORIZE_TOOL"); then
#     printf "Package '$SHELL_COLORIZE_TOOL' is not installed!" >&2
#     return 1
#   fi

#   unset Self

#   return 0
# }

# colorize_cat() {
#   # shellcheck disable=SC1090 disable=SC1091
#   . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

#   # shellcheck disable=SC2034
#   Self="colorize_cat"

#   if [ $# -lt 1 ]; then
#     log "warn" "Needs a string to search for!" >&2
#     return 1
#   fi

#   !(colorize_check_requirements) && return 1

#   # If the environment variable SHELL_COLORIZE_STYLE
#   # is set, use that theme instead. Otherwise,
#   # use the default.
#   if [ -z "$SHELL_COLORIZE_STYLE" ]; then
#     # Both pygmentize & chroma support 'emacs'
#     SHELL_COLORIZE_STYLE='emacs'
#   fi

#   # Use stdin if no arguments have been passed.
#   if [ $# -eq 0 ]; then
#     if [ "$SHELL_COLORIZE_TOOL" = 'pygmentize' ]; then
#       command pygmentize -O style="$SHELL_COLORIZE_STYLE" -g
#     else
#       command chroma --style="$SHELL_COLORIZE_STYLE" --formatter="${SHELL_COLORIZE_CHROMA_FORMATTER:-terminal}"
#     fi

#     return $?
#   fi

#   # Guess lexer from file extension, or guess it from file contents if unsuccessful.
#   local FNAME
#   local lexer

#   for FNAME in "$@"; do
#     if [ "$SHELL_COLORIZE_TOOL" = 'pygmentize' ]; then
#       lexer=$(command pygmentize -N "$FNAME")
#       if [[ $lexer != text ]]; then
#         command pygmentize -O style="$SHELL_COLORIZE_STYLE" -l "$lexer" "$FNAME"
#       else
#         command pygmentize -O style="$SHELL_COLORIZE_STYLE" -g "$FNAME"
#       fi
#     else
#       command chroma --style="$SHELL_COLORIZE_STYLE" --formatter="${SHELL_COLORIZE_CHROMA_FORMATTER:-terminal}" "$FNAME"
#     fi
#   done

#   unset Self
# }

# # The less option 'F - Forward forever; like "tail -f".' will not work in this implementation
# # caused by the lack of the ability to follow the file within pygmentize.
# colorize_less() {
#   # shellcheck disable=SC1090 disable=SC1091
#   . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

#   # shellcheck disable=SC2034
#   Self="colorize_less"

#   if [ $# -lt 1 ]; then
#     log "warn" "Needs a string to search for!" >&2
#     return 1
#   fi

#   !(colorize_check_requirements) && return 1

#   _cless() {
#     # shellcheck disable=SC1090 disable=SC1091
#     . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

#     # shellcheck disable=SC2034
#     Self="_cless"

#     if [ $# -lt 1 ]; then
#       log "warn" "Needs a string to search for!" >&2
#       return 1
#     fi

#     # LESS="-R $LESS" enables raw ANSI colors, while maintain already set options.
#     local LESS="-R $LESS"

#     # This variable tells less to pipe every file through the specified command
#     # (see the man page of less INPUT PREPROCESSOR).
#     # 'zsh -ic "colorize_cat %s 2> /dev/null"' would not work for huge files like
#     # the ~/.zsh_history. For such files the tty of the preprocessor will be suspended.
#     # Therefore we must source this file to make colorize_cat available in the
#     # preprocessor without the interactive mode.
#     # `2>/dev/null` will suppress the error for large files 'broken pipe' of the python
#     # script pygmentize, which will show up if less has not fully "loaded the file"
#     # (e.g. when not scrolled to the bottom) while already the next file will be displayed.
#     local LESSOPEN="| ${CURRENT_SHELL} -c 'source \"$SHELL_COLORIZE_PLUGIN_PATH\"; \
#     SHELL_COLORIZE_TOOL=$SHELL_COLORIZE_TOOL SHELL_COLORIZE_STYLE=$SHELL_COLORIZE_STYLE \
#     colorize_cat %s 2> /dev/null'"

#     # LESSCLOSE will be set to prevent any errors by executing a user script
#     # which assumes that his LESSOPEN has been executed.
#     local LESSCLOSE=""

#     LESS="$LESS" LESSOPEN="$LESSOPEN" LESSCLOSE="$LESSCLOSE" command less "$@"
#   }

#   if [ -t 0 ]; then
#     _cless "$@"
#   else
#     # The input is not associated with a terminal, therefore colorize_cat will
#     # colorize this input and pass it to less.
#     # Less has now to decide what to use. If any files have been provided, less
#     # will ignore the input by default, otherwise the colorized input will be used.
#     # If files have been supplied and the input has been redirected, this will
#     # lead to unnecessary overhead, but retains the ability to use the less options
#     # without checking for them inside this script.
#     colorize_cat | _cless "$@"
#   fi

#   unset Self
# }

# cd when exiting lf
#
LC() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='lc'

  if [ $# -lt 1 ]; then
    # printf "%b %b\n" "Error:" "first argument should be specified." >&2
    LogError "first argument should be specified."
    return 1
  fi

  local tmp=

  tmp="$(mktemp)"

  ! (hash lf 1>/dev/null 2>&1) && {
    LogError "'lf' command not found"
    Die 1
  }

  command lf -last-dir-path="$tmp" "$@"

  if [ -f "$tmp" ]; then
    dir="$(command cat "$tmp")"
    command rm -rf "$tmp"
    # shellcheck disable=SC2164
    [ -d "$dir" ] && ! [ "$dir" = "$(pwd)" ] && command cd "$dir"
  fi

  unset Self
}

# GetSite
#
GetSite() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='getsite'

  if [ $# -lt 1 ]; then
    # printf "%b %b\n" "Error:" "first argument should be specified." >&2
    LogError "first argument should be specified."
    return 1
  fi

  local Url="${1:-}"
  local Domain=

  Domain="$(basename "${Url}")"

  command wget -c \
    --recursive \
    --no-clobber \
    --page-requisites \
    --html-extension \
    --convert-links \
    --restrict-file-names=windows \
    --domains "${Domain}" \
    --no-parent \
    "${Url}"

  unset Self
}

# Unix
#
# original artwork by http://www.sanderfocus.nl/#/portfolio/tech-heroes
# converted to shell by #nixers @ irc.unix.chat
#
Unix() {
  /usr/bin/cat <<'eof'
                     [38;5;255m,_ ,_==▄▂[0m
                  [38;5;255m,  ▂▃▄▄▅▅[48;5;240m▅[48;5;20m▂[48;5;240m▅¾[0m.            [38;5;199m/    [38;5;20m/[0m
                   [38;5;255m[48;5;20m▄[0m[38;5;255m[48;5;199m▆[38;5;16m[48;5;255m<´  [38;5;32m"[38;5;34m»[38;5;255m▓▓[48;5;32m▓[48;5;240m%[0m\       [38;5;199m/ [38;5;20m/   [38;5;45m/ [38;5;118m/[0m
                 [38;5;255m,[38;5;255m[48;5;240m▅[38;5;16m[48;5;255m7"     [38;5;160m´[38;5;34m>[38;5;255m[48;5;39m▓▓[38;5;199m[48;5;255m▓[0m[38;5;255m%   [38;5;20m/  [38;5;118m/ [38;5;199m> [38;5;118m/ [38;5;199m>[38;5;255m/[38;5;45m%[0m
                 [38;5;255m▐[48;5;240m[38;5;255m¶[48;5;240m[38;5;255m▓[48;5;255m       [38;5;196m,[38;5;34m»[48;5;201m[38;5;255m▓▓[0m[38;5;255m¾´[0m  [38;5;199m/[38;5;255m> %[38;5;199m/[38;5;118m%[38;5;255m/[38;5;199m/ [38;5;45m/  [38;5;199m/[0m
                  [38;5;255m[48;5;240m▓[48;5;255m[38;5;16m▃[48;5;16m[38;5;255m▅▅[38;5;16m[48;5;255m▅▃,,[38;5;32m▄[38;5;16m▅[38;5;255m[48;5;16m▅▅[38;5;255m[48;5;20mÆ[0m[38;5;255m\[0m[38;5;20m/[38;5;118m/[38;5;255m /[38;5;118m/[38;5;199m/[38;5;255m>[38;5;45m// [38;5;255m/[38;5;118m>[38;5;199m/   [38;5;20m/[0m
                 [48;5;20m[38;5;255mV[48;5;255m[38;5;16m║[48;5;20m[38;5;255m«[0m[38;5;255m¼.;[48;5;240m[38;5;255m→[48;5;255m[38;5;16m ║[0m[38;5;255m<«.,[48;5;25m[38;5;255m`[48;5;240m=[0m[38;5;20m/[38;5;199m/ [38;5;255m/>[38;5;45m/[38;5;118m/[38;5;255m%/[38;5;199m% / [38;5;20m/[0m
               [38;5;20m//[48;5;255m[38;5;16m╠<´ -²,)[48;5;16m[38;5;255m(▓[48;5;255m[38;5;16m~"-[38;5;199m╝/[0m[38;5;255m¾[0m[38;5;199m/ [38;5;118m%[38;5;255m/[38;5;118m>[38;5;45m/ [38;5;118m/[38;5;199m>[0m
           [38;5;20m/ / [38;5;118m/ [48;5;20m[38;5;255m▐[48;5;240m[38;5;16m%[48;5;255m -./▄▃▄[48;5;16m[38;5;255m▅[48;5;255m[38;5;16m▐[48;5;255m[38;5;16m, [38;5;199m/[48;5;199m[38;5;255m7[0m[38;5;20m/[38;5;199m/[38;5;255m;/[38;5;199m/[38;5;118m% [38;5;20m/ /[0m
           [38;5;20m/ [38;5;199m/[38;5;255m/[38;5;45m/[38;5;118m/[38;5;255m[48;5;240m`[48;5;20m[38;5;255m▌[48;5;20m[38;5;255m▐[48;5;255m[38;5;16m %z[0m[38;5;255mWv xX[48;5;20m[38;5;255m▓[48;5;34m[38;5;255m▇[48;5;199m[38;255m▌[0m[38;5;20m/[38;5;199m/[38;5;255m&;[38;5;20m% [38;5;199m/ [38;5;20m/[0m
       [38;5;20m/ / [38;5;255m/ [38;5;118m%[38;5;199m/[38;5;255m/%/[48;5;240m[38;5;255m¾[48;5;255m[38;5;16m½´[38;5;255m[48;5;16m▌[0m[38;5;246m▃▄[38;5;255m▄▄[38;5;246m▄▃▃[0m[48;5;16m[38;5;255m▐[38;5;255m[48;5;199m¶[48;5;20m[38;5;255m\[0m[38;5;20m/[0m[48;5;255m[38;5;240m&[0m [38;5;20m/[0m
         [38;5;199m<[38;5;118m/ [38;5;45m/[38;5;255m</[38;5;118m%[38;5;255m/[38;5;45m/[38;5;255m`[48;5;16m▓[48;5;255m[38;5;16m![48;5;240m[38;5;255m%[48;5;16m[38;5;255m▓[0m[38;5;255m%[48;5;240m[38;5;255m╣[48;5;240m[38;5;255;╣[0m[38;5;255mW[0m[38;5;250mY<Y)[48;5;255m[38;5;16my&[0m[38;5;255m/`[48;5;240m\[0m
     [38;5;20m/ [38;5;199m/ [38;5;199m%[38;5;255m/%[38;5;118m/[38;5;45m/[38;5;255m<[38;5;118m/[38;5;199m%[38;5;45m/[38;5;20m/[48;5;240m[38;5;255m\[38;5;16m[48;5;255mi7; ╠N[0m[38;5;246m>[38;5;255m)VY>[48;5;240m[38;5;255m7[0m[38;5;255m;  [38;5;255m[48;5;240m\[0m[38;5;255m_[0m    [38;5;255mUNIX IS VERY SIMPLE [38;5;45mIT JUST NEEDS A[0m
  [38;5;20m/   [38;5;255m/[38;5;118m<[38;5;255m/ [38;5;45m/[38;5;255m/<[38;5;199m/[38;5;20m/[38;5;199m/[38;5;20m<[38;5;255m_/%\[38;5;255m[48;5;16m▓[48;5;255m[38;5;16m  V[0m[38;5;255m%[48;5;255m[38;5;16mW[0m[38;5;255m%£)XY[0m  [38;5;240m_/%[38;5;255m‾\_,[0m   [38;5;45mGENIUS TO UNDERSTAND ITS SIMPLICITY[38;5;255m[0m
   [38;5;199m/ [38;5;255m/ [38;5;199m/[38;5;255m/[38;5;118m%[38;5;199m/[48;5;240m[38;5;255m_,=-[48;5;20m-^[0m[38;5;255m/%/%%[48;5;255m[38;5;16m\¾%[0m[38;5;255m¶[0m[48;5;255m[38;5;16m%[0m[38;5;255m%}[0m    [38;5;240m/%%%[38;5;20m%%[38;5;240m%;\,[0m
    [38;5;45m%[38;5;20m/[38;5;199m< [38;5;20m/[48;5;20m[38;5;255m_/[48;5;240m [0m[38;5;255m%%%[38;5;240m%%[38;5;20m;[38;5;255mX[38;5;240m%[38;5;20m%[38;5;255m\%[38;5;240m%;,     _/%%%;[38;5;20m,[38;5;240m     \[0m
   [38;5;118m/ [38;5;20m/ [38;5;240m%[38;5;20m%%%%[38;5;240m%;,    [38;5;255m\[38;5;240m%[38;5;20m%[38;5;255ml[38;5;240m%%;// _/[38;5;20m%;,[0m [38;5;234mdmr[0m
 [38;5;20m/    [38;5;240m%[38;5;20m%%;,[0m         [38;5;255m<[38;5;20m;[38;5;240m\-=-/ /[0m
     [38;5;20m;,[0m                [38;5;240ml[0m
eof
}

# GradleOrGradlew
#
# Looks for a gradlew file in the current working directory
# or any of its parent directories, and executes it if found.
# Otherwise it will call gradle directly.
#
GradleOrGradlew() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='gradle_or_gradlew'

  # find project root
  # taken from https://github.com/gradle/gradle-completion
  local dir=

  dir="$(pwd)"

  local project_root=

  project_root="$(pwd)"

  while ! [ "$dir" = '/' ]; do
    if [ -f "$dir/settings.gradle" ] || [ -f "$dir/settings.gradle.kts" ] || [ -f "$dir/gradlew" ]; then
      project_root="$dir"
      break
    fi

    # current_dir=$(echo "$dir" | awk '{ current_dir=substr($0, 1, 3); print current_dir; }')
    # dir="${dir:h}"

    # Parent directory
    dir=$(dirname "${dir}")
  done

  local tick="${DIM}\`${FINE}"

  # If gradlew found, run it instead of gradle
  if [ -f "$project_root/gradlew" ]; then
    # echo "\033[02m\033[32m[info]\033[0m executing \033[02m\`\033[0mgradlew\033[02m\`\033[0m instead of \033[02m\`\033[0mgradle\033[02m\`\033[0m"
    log "info" "Executing ${tick}gradlew${tick} instead of ${tick}gradle${tick}"
    "$project_root/gradlew" "$@"
  else
    command gradle "$@"
  fi

  unset Self
}

# FakeGrepColor
#
# Pretend to be busy in office to enjoy a cup of coffee
#
FakeGrepColor() {
  # shellcheck disable=SC2002
  command cat /dev/urandom | command hexdump -C | command grep --color=auto "ca fe"
}

# AsciiColoredTable
#
AsciiColoredTable() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='ascii'

  # if [ $# -lt 1 ]; then
  #   # printf "%b %b\n" "Error:" "first argument should be specified." >&2
  #   LogError "first argument should be specified."
  #   return 1
  # fi

  # Custom definitions
  #
  # local Head="\e[1;4;31m"
  # local Decimal="\e[33m"
  # local Hex="\e[36m"
  # local Char="\e[31m"
  # local Separator="\e[1;90m|\e[22m"
  local -r Head="\e[1;4;31m"
  local -r Decimal="\e[33m"
  local -r Hex="${FgCyan}"
  local -r Char="\e[31m"
  local -r Separator="${Faint}${FgBrightBlack}|\033[22m"

  # # shellcheck disable=SC2016 disable=SC2046
  # command cat "$SHELLSCRIPTS_PATH"/data/ascii.txt |
  #   command sed -e 's~$C~'$(printf "$Char")'~g' |
  #   command sed -e 's~$HX~'$(printf "$Hex")'~g' |
  #   command sed -e 's~$T~'$(printf "$Separator")'~g' |
  #   command sed -e 's~$D~'$(printf "$Decimal")'~g' |
  #   command sed -e 's~$R~'$(printf "\e[0m")'~g' |
  #   command sed -e 's~$HD~'$(printf "$Head")'~g' |
  #   command less -R --chop-long-lines

  # shellcheck disable=SC2016 disable=SC2046
  command cat "$SHELLSCRIPTS_PATH"/data/ascii.txt |
    command sed -e 's~$C~'$(printf "$Char")'~g' |
    command sed -e 's~$HX~'$(printf "$Hex")'~g' |
    command sed -e 's~$T~'$(printf "$Separator")'~g' |
    command sed -e 's~$D~'$(printf "$Decimal")'~g' |
    command sed -e 's~$R~'$(printf "\e[0m")'~g' |
    command sed -e 's~$HD~'$(printf "$Head")'~g'

  unset Self

  return 0
}

# UpdateGitRepos
#
UpdateGitRepos() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='gitup'

  if [ $# -lt 1 ]; then
    # printf "%b %b\n" "Error:" "first argument should be specified." >&2
    LogError "first argument should be specified."
    return 1
  fi

  # Perform
  #
  Perform() {
    local Target=

    # Target="${1:-$(pwd)}"
    Target="${1:-/*}"

    # # shellcheck disable=SC3044
    # (IsBash) || (IsZsh) && shopt -s dotglob

    if [ "$Target" = '.' ]; then
      Target=$(pwd)
    fi

    # command find -path "${Target}/[^.]*" -prune -type d | while IFS= read -r Each; do
    for Each in "$Target"/*; do
      local Directory=
      local ParentDir=
      local GitBranches=
      local Branches=

      ParentDir=$(pwd)
      Branches=$(Array)

      Each=$(realpath "$Each")

      if (IsDir "$Each") && (IsDir "$Each/.git"); then
        Directory=$(basename "$Each")

        Log -i "Opening directory ${Faint}'${Reset}${FgWhite}${Directory}${Reset}${Faint}'${Reset}"

        # command cd "$Each" || return
        eval command cd "$Each" || return

        # shellcheck disable=SC2034
        export GIT_DISCOVERY_ACROSS_FILESYSTEM=1

        GitBranches=$(eval 'command git branch | command cut -c 3-')

        if [ -n "$GitBranches" ]; then
          for Branch in $GitBranches; do
            # Branch=$(StrReplace "* " "" "${Branch}")
            Branches=$(ArrayAppend "$Branches" "$Branch")
          done

          FirstItem=$(printf '%s\n' "$Branches" | ArrayNth 0)
          Branches=$(printf '%s\n' "$Branches" | ArrayRemove "$FirstItem")

          unset Branch FirstItem
        fi

        (InArray 'master' "$Branches") && {
          command git checkout master 1>/dev/null 2>&1
        }

        (InArray 'main' "$Branches") && {
          command git checkout main 1>/dev/null 2>&1
        }

        Log -i "Refreshing repository in ${Faint}'${Reset}${FgWhite}${Directory}${Reset}${Faint}'${Reset}"

        (command git reset --hard) 1>/dev/null 2>&1
        (command git pull) 1>/dev/null 2>&1

        Log -i "Go to parent directory"

        command cd "${ParentDir}" || return

        printf '%b' '\n'
      fi

      unset Directory
      unset ParentDir
      unset GitBranches
      unset Branches
    done

    unset Each

    unset GIT_DISCOVERY_ACROSS_FILESYSTEM

    # for d in *; do cd "$d"; git pull; cd ..; done;

    # for Each in $Target; do
    #   local Directory=

    #   Each=$(realpath "$Each")

    #   if (IsDir "$Each"); then
    #     Directory=$(basename "$Each")
    #     Log -i "Opening directory ${Faint}'${Reset}${FgWhite}${Directory}${Reset}${Faint}'${Reset}"
    #     command cd "$Each" || return

    #     Log -d "Each: $Each"

    #     if (IsDir "$Each/.git"); then
    #       Log -i "Refreshing repository in ${Faint}'${Reset}${FgWhite}${Directory}${Reset}${Faint}'${Reset}"
    #       GitBranches=$(command git branch --list)

    #       if [ -n "$GitBranches" ]; then
    #         for Branch in $GitBranches; do
    #           Branch=$(StrReplace "* " "" "${Branch}")
    #           Branches=$(ArrayAppend "$Branches" "$Branch")
    #         done

    #         unset Branch
    #       fi

    #       (InArray 'master' "$Branches") && command git checkout master
    #       (InArray 'main' "$Branches") && command git checkout main
    #       (command git pull) 1>/dev/null 2>&1
    #     fi

    #     Log -i "Go to parent directory"
    #     command cd "${ParentDir}" || return
    #     printf '%b' '\n'
    #   fi

    #   unset Directory
    # done

    unset Each

    unset Target ParentDir Branches GitBranches
  }

  local Dirs=

  # shellcheck disable=SC2124
  Dirs=$(Array "$@")

  # if [ $# -gt 1 ]; then
  #   # shellcheck disable=SC2124
  #   Dirs="$@"
  # else
  #   Dirs="${1:-$(pwd)}"
  # fi

  for Each in $(printf '%s\n' "$Dirs" | ArrayElementDecode); do
    Perform "$Each"
  done

  unset Dirs Each Self

  return 0
}

# Npx
#
Npx() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='nupx'

  # ! (hash upx 1>/dev/null 2>&1) && {
  #     printf "%b %b\n" "Error:" "'upx' command not found"
  # 	exit 1
  # }

  ProgramExists "upx"

  if [ $# -lt 1 ]; then
    # printf "%b %b\n" "Error:" "first argument should be specified." >&2
    LogError "first argument should be specified."
    return 1
  fi

  local file="${1:-}"
  file=$(realpath "${file}")
  local backup="${2:-${file}.bak}"
  cp "${file}" "${backup}"

  # execute upx -q -k "${file}"
  command upx -q -k "${file}"

  Log -i "File '${file}' packed."

  # if (ProgramExists "upx"); then
  # 	# shellcheck disable=SC2039
  # 	backup="${2:-${file}.bak}"

  # 	copy "${file}" "${backup}"
  # 	execute upx -q -k "${file}"
  # 	log "info" "File '${file}' packed."
  # fi

  unset Self

  return 0
}

# region Aliases
#

# Easier alias to use the plugin
#
# alias ccat=colorize_cat
# alias cless=colorize_less
alias touch=Touch
alias man=Man
alias pman=PreviewManPage
alias alert=Alert
alias spell=Spell
alias lc=LC
alias getsite=GetSite
alias unix=Unix
alias gradle=GradleOrGradlew
alias grepcolor=FakeGrepColor
alias ascii=AsciiColoredTable
alias gitup=UpdateGitRepos
alias nupx=Npx

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
