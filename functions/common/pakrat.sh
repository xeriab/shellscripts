#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./functions/common/pakrat.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2020-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# Pakrat
#
# Create Shell aliases for Flatpaks so you can easily launch them from
# your shell.
#
Pakrat() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='pakrat'

  # shellcheck disable=SC2034
  TEMP_DIR="/tmp/${Self}-tmp"

  # shellcheck disable=SC2034
  OUT_FILE="${TEMP_DIR}/script.Log"

  # shellcheck disable=SC2034
  local -r Date="(12/12/2020)"

  # shellcheck disable=SC2034
  local -r Version="0.1.0"

  # shellcheck disable=SC2034
  local FunctionDebug=False

  # shellcheck disable=SC2034
  local ExitStatus=0

  # shellcheck disable=SC2034
  local Exitable=False

  # shellcheck disable=SC2034
  local Verbose=

  # shellcheck disable=SC2034
  local VerboseOptions=

  # shellcheck disable=SC2034 disable=SC2155
  local NoAnsi=False

  # shellcheck disable=SC2034
  local AdditionalArguments=

  # shellcheck disable=SC2034
  local OptionIndex=

  # shellcheck disable=SC2034
  local Command=

  # shellcheck disable=SC2034
  local Target=

  # shellcheck disable=SC2034
  local Mode=

  # shellcheck disable=SC2034
  local PreviousCommand=

  # shellcheck disable=SC2034
  local SymRC=

  # shellcheck disable=SC2034
  local SymDirectory=

  # shellcheck disable=SC2034
  local SymFile=flatpak-aliases.sh

  if (IsBash); then
    SymRC=.bashrc.d
  else
    SymRC=.local/share/shellscripts/aliases
  fi

  SymDirectory="${HOME}"/$SymRC

  # Usage
  #
  Usage() {
    local Arrow="${Faint}›${Reset} "
    local Seperator="${Faint}${FgBrightBlack}-${Reset}"
    local DSQ="${Normal}${FgBlack}'${Reset}"
    local LSQ="${Faint}'${Reset}"
    # local Pipe="${Faint}|${Reset}"

    # Enables verbose output (may be supplied multiple times).
    printf "\n"
    printf "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset} ${Bold}${Faint}Create Shell aliases for Flatpaks so you can easily launch them from your shell.${Reset}\n"
    printf "\n"
    printf "${Faint}Copyright (C) 2021 - ${CURRENT_YEAR} Xeriab Nabil (aka KodeBurner) $(FormatLink "<xeriab@tuta.io>" "<xeriab@tuta.io>")${Reset}\n"
    printf "\n"
    printf "${Bold}Usage:${Reset} ${FgGreen}${Self}${Reset} ${Faint}[${Reset}COMMANDS${Reset}${Faint}]${Reset} ${Faint}[${Reset}OPTIONS${Reset}${Faint}]${Reset} ${Faint}<${Reset}target${Reset}${Faint}>${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Available Commands${Reset}:\n"
    printf "  ${Normal}${FgYellow}add${Reset}           ${Seperator} ${Normal}Add.${Reset}\n"
    printf "  ${Normal}${FgYellow}list${Reset}          ${Seperator} ${Normal}List all active flatpak symlinks.${Reset}\n"
    printf "  ${Normal}${FgYellow}remove${Reset}        ${Seperator} ${Normal}Remove alias.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Available Options${Reset}:\n"
    printf "  ${Faint}-${Reset}${Normal}c${Reset} ${Faint}--${Reset}${Normal}${FgYellow}command${Reset}  ${Seperator} ${Normal}The command to add alias for. ${Faint}${FgBrightBlack}(${Reset}${Faint}Used only with${Reset} ${Bold}add${Normal} ${Faint}command${Reset}${Faint}${FgBrightBlack})${Reset}${Faint}.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}v${Reset} ${Faint}--${Reset}${Normal}${FgYellow}verbose${Reset}  ${Seperator} ${Normal}Enable verbose output.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}V${Reset} ${Faint}--${Reset}${Normal}${FgYellow}version${Reset}  ${Seperator} ${Normal}Output version information and exit.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}h${Reset} ${Faint}--${Reset}${Normal}${FgYellow}help${Reset}     ${Seperator} ${Normal}Display this help message and exit.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}i${Reset} ${Faint}--${Reset}${Normal}${FgYellow}info${Reset}     ${Seperator} ${Normal}Display information and exit.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Additional Options${Reset}:\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}ansi${Reset}       ${Seperator} ${Normal}Force ANSI output.${Reset}\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}no-ansi${Reset}    ${Seperator} ${Normal}Disable ANSI output.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Arguments${Reset}:\n"
    printf "  ${Normal}target${Reset}        ${Seperator} ${Normal}Flatpak package alias.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Examples${Reset}:\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} add ${Faint}--${Reset}command ${LSQ}flatpak run org.gnu.emacs${LSQ} emacs        ${Faint}# Create alias for emacs${Reset}\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} add ${Faint}--${Reset}command ${LSQ}flatpak run org.gnu.emacs -fs${LSQ} emacs-fs ${Faint}# Create alias for emacs in fullscreen${Reset}\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} remove emacs                                           ${Faint}# Remove emacs alias${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Information${Reset}:\n"
    printf "  ${Normal}Aliases File${Reset}: ${DSQ}${Faint}${SymDirectory}/${SymFile}${Reset}${DSQ}\n"
    printf "\n"
  }

  # Info
  #
  Info() {
    local -r Arrow="${Faint}›${Reset} "
    local -r Seperator="${Faint}-${Reset}"
    local -r DSQ="${Normal}${FgBlack}'${Reset}"

    printf "\n"
    printf "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset} ${Bold}${Faint}Create Shell aliases for Flatpaks so you can easily launch them from your shell.${Reset}\n"
    printf "\n"
    printf "${Faint}Copyright (C) 2021 - ${CURRENT_YEAR} Xeriab Nabil (aka KodeBurner) $(FormatLink "<xeriab@tuta.io>" "<xeriab@tuta.io>")${Reset}\n"
    printf "\n"
    printf "${Bold}${FgWhite}Information${Reset}:\n"
    printf " ${Arrow}${Normal}Aliases File${Reset}: ${DSQ}${Faint}${SymDirectory}/${SymFile}${Reset}${DSQ}\n"
    printf "\n"
  }

  # ArgumentsParser
  #
  ArgumentsParser() {
    # shellcheck disable=SC2034
    local Argument=""

    # shellcheck disable=SC2034
    local VerboseOptionsHolder=""

    # shellcheck disable=SC2034
    local VerboseOpts=""

    # shellcheck disable=SC2034
    local Option=

    # shellcheck disable=SC2034
    local OptionArgument=

    [ $# -eq 0 ] && {
      Usage
      ExitStatus=$(($# > 1))
      return $ExitStatus
    }

    while [ $# -gt 0 ]; do
      case $1 in
      #
      # Common
      #

      #
      #
      -- | -)
        Exitable=False
        ExitStatus=0
        shift
        Arguments="${Arguments} ${*}"
        Arguments=$(StrNormalize "$Arguments")
        # Log -d "Arguments: '$Arguments'"
        break
        ;;

      #
      # Commands
      #

      # Add
      #
      # -a | --add | -add | add)
      add)
        Exitable=False
        Mode='add'
        PreviousCommand="${1}"
        shift
        ;;

      # List
      #
      # -l | --list | -list | list)
      list)
        Exitable=False
        Mode='list'
        shift
        ;;

      # Remove
      #
      # -r | --remove | -remove | remove)
      remove)
        Exitable=False
        Mode='remove'
        Target="${2}"
        shift
        ;;

      #
      # Options/Flags
      #

      # Command
      #
      -c=* | --command=* | -command=*)
        Exitable=False

        if [ -n "${PreviousCommand}" ] || [ "${PreviousCommand}" = "add" ]; then
          Command="${2}"
          Target="${3}"
        else
          Log -e "You cannot use '${Bold}${1}${Reset}' option outside of the '${Bold}add${Reset}' command." 1>&2
          Exitable=True
          ExitStatus=2
          break
          return $ExitStatus
        fi

        shift 2
        ;;

      # Command
      #
      -c | --command | -command)
        Exitable=False

        if [ -n "${PreviousCommand}" ] || [ "${PreviousCommand}" = "add" ]; then
          Command="${2}"
          Target="${3}"
        else
          Log -e "You cannot use '${Bold}${1}${Reset}' option outside of the '${Bold}add${Reset}' command." 1>&2
          Exitable=True
          ExitStatus=2
          break
          return $ExitStatus
        fi

        shift 2
        ;;

      # Verbose
      #
      -v | --verbose | -verbose)
        # shellcheck disable=SC2034
        FunctionDebug=True
        Verbose=True
        Exitable=False
        VerboseOptions="-v ${VerboseOptions}"
        # shellcheck disable=SC2034
        VerboseOpts=$VerboseOptions
        shift
        ;;

      # Help
      #
      -h | --help | -help)
        Usage
        Exitable=True
        ExitStatus=$(($# > 1))
        return $ExitStatus
        ;;

      # Info
      #
      -i | --info | -info)
        Info
        Exitable=True
        ExitStatus=$(($# > 1))
        return $ExitStatus
        ;;

      # Version
      #
      -V | --version | -version)
        Log -r "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset}\n"
        Exitable=True
        ExitStatus=0
        return $ExitStatus
        ;;

      # ANSI
      #
      --ansi | -ansi)
        Exitable=False
        ExitStatus=0
        # NoAnsi=False
        # shellcheck disable=SC2034
        # DISABLE_CONSOLE_OUTPUT_COLORS=False
        # DISABLE_CONSOLE_OUTPUT_STYLES=False
        SetupAnsiColors
        SetupAnsiStyles
        shift
        ;;

      # No ANSI
      #
      --no-ansi | -no-ansi)
        Exitable=False
        ExitStatus=0
        # NoAnsi=True
        # shellcheck disable=SC2034
        # DISABLE_CONSOLE_OUTPUT_COLORS=True
        # DISABLE_CONSOLE_OUTPUT_STYLES=True
        DisableAnsiColors
        DisableAnsiStyles
        shift
        ;;

      #
      # Misc
      #

      #
      #
      --* | -*)
        Log -e "Unrecognized option(s)${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}" 1>&2
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      # Bad short option
      #
      \?)
        Log -e "Unrecognized option(s)${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}" 1>&2
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      #
      #
      *)
        ExitStatus=0
        Exitable=False
        Target="${1:-}"
        [ $# -gt 0 ] && shift
        # break
        ;;
      esac
    done

    set -- "${Arguments}"

    # shellcheck disable=SC2034
    AdditionalArguments=$Arguments
  }

  ! (IsInteractive) && {
    Log -e "Cannot run in non-interactive shell." 1>&2
    return 1
  }

  #
  #
  ArgumentsParser "$@"

  [ "$FunctionDebug" = True ] || [ "$FunctionDebug" = 1 ] && Verbose=True

  # This is where the aliases lives
  if [ ! -d "$SymDirectory" ]; then
    command mkdir "${SymDirectory}"
    command touch "${SymDirectory}"/"${SymFile}"
  fi

  # SourceAliases
  #
  SourceAliases() {
    printf "\n"
    printf "${Faint}${FgBlue}›${Reset} ${Bold}${Faint}${FgWhite}Run the following command to update your shell${Reset}:\n"
    printf "${Faint}${FgWhite}. ${SymDirectory}/${SymFile}\n"
    printf "\n"

    # shellcheck disable=SC1012
    if (IsBash); then
      [ "$Verbose" = True ] && printf "${Faint}${FgBlue}›${Reset} ${Bold}${Faint}${FgWhite}To make this persisntent, add the following code to your '~/.bashrc' file${Reset}:\n"
      [ "$Verbose" = True ] && printf "\n"
      [ "$Verbose" = True ] && printf "${Faint}${Normal}if [ -d ~/.bashrc.d ]; then${Reset}\n"
      [ "$Verbose" = True ] && printf "  ${Faint}${Normal}for rc in ~/.bashrc.d/*.sh; do${Reset}\n"
      [ "$Verbose" = True ] && printf "    ${Faint}${Normal}if [ -f "$\rc" ]; then${Reset}\n"
      [ "$Verbose" = True ] && printf "      ${Faint}${Normal}# shellcheck disable=SC1090${Reset}\n"
      [ "$Verbose" = True ] && printf "      ${Faint}${Normal}. "$\rc"${Reset}\n"
      [ "$Verbose" = True ] && printf "    ${Faint}${Normal}fi${Reset}\n"
      [ "$Verbose" = True ] && printf "  ${Faint}${Normal}done${Reset}\n"
      [ "$Verbose" = True ] && printf "${Faint}${Normal}fi${Reset}\n"
      [ "$Verbose" = True ] && printf "${Faint}${Normal}unset rc${Reset}\n"
      [ "$Verbose" = True ] && printf "\n"
    else
      [ "$Verbose" = True ] && printf "${Faint}${FgBlue}›${Reset} ${Bold}${Faint}${FgWhite}To make this persisntent, add the following code to your '~/.zsh_aliases' file${Reset}:\n"
      [ "$Verbose" = True ] && printf "\n"
      [ "$Verbose" = True ] && printf "${Faint}${Normal}if [ -d ~/.local/share/shellscripts/aliases ]; then${Reset}\n"
      [ "$Verbose" = True ] && printf "  ${Faint}${Normal}for rc in ~/.local/share/shellscripts/aliases/*.sh; do${Reset}\n"
      [ "$Verbose" = True ] && printf "    ${Faint}${Normal}if [ -f "$\rc" ]; then${Reset}\n"
      [ "$Verbose" = True ] && printf "      ${Faint}${Normal}# shellcheck disable=SC1090${Reset}\n"
      [ "$Verbose" = True ] && printf "      ${Faint}${Normal}. "$\rc"${Reset}\n"
      [ "$Verbose" = True ] && printf "    ${Faint}${Normal}fi${Reset}\n"
      [ "$Verbose" = True ] && printf "  ${Faint}${Normal}done${Reset}\n"
      [ "$Verbose" = True ] && printf "${Faint}${Normal}fi${Reset}\n"
      [ "$Verbose" = True ] && printf "${Faint}${Normal}unset rc${Reset}\n"
      [ "$Verbose" = True ] && printf "\n"
    fi
  }

  # +------------------+-----------------------------------------------------------+
  # |       Alias      |                         Command                           |
  # +------------------+-----------------------------------------------------------+
  # |                  |                                                           |
  # +------------------+-----------------------------------------------------------+

  # ListAliases
  #
  ListAliases() {
    local AliasName=
    local AliasCommand=
    local FileContent=

    if (IsFile "${SymDirectory}/${SymFile}"); then
      FileContent=$(command cat "${SymDirectory}/${SymFile}")
      FileContent=$(printf "$FileContent" | command grep -o '^[^#]*')
    fi

    printf '%s\n' "$FileContent" | while IFS="$(printf '\n')" read -r Line; do
      if (StrHasPrefix "$Line" "alias"); then
        AliasName=$(printf "$Line" | $AWK_COMMAND -F'=' "{ print \$1 }" | $AWK_COMMAND -F' ' "{ print \$NF }")
        AliasCommand=$(printf "$Line" | $AWK_COMMAND -F'=' "{ print \$2 }" | command cut -d "'" -f2)
        Log -r "${FgBrightGreen}alias${Reset} ${FgCyan}${AliasName}${Reset}${Faint}=${Reset}${Faint}'${Reset}${FgWhite}${AliasCommand}${Reset}${Faint}'${Reset}\n"
      fi
    done

    unset Line
    unset AliasName
    unset AliasCommand
    unset FileContent

    return 0
  }

  # AddAlias
  #
  AddAlias() {
    command grep "alias ${Target}\=" "${SymDirectory}"/"${SymFile}" && i=1

    [ "$Verbose" = True ] && printf "${Faint}${FgRed}›${Reset} ${Normal}${FgBrightRed}${i}\n"

    # shellcheck disable=SC2210
    if [ $i ] >0; then
      printf "${Faint}${FgYellow}›${Reset} ${Normal}${FgBrightYellow}Alias for '${Target}' already exists${Reset}:\n"
      command grep "alias ${Target}=" "${SymDirectory}"/"${SymFile}"
      Die
    else
      printf "alias ${Target}='${Command}'" >>"${SymDirectory}"/"${SymFile}"

      [ "$Verbose" = True ] && {
        printf "${Faint}${FgBrightYellow}›${Reset} ${Normal}${FgBrightGreen}Alias for '${Target}' added${Reset}\n"
      }

      SourceAliases
    fi

    unset i
  }

  # RemoveAlias
  #
  RemoveAlias() {
    printf "${Faint}${RED}›${Reset} ${Normal}${LIGHT_RED}Removing '${Target}' alias.${Reset}\n"
    command sed -i "/alias ${Target}\=/d" "${SymDirectory}"/"${SymFile}"
    SourceAliases
  }

  # [ $Verbose ] && Log -v "Verbose Count: $(StrCountWords "$VerboseOptions")"

  if [ $# -gt 0 ] && [ "$Exitable" = False ] || [ "$Exitable" = 1 ]; then
    case $Mode in
    add)
      AddAlias
      ;;
    list)
      ListAliases
      return
      ;;
    remove)
      RemoveAlias
      ;;
    *)
      # Log -w "You must specify a command <list|add|remove>"
      ExitStatus=$((2))
      return
      ;;
    esac

    # [ $Mode ] || [ $Mode = 'add' ] && AddAlias || return
    # [ $Mode ] || [ $Mode = 'list' ] && ListAliases; return || return
    # [ $Mode ] || [ $Mode = 'remove' ] && RemoveAlias || return

    printf "${Faint}${FgBrightYellow}›${Reset} ${Normal}${FgBrightGreen}${Bold}Aliasing completed!${Reset}\n"
  fi

  unset Self
  unset TEMP_DIR
  unset OUT_FILE

  return $ExitStatus
}

# region Aliases
#

alias pakrat=Pakrat

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
