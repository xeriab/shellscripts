#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154 disable=SC2181
#
# ./functions/common/perms.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# setrex
#
# Remove executable bit recursively from files not directories
#
setrex() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  Self='setrex'

  chmod -R -x+X .

  unset Self
}

# set644
#
# Set all files' permissions to 644 recursively in a directory
#
set644() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  Self='set644'

  find "${@:-.}" -type f ! -perm 644 -print0 | xargs -0 chmod 644

  unset Self
}

# set755
#
# Set all directories' permissions to 755 recursively in a directory
#
set755() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  Self='set755'

  find "${@:-.}" -type d ! -perm 755 -print0 | xargs -0 chmod 755

  unset Self
}

# set775
#
# Set all directories' permissions to 775 recursively in a directory
#
set775() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  Self='set775'

  find "${@:-.}" -type d ! -perm 775 -print0 | xargs -0 chmod 775

  unset Self
}

# fixperms - fix permissions on files and directories, with confirmation
# Returns 0 on success, nonzero if any errors occurred
#
FixPerms() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='fixperms'

  # shellcheck disable=SC2034
  TEMP_DIR="/tmp/${Self}-tmp"

  # shellcheck disable=SC2034
  OUT_FILE="${TEMP_DIR}/script.log"

  # shellcheck disable=SC2034
  local -r Date="(12/03/2020)"

  # shellcheck disable=SC2034
  local -r Version="0.1.0"

  # shellcheck disable=SC2034
  local Exitable=False

  # shellcheck disable=SC2034
  local FunctionDebug=False

  # shellcheck disable=SC2034
  local ExitStatus=0

  # shellcheck disable=SC2034
  local Exitable=False

  # shellcheck disable=SC2034
  local Verbose=

  # shellcheck disable=SC2034
  local AdditionalArguments=

  # shellcheck disable=SC2034
  local OptionIndex=

  # shellcheck disable=SC2034
  local ChmodOptions=

  # shellcheck disable=SC2034
  local SlowMode=False

  # shellcheck disable=SC2034
  local Target=

  # shellcheck disable=SC2034
  local DirPerms=755

  # shellcheck disable=SC2034
  local FilePerms=644

  # shellcheck disable=SC2034 disable=SC2155
  local NoAnsi=False

  # Usage
  #
  Usage() {
    local Arrow="${Faint}›${Reset} "
    local Seperator="${Faint}${FgBrightBlack}-${Reset}"
    local DSQ="${Normal}${FgBlack}'${Reset}"
    local LSQ="${Faint}'${Reset}"
    # local Pipe="${Faint}|${Reset}"

    # Enables verbose output (may be supplied multiple times).
    printf "\n"
    printf "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset} ${Bold}${Faint}Fix permissions on files and directories, with confirmation.${Reset}\n"
    printf "\n"
    printf "${Faint}Copyright (C) 2019 - ${CURRENT_YEAR} Xeriab Nabil (aka KodeBurner) $(FormatLink "<xeriab@tuta.io>" "<xeriab@tuta.io>")${Reset}\n"
    printf "\n"
    printf "${Bold}Usage:${Reset} ${FgGreen}${Self}${Reset} ${Faint}[${Reset}OPTIONS${Reset}${Faint}]${Reset} ${Faint}<${Reset}target${Reset}${Faint}>${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Available Options${Reset}:\n"
    printf "  ${Faint}-${Reset}${Normal}s${Reset} ${Faint}--${Reset}${Normal}${FgYellow}slow${Reset}     ${Seperator} ${Normal}Will use a slower but more robust mode, which is effective if${Reset}\n"
    printf "                  ${Normal}directories themselves have permissions that forbid you from${Reset}\n"
    printf "                  ${Normal}traversing them.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}v${Reset} ${Faint}--${Reset}${Normal}${FgYellow}verbose${Reset}  ${Seperator} ${Normal}Enable verbose output.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}V${Reset} ${Faint}--${Reset}${Normal}${FgYellow}version${Reset}  ${Seperator} ${Normal}Output version information and exit.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}h${Reset} ${Faint}--${Reset}${Normal}${FgYellow}help${Reset}     ${Seperator} ${Normal}Display this help message and exit.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}i${Reset} ${Faint}--${Reset}${Normal}${FgYellow}info${Reset}     ${Seperator} ${Normal}Display information and exit.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Additional Options${Reset}:\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}file-perms${Reset}  ${Seperator} ${Normal}File permissions to set. ${Faint}${FgBrightBlack}(${Reset}${Faint}${Bold}Default${Reset}${Reset}${Faint}${FgBrightBlack}:${Reset} ${FgBrightBlue}644${Reset}${Faint}${FgBrightBlack})${Reset}${Faint}.${Reset}\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}dir-perms${Reset}   ${Seperator} ${Normal}Directory permissions to set. ${Faint}${FgBrightBlack}(${Reset}${Faint}${Bold}Default${Reset}${Reset}${Faint}${FgBrightBlack}:${Reset} ${FgBrightBlue}755${Reset}${Faint}${FgBrightBlack})${Reset}${Faint}.${Reset}\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}ansi${Reset}        ${Seperator} ${Normal}Force ANSI output.${Reset}\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}no-ansi${Reset}     ${Seperator} ${Normal}Disable ANSI output.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Arguments${Reset}:\n"
    printf "  ${Normal}target${Reset}        ${Seperator} ${Normal}The path to the file or directory to change permissions on. If omitted,${Reset}\n"
    printf "                  ${Normal}the current directory is taken to be the target.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Examples${Reset}:\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} ${LSQ}.${LSQ}                                 ${Faint}# Example #1${Reset}\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} ${LSQ}/tmp/path/to/directory${LSQ}            ${Faint}# Example #2${Reset}\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} ${Faint}--${Reset}slow ${LSQ}/tmp/path/to/directory${LSQ}     ${Faint}# Example #3${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Information${Reset}:\n"
    printf "  ${Normal}Target${Reset}: ${DSQ}${Faint}${Target:-Not Specified!}${Reset}${DSQ}\n"
    printf "\n"
  }

  # Info
  #
  Info() {
    local -r Arrow="${Faint}›${Reset} "
    local -r Seperator="${Faint}-${Reset}"
    local -r DSQ="${Normal}${FgBlack}'${Reset}"

    printf "\n"
    printf "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset} ${Bold}${Faint}Fix permissions on files and directories, with confirmation.${Reset}\n"
    printf "\n"
    printf "${Faint}Copyright (C) 2019 - ${CURRENT_YEAR} Xeriab Nabil (aka KodeBurner) $(FormatLink "<xeriab@tuta.io>" "<xeriab@tuta.io>")${Reset}\n"
    printf "\n"
    printf "${Bold}${FgWhite}Information${Reset}:\n"
    printf " ${Arrow}${Normal}Target${Reset}: ${DSQ}${Faint}${Target:-Not Specified!}${Reset}${DSQ}\n"
    printf "\n"
  }

  # ArgumentsParser
  #
  ArgumentsParser() {
    # shellcheck disable=SC2034
    local Argument=""

    # shellcheck disable=SC2034
    local VerboseOptionsHolder=""

    # shellcheck disable=SC2034
    local VerboseOpts="-v"

    # shellcheck disable=SC2034
    local Option=

    # shellcheck disable=SC2034
    local OptionArgument=

    [ $# -eq 0 ] && {
      Usage
      ExitStatus=$(($# > 1))
      return $ExitStatus
    }

    while [ $# -gt 0 ]; do
      case $1 in
      #
      # Common
      #

      #
      #
      -- | -)
        Exitable=False
        ExitStatus=0
        shift
        Arguments="${Arguments} ${*}"
        Arguments=$(StrNormalize "$Arguments")
        # Log -d "Arguments: '$Arguments'"
        break
        ;;

      #
      # Options/Flags
      #

      # Slow
      #
      -s | --slow | -slow)
        SlowMode=True
        Exitable=False
        shift
        ;;

      # Verbose
      #
      -v | --verbose | -verbose)
        # shellcheck disable=SC2034
        FunctionDebug=True
        Verbose=True
        Exitable=False
        VerboseOpts="-v ${VerboseOpts}"
        ChmodOptions=$VerboseOpts
        shift
        ;;

      # Help
      #
      -h | --help | -help)
        Usage
        Exitable=True
        ExitStatus=$(($# > 1))
        return $ExitStatus
        ;;

      # Info
      #
      -i | --info | -info)
        Info
        Exitable=True
        ExitStatus=$(($# > 1))
        return $ExitStatus
        ;;

      # Version
      #
      -V | --version | -version)
        log "raw" "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset}\n"
        Exitable=True
        ExitStatus=0
        return $ExitStatus
        ;;

      # Directory Permissions
      #
      --dir-perms | -dir-perms | --directory-permissions | -directory-permissions)
        Exitable=False
        DirPerms="${1:-755}"
        shift
        ;;

      # File Permissions
      #
      --file-perms | -file-perms | --file-permissions | -file-permissions)
        Exitable=False
        FilePerms="${1:-644}"
        shift
        ;;

      # ANSI
      #
      --ansi | -ansi)
        Exitable=False
        ExitStatus=0
        # NoAnsi=False
        # shellcheck disable=SC2034
        # DISABLE_CONSOLE_OUTPUT_COLORS=False
        # DISABLE_CONSOLE_OUTPUT_STYLES=False
        SetupAnsiColors
        SetupAnsiStyles
        shift
        ;;

      # No ANSI
      #
      --no-ansi | -no-ansi)
        Exitable=False
        ExitStatus=0
        # NoAnsi=True
        # shellcheck disable=SC2034
        # DISABLE_CONSOLE_OUTPUT_COLORS=True
        # DISABLE_CONSOLE_OUTPUT_STYLES=True
        DisableAnsiColors
        DisableAnsiStyles
        shift
        ;;

      #
      # Misc
      #

      #
      #
      --* | -*)
        Log -e "Unrecognized option(s)${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}" 1>&2
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      # Bad short option
      #
      \?)
        Log -e "Unrecognized option(s)${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}" 1>&2
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      #
      #
      *)
        Exitable=False
        Target="${1:-.}"
        shift
        # break
        ;;
      esac
    done

    set -- "${Arguments}"

    # shellcheck disable=SC2034
    AdditionalArguments=$Arguments
  }

  ! (IsInteractive) && {
    Log -e "Cannot run in non-interactive shell." 1>&2
    return 1
  }

  #
  #
  ArgumentsParser "$@"

  # shellcheck disable=SC2034
  [ "$FunctionDebug" = True ] || [ "$FunctionDebug" = 1 ] && Verbose=True

  if [ $# -gt 0 ] && [ "$Exitable" = False ] || [ "$Exitable" = 1 ]; then
    [ "$Target" ] && [ "$Target" = '.' ] && Target=$(pwd) || Target=$(realpath "$Target")

    local TargetName="${Normal}${FgBrightBlack}'${Reset}${Normal}${FgWhite}${Target}${Reset}${Normal}${FgBrightBlack}'${Reset}"

    # Log -raw "${Normal}${Bold}Fixing permissions on${Reset} ${name}${Normal}${Bold}?${Reset}"
    # Log -raw "${Faint}${FgBlue}›${Reset} ${Normal}${FgWhite}${Bold}Fixing permissions on${Reset} ${TargetName}${Normal}${FgWhite}${Bold}.${Reset} "

    Question 'yes|no' "${Normal}${FgWhite}${Bold}Fixing permissions on${Reset} ${TargetName}${Normal}${FgWhite}${Bold}.${Reset} ${Normal}${FgWhite}${Bold}Proceed?${Reset}" 'Choice'

    # Check input
    #
    # shellcheck disable=SC2154
    case "$Choice" in
    # Yes
    "$EMPTY_STR" | "$NewLine" | 1 | [Yy][Ee][Ss] | [Yy])
      {
        if ! [ "$ChmodOptions" = "" ]; then
          ChmodOptions=$(StrReplace "${ChmodOptions}" "verbose" "v")
          ChmodOptions=$(StrTrimSpaces "$ChmodOptions")
        fi

        # Target=$(printf "'%s'" "$Target")

        # DirFind
        #
        DirFind() {
          command find "$Target" -type d ! -perm "$DirPerms"
        }

        # FileFind
        #
        FileFind() {
          command find "$Target" -type f ! -perm "$FilePerms"
        }

        if [ $SlowMode = True ] || [ $SlowMode = 1 ]; then
          # Process directories first so non-traversable ones are fixed as we go

          if (IsDir "${Target}"); then
            DirFind -exec chmod "$ChmodOptions" "$DirPerms" {} \;
            if ! [ $? = 0 ]; then ExitStatus=$?; fi
          fi

          if (IsFile "${Target}"); then
            FileFind -exec chmod "$ChmodOptions" "$FilePerms" {} \;
            if ! [ $? = 0 ]; then ExitStatus=$?; fi
          fi
        else
          if (IsDir "${Target}"); then
            DirFind -print0 | command xargs -0 chmod "$ChmodOptions" "$DirPerms" 1>/dev/null 2>&1
            if [ $? -ne 0 ]; then ExitStatus=$?; fi
          fi

          if (IsFile "${Target}"); then
            FileFind -print0 | command xargs -0 chmod "$ChmodOptions" "$FilePerms" 1>/dev/null 2>&1
            if [ $? -ne 0 ]; then ExitStatus=$?; fi
          fi
        fi

        # if (IsBash) || (IsZsh); then
        #   # shellcheck disable=SC3047
        #   trap 'ExitStatus=$?' ERR
        # else
        #   trap 'ExitStatus=$?' EXIT
        # fi

        if [ $ExitStatus = 0 ]; then
          Log -r "${Faint}${FgCyan}›${Reset} ${Normal}${FgBrightGreen}${Bold}Completed!${Reset}\n"
          ExitStatus=0
        else
          Log -e "Unknown error occurred!" >&2
          ExitStatus=2
        fi
      }
      ;;
    # User aborted
    0 | [Nn][Oo] | [Nn]) ExitStatus=1 ;;
    # User aborted
    *) ExitStatus=1 ;;
    esac
  fi

  unset Self TEMP_DIR OUT_FILE Choice

  return $ExitStatus
}

# region Aliases
#

alias fixperms=FixPerms

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
