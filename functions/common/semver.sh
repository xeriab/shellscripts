#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./functions/common/semver.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2020-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# SemVer
#
# Template Description.
#
SemVer() {
  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='semver'

  # shellcheck disable=SC2034
  TEMP_DIR="/tmp/${Self}-tmp"

  # shellcheck disable=SC2034
  OUT_FILE="${TEMP_DIR}/script.Log"

  # shellcheck disable=SC2034
  local -r Date="(12/12/2020)"

  # shellcheck disable=SC2034
  local -r Version="0.1.0"

  # shellcheck disable=SC2034
  local FunctionDebug=False

  # shellcheck disable=SC2034
  local ExitStatus=0

  # shellcheck disable=SC2034
  local Exitable=False

  # shellcheck disable=SC2034
  local Verbose=False

  # shellcheck disable=SC2034
  local VerboseOptions=

  # shellcheck disable=SC2034
  local Output=

  # shellcheck disable=SC2034
  local AdditionalArguments=

  # shellcheck disable=SC2034
  local OptionIndex=

  # shellcheck disable=SC2034 disable=SC2155
  local NoAnsi=False

  # Usage
  #
  Usage() {
    local Arrow="${Faint}›${Reset} "
    local Seperator="${Faint}${FgBrightBlack}-${Reset}"
    # local DSQ="${Normal}${FgBlack}'${Reset}"
    local LSQ="${Faint}'${Reset}"
    # local Pipe="${Faint}|${Reset}"

    # Enables verbose output (may be supplied multiple times).
    printf "\n"
    printf "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset} ${Bold}${Faint}Template Description.${Reset}\n"
    printf "\n"
    printf "${Faint}Copyright (C) 2021 - ${CURRENT_YEAR} Xeriab Nabil (aka KodeBurner) $(FormatLink "<xeriab@tuta.io>" "<xeriab@tuta.io>")${Reset}\n"
    printf "\n"
    printf "${Faint}Please keep in mind that your terminal must support the Code in order for you${Reset}\n"
    printf "${Faint}to see the effect properly.${Reset}\n"
    printf "\n"
    printf "${Bold}Usage:${Reset} ${FgGreen}${Self}${Reset} ${Faint}[${Reset}OPTIONS${Reset}${Faint}]${Reset} ${Faint}[${Reset}ARGUMENT${Reset}${Faint}]${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Available Options${Reset}:\n"
    printf "  ${Faint}-${Reset}${Normal}v${Reset} ${Faint}--${Reset}${Normal}${FgYellow}verbose${Reset}    ${Seperator} ${Normal}Enable verbose output.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}V${Reset} ${Faint}--${Reset}${Normal}${FgYellow}version${Reset}    ${Seperator} ${Normal}Output version information and exit.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}h${Reset} ${Faint}--${Reset}${Normal}${FgYellow}help${Reset}       ${Seperator} ${Normal}Display this help message and exit.${Reset}\n"
    printf "  ${Faint}-${Reset}${Normal}i${Reset} ${Faint}--${Reset}${Normal}${FgYellow}info${Reset}       ${Seperator} ${Normal}Display information and exit.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Additional Options${Reset}:\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}ansi${Reset}         ${Seperator} ${Normal}Force ANSI output.${Reset}\n"
    printf "  ${Faint}--${Reset}${Normal}${FgYellow}no-ansi${Reset}      ${Seperator} ${Normal}Disable ANSI output.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Arguments${Reset}:\n"
    printf "  ${Normal}output${Reset}          ${Seperator} ${Normal}Text to output.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Examples${Reset}:\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} ${Faint}--${Reset}test           ${Faint}# Test option.${Reset}\n"
    printf "  ${Bold}${FgGreen}${Self}${Reset} ${Faint}--${Reset}test${Faint}-${Reset}option    ${Faint}# Test option.${Reset}\n"
    printf "\n"
    printf "${Arrow}${Bold}${FgWhite}Information${Reset}:\n"
    printf "  ${Normal}Information Variable${Reset}: ${DSQ}${Faint}${InfoVar:-Not Specified!}${Reset}${DSQ}\n"
    printf "\n"
  }

  # Info
  #
  Info() {
    local -r Arrow="${Faint}›${Reset} "
    local -r Seperator="${Faint}-${Reset}"
    local -r DSQ="${Normal}${FgBlack}'${Reset}"

    printf "\n"
    printf "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset} ${Bold}${Faint}Template Description.${Reset}\n"
    printf "\n"
    printf "${Faint}Copyright (C) 2021 - ${CURRENT_YEAR} Xeriab Nabil (aka KodeBurner) $(FormatLink "<xeriab@tuta.io>" "<xeriab@tuta.io>")${Reset}\n"
    printf "\n"
    printf "${Bold}${FgWhite}Information${Reset}:\n"
    printf " ${Arrow}${Normal}Information Variable${Reset}: ${DSQ}${Faint}${InfoVar:-Not Specified!}${Reset}${DSQ}\n"
    printf "\n"
  }

  # ArgumentsParser
  #
  ArgumentsParser() {
    # shellcheck disable=SC2034
    local Argument=""

    # shellcheck disable=SC2034
    local VerboseOptionsHolder=""

    # shellcheck disable=SC2034
    local VerboseOpts=""

    # shellcheck disable=SC2034
    local Option=

    # shellcheck disable=SC2034
    local OptionArgument=

    [ $# -eq 0 ] && {
      Usage
      ExitStatus=$(($# > 1))
      return $ExitStatus
    }

    while [ $# -gt 0 ]; do
      case $1 in
      #
      # Common
      #

      #
      #
      -- | -)
        Exitable=False
        ExitStatus=0
        shift
        Arguments="${Arguments} ${*}"
        Arguments=$(StrNormalize "$Arguments")
        # Log -d "Arguments: '$Arguments'"
        break
        ;;

      # Help
      #
      -h | --help | -help)
        Usage
        Exitable=True
        ExitStatus=$(($# > 1))
        return $ExitStatus
        ;;

      # Info
      #
      -i | --info | -info)
        Info
        Exitable=True
        ExitStatus=$(($# > 1))
        return $ExitStatus
        ;;

      # ANSI
      #
      --ansi | -ansi)
        Exitable=False
        ExitStatus=0
        # NoAnsi=False
        # shellcheck disable=SC2034
        # DISABLE_CONSOLE_OUTPUT_COLORS=False
        # DISABLE_CONSOLE_OUTPUT_STYLES=False
        SetupAnsiColors
        SetupAnsiStyles
        shift
        ;;

      # No ANSI
      #
      --no-ansi | -no-ansi)
        Exitable=False
        ExitStatus=0
        # NoAnsi=True
        # shellcheck disable=SC2034
        # DISABLE_CONSOLE_OUTPUT_COLORS=True
        # DISABLE_CONSOLE_OUTPUT_STYLES=True
        DisableAnsiColors
        DisableAnsiStyles
        shift
        ;;

      # Verbose
      #
      -v | --verbose | -verbose)
        # shellcheck disable=SC2034
        Exitable=False
        FunctionDebug=True
        # Verbose=True
        VerboseOptionsHolder="-v ${VerboseOptionsHolder}"
        # shellcheck disable=SC2034
        VerboseOptions=$VerboseOptionsHolder
        shift
        ;;

      # Version
      #
      -V | --version | -version)
        Log -r "${Bold}${Self}${Reset} ${FgYellow}${Bold}${Version}${Reset} ${Faint}${FgYellow}${Date}${Reset}\n"
        Exitable=True
        ExitStatus=0
        return $ExitStatus
        ;;

      #
      # Misc
      #

      #
      #
      --* | -*)
        Log -e "Unrecognized option(s)${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}" 1>&2
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      # Bad short option
      #
      \?)
        Log -e "Unrecognized option(s)${Faint}:${Reset} ${Faint}'${Reset}${Bold}${1}${Reset}${Faint}'${Reset}" 1>&2
        Exitable=True
        ExitStatus=2
        return $ExitStatus
        # break
        ;;

      #
      #
      *)
        Exitable=False
        ExitStatus=0
        Output="${1:-}"
        [ $# -gt 0 ] && shift
        ;;
      esac
    done

    set -- "${Arguments}"

    # shellcheck disable=SC2034
    AdditionalArguments=$Arguments
  }

  ! (IsInteractive) && {
    Log -e "Cannot run in non-interactive shell." 1>&2
    return 1
  }

  #
  #
  ArgumentsParser "$@"

  [ "$FunctionDebug" = True ] || [ "$FunctionDebug" = 1 ] && Verbose=True

  if [ $# -gt 0 ] && [ "$Exitable" = False ] || [ "$Exitable" = 0 ]; then
    Log -v "Output: $Output"
  fi

  unset Self
  unset TEMP_DIR
  unset OUT_FILE

  return $ExitStatus
}

# region Aliases
#

alias semver=SemVer

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
