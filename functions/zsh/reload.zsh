#!/usr/bin/env zsh
#
# shellcheck disable=SC3043
#
# ./functions/zsh/reload.zsh
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# ZshReload
#
# Reload the ZSH session with just a few keystrokes.
#
ZshReload() {
  # shellcheck disable=SC2034
  SHOW_LOG_ENABLE_FULL_PATH=False

  # shellcheck disable=SC2034
  SHOW_LOG_ENABLE_FILE_NAME=False

  # shellcheck disable=SC1090 disable=SC1091
  . "$XDG_DATA_HOME/shellscripts/lib/stdlib.sh"

  # shellcheck disable=SC2034
  Self='reload'

  local Name=
  local CacheDir=
  local CacheFile=
  local ZshResFile=
  local -r Cache=${ZSH_CACHE_DIR:-}
  local Shell=${ZSH_ARGZERO:-}
  local ExitStatus=0

  Name="${Faint}'${Reset}${FgBrightBlue}${Bold}${CURRENT_SHELL}${Reset}${Faint}'${Reset}"

  Question 'yes|no' "${Bold}Do you really want to reload ${Name}${Normal}${Bold}?${Reset}" 'Choice'

  # Check input
  #
  # shellcheck disable=SC2154
  case "$Choice" in
  # Yes
  "$EMPTY_STR" | "$NewLine" | 1 | [Yy][Ee][Ss] | [Yy])
    {
      CacheDir="${Cache:-${HOME}/.cache/shell}"
      CacheFile="${CacheDir}/zcomp-${HOST}"
      ZshResFile="${ZDOTDIR:-~}/.zshrc"

      # zrecompile -p -R ~/.zshrc -- -M ~/.zcompdump -- ~/zsh/comp.zwc ~/zsh/completion/*/_*

      if [ -z "$skip_global_compinit" ]; then
        # autoload -Uz compinit zrecompile
        # compinit -C -d "$HOME"/.cache/shell/zcompdump
        [ -f "${HOME}"/.zshrc.zwc ] && {
          # printf "%s\n" "$HOME"/.zshrc.zwc
          rm -rf "${HOME}"/.zshrc.zwc
        }

        autoload -Uz compinit zrecompile
        compinit -i -d "${CacheFile}"
      else
        [ -f "${HOME}"/.zshrc.zwc ] && {
          # printf "%s\n" "$HOME"/.zshrc.zwc
          rm -rf "${HOME}"/.zshrc.zwc
        }

        autoload -U compinit zrecompile
        compinit -C -d "${CacheFile}"
      fi

      for Each in $ZshResFile $CacheFile; do
        # [ -f "$HOME"/.zshrc.zwc ] && command rm -rf "${HOME}"/.zshrc.zwc
        # zrecompile -p "${Each}" && command rm -f "${Each}.zwc.old"
        if IsFile "$Each.zwc"; then
          # printf "%s\n" "$Each.zwc"
          zrecompile -p -R $Each -- && rm -f "$Each.zwc.old"
        fi
        # zrecompile -p -R "$Each" -- && command rm -f "$Each.zwc.old"
      done

      unset Each

      # autoload -U compinit zrecompile
      # compinit -i -d "$Cache/zcomp-$HOST"

      # for f in ${ZDOTDIR:-~}/.zshrc "$Cache/zcomp-$HOST"; do
      # 	zrecompile -p $f && command rm -f $f.zwc.old
      # done

      # Use $SHELL if it's available and a ZSH Shell
      (IsZsh) && Shell="${SHELL}"

      [ "$1" = "-v" ] && Log -i "The ${Name} shell is now reloaded!"

      # Use $SHELL if available; remove leading dash if login Shell
      # [ -n "$SHELL" ] && exec "${SHELL#-}" || exec zsh

      # Remove leading dash if login Shell and run accordingly
      if [ "${Shell:0:1}" = "-" ]; then
        exec -l "${Shell#-}"
      else
        exec "$Shell"
      fi

      ExitStatus=0
    }
  ;;
  # User aborted
  0 | [Nn][Oo] | [Nn]) ExitStatus=1 ;;
  # User aborted
  *) ExitStatus=1 ;;
  esac

  unset Self Name Choice CacheDir CacheFile ZshResFile Shell

  return $ExitStatus
}

# region Aliases
#

alias reload=ZshReload

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
