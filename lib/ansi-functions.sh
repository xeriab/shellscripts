#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2155
#
# ./lib/ansi-functions.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2021-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/ansi.sh"

SetupAnsi

#
# endregion Imports

# region ANSI Functions
#

AnsiColorCodes() {
  local Code i j

  printf 'Standard: '
  AnsiBold
  AnsiWhite

  for Code in 0 1 2 3 4 5 6 7; do
    if [ "$Code" = 7 ]; then
      AnsiBlack
    fi
    AnsiColorCodePatch "$Code"
  done

  AnsiResetForeground
  AnsiNormal
  printf '\nIntense:  '
  AnsiWhite

  for Code in 8 9 10 11 12 13 14 15; do
    if [ "$Code" = 9 ]; then
      AnsiBlack
    fi
    AnsiColorCodePatch "$Code"
  done

  AnsiResetForeground
  printf '\n\n'

  # for i in 16 22 28 34 40 46; do
  for i in 16 22 28; do
    for j in $i $((i + 36)) $((i + 72)) $((i + 108)) $((i + 144)) $((i + 180)); do
      AnsiWhite
      AnsiBold

      for Code in $j $((j + 1)) $((j + 2)) $((j + 3)) $((j + 4)) $((j + 5)); do
        AnsiColorCodePatch "$Code"
      done

      AnsiNormal
      AnsiResetForeground
      printf '    '
      AnsiBlack

      for Code in $((j + 18)) $((j + 19)) $((j + 20)) $((j + 21)) $((j + 22)) $((j + 23)); do
        AnsiColorCodePatch "$Code"
      done

      AnsiResetForeground
      printf '\n'
    done

    printf '\n'
  done

  printf 'Grays:    '
  AnsiBold
  AnsiWhite

  for Code in 232 233 234 235 236 237 238 239 240 241 242 243; do
    AnsiColorCodePatch "$Code"
  done

  AnsiResetForeground
  AnsiNormal
  printf '\n          '
  AnsiBlack

  for Code in 244 245 246 247 248 249 250 251 252 253 254 255; do
    AnsiColorCodePatch "$Code"
  done

  AnsiResetForeground
  printf '\n'
}

AnsiColorCodePatch() {
  AnsiBgColor "$1"
  printf ' %3s ' "$1"
  AnsiResetBackground
}

AnsiColorTable() {
  local colorLabel Counter fnbLower fnbUpper functionName IFS resetFunction

  fnbLower="$(
    AnsiFaint
    printf f
    AnsiNormal
    printf n
    AnsiBold
    printf b
    AnsiNormal
  )"
  fnbUpper="$(
    AnsiFaint
    printf F
    AnsiNormal
    printf N
    AnsiBold
    printf B
    AnsiNormal
  )"
  IFS=$(printf ' \n')
  Counter=

  while read -r colorLabel functionName resetFunction; do
    printf -- '--%s ' "$colorLabel"
    $functionName
    printf 'Sample'
    $resetFunction

    if [ "$Counter" = "x" ]; then
      Counter=
      printf '\n'
    else
      Counter=x
      AnsiColumn 40
    fi
  done <<END
bold AnsiBold AnsiNormal
faint AnsiFaint AnsiNormal
italic AnsiItalic AnsiPlain
fraktur AnsiFraktur AnsiPlain
underline AnsiUnderline AnsiNoUnderline
double-underline AnsiDoubleUnderline AnsiNoUnderline
blink AnsiBlink AnsiNoBlink
rapid-blink AnsiRapidBlink AnsiNoBlink
inverse AnsiInverse AnsiNoInverse
invisible AnsiInvisible AnsiVisible
strike AnsiStrike AnsiNoStrike
frame AnsiFrame AnsiNoBorder
encircle AnsiEncircle AnsiNoBorder
overline AnsiOverline AnsiNoOverline
ideogram-right AnsiIdeogramRight AnsiResetIdeogram
ideogram-right-double AnsiIdeogramRightDouble AnsiResetIdeogram
ideogram-left AnsiIdeogramLeft AnsiResetIdeogram
ideogram-left-double AnsiIdeogramLeftDouble AnsiResetIdeogram
ideogram-stress AnsiIdeogramStress AnsiResetIdeogram
END

  if [ -n "$Counter" ]; then
    printf '\n'
  fi
  printf '\n'
  printf '            black   red   green  yellow  blue  magenta cyan  white\n'
  AnsiColorTableLine "(none)" "AnsiResetBackground" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "bg-black" "AnsiBgBlack" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "+ intense" "AnsiBgBlackIntense" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "bg-red" "AnsiBgRed" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "+ intense" "AnsiBgRedIntense" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "bg-green" "AnsiBgGreen" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "+ intense" "AnsiBgGreenIntense" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "bg-yellow" "AnsiBgYellow" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "+ intense" "AnsiBgYellowIntense" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "bg-blue" "AnsiBgBlue" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "+ intense" "AnsiBgBlueIntense" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "bg-magenta" "AnsiBgMagenta" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "+ intense" "AnsiBgMagentaIntense" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "bg-cyan" "AnsiBgCyan" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "+ intense" "AnsiBgCyanIntense" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "bg-white" "AnsiBgWhite" "$fnbLower" "$fnbUpper"
  AnsiColorTableLine "+ intense" "AnsiBgWhiteIntense" "$fnbLower" "$fnbUpper"

  printf '\n'
  printf 'Legend:\n'
  printf '    Normal color:  f = faint, n = normal, b = bold.\n'
  printf '    Intense color:  F = faint, N = normal, B = bold.\n'
}

AnsiColorTableLine() {
  local Function

  printf '%-12s' "$1"
  for Function in AnsiBlack AnsiRed AnsiGreen AnsiYellow AnsiBlue AnsiMagenta AnsiCyan AnsiWhite; do
    $2
    ${Function}
    printf '%s' "$3"
    ${Function}Intense
    printf '%s' "$4"
    AnsiResetForeground
    AnsiResetBackground

    if ! [ "$Function" = "AnsiWhite" ]; then
      printf ' '
    fi
  done
  printf '\n'
}

AnsiIsAnsiSupported() {
  # Optionally override detection logic
  # to support post processors that interpret color codes _after_ output is generated.
  # Use environment variable "ANSI_FORCE_SUPPORT=<anything>" to enable the override.
  if [ -n "${ANSI_FORCE_SUPPORT-}" ]; then
    return 0
  fi

  if (hash tput 1>/dev/null 2>&1); then
    if [ "$(tput colors)" -lt 8 ]; then
      return 1
    fi

    return 0
  fi

  # Query the console and see if we get ANSI codes back.
  # CSI 0 c == CSI c == Primary Device Attributes.
  # Idea:  CSI c
  # Response = CSI ? 6 [234] ; 2 2 c
  # The "22" means ANSI color, but terminals don't need to send that back.
  # If we get anything back, let's assume it works.
  AnsiReport c "$AnsiCsi?" c || return 1

  [ -n "$AnsiReport" ]
}

AnsiReport() {
  local Buffer Char Report

  Report=""

  # Note: read bypass piping, which lets this work:
  # Ansi --report-window-chars | cut -d , -f 1
  read -p "${AnsiCsi}$1" -r -N "${#2}" -s -t 1 Buffer

  if [ "$Buffer" != "$2" ]; then
    return 1
  fi

  read -r -N "${#3}" -s -t 1 Buffer

  while ! [ "$Buffer" = "$3" ]; do
    # Report="$Report${Buffer:0:1}"
    Report="$Report$(printf '%c' "$Buffer")"
    read -r -N 1 -s -t 1 Char || exit 1
    Buffer="${Buffer:1}$Char"
  done

  AnsiReport=$Report
}

AnsiReportPosition() {
  AnsiReport 6n "$AnsiCsi" R || return 1
  printf '%s\n' "$(printf '%s' "$AnsiReport" | command sed -Ere 's~;~,~g')"
}

AnsiReportIcon() {
  AnsiReport 20t "${AnsiOsc}L" "$AnsiSt" || return 1
  printf '%s\n' "$(printf '%s' "$AnsiReport" | command sed -Ere 's~;~,~g')"
}

AnsiReportScreenChars() {
  AnsiReport 19t "${AnsiCsi}9;" t || return 1
  printf '%s\n' "$(printf '%s' "$AnsiReport" | command sed -Ere 's~;~,~g')"
}

AnsiReportTitle() {
  AnsiReport 21t "${AnsiOsc}l" "$AnsiSt" || return 1
  printf '%s\n' "$(printf '%s' "$AnsiReport" | command sed -Ere 's~;~,~g')"
}

AnsiReportWindowChars() {
  AnsiReport 18t "${AnsiCsi}8;" t || return 1
  printf '%s\n' "$(printf '%s' "$AnsiReport" | command sed -Ere 's~;~,~g')"
}

AnsiReportWindowPixels() {
  AnsiReport 14t "${AnsiCsi}4;" t || return 1
  printf '%s\n' "$(printf '%s' "$AnsiReport" | command sed -Ere 's~;~,~g')"
}

AnsiReportWindowPosition() {
  AnsiReport 13t "${AnsiCsi}3;" t || return 1
  printf '%s\n' "$(printf '%s' "$AnsiReport" | command sed -Ere 's~;~,~g')"
}

AnsiReportWindowState() {
  AnsiReport 11t "$AnsiCsi" t || return 1
  case "$AnsiReport" in
  1)
    printf 'open\n'
    ;;

  2)
    printf 'iconified\n'
    ;;

  *)
    printf 'unknown (%s)\n' "$AnsiReport"
    ;;
  esac
}

AnsiShowHelp() {
  command cat <<EOF
Generate ANSI escape codes

Please keep in mind that your terminal must support the Code in order for you
to see the effect properly.

Usage
    ansi [OPTIONS] [TEXT TO OUTPUT]

Option processing stops at the first unknown option or at "--".  Options
are applied in order as specified on the command line.  Unless --no-restore
is used, the options are unapplied in reverse order, restoring your
terminal to normal.

Optional parameters are surrounded in brackets and use reasonable defaults.
For instance, using --down will move the cursor down one line and --down=10
moves the cursor down 10 lines.

Display Manipulation
    --insert-chars[=N], --insert-char[=N], --ich[=N]
                             Insert blanks at cursor, shifting the line right.
    --erase-display[=N], --ed[=N]
                             Erase in display. 0=below, 1=above, 2=all,
                             3=saved.
    --erase-line=[N], --el[=N]
                             Erase in line. 0=right, 1=left, 2=all.
    --insert-lines[=N], --insert-line[=N], --il[=N]
    --delete-lines[=N], --delete-line[=N], --dl[=N]
    --delete-chars[=N], --delete-char[=N], --dch[=N]
    --scroll-up[=N], --su[=N]
    --scroll-down[=N], --sd[=N]
    --erase-chars[=N], --erase-char[=N], --ech[=N]
    --repeat[=N], --rep[=N]  Repeat preceding character N times.

Cursor:
    --up[=N], --cuu[=N]
    --down[=N], --cud[=N]
    --forward[=N], --cuf[=N]
    --backward[=N], --cub[=N]
    --next-line[=N], --cnl[=N]
    --previous-line[=N], --prev-line[=N], --cpl[=N]
    --column[=N], --cha[=N]
    --Position[=[ROW],[COL]], --cup[=[ROW],[=COL]]
    --tab-forward[=N]        Move forward N tab stops.
    --tab-backward[=N]       Move backward N tab stops.
    --column-relative[=N], --hpr[=N]
    --line[=N], --vpa[=N]
    --line-relative[=N], --vpr[=N]
    --save-cursor            Saves the cursor Position.  Restores the cursor
                             after writing text to the terminal unless
                             --no-restore is also used.
    --restore-cursor         Just restores the cursor.
    --hide-cursor            Will automatically show cursor unless --no-restore
                             is also used.
    --show-cursor

Text:
    Attributes:
        --bold, --faint, --normal
        --italic, --fraktur, --plain
        --underline, --double-underline, --no-underline
        --blink, --rapid-blink, --no-blink
        --inverse, --no-inverse
        --invisible, --visible
        --strike, --no-strike
        --frame, --encircle, --no-border
        --overline, --no-overline
        --ideogram-right, --ideogram-right-double, --ideogram-left,
        --ideogram-left-double, --ideogram-stress, --reset-ideogram
        --font=NUM (NUM must be from 0 through 9 and 0 is the primary font)
    Foreground:
        --black, --red, --green, --yellow, --blue, --magenta, --cyan, --white,
        --black-intense, --red-intense, --green-intense, --yellow-intense,
        --blue-intense, --magenta-intense, --cyan-intense, --white-intense,
        --color=CODE, --rgb=R,G,B
    Background:
        --bg-black, --bg-red, --bg-green, --bg-yellow, --bg-blue,
        --bg-magenta, --bg-cyan, --bg-white, --bg-black-intense,
        --bg-red-intense, --bg-green-intense, --bg-yellow-intense,
        --bg-blue-intense, --bg-magenta-intense, --bg-cyan-intense,
        --bg-white-intense, --bg-color=CODE, --bg-rgb=R,G,B
    Reset:
        --reset-attrib       Removes bold, italics, etc.
        --reset-foreground   Sets foreground to default color.
        --reset-background   Sets background to default color.
        --reset-color        Resets attributes, foreground, background.
        --reset-font         Switches back to the primary font.

Report:
    ** NOTE:  These require reading from stdin.  Results are sent to stdout.
    ** If no response from terminal in 1 second, these commands fail.
    --report-Position        ROW,COL
    --report-window-state    "open" or "iconified"
    --report-window-Position X,Y
    --report-window-pixels   HEIGHT,WIDTH
    --report-window-chars    ROWS,COLS
    --report-screen-chars    ROWS,COLS of the entire screen
    --report-icon
    --report-title

Miscellaneous:
    --color-table            Display a color table.
    --color-codes            Show the different color codes.
    --icon=NAME              Set the terminal's icon name.
    --title=TITLE            Set the terminal's window title.
    --no-restore             Do not issue reset codes when changing colors.
                             For example, if you change the color to green,
                             normally the color is restored to default
                             afterwards.  With this flag, the color will
                             stay green even when the command finishes.
    -n, --no-newline         Suppress newline at the end of the line.
    --bell                   Add the terminal's bell sequence to the output.
    --reset                  Reset colors, clear screen, show cursor, move
                             cursor to (1,1), and reset the font.
EOF
}

Ansi() {
  local addNewline b g m r readOptions restoreText restoreCursorPosition restoreCursorVisibility supported triggerRestore
  local m10 m22 m23 m24 m25 m27 m28 m29 m39 m49 m54 m55 m65

  addNewline=True
  m10=
  m22=
  m23=
  m24=
  m25=
  m27=
  m28=
  m29=
  m39=
  m49=
  m54=
  m55=
  m65=
  readOptions=True
  restoreCursorPosition=False
  restoreCursorVisibility=False
  restoreText=False
  supported=True
  triggerRestore=True

  if ! AnsiIsAnsiSupported; then
    supported=False
  fi

  while $readOptions && [ $# -gt 0 ]; do
    case "$1" in
    --help | -h | -\?)
      AnsiShowHelp
      return 0
      ;;

    # Display Manipulation
    --insert-chars | --insert-char | --ich)
      $supported && AnsiInsertChars
      ;;

    --insert-chars=* | insert-char=* | --ich=*)
      $supported && AnsiInsertChars "${1#*=}"
      ;;

    --erase-display | --ed)
      $supported && AnsiEraseDisplay
      ;;

    --erase-display=* | --ed=*)
      $supported && AnsiEraseDisplay "${1#*=}"
      ;;

    --erase-line | --el)
      $supported && AnsiEraseLine
      ;;

    --erase-line=* | --el=*)
      $supported && AnsiEraseLine "${1#*=}"
      ;;

    --insert-lines | --insert-line | --il)
      $supported && AnsiInsertLines
      ;;

    --insert-lines=* | --insert-line=* | --il=*)
      $supported && AnsiInsertLines "${1#*=}"
      ;;

    --delete-lines | --delete-line | --dl)
      $supported && AnsiDeleteLines
      ;;

    --delete-lines=* | --delete-line=* | --dl=*)
      $supported && AnsiDeleteLines "${1#*=}"
      ;;

    --delete-chars | --delete-char | --dch)
      $supported && AnsiDeleteChars
      ;;

    --delete-chars=* | --delete-char=* | --dch=*)
      $supported && AnsiDeleteChars "${1#*=}"
      ;;

    --scroll-up | --su)
      $supported && AnsiScrollUp
      ;;

    --scroll-up=* | --su=*)
      $supported && AnsiScrollUp "${1#*=}"
      ;;

    --scroll-down | --sd)
      $supported && AnsiScrollDown
      ;;

    --scroll-down=* | --sd=*)
      $supported && AnsiScrollDown "${1#*=}"
      ;;

    --erase-chars | --erase-char | --ech)
      $supported && AnsiEraseChars
      ;;

    --erase-chars=* | --erase-char=* | --ech=*)
      $supported && AnsiEraseChars "${1#*=}"
      ;;

    --repeat | --rep)
      $supported && AnsiRepeat
      ;;

    --repeat=* | --rep=N)
      $supported && AnsiRepeat "${1#*=}"
      ;;

    # Cursor Positioning
    --up | --cuu)
      $supported && AnsiUp
      ;;

    --up=* | --cuu=*)
      $supported && AnsiUp "${1#*=}"
      ;;

    --down | --cud)
      $supported && AnsiDown
      ;;

    --down=* | --cud=*)
      $supported && AnsiDown "${1#*=}"
      ;;

    --forward | --cuf)
      $supported && AnsiForward
      ;;

    --forward=* | --cuf=*)
      $supported && AnsiForward "${1#*=}"
      ;;

    --backward | --cub)
      $supported && AnsiBackward
      ;;

    --backward=* | --cub=*)
      $supported && AnsiBackward "${1#*=}"
      ;;

    --next-line | --cnl)
      $supported && AnsiNextLine
      ;;

    --next-line=* | --cnl=*)
      $supported && AnsiNextLine "${1#*=}"
      ;;

    --previous-line | --prev-line | --cpl)
      $supported && AnsiPreviousLine
      ;;

    --previous-line=* | --prev-line=* | --cpl=*)
      $supported && AnsiPreviousLine "${1#*=}"
      ;;

    --column | --cha)
      $supported && AnsiColumn
      ;;

    --column=* | --cha=*)
      $supported && AnsiColumn "${1#*=}"
      ;;

    --Position | --cup)
      $supported && AnsiPosition
      ;;

    --Position=* | --cup=*)
      $supported && AnsiPosition "${1#*=}"
      ;;

    --tab-forward | --cht)
      $supported && AnsiTabForward
      ;;

    --tab-forward=* | --cht=*)
      $supported && AnsiTabForward "${1#*=}"
      ;;

    --tab-backward | --cbt)
      $supported && AnsiTabBackward
      ;;

    --tab-backward=* | --cbt=*)
      $supported && AnsiTabBackward "${1#*=}"
      ;;

    --column-relative | --hpr)
      $supported && AnsiColumnRelative
      ;;

    --column-relative=* | --hpr=*)
      $supported && AnsiColumnRelative "${1#*=}"
      ;;

    --line | --vpa)
      $supported && AnsiLine
      ;;

    --line=* | --vpa=*)
      $supported && AnsiLine "${1#*=}"
      ;;

    --line-relative | --vpr)
      $supported && AnsiLineRelative
      ;;

    --line-relative=* | --vpr=*)
      $supported && AnsiLineRelative "${1#*=}"
      ;;

    --save-cursor)
      $supported && AnsiSaveCursor
      restoreCursorPosition=True
      ;;

    --restore-cursor)
      $supported && AnsiRestoreCursor
      ;;

    --hide-cursor)
      $supported && AnsiHideCursor
      restoreCursorVisibility=True
      ;;

    --show-cursor)
      $supported && AnsiShowCursor
      ;;

    # Colors - Attributes
    --bold)
      $supported && AnsiBold
      restoreText=True
      m22="22;"
      ;;

    --faint)
      $supported && AnsiFaint
      restoreText=True
      m22="22;"
      ;;

    --italic)
      $supported && AnsiItalic
      restoreText=True
      m23="23;"
      ;;

    --underline)
      $supported && AnsiUnderline
      restoreText=True
      m24="24;"
      ;;

    --blink)
      $supported && AnsiBlink
      restoreText=True
      m25="25;"
      ;;

    --rapid-blink)
      $supported && AnsiRapidBlink
      restoreText=True
      m25="25;"
      ;;

    --inverse)
      $supported && AnsiInverse
      restoreText=True
      m27="27;"
      ;;

    --invisible)
      $supported && AnsiInvisible
      restoreText=True
      m28="28;"
      ;;

    --strike)
      $supported && AnsiStrike
      restoreText=True
      m29="29;"
      ;;

    --font | --font=0)
      $supported && AnsiResetFont
      ;;

    --font=[123456789])
      $supported && AnsiFont "${1#*=}"
      restoreText=True
      m10="10;"
      ;;

    --fraktur)
      $supported && AnsiFraktur
      restoreText=True
      m23="23;"
      ;;

    --double-underline)
      $supported && AnsiDoubleUnderline
      restoreText=True
      m24="24;"
      ;;

    --normal)
      $supported && AnsiNormal
      ;;

    --plain)
      $supported && AnsiPlain
      ;;

    --no-underline)
      $supported && AnsiNoUnderline
      ;;

    --no-blink)
      $supported && AnsiNoBlink
      ;;

    --no-inverse)
      $supported && AnsiNoInverse
      ;;

    --visible)
      $supported && AnsiVisible
      ;;

    --no-strike)
      $supported && AnsiNoStrike
      ;;

    --frame)
      $supported && AnsiFrame
      restoreText=True
      m54="54;"
      ;;

    --encircle)
      $supported && AnsiEncircle
      restoreText=True
      m54="54;"
      ;;

    --overline)
      $supported && AnsiOverline
      restoreText=True
      m55="55;"
      ;;

    --no-border)
      $supported && AnsiNoBorder
      ;;

    --no-overline)
      $supported && AnsiNoOverline
      ;;

    # Colors - Foreground
    --black)
      $supported && AnsiBlack
      restoreText=True
      m39="39;"
      ;;

    --red)
      $supported && AnsiRed
      restoreText=True
      m39="39;"
      ;;

    --green)
      $supported && AnsiGreen
      restoreText=True
      m39="39;"
      ;;

    --yellow)
      $supported && AnsiYellow
      restoreText=True
      m39="39;"
      ;;

    --blue)
      $supported && AnsiBlue
      restoreText=True
      m39="39;"
      ;;

    --magenta)
      $supported && AnsiMagenta
      restoreText=True
      m39="39;"
      ;;

    --cyan)
      $supported && AnsiCyan
      restoreText=True
      m39="39;"
      ;;

    --white)
      $supported && AnsiWhite
      restoreText=True
      m39="39;"
      ;;

    --black-intense)
      $supported && AnsiBlackIntense
      restoreText=True
      m39="39;"
      ;;

    --red-intense)
      $supported && AnsiRedIntense
      restoreText=True
      m39="39;"
      ;;

    --green-intense)
      $supported && AnsiGreenIntense
      restoreText=True
      m39="39;"
      ;;

    --yellow-intense)
      $supported && AnsiYellowIntense
      restoreText=True
      m39="39;"
      ;;

    --blue-intense)
      $supported && AnsiBlueIntense
      restoreText=True
      m39="39;"
      ;;

    --magenta-intense)
      $supported && AnsiMagentaIntense
      restoreText=True
      m39="39;"
      ;;

    --cyan-intense)
      $supported && AnsiCyanIntense
      restoreText=True
      m39="39;"
      ;;

    --white-intense)
      $supported && AnsiWhiteIntense
      restoreText=True
      m39="39;"
      ;;

    --rgb=*,*,*)
      r=${1#*=}
      b=${r##*,}
      g=${r#*,}
      g=${g%,*}
      r=${r%%,*}
      $supported && AnsiRgb "$r" "$g" "$b"
      restoreText=True
      m39="39;"
      ;;

    --color=*)
      $supported && AnsiColor "${1#*=}"
      restoreText=True
      m39="39;"
      ;;

    # Colors - Background
    --bg-black)
      $supported && AnsiBgBlack
      restoreText=True
      m49="49;"
      ;;

    --bg-red)
      $supported && AnsiBgRed
      restoreText=True
      m49="49;"
      ;;

    --bg-green)
      $supported && AnsiBgGreen
      restoreText=True
      m49="49;"
      ;;

    --bg-yellow)
      $supported && AnsiBgYellow
      restoreText=True
      m49="49;"
      ;;

    --bg-blue)
      $supported && AnsiBgBlue
      restoreText=True
      m49="49;"
      ;;

    --bg-magenta)
      $supported && AnsiBgMagenta
      restoreText=True
      m49="49;"
      ;;

    --bg-cyan)
      $supported && AnsiBgCyan
      restoreText=True
      m49="49;"
      ;;

    --bg-white)
      $supported && AnsiBgWhite
      restoreText=True
      m49="49;"
      ;;

    --bg-black-intense)
      $supported && AnsiBgBlackIntense
      restoreText=True
      m49="49;"
      ;;

    --bg-red-intense)
      $supported && AnsiBgRedIntense
      restoreText=True
      m49="49;"
      ;;

    --bg-green-intense)
      $supported && AnsiBgGreenIntense
      restoreText=True
      m49="49;"
      ;;

    --bg-yellow-intense)
      $supported && AnsiBgYellowIntense
      restoreText=True
      m49="49;"
      ;;

    --bg-blue-intense)
      $supported && AnsiBgBlueIntense
      restoreText=True
      m49="49;"
      ;;

    --bg-magenta-intense)
      $supported && AnsiBgMagentaIntense
      restoreText=True
      m49="49;"
      ;;

    --bg-cyan-intense)
      $supported && AnsiBgCyanIntense
      restoreText=True
      m49="49;"
      ;;

    --bg-white-intense)
      $supported && AnsiBgWhiteIntense
      restoreText=True
      m49="49;"
      ;;

    --bg-rgb=*,*,*)
      r=${1#*=}
      b=${r##*,}
      g=${r#*,}
      g=${g%,*}
      r=${r%%,*}
      $supported && AnsiBgRgb "$r" "$g" "$b"
      restoreText=True
      m49="49;"
      ;;

    --bg-color=*)
      $supported && AnsiBgColor "${1#*=}"
      restoreText=True
      m49="49;"
      ;;

    # Colors - Reset
    --reset-attrib)
      $supported && AnsiResetAttributes
      ;;

    --reset-foreground)
      $supported && AnsiResetForeground
      ;;

    --reset-background)
      $supported && AnsiResetBackground
      ;;

    --reset-color)
      $supported && AnsiResetColor
      ;;

    --reset-font)
      $supported && AnsiResetFont
      ;;

    # Reporting
    --report-Position)
      $supported || return 1
      AnsiReportPosition || return $?
      ;;

    --report-window-state)
      $supported || return 1
      AnsiReportWindowState || return $?
      ;;

    --report-window-Position)
      $supported || return 1
      AnsiReportWindowPosition || return $?
      ;;

    --report-window-pixels)
      $supported || return 1
      AnsiReportWindowPixels || return $?
      ;;

    --report-window-chars)
      $supported || return 1
      AnsiReportWindowChars || return $?
      ;;

    --report-screen-chars)
      $supported || return 1
      AnsiReportScreenChars || return $?
      ;;

    --report-icon)
      $supported || return 1
      AnsiReportIcon || return $?
      ;;

    --report-title)
      $supported || return 1
      AnsiReportTitle || return $?
      ;;

    --ideogram-right)
      $supported && AnsiIdeogramRight
      restoreText=True
      m65="65;"
      ;;

    --ideogram-right-double)
      $supported && AnsiIdeogramRightDouble
      restoreText=True
      m65="65;"
      ;;

    --ideogram-left)
      $supported && AnsiIdeogramLeft
      restoreText=True
      m65="65;"
      ;;

    --ideogram-left-double)
      $supported && AnsiIdeogramLeftDouble
      restoreText=True
      m65="65;"
      ;;

    --ideogram-stress)
      $supported && AnsiIdeogramStress
      restoreText=True
      m65="65;"
      ;;

    --reset-ideogram)
      $supported && AnsiNoIdeogram
      ;;

    # Miscellaneous
    --color-codes)
      if ! $supported; then
        echo "ANSI is not supported in this terminal."
      else
        AnsiColorCodes
      fi

      return 0
      ;;

    --color-table)
      if ! $supported; then
        echo "ANSI is not supported in this terminal."
      else
        AnsiColorTable
      fi

      return 0
      ;;

    --icon)
      $supported && AnsiIcon ""
      ;;

    --icon=*)
      $supported && AnsiIcon "${1#*=}"
      ;;

    --title)
      $supported && AnsiTitle ""
      ;;

    --title=*)
      $supported && AnsiTitle "${1#*=}"
      ;;

    --no-restore)
      triggerRestore=False
      ;;

    -n | --no-newline)
      addNewline=False
      ;;

    --bell)
      AnsiBell
      ;;

    --reset)
      $supported || return 0
      AnsiReset
      ;;

    --)
      readOptions=False
      shift
      ;;

    *)
      readOptions=False
      ;;
    esac

    if $readOptions; then
      shift
    fi
  done

  printf '%s' "${1-}"

  if [ "$#" -gt 1 ]; then
    shift || :
    local Temp=
    Temp=$(printf '%c' "$IFS")
    printf "${Temp}%s" "${@}"
    unset Temp
  fi

  if $supported && $triggerRestore; then
    if $restoreCursorPosition; then
      AnsiRestoreCursor
    fi

    if $restoreCursorVisibility; then
      AnsiShowCursor
    fi

    if $restoreText; then
      m="$m10$m22$m23$m24$m25$m27$m28$m29$m39$m49$m54$m55$m65"
      printf '%s%sm' "$AnsiCsi" "${m%;}"
    fi
  fi

  if $addNewline; then
    printf '\n'
  fi
}

# # Run if not sourced
# if [ "$0" = "${BASH_SOURCE[0]}" ]; then
#   Ansi "$@" || exit $?
# fi

#
# endregion ANSI Functions

# Aliases
#
alias ansi=Ansi

#  vim: set ts=2 sw=2 tw=80 noet :
