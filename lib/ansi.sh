#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2155 disable=SC2034
#
# ./lib/ansi.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2021-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/os.sh"

#
# endregion Imports

# region ANSI
#

# region Mutables
#

# shellcheck disable=SC2034
[ -z "$DISABLE_CONSOLE_OUTPUT_COLORS" ] && DISABLE_CONSOLE_OUTPUT_COLORS=False

# shellcheck disable=SC2034
[ -z "$DISABLE_CONSOLE_OUTPUT_STYLES" ] && DISABLE_CONSOLE_OUTPUT_STYLES=False

#
# endregion Mutables

if [ -t 1 ]; then
  # IsTTY
  #
  # The [ -t 1 ] check only works when the function is not called from
  # a subshell (like in `$(...)` or `(...)`, so this hack redefines the
  # function at the top level to always return False when stdout is not
  # a tty.
  #
  IsTTY() {
    true
  }
else
  # IsTTY
  #
  # The [ -t 1 ] check only works when the function is not called from
  # a subshell (like in `$(...)` or `(...)`, so this hack redefines the
  # function at the top level to always return False when stdout is not
  # a tty.
  #
  IsTTY() {
    false
  }
fi

# IsConsoleOutputColorsDisabled
#
IsConsoleOutputColorsDisabled() {
  [ -n "$DISABLE_CONSOLE_OUTPUT_COLORS" ] && {
    [ "$DISABLE_CONSOLE_OUTPUT_COLORS" = 1 ] || [ "$DISABLE_CONSOLE_OUTPUT_COLORS" = True ]
  }
}

# IsConsoleOutputStylesDisabled
#
IsConsoleOutputStylesDisabled() {
  [ -n "$DISABLE_CONSOLE_OUTPUT_STYLES" ] && {
    [ "$DISABLE_CONSOLE_OUTPUT_STYLES" = 1 ] || [ "$DISABLE_CONSOLE_OUTPUT_STYLES" = True ]
  }
}

# IsConsoleAnsiEnabled
#
IsConsoleAnsiEnabled() {
  ! (IsConsoleOutputColorsDisabled) && ! (IsConsoleOutputStylesDisabled)
}

# IsConsoleAnsiDisabled
#
IsConsoleAnsiDisabled() {
  (IsConsoleOutputColorsDisabled) && (IsConsoleOutputStylesDisabled)
}

# SupportsTruecolor
#
# Adapted from Code and information by Anton Kochkov (@XVilka)
# Source: https://gist.github.com/XVilka/8346728
#
SupportsTruecolor() {
  case "$COLORTERM" in
  truecolor | 24bit) return 0 ;;
  esac

  case "$TERM" in
  iterm | \
    tmux-truecolor | \
    linux-truecolor | \
    xterm-truecolor | \
    screen-truecolor) return 0 ;;
  esac

  return 1
}

# DisableAnsiColors
#
DisableAnsiColors() {
  # region Colors
  #

  Black=''
  Red=''
  Green=''
  Yellow=''
  Blue=''
  Magenta=''
  Cyan=''
  White=''

  #
  # endregion Colors

  # region Foreground Colors
  #

  FgBlack=''
  FgRed=''
  FgGreen=''
  FgYellow=''
  FgBlue=''
  FgMagenta=''
  FgCyan=''
  FgWhite=''
  FgFine=''
  FgBrightBlack=''
  FgBrightRed=''
  FgBrightGreen=''
  FgBrightYellow=''
  FgBrightBlue=''
  FgBrightMagenta=''
  FgBrightCyan=''
  FgBrightWhite=''
  FgBrightFine=''

  #
  # endregion Foreground Colors

  # region Background Colors
  #

  BgBlack=''
  BgRed=''
  BgGreen=''
  BgYellow=''
  BgBlue=''
  BgMagenta=''
  BgCyan=''
  BgWhite=''
  BgFine=''
  BgBrightBlack=''
  BgBrightRed=''
  BgBrightGreen=''
  BgBrightYellow=''
  BgBrightBlue=''
  BgBrightMagenta=''
  BgBrightCyan=''
  BgBrightWhite=''
  BgBrightFine=''

  #
  # endregion Background Colors

  Rainbow=''

  return 0
}

# DisableAnsiStyles
#
DisableAnsiStyles() {
  # region Styles
  #

  Reset=''
  Bold=''
  Faint=''
  DoublyUnderline=''
  Fraktur=''
  Italic=''
  Underline=''
  SlowBlink=''
  RapidBlink=''
  Blink=''
  Reverse=''
  Inverse=''
  Conceal=''
  Hidden=''
  CrossedOut=''
  StrikeThrough=''
  Framed=''
  Encircled=''
  Overlined=''
  Normal=''
  Fine=''

  #
  # endregion Styles

  return 0
}

# DisableAnsi
#
DisableAnsi() {
  AnsiEsc=''
  AnsiCsi=''
  AnsiOsc=''
  AnsiSt=''
  AnsiReport=''

  DisableAnsiColors
  DisableAnsiStyles

  return 0
}

# SetupAnsiColors
#
SetupAnsiColors() {
  # Only use colors if connected to a terminal
  #
  if ! (IsTTY) || (IsConsoleOutputColorsDisabled); then
    DisableAnsiColors

    return
  fi

  AnsiEsc=$(printf '\033')
  AnsiCsi="${AnsiEsc}["
  AnsiOsc="${AnsiEsc}]"
  AnsiSt="${AnsiEsc}\\"
  AnsiReport=""

  # For text colours

  # region Colors
  #

  Black="${AnsiCsi}30m"
  Red="${AnsiCsi}31m"
  Green="${AnsiCsi}32m"
  Yellow="${AnsiCsi}33m"
  Blue="${AnsiCsi}34m"
  Magenta="${AnsiCsi}35m"
  Cyan="${AnsiCsi}36m"
  White="${AnsiCsi}37m"

  #
  # endregion Colors

  # region Foreground Colors
  #

  FgBlack="${AnsiCsi}30m"
  FgRed="${AnsiCsi}31m"
  FgGreen="${AnsiCsi}32m"
  FgYellow="${AnsiCsi}33m"
  FgBlue="${AnsiCsi}34m"
  FgMagenta="${AnsiCsi}35m"
  FgCyan="${AnsiCsi}36m"
  FgWhite="${AnsiCsi}37m"
  FgFine="${AnsiCsi}39m"
  FgBrightBlack="${AnsiCsi}90m"
  FgBrightRed="${AnsiCsi}91m"
  FgBrightGreen="${AnsiCsi}92m"
  FgBrightYellow="${AnsiCsi}93m"
  FgBrightBlue="${AnsiCsi}94m"
  FgBrightMagenta="${AnsiCsi}95m"
  FgBrightCyan="${AnsiCsi}96m"
  FgBrightWhite="${AnsiCsi}97m"
  FgBrightFine="${AnsiCsi}99m"

  #
  # endregion Foreground Colors

  # region Background Colors
  #

  BgBlack="${AnsiCsi}40m"
  BgRed="${AnsiCsi}41m"
  BgGreen="${AnsiCsi}42m"
  BgYellow="${AnsiCsi}43m"
  BgBlue="${AnsiCsi}44m"
  BgMagenta="${AnsiCsi}45m"
  BgCyan="${AnsiCsi}46m"
  BgWhite="${AnsiCsi}47m"
  BgFine="${AnsiCsi}49m"
  BgBrightBlack="${AnsiCsi}100m"
  BgBrightRed="${AnsiCsi}101m"
  BgBrightGreen="${AnsiCsi}102m"
  BgBrightYellow="${AnsiCsi}103m"
  BgBrightBlue="${AnsiCsi}104m"
  BgBrightMagenta="${AnsiCsi}105m"
  BgBrightCyan="${AnsiCsi}106m"
  BgBrightWhite="${AnsiCsi}107m"
  BgBrightFine="${AnsiCsi}109m"

  Rainbow=

  #
  # endregion Background Colors

  if SupportsTruecolor; then
    Rainbow="
      $(printf "${AnsiCsi}38;2;255;0;0m")
      $(printf "${AnsiCsi}38;2;255;97;0m")
      $(printf "${AnsiCsi}38;2;247;255;0m")
      $(printf "${AnsiCsi}38;2;0;255;30m")
      $(printf "${AnsiCsi}38;2;77;0;255m")
      $(printf "${AnsiCsi}38;2;168;0;255m")
      $(printf "${AnsiCsi}38;2;245;0;172m")
    "
  else
    Rainbow="
      $(printf "${AnsiCsi}38;5;196m")
      $(printf "${AnsiCsi}38;5;202m")
      $(printf "${AnsiCsi}38;5;226m")
      $(printf "${AnsiCsi}38;5;082m")
      $(printf "${AnsiCsi}38;5;021m")
      $(printf "${AnsiCsi}38;5;093m")
      $(printf "${AnsiCsi}38;5;163m")
    "
  fi
}

# SetupAnsiStyles
#
SetupAnsiStyles() {
  if ! (IsTTY) || (IsConsoleOutputStylesDisabled); then
    DisableAnsiStyles

    return
  fi

  AnsiEsc=$(printf '\033')
  AnsiCsi="${AnsiEsc}["
  AnsiOsc="${AnsiEsc}]"
  AnsiSt="${AnsiEsc}\\"
  AnsiReport=""

  # region Styles
  #

  Reset="${AnsiCsi}0m"
  Bold="${AnsiCsi}1m"
  Faint="${AnsiCsi}2m"
  DoublyUnderline="${AnsiCsi}21m"
  Fraktur="${AnsiCsi}20m"
  Italic="${AnsiCsi}3m"
  Underline="${AnsiCsi}4m"
  SlowBlink="${AnsiCsi}02m"
  RapidBlink="${AnsiCsi}6m"
  Blink="${AnsiCsi}5m"
  Reverse="${AnsiCsi}7m"
  Inverse="${AnsiCsi}7m"
  Conceal="${AnsiCsi}8m"
  Hidden="${AnsiCsi}8m"
  CrossedOut="${AnsiCsi}9m"
  StrikeThrough="${AnsiCsi}9m"
  Framed="${AnsiCsi}51m"
  Encircled="${AnsiCsi}52m"
  Overlined="${AnsiCsi}53m"
  Normal="${AnsiCsi}00m"
  Fine="${AnsiCsi}39m"

  #
  # endregion Styles
}

# SetupAnsi
#
SetupAnsi() {
  if ! (IsTTY) || (IsConsoleOutputStylesDisabled); then
    AnsiEsc=$(printf '\033')
    AnsiCsi="${AnsiEsc}["
    AnsiOsc="${AnsiEsc}]"
    AnsiSt="${AnsiEsc}\\"
    AnsiReport=""

    return
  fi

  SetupAnsiColors
  SetupAnsiStyles

  return 0
}

# AnsiBackward
#
AnsiBackward() {
  printf '%s%sD' "$AnsiCsi" "${1-}"
}

# AnsiBell
#
AnsiBell() {
  printf "%s" "$(printf '\007')"
}

# AnsiBlack
#
AnsiBlack() {
  printf '%s30m' "$AnsiCsi"
}

# AnsiBlackIntense
#
AnsiBlackIntense() {
  printf '%s90m' "$AnsiCsi"
}

# AnsiBlink
#
AnsiBlink() {
  printf '%s5m' "$AnsiCsi"
}

# AnsiBlue
#
AnsiBlue() {
  printf '%s34m' "$AnsiCsi"
}

# AnsiBlueIntense
#
AnsiBlueIntense() {
  printf '%s94m' "$AnsiCsi"
}

# AnsiBgBlack
#
AnsiBgBlack() {
  printf '%s40m' "$AnsiCsi"
}

# AnsiBgBlackIntense
#
AnsiBgBlackIntense() {
  printf '%s100m' "$AnsiCsi"
}

# AnsiBgBlue
#
AnsiBgBlue() {
  printf '%s44m' "$AnsiCsi"
}

# AnsiBgBlueIntense
#
AnsiBgBlueIntense() {
  printf '%s104m' "$AnsiCsi"
}

# AnsiBgColor
#
AnsiBgColor() {
  printf '%s48;5;%sm' "$AnsiCsi" "$1"
}

# AnsiBgCyan
#
AnsiBgCyan() {
  printf '%s46m' "$AnsiCsi"
}

# AnsiBgCyanIntense
#
AnsiBgCyanIntense() {
  printf '%s106m' "$AnsiCsi"
}

# AnsiBgGreen
#
AnsiBgGreen() {
  printf '%s42m' "$AnsiCsi"
}

# AnsiBgGreenIntense
#
AnsiBgGreenIntense() {
  printf '%s102m' "$AnsiCsi"
}

# AnsiBgMagenta
#
AnsiBgMagenta() {
  printf '%s45m' "$AnsiCsi"
}

# AnsiBgMagentaIntense
#
AnsiBgMagentaIntense() {
  printf '%s105m' "$AnsiCsi"
}

# AnsiBgRed
#
AnsiBgRed() {
  printf '%s41m' "$AnsiCsi"
}

# AnsiBgRgb
#
AnsiBgRgb() {
  printf '%s48;2;%s;%s;%sm' "$AnsiCsi" "$1" "$2" "$3"
}

# AnsiBgRedIntense
#
AnsiBgRedIntense() {
  printf '%s101m' "$AnsiCsi"
}

# AnsiBgWhite
#
AnsiBgWhite() {
  printf '%s47m' "$AnsiCsi"
}

# AnsiBgWhiteIntense
#
AnsiBgWhiteIntense() {
  printf '%s107m' "$AnsiCsi"
}

# AnsiBgYellow
#
AnsiBgYellow() {
  printf '%s43m' "$AnsiCsi"
}

# AnsiBgYellowIntense
#
AnsiBgYellowIntense() {
  printf '%s103m' "$AnsiCsi"
}

# AnsiBold
#
AnsiBold() {
  printf '%s1m' "$AnsiCsi"
}

# AnsiColor
#
AnsiColor() {
  printf '%s38;5;%sm' "$AnsiCsi" "$1"
}

# AnsiColumn
#
AnsiColumn() {
  printf '%s%sG' "$AnsiCsi" "${1-}"
}

# AnsiColumnRelative
#
AnsiColumnRelative() {
  printf '%s%sa' "$AnsiCsi" "${1-}"
}

# AnsiCyan
#
AnsiCyan() {
  printf '%s36m' "$AnsiCsi"
}

# AnsiCyanIntense
#
AnsiCyanIntense() {
  printf '%s96m' "$AnsiCsi"
}

# AnsiDeleteChars
#
AnsiDeleteChars() {
  printf '%s%sP' "$AnsiCsi" "${1-}"
}

# AnsiDeleteLines
#
AnsiDeleteLines() {
  printf '%s%sM' "$AnsiCsi" "${1-}"
}

# AnsiDoubleUnderline
#
AnsiDoubleUnderline() {
  printf '%s21m' "$AnsiCsi"
}

# AnsiDown
#
AnsiDown() {
  printf '%s%sB' "$AnsiCsi" "${1-}"
}

# AnsiEncircle
#
AnsiEncircle() {
  printf '%s52m' "$AnsiCsi"
}

# AnsiEraseDisplay
#
AnsiEraseDisplay() {
  printf '%s%sJ' "$AnsiCsi" "${1-}"
}

# AnsiEraseChars
#
AnsiEraseChars() {
  printf '%s%sX' "$AnsiCsi" "${1-}"
}

# AnsiEraseLine
#
AnsiEraseLine() {
  printf '%s%sK' "$AnsiCsi" "${1-}"
}

# AnsiFaint
#
AnsiFaint() {
  printf '%s2m' "$AnsiCsi"
}

# AnsiFont
#
AnsiFont() {
  printf '%s1%sm' "$AnsiCsi" "${1-0}"
}

# AnsiForward
#
AnsiForward() {
  printf '%s%sC' "$AnsiCsi" "${1-}"
}

# AnsiFraktur
#
AnsiFraktur() {
  printf '%s20m' "$AnsiCsi"
}

# AnsiFrame
#
AnsiFrame() {
  printf '%s51m' "$AnsiCsi"
}

# AnsiGreen
#
AnsiGreen() {
  printf '%s32m' "$AnsiCsi"
}

# AnsiGreenIntense
#
AnsiGreenIntense() {
  printf '%s92m' "$AnsiCsi"
}

# AnsiHideCursor
#
AnsiHideCursor() {
  printf '%s?25l' "$AnsiCsi"
}

# AnsiIdeogramLeft
#
AnsiIdeogramLeft() {
  printf '%s62m' "$AnsiCsi"
}

# AnsiIdeogramLeftDouble
#
AnsiIdeogramLeftDouble() {
  printf '%s63m' "$AnsiCsi"
}

# AnsiIdeogramRight
#
AnsiIdeogramRight() {
  printf '%s60m' "$AnsiCsi"
}

# AnsiIdeogramRightDouble
#
AnsiIdeogramRightDouble() {
  printf '%s61m' "$AnsiCsi"
}

# AnsiIdeogramStress
#
AnsiIdeogramStress() {
  printf '%s64m' "$AnsiCsi"
}

# AnsiInsertChars
#
AnsiInsertChars() {
  printf '%s%s@' "$AnsiCsi" "${1-}"
}

# AnsiInsertLines
#
AnsiInsertLines() {
  printf '%s%sL' "$AnsiCsi" "${1-}"
}

# AnsiInverse
#
AnsiInverse() {
  printf '%s7m' "$AnsiCsi"
}

# AnsiInvisible
#
AnsiInvisible() {
  printf '%s8m' "$AnsiCsi"
}

# AnsiItalic
#
AnsiItalic() {
  printf '%s3m' "$AnsiCsi"
}

# AnsiLine
#
AnsiLine() {
  printf '%s%sd' "$AnsiCsi" "${1-}"
}

# AnsiLineRelative
#
AnsiLineRelative() {
  printf '%s%se' "$AnsiCsi" "${1-}"
}

# AnsiMagenta
#
AnsiMagenta() {
  printf '%s35m' "$AnsiCsi"
}

# AnsiMagentaIntense
#
AnsiMagentaIntense() {
  printf '%s95m' "$AnsiCsi"
}

# AnsiNextLine
#
AnsiNextLine() {
  printf '%s%sE' "$AnsiCsi" "${1-}"
}

# AnsiNoBlink
#
AnsiNoBlink() {
  printf '%s25m' "$AnsiCsi"
}

# AnsiNoBorder
#
AnsiNoBorder() {
  printf '%s54m' "$AnsiCsi"
}

# AnsiNoInverse
#
AnsiNoInverse() {
  printf '%s27m' "$AnsiCsi"
}

# AnsiNormal
#
AnsiNormal() {
  printf '%s22m' "$AnsiCsi"
}

# AnsiNoOverline
#
AnsiNoOverline() {
  printf '%s55m' "$AnsiCsi"
}

# AnsiNoStrike
#
AnsiNoStrike() {
  printf '%s29m' "$AnsiCsi"
}

# AnsiNoUnderline
#
AnsiNoUnderline() {
  printf '%s24m' "$AnsiCsi"
}

# AnsiOverline
#
AnsiOverline() {
  printf '%s53m' "$AnsiCsi"
}

# AnsiPlain
#
AnsiPlain() {
  printf '%s23m' "$AnsiCsi"
}

# AnsiPosition
#
AnsiPosition() {
  local Position="${1-}"
  # printf '%s%sH' "$AnsiCsi" "${Position/,/;}"
  printf '%s%sH' "$AnsiCsi" "$(printf '%s' "$Position" | command sed -Ere 's~,~;~g')"
}

# AnsiPreviousLine
#
AnsiPreviousLine() {
  printf '%s%sF' "$AnsiCsi" "${1-}"
}

# AnsiRapidBlink
#
AnsiRapidBlink() {
  printf '%s6m' "$AnsiCsi"
}

# AnsiRed
#
AnsiRed() {
  printf '%s31m' "$AnsiCsi"
}

# AnsiRedIntense
#
AnsiRedIntense() {
  printf '%s91m' "$AnsiCsi"
}

# AnsiRepeat
#
AnsiRepeat() {
  printf '%s%sb' "$AnsiCsi" "${1-}"
}

# AnsiReset
#
AnsiReset() {
  AnsiResetColor
  AnsiResetFont
  AnsiEraseDisplay 2
  AnsiPosition "1;1"
  AnsiShowCursor
}

# AnsiResetAttributes
#
AnsiResetAttributes() {
  printf '%s22;23;24;25;27;28;29;54;55m' "$AnsiCsi"
}

# AnsiResetBackground
#
AnsiResetBackground() {
  printf '%s49m' "$AnsiCsi"
}

# AnsiResetColor
#
AnsiResetColor() {
  printf '%s0m' "$AnsiCsi"
}

# AnsiResetFont
#
AnsiResetFont() {
  printf '%s10m' "$AnsiCsi"
}

# AnsiResetForeground
#
AnsiResetForeground() {
  printf '%s39m' "$AnsiCsi"
}

# AnsiResetIdeogram
#
AnsiResetIdeogram() {
  printf '%s65m' "$AnsiCsi"
}

# AnsiRestoreCursor
#
AnsiRestoreCursor() {
  printf '%su' "$AnsiCsi"
}

# AnsiRgb
#
AnsiRgb() {
  printf '%s38;2;%s;%s;%sm' "$AnsiCsi" "$1" "$2" "$3"
}

# AnsiSaveCursor
#
AnsiSaveCursor() {
  printf '%ss' "$AnsiCsi"
}

# AnsiScrollDown
#
AnsiScrollDown() {
  printf '%s%sT' "$AnsiCsi" "${1-}"
}

# AnsiScrollUp
#
AnsiScrollUp() {
  printf '%s%sS' "$AnsiCsi" "${1-}"
}

# AnsiIcon
#
AnsiIcon() {
  printf '%s1;%s%s' "$AnsiOsc" "${1-}" "$AnsiSt"
}

# AnsiTitle
#
AnsiTitle() {
  printf '%s2;%s%s' "$AnsiOsc" "${1-}" "$AnsiSt"
}

# AnsiShowCursor
#
AnsiShowCursor() {
  printf '%s?25h' "$AnsiCsi"
}

# AnsiStrike
#
AnsiStrike() {
  printf '%s9m' "$AnsiCsi"
}

# AnsiTabBackward
#
AnsiTabBackward() {
  printf '%s%sZ' "$AnsiCsi" "${1-}"
}

# AnsiTabForward
#
AnsiTabForward() {
  printf '%s%sI' "$AnsiCsi" "${1-}"
}

# AnsiUnderline
#
AnsiUnderline() {
  printf '%s4m' "$AnsiCsi"
}

# AnsiUp
#
AnsiUp() {
  printf '%s%sA' "$AnsiCsi" "${1-}"
}

# AnsiVisible
#
AnsiVisible() {
  printf '%s28m' "$AnsiCsi"
}

# AnsiWhite
#
AnsiWhite() {
  printf '%s37m' "$AnsiCsi"
}

# AnsiWhiteIntense
#
AnsiWhiteIntense() {
  printf '%s97m' "$AnsiCsi"
}

# AnsiYellow
#
AnsiYellow() {
  printf '%s33m' "$AnsiCsi"
}

# AnsiYellowIntense
#
AnsiYellowIntense() {
  printf '%s93m' "$AnsiCsi"
}

# AnsiStrikeThrough
#
AnsiStrikeThrough() {
  command sed "s~\(.\)~\1""$(printf '\xcc\xb6')""~g"
}

#
# endregion ANSI

#  vim: set ts=2 sw=2 tw=80 noet :
