#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2120
#
# ./lib/array.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/os.sh"

# # shellcheck disable=SC1090 disable=SC1091
# . "$XDG_DATA_HOME/shellscripts/lib/string.sh"

#
# endregion Imports

# region Array
#

##
# Definitions:
# - An array is a newline separated list of array elements.
# - An array element is an `ArrayElementEncode`d string.

# ArrayElementEncode
#
# Turn stdin into an array element.
#
# This is similar to urlencode but only for % and \n (and not \0).
#
ArrayElementEncode() {
  # command sed 's~%~%25~g' |
  #   command sed -e ':a' -e 'N' -e '$!ba' -e 's~\n~%0A~'

  command sed 's~%~%25~g' |
    command sed -e :a -e '$!N; s~\n~%0A~; ta' |
    command sed -Ere "\$!N; s~([)]$.*+\^?[(])+~\\\\\1~g; s~('\")+~\\\\\1~g"
}

# ArrayElementDecode
#
# Inverse function of ArrayElementEncode.
#
ArrayElementDecode() {
  # shellcheck disable=SC1004
  command sed -e 's~%0[aA]~\
~g' -e 's~%25~%~g'
}

# Array
#
# Array constructor
#
# Turn arguments into an array.
#
# $@: Arguments
#
Array() {
  local -r Argc=$#

  if ! [ "$Argc" -eq 0 ] || [ "$Argc" -gt 0 ]; then
    for Element in "$@"; do
      printf '%s\n' "$Element" | ArrayElementEncode
    done

    unset Element
  else
    printf '%s' "" | ArrayElementEncode
  fi

  # for Element in "$@"; do
  #   printf '%s\n' "$Element" | ArrayElementEncode
  # done
  # unset Element
}

# ArrayAppend
#
# Append items to an array
#
# $1: Array
# $@: Arguments
#
# Example:
#
# Pass your array as the first argument:
#   ArrayAppend "$Array" 'next' 'another'
#
ArrayAppend() {
  printf '%s\n' "$1" && shift && {
    for Element in "$@"; do
      printf '%s\n' "$Element" | ArrayElementEncode
    done
    unset Element
  }
}

# ArrayPrepend
#
# Prepends items to an array
#
# $1: Array
# $@: Arguments
#
# Example:
#
# Pass your array as the first argument:
#   ArrayPrepend "$Array" 'previous' 'another'
#
ArrayPrepend() {
  local Array="$1"
  shift 1

  {
    for Element in "$@"; do
      printf '%s\n' "$Element" | ArrayElementEncode
    done
    unset Element

    printf '%s\n' "$Array"
  }
}

# ArrayLen
#
# Get length of array
#
# $1: Array
#
# Example:
#
# Pass your array to:
#   ArrayLen "$Array"
#
# Pipe your array in:
#   printf '%s\n' "$Array" | ArrayLen
#
ArrayLen() {
  local Array="${1:-}"
  local Length=0

  if [ -z "$Array" ] && ! [ -t 0 ]; then
    Array=$(command cat <&0)
  fi

  for Each in $(printf '%s\n' "$Array" | ArrayElementDecode); do
    [ -n "$Each" ] && Length=$(( Length + 1 ))
  done

  unset Each

  printf '%d' "$Length"
}

# ArrayNth
#
# Element accessor
#
# Uses first argument as (zero-based) index.
#
# $1: Index
#
# Exit status:
#    0  Success.
#   >0  An error occurred.
#
# Example:
#
# Pipe your array in:
#   printf '%s\n' "$Array" | ArrayNth $index
#
ArrayNth() {
  # shellcheck disable=SC2016
  printf '%d' "$1" 1>/dev/null 2>&1 && $AWK_COMMAND -v Element="$1" '
    BEGIN {
      Code = 1;
    }

    NR == Element + 1 {
      print $0;
      Code = 0;
    }

    END {
      exit Code;
    }
  ' | ArrayElementDecode
}

# ArrayIndexOf
#
# Get the first (zero-based) index of an element.
#
# $1: Element
#
# Exit status:
#    0  Success.
#   >0  An error occurred (element not in array).
#
# Example:
#
# Pipe your array in:
#   printf '%s\n' "$Array" | ArrayIndexOf "$Element"
#
ArrayIndexOf() {
  # shellcheck disable=SC2016
  Element=$(printf '%s\n' "$1" | ArrayElementEncode) $AWK_COMMAND '
    BEGIN {
      Element = ENVIRON["Element"];
      Index = -1;
      Code = 1;
    }

    {
      if (Index < 0 && Element == $0) {
        Index = NR - 1;
        print Index;
        Code = 0;
      }
    }

    END {
      exit Code
    }
  '
}

# ArrayRemove
#
# Remove the first occurence of an element.
#
# $1: Element
#
# Exit status:
#    0  Success.
#   >0  An error occurred (element not in array).
#
# Example:
#
# Pipe your array in:
#   printf '%s\n' "$Array" | ArrayRemove "$Element"
#
ArrayRemove() {
  # local Element=
  # shellcheck disable=SC2016
  Element=$(printf '%s\n' "$1" | ArrayElementEncode) $AWK_COMMAND '
    BEGIN {
      Element = ENVIRON["Element"];
      Index = -1;
      Code = 1;
    }

    {
      if (Index < 0 && Element == $0) {
        Index = NR - 1;
        Code = 0;
      } else {
        print $0;
      }
    }

    END {
      exit Code;
    }
  '
}

# InArray
#
# Determines if the given element is in the given array.
#
# $1: Element
# $2: Array
#
InArray() {
  local -r Element="${1:-}"
  local -r Array="${2:-}"
  local RetVal=1

  for Each in $Array; do
    if [ "$Element" = "$Each" ]; then
      RetVal=0
      break
    fi
  done

  unset Each

  return $RetVal
}

# ArrayHas
#
# Determines if the given array has the given element.
#
# $1: Array
# $2: Element
#
ArrayHas() {
  local -r Array="${1:-}"
  local -r Element="${2:-}"
  local RetVal=1

  for Each in $Array; do
    if [ "$Element" = "$Each" ]; then
      RetVal=0
      break
    fi
  done

  unset Each

  return $RetVal
}

# # ArrayJoin
# #
# # Joins the given array by the given separator.
# #
# # $1: Array
# # $2: Separator
# #
# ArrayJoin() {
#   local -r Array="${1:-}"
#   local -r Separator="${2:-,}"
#   local Escaped=
#   local RetVal=

#   Escaped=$(printf '%s' "${Separator}" |
#     command sed -Ere "\$!N; s~([)]$.*+\^?[(])+~\\\\\1~g; s~('\")+~\\\\\1~g")

#   Escaped="\$!N; s~${Escaped}$~~"

#   # printf '%s' "$Array" |
#   #   command tr '\n' "${Separator}" |
#   #   command sed -e "s~$(StrEscape -m regex "${Separator}")$~~"

#   # RetVal=$(printf '%s' "$Array" |
#   #   command tr '\n' "${Separator}" |
#   #   command sed -Ere "$Escaped")

#   RetVal=$(printf '%s' "$Array" |
#     command sed -Ere "\$!N; s~(\n)+~${Separator} ~g" |
#     command sed -Ere "$Escaped")

#   printf '%s\n' "$RetVal"

#   # local IFS="$1"
#   # shift
#   # printf '%s' "$*"
# }

# ArrayJoin
#
# Joins the given array by the given separator.
#
# $1: Separator
# $2: Array
#
ArrayJoin() {
  local Separator=
  local Array=

  if [ "${1}" = "--separator" ] || [ "${1}" = "-separator" ] || [ "${1}" = "-s" ]; then
    Separator="${2}"
    shift 2
  else
    Separator=','
  fi

  if [ -z "${1}" ]; then
    return 1
  fi

  if [ -z "${1}" ] && ! [ -t 0 ]; then
    Array=$(command cat <&0)
  else
    Array="$*"
  fi

  local Escaped=
  local RetVal=

  Escaped=$(printf '%s' "${Separator}" |
    command sed -Ere "\$!N; s~([)]$.*+\^?[(])+~\\\\\1~g; s~('\")+~\\\\\1~g")

  Escaped="\$!N; s~${Escaped}$~~"

  # printf '%s' "$Array" |
  #   command tr '\n' "${Separator}" |
  #   command sed -e "s~$(StrEscape -m regex "${Separator}")$~~"

  # RetVal=$(printf '%s' "$Array" |
  #   command tr '\n' "${Separator}" |
  #   command sed -Ere "$Escaped")

  RetVal=$(printf '%s' "$Array" |
    command sed -Ere "\$!N; s~(\n)+~${Separator} ~g" |
    command sed -Ere "$Escaped")

  printf '%s\n' "$RetVal"

  # local IFS="$1"
  # shift
  # printf '%s' "$*"
}

# ArraySplit
#
# Split the given string by the given separator and return an array.
#
# $1: Separator
# $2: String
#
ArraySplit() {
  local -r Separator="${1:-([[:space:]|,|[:space:],|,[:space:]|[:space:],[:space:]|\n])}"
  local -r String="${2:-}"
  local RetVal=

  RetVal=$(
    printf '%s\n' "$String" | command sed -Ere "\$!N; s~${Separator}+~\n~g"
  )

  printf '%s\n' "$RetVal" | ArrayElementEncode

  # IFS=
  # read -d "$1" -r "$2" || [ -n "${!2}" ];
}

# ArrayPeek
#
# Peek at a value from the end of the provided array without removing it.
#
# $1: Array
#
ArrayPeek() {
  local -r Array="${1:-}"
  local Len=
  local LastElement=

  Len="$(printf '%s\n' "$Array" | ArrayLen)"
  LastElement=$(printf '%s\n' "$Array" | ArrayNth $((Len - 1)))

  # local Index=
  # Index="$(( $(ArrayCount "${Array}") - 1 ))"
  # eval "printf \"\${${Array}ArrayIndex${Index}}\"" || :

  printf '%s\n' "$LastElement"
}

# ArrayPop
#
# Pops the latest item from an array.
#
# $1: Array
#
# Example:
#
# Pass your array as the first argument:
#   ArrayPop "$Array"
#
ArrayPop() {
  local -r Array="${1:-}"
  local Len=
  local LastElement=

  Len="$(printf '%s\n' "$Array" | ArrayLen)"
  LastElement=$(printf '%s\n' "$Array" | ArrayNth "$Len")

  # local Index=
  # Index="$(( $(ArrayCount "${Array}") - 1 ))"

  # eval "printf \"\${${Array}ArrayIndex${Index}}\"" || :
  # eval "${Array}ArrayCount=${Index}" || :
  # eval "unset ${Array}ArrayIndex${Index}" || :

  printf '%s\n' "$Array" | ArrayRemove "$LastElement"
}

# IsArray
#
# Determines if the given argument is an array.
#
# $1: Argument
#
IsArray() {
  local -r Argument="${1:-}"

  return 0
}

# ArraySize
#
# Get the size of the given array.
#
# $1: Array
#
ArraySize() {
  local -r Array="${1:-}"
  shift

  local i=0

  printf '%s\n' "$Array" | while true; do
    if ! read -r Element; then
      printf '%d\n' "$i"
      return
    fi

    i=$((i + 1))
  done

  unset i
}

# ArrayGet
#
# Get array element by the given index.
#
# $1: Index
# $@: Arrays
#
ArrayGet() {
  local -r Index="${1:-}"
  shift

  local -r Arrays="${1:-}"
  shift

  local i=-0

  printf '%s\n' "$Arrays" | while read -r Element; do
    if test "$((i = i + 1))" -eq "$Index"; then
      printf '%s\n' "$Element"
      return
    fi
  done

  unset i

  printf %b "\n"
}

# ArrayShow
#
# Joins the given array(s) by the given separator.
#
# $1: Separator
# $@: Arrays
#
ArrayShow() {
  local Separator="${1:-}"
  shift

  local -r Arrays="${1:-}"
  shift

  local RetVal=""
  local Escaped=

  Separator=$(printf '%s\n' "$Separator" | command sed -Ere 's~\s+~~g')

  Escaped=$(printf '%s' "${Separator}" |
    command sed -Ere "\$!N; s~([)]$.*+\^?[(])+~\\\\\1~g; s~('\")+~\\\\\1~g")

  Escaped="\$!N; s~${Escaped}\s$~~"

  printf '%s\n' "$Arrays" | while true; do
    if read -r Element; then
      RetVal="${RetVal}${Element}${Separator} "
      continue
    fi

    printf '%s\n' "$RetVal" | command sed -Ere "$Escaped"

    return
  done
}

# ArrayDef
#
# $1: Index
# $2: Value
#
ArrayDef() {
  local -r Index="${1:-}"
  shift

  local -r Value="${1:-}"
  shift

  local i=-1

  printf '%s\n' "$@" | while read -r Element; do
    test "$((i = i + 1))" -eq "$Index" && Element="$Value"
    printf '%s\n' "$Element"
  done
}

# ArrayDefine
#
# Define a variable as an empty array.
#
# The variable should already exist in the scope you want to use it in.
#
# $1: Array
#
ArrayDefine() {
  local -r Array="${1:-}"
  eval "${Array}=\$(Array)" || :
}

#
# endregion Array

# region Aliases
#

alias ArrayPush=ArrayAppend
alias ArrayNew=Array

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
