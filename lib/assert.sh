#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./lib/assert.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2021-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/os.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/terminal.sh"

# # shellcheck disable=SC1090 disable=SC1091
# . "$XDG_DATA_HOME/shellscripts/lib/ansi.sh"

# SetupAnsi

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/string.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/logger.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/utils.sh"

#
# endregion Imports

# region Assert
#

# # AssertSuccess
# #
# AssertSuccess() {
#   if [ "$status" -ne 0 ]; then
#     Flunk "command failed with exit status $status"
#   elif [ "$#" -gt 0 ]; then
#     AssertOutput "$1"
#   fi
# }

# # AssertFailure
# #
# AssertFailure() {
#   if [ "$status" -eq 0 ]; then
#     Flunk "expected failed exit status"
#   elif [ "$#" -gt 0 ]; then
#     AssertOutput "$1"
#   fi
# }

# Flunk
#
Flunk() {
  # local Arguments="${1:-}"
  # [ "$#" -gt 0 ] && shift

  {
    if [ "$#" -eq 0 ]; then
      command cat -
    else
      Log -a "$@"
    fi
  } >&2

  return 1
}

# AssertEq
#
# Takes two strings and checks whether they are the same based on the
# character strings.
#
# $1: Expected
# $2: Actual
# $2: Message
#
AssertEq() {
  local -r Expected="${1:-}"
  local -r Actual="${2:-}"
  local -r Message="${3:-}"

  if [ "$Expected" = "$Actual" ]; then
    return 0
  else
    if [ "${#Message}" -gt 0 ]; then
      LogFailure "$Expected ${Faint}==${Reset} $Actual ${Faint}::${Reset} $Message" || True
    fi

    return 1
  fi
}

# AssertNotEq
#
# Is the opposite of `AssertEq`.
#
# $1: Expected
# $2: Actual
# $2: Message
#
AssertNotEq() {
  local -r Expected="${1:-}"
  local -r Actual="${2:-}"
  local -r Message="${3:-}"

  if [ "$Expected" = "$Actual" ]; then
    if [ "${#Message}" -gt 0 ]; then
      LogFailure "$Expected ${Faint}!=${Reset} $Actual ${Faint}::${Reset} $Message" || True
    fi

    return 1
  else
    return 0
  fi
}

# AssertTrue
#
# Takes a parameter and returns 0 confirming the parameter is True.
#
# $1: Actual
# $2: Message
#
AssertTrue() {
  local -r Actual="${1:-}"
  local -r Message="${2:-}"

  AssertEq True "$Actual" "$Message"

  return $?
}

# AssertFalse
#
# Takes a parameter and decides whether it is False.
#
# $1: Actual
# $2: Message
#
AssertFalse() {
  local -r Actual="${1:-}"
  local -r Message="${2:-}"

  AssertEq False "$Actual" "$Message"

  return $?
}

# AssertNull
#
# Takes a parameter and decides whether it is null.
#
# $1: Actual
# $2: Message
#
AssertNull() {
  local -r Actual="${1:-}"
  local -r Message="${2:-}"

  AssertEq null "$Actual" "$Message" ||
    AssertEq Null "$Actual" "$Message" ||
    AssertEq '' "$Actual" "$Message"

  return $?
}

# # AssertArrayEq
# #
# # Takes two arrays and compare them by items.
# #
# AssertArrayEq() {
#   if (IsBash) || (IsZsh); then
#     # shellcheck disable=SC3044 disable=SC3053 disable=SC3030
#     declare -a Expected=("${!1-}")
#     # echo "AAE ${Expected[@]}"

#     # shellcheck disable=SC3044 disable=SC3053 disable=SC3030
#     declare -a Actual=("${!2}")
#     # echo "AAE ${Actual[@]}"
#   else
#     local -r Expected="${1:-}"
#     local -r Actual="${2:-}"
#   fi

#   local -r Message="${3:-}"

#   local ReturnCode=0

#   if ! [ "${#Expected[@]}" = "${#Actual[@]}" ]; then
#     ReturnCode=1
#   fi

#   local i

#   for ((i = 1; i < ${#Expected[@]} + 1; i += 1)); do
#     if ! [ "${Expected[$i - 1]}" = "${Actual[$i - 1]}" ]; then
#       ReturnCode=1
#       break
#     fi
#   done

#   if [ "$ReturnCode" = 1 ]; then
#     if [ "${#Message}" -gt 0 ]; then
#       LogFailure "(${Expected[*]}) != (${Actual[*]}) :: $Message"
#     else
#       True
#     fi

#     return 1
#   fi

#   return "$ReturnCode"
# }

# # AssertArrayNotEq
# #
# # Takes two arrays and return 0 if the items are not the same on the same index.
# #
# AssertArrayNotEq() {
#   declare -a Expected=("${!1-}")
#   declare -a Actual=("${!2}")

#   local -r Message="${3:-}"

#   local ReturnCode=1

#   if [ ! "${#Expected[@]}" = "${#Actual[@]}" ]; then
#     ReturnCode=0
#   fi

#   local i

#   for ((i = 1; i < ${#Expected[@]} + 1; i += 1)); do
#     if [ ! "${Expected[$i - 1]}" = "${Actual[$i - 1]}" ]; then
#       ReturnCode=0
#       break
#     fi
#   done

#   if [ "$ReturnCode" = 1 ]; then
#     if [ "${#Message}" -gt 0 ]; then
#       LogFailure "(${Expected[*]}) == (${Actual[*]}) :: $Message"
#     else
#       True
#     fi

#     return 1
#   fi

#   return "$ReturnCode"
# }

# AssertEmpty
#
# Takes a string and returns 0 if it is empty.
#
# $1: Actual
# $2: Message
#
AssertEmpty() {
  local -r Actual="${1:-}"
  local -r Message="${2:-}"

  AssertEq "" "$Actual" "$Message"

  return $?
}

# AssertNotEmpty
#
# Is the opposite of `AssertEmpty`.
#
# $1: Actual
# $2: Message
#
AssertNotEmpty() {
  local -r Actual="${1:-}"
  local -r Message="${2:-}"

  AssertNotEq "" "$Actual" "$Message"

  return $?
}

# AssertContain
#
# Checks whether the first argument contains the second one.
#
# $1: HayStack
# $2: Needle
# $2: Message
#
AssertContain() {
  local HayStack="$1"
  local Needle="${2-}"
  local -r Message="${3:-}"

  if [ -z "${Needle:+x}" ]; then
    return 0
  fi

  if [ -z "${HayStack##*"$Needle"*}" ]; then
    return 0
  else
    if [ "${#Message}" -gt 0 ]; then
      LogFailure "$HayStack ${Faint}doesn't contains${Reset} $Needle ${Faint}::${Reset} $Message"
    else
      True
    fi

    return 1
  fi
}

# AssertNotContain
#
# Check whether the first argument does not contain the second one.
#
# $1: HayStack
# $2: Needle
# $2: Message
#
AssertNotContain() {
  local HayStack="$1"
  local Needle="${2-}"
  local -r Message="${3:-}"

  if [ -z "${Needle:+x}" ]; then
    return 0
  fi

  if [ "${HayStack##*"$Needle"*}" ]; then
    return 0
  else
    if [ "${#Message}" -gt 0 ]; then
      LogFailure "$HayStack ${Faint}contains${Reset} $Needle ${Faint}::${Reset} $Message"
    else
      True
    fi

    return 1
  fi
}

# AssertGt
#
# Checks whether the first param is greater than the second.
#
# $1: First
# $2: Second
# $2: Message
#
AssertGt() {
  local -r First="${1:-}"
  local -r Second="${2:-}"
  local -r Message="${3:-}"

  if [ "$First" -gt "$Second" ]; then
    return 0
  else
    if [ "${#Message}" -gt 0 ]; then
      LogFailure "$First ${Faint}>${Reset} $Second ${Faint}::${Reset} $Message"
    else
      True
    fi

    return 1
  fi
}

# AssertGe
#
# Checks whether the first param is greator than or equal to the second one.
#
# $1: First
# $2: Second
# $2: Message
#
AssertGe() {
  local -r First="${1:-}"
  local -r Second="${2:-}"
  local -r Message="${3:-}"

  if [ "$First" -ge "$Second" ]; then
    return 0
  else
    if [ "${#Message}" -gt 0 ]; then
      LogFailure "$First ${Faint}>=${Reset} $Second ${Faint}::${Reset} $Message"
    else
      True
    fi

    return 1
  fi
}

# AssertLt
#
# Checks whether the first param is less than the second one.
#
# $1: First
# $2: Second
# $2: Message
#
AssertLt() {
  local -r First="${1:-}"
  local -r Second="${2:-}"
  local -r Message="${3:-}"

  if [ "$First" -lt "$Second" ]; then
    return 0
  else
    if [ "${#Message}" -gt 0 ]; then
      LogFailure "$First ${Faint}<${Reset} $Second ${Faint}::${Reset} $Message"
    else
      True
    fi

    return 1
  fi
}

# AssertLe
#
# Checks whether the first param is less than or equal to the second one.
#
# $1: First
# $2: Second
# $2: Message
#
AssertLe() {
  local -r First="${1:-}"
  local -r Second="${2:-}"
  local -r Message="${3:-}"

  if [ "$First" -le "$Second" ]; then
    return 0
  else
    if [ "${#Message}" -gt 0 ]; then
      LogFailure "$First ${Faint}<=${Reset} $Second ${Faint}::${Reset} $Message"
    else
      True
    fi

    return 1
  fi
}

# Assert
#
Assert() {
  if ! ("$@") 1>/dev/null 2>&1; then
    # shellcheck disable=SC2145
    Flunk "${Bold}${FgBrightRed}Failed${Reset}${Faint}:${Reset} $@"
  fi
}

#
# endregion Assert

#  vim: set ts=2 sw=2 tw=80 noet :
