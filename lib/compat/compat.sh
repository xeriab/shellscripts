#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./lib/compat/compat.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Compatibility
#

if [ -n "${BASH_VERSION}" ]; then
  # # shellcheck disable=SC3028 disable=SC1090 disable=SC3046 disable=SC2128
  # . "$(cd "$(dirname "${BASH_SOURCE}")" && pwd)/bash/"*.bash

  # shellcheck disable=SC2231
  for Each in "$(cd "$(dirname "${0}")" && pwd)/bash/"*.bash; do
    local Tmp=

    if [ -r "$Each" ]; then
      Tmp="$(basename "$Each")"

      if ! [ "$Tmp" = "loader.bash" ] || ! [ "$Tmp" = "loader-include-prototype.bash" ]; then
        # shellcheck disable=SC1090
        . "$Each" 1>/dev/null 2>&1
      fi
    fi

    unset Tmp
  done

  unset Each
elif [ -n "${ZSH_VERSION}" ]; then
  # # shellcheck disable=SC3028 disable=SC1090 disable=SC3046 disable=SC2128
  # . "$(cd "$(dirname "${0}")" && pwd)/zsh/"*.zsh

  # shellcheck disable=SC2231
  for Each in "$(cd "$(dirname "${0}")" && pwd)/zsh/"*.zsh; do
    local Tmp=

    if [ -r "$Each" ]; then
      Tmp="$(basename "$Each")"

      if ! [ "$Tmp" = "loader.zsh" ]; then
        # shellcheck disable=SC1090
        . "$Each" 1>/dev/null 2>&1
      fi
    fi

    unset Tmp
  done

  unset Each
else
  # # shellcheck disable=SC3028 disable=SC1090 disable=SC3046 disable=SC2128
  # . "${SHELLSCRIPTS_PATH}/lib/compat/default/"*.sh

  # shellcheck disable=SC2231
  for Each in "$(cd "$(dirname "${0}")" && pwd)/default/"*.sh; do
    local Tmp=

    if [ -r "$Each" ]; then
      Tmp="$(basename "$Each")"

      if ! [ "$Tmp" = "loader.sh" ]; then
        # shellcheck disable=SC1090
        . "$Each" 1>/dev/null 2>&1
      fi
    fi

    unset Tmp
  done

  unset Each
fi

# endregion Compatibility

#  vim: set ts=2 sw=2 tw=80 noet :
