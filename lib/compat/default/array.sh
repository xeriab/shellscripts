#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./lib/compat/default/array.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# # shellcheck disable=SC1090 disable=SC1091
# . "$XDG_DATA_HOME/shellscripts/lib/string.sh"

#
# endregion Imports

# region Globals
#

# shellcheck disable=SC2034
OLD_SET=$-

# shellcheck disable=SC2034
SAVEIFS=$IFS

#
# endregion Globals

# region Array
#

##
# Definitions:
# - An array is a newline separated list of array elements.
# - An array element is an `ArrayElementEncode`d string.

# ArrayElementEncode
#
# Turn stdin into an array element.
#
# This is similar to urlencode but only for % and \n (and not \0).
#
ArrayElementEncode() {
  # command sed 's~%~%25~g' |
  #   command sed -e ':a' -e 'N' -e '$!ba' -e 's~\n~%0A~'

  command sed 's~%~%25~g' |
    command sed -e :a -e '$!N; s~\n~%0A~; ta' |
    command sed -Ere "\$!N; s~([)]$.*+\^?[(])+~\\\\\1~g; s~('\")+~\\\\\1~g"
}

# ArrayElementDecode
#
# Inverse function of ArrayElementEncode.
#
ArrayElementDecode() {
  # shellcheck disable=SC1004
  command sed -e 's~%0[aA]~\
~g' -e 's~%25~%~g'
}

# Array
#
# Array constructor
#
# Turn arguments into an array.
#
# $@: Arguments
#
Array() {
  for Element in "$@"; do
    printf '%s\n' "$Element" | ArrayElementEncode
  done
  unset Element
}

# ArrayAppend
#
# Append items to an array
#
# $1: Array
# $@: Arguments
#
# Pass your array as the first argument:
#   ArrayAppend "$Array" 'next' 'another'
#
ArrayAppend() {
  printf '%s\n' "$1" && shift && {
    for Element in "$@"; do
      printf '%s\n' "$Element" | ArrayElementEncode
    done
    unset Element
  }
}

# ArrayPrepend
#
# Prepends items to an array
#
# $1: Array
# $@: Arguments
#
# Pass your array as the first argument:
#   ArrayPrepend "$Array" 'previous' 'another'
#
ArrayPrepend() {
  local Array="$1"
  shift 1

  {
    for Element in "$@"; do
      printf '%s\n' "$Element" | ArrayElementEncode
    done
    unset Element

    printf '%s\n' "$Array"
  }
}

# ArrayLen
#
# Get length of array
#
# $1: Array
#
# Pipe your array in:
#   printf '%s\n' "$Array" | ArrayLen
#
ArrayLen() {
  command wc -l
}

# ArrayNth
#
# Element accessor
#
# Uses first argument as (zero-based) index.
#
# $1: Index
#
# Exit status:
#    0  Success.
#   >0  An error occurred.
#
# Pipe your array in:
#   printf '%s\n' "$Array" | ArrayNth $index
#
ArrayNth() {
  # shellcheck disable=SC2016
  printf '%d' "$1" 1>/dev/null 2>&1 && $AWK_COMMAND -v Element="$1" '
    BEGIN { Code=1 }
    NR == Element + 1 { print $0; Code=0 }
    END { exit Code }
    ' | ArrayElementDecode
}

# ArrayIndexOf
#
# Get the first (zero-based) index of an element.
#
# $1: Element
#
# Exit status:
#    0  Success.
#   >0  An error occurred (element not in array).
#
# Pipe your array in:
#   printf '%s\n' "$Array" | ArrayIndexOf "$element"
#
ArrayIndexOf() {
  # local Element=
  # shellcheck disable=SC2016
  Element=$(printf '%s\n' "$1" | ArrayElementEncode) $AWK_COMMAND '
    BEGIN { Element=ENVIRON["Element"] ; x = -1 ; Code = 1 }
    x < 0 && Element == $0 { x = NR - 1 ; print x ; Code = 0 }
    END { exit Code }
    '
}

# ArrayRemove
#
# Remove the first occurence of an element.
#
# $1: Element
#
# Exit status:
#    0  Success.
#   >0  An error occurred (element not in array).
#
# Pipe your array in:
#   printf '%s\n' "$Array" | ArrayRemove "$Element"
#
ArrayRemove() {
  # local Element=
  # shellcheck disable=SC2016
  Element=$(printf '%s\n' "$1" | ArrayElementEncode) $AWK_COMMAND '
    BEGIN { Element=ENVIRON["Element"] ; x = -1 ; Code = 1 }
    { if (x < 0 && Element == $0) { x = NR - 1 ; Code = 0 } else { print $0 } }
    END { exit Code }
    '
}

# InArray
#
# Determines if the given element is in the given array.
#
# $1: Element
# $2: Array
#
InArray() {
  local -r Element="${1:-}"
  local -r Array="${2:-}"
  local RetVal=1

  for Each in $Array; do
    if [ "$Element" = "$Each" ]; then
      RetVal=0
      break
    fi
  done

  unset Each

  return $RetVal
}

# ArrayHas
#
# Determines if the given array has the given element.
#
# $1: Array
# $2: Element
#
ArrayHas() {
  local -r Array="${1:-}"
  local -r Element="${2:-}"
  local RetVal=1

  for Each in $Array; do
    if [ "$Element" = "$Each" ]; then
      RetVal=0
      break
    fi
  done

  unset Each

  return $RetVal
}

# ArrayJoin
#
# Joins the given array by the given separator.
#
# $1: Array
# $2: Separator
#
ArrayJoin() {
  local -r Array="${1:-}"
  local -r Separator="${2:-,}"
  local Escaped=
  local RetVal=

  Escaped=$(printf '%s' "${Separator}" |
    command sed -Ere "\$!N; s~([)]$.*+\^?[(])+~\\\\\1~g; s~('\")+~\\\\\1~g")

  Escaped="\$!N; s~${Escaped}$~~"

  # printf '%s' "$Array" |
  #   command tr '\n' "${Separator}" |
  #   command sed -e "s~$(StrEscape -m regex "${Separator}")$~~"

  # RetVal=$(printf '%s' "$Array" |
  #   command tr '\n' "${Separator}" |
  #   command sed -Ere "$Escaped")

  RetVal=$(printf '%s' "$Array" |
    command sed -Ere "\$!N; s~(\n)+~${Separator} ~g" |
    command sed -Ere "$Escaped")

  printf '%s\n' "$RetVal"

  # local IFS="$1"
  # shift
  # printf '%s' "$*"
}

# ArraySplit
#
# Split the given string by the given separator and return an array.
#
# $1: Separator
# $2: String
#
ArraySplit() {
  local -r Separator="${1:-([[:space:]|,|[:space:],|,[:space:]|[:space:],[:space:]|\n])}"
  local -r String="${2:-}"
  local RetVal=

  RetVal=$(
    printf '%s\n' "$String" | command sed -Ere "\$!N; s~${Separator}+~\n~g"
  )

  printf '%s\n' "$RetVal" | ArrayElementEncode

  # IFS=
  # read -d "$1" -r "$2" || [ -n "${!2}" ];
}

# ArrayPeek
#
# Peek at a value from the end of the provided array without removing it.
#
# $1: Array
#
ArrayPeek() {
  local -r Array="${1:-}"
  local Len=
  local LastElement=

  Len="$(printf '%s\n' "$Array" | ArrayLen)"
  LastElement=$(printf '%s\n' "$Array" | ArrayNth $((Len - 1)))

  # local Index=
  # Index="$(( $(ArrayCount "${Array}") - 1 ))"
  # eval "printf \"\${${Array}ArrayIndex${Index}}\"" || :

  printf '%s\n' "$LastElement"
}

# IsArray
#
# Determines if the given argument is an array.
#
# $1: Argument
#
IsArray() {
  local -r Argument="${1:-}"

  return 0
}

# ArrayPop
#
# Pops the latest item from an array
#
# $1: Array
#
# Pass your array as the first argument:
#   ArrayPop "$Array"
#
ArrayPop() {
  local -r Array="${1:-}"
  local Len=
  local LastElement=

  Len="$(printf '%s\n' "$Array" | ArrayLen)"
  LastElement=$(printf '%s\n' "$Array" | ArrayNth $((Len - 1)))

  # local Index=
  # Index="$(( $(ArrayCount "${Array}") - 1 ))"

  # eval "printf \"\${${Array}ArrayIndex${Index}}\"" || :
  # eval "${Array}ArrayCount=${Index}" || :
  # eval "unset ${Array}ArrayIndex${Index}" || :

  printf '%s\n' "$Array" | ArrayRemove "$LastElement"
}

# ArrayDefine
#
# Define a variable as an empty array.
#
# The variable should already exist in the scope you want to use it in.
#
# $1: Array
##
ArrayDefine() {
  local -r Array="${1:-}"
  eval "${Array}ArrayCount=0" || :
}

# ArrayCount
#
# Get the current amount of entries inside an existing array.
#
# $1: Array
##
ArrayCount() {
  local -r Array="${1:-}"
  eval "printf \"\${${Array}ArrayCount}\"" || :
}

# ArrayPush
#
# Push a given value to the end of the provided array.
#
# $1: Array
# $2: Value
##
ArrayPush() {
  local Array="${1:-}"
  local Value="${2:-}"
  local Index=

  Index="$(ArrayCount "${Array}")"

  eval "${Array}ArrayIndex${Index}='${Value}'" || :
  # eval "${Array}ArrayCount=$(( ${Index} + 1 ))" || :
  eval "${Array}ArrayCount=$(( Index + 1 ))" || :
}

#
# endregion Array

# region Aliases
#

# alias ArrayPush=ArrayAppend

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
