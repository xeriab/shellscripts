#!/usr/bin/env zsh
#
# shellcheck disable=SC3043
#
# ./lib/compat/zsh/array.zsh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# # shellcheck disable=SC1090 disable=SC1091
# . "$XDG_DATA_HOME/shellscripts/lib/string.sh"

#
# endregion Imports

# region Globals
#

# shellcheck disable=SC2034
OLD_SET=$-

# shellcheck disable=SC2034
SAVEIFS=$IFS

#
# endregion Globals

# region Array
#

# ArrayDefine
#
# Define a variable as an empty array.
#
# The variable should already exist in the scope you want to use it in.
#
# $1: Array
#
ArrayDefine() {
  local -r Array="${1:-}"
  eval "${Array}=()" || :
}

# ArrayCount
#
# Get the current amount of entries inside an existing array.
#
# $1: Array
#
ArrayCount() {
  local -r Array="${1:-}"

  if [ -z "$(eval "printf '%s\n' \"\${${Array}}\"" || :)" ]; then
    printf '%d\n' "0"
  else
    eval "printf '%s\n' \"\${#${Array}[@]}\"" || :
  fi
}

# ArrayGet
#
# Get a certain index stored inside an array.
#
# The index is zero based. The first entry entry is therefore supposed to be 0.
# If the used shell does only provide 1-based arrays this needs to be mapped
# inside of this function accordingly.
#
# $1: Array
# $2: Index
#
ArrayGet() {
  local -r Array="${1:-}"
  local -r Index="$((${2} + 1))"

  eval "printf '%s\n' \"\${${Array}[${Index}]}\"" || :
}

# ArraySet
#
# Set a certain value stored to a certain index inside an array.
#
# The index is zero based. The first entry entry is therefore supposed to be 0.
# If the used shell does only provide 1-based arrays this needs to be mapped
# inside of this function accordingly.
#
# $1: Array
# $2: Index
# $3: Value
#
ArraySet() {
  local -r Array="${1:-}"
  local -r Index="$((${2} + 1))"
  local -r Value="${3:-}"

  eval "${Array}[${Index}]=\"${Value}\"" || :
}

# ArrayPush
#
# Push a given value to the end of the provided array.
#
# $1: Array
# $2: Value
#
ArrayPush() {
  local -r Array="${1:-}"
  local -r Value="${2:-}"
  local Index=

  Index="$(($(eval "printf '%s\n' \${#${Array}[@]}") + 1))"

  eval "${Array}[${Index}]=\"${Value}\"" || :
}

# ArrayPeek
#
# Peek at a value from the end of the provided array without removing it.
#
# $1: Array
#
ArrayPeek() {
  local -r Array="${1:-}"

  eval "printf '%s\n' \"\${${Array}[\${#${Array}[@]}]}\"" || :
}

# ArrayPop
#
# Pop a value from the end of the provided array name.
#
# $1: Array
#
ArrayPop() {
  local -r Array="${1:-}"
  local Index=

  Index="$(eval "printf '%s\n' \${#${Array}[@]}" || :)"

  printf '%s\n' "$(ArrayPeek "${Array}")"
  ArrayUnset "${Array}" "$((${Index} - 1))"
}

# ArrayUnset
#
# Unset a certain index from the given array.
#
# $1: Array
#
ArrayUnset() {
  local -r Array="${1:-}"
  local -r Index="$((${2} + 1))"

  eval "${Array}[${Index}]=()" || :
}

#
# endregion Array

#  vim: set ts=2 sw=2 tw=80 noet :
