#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./lib/global.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Global
#

# region Mutables
#

# shellcheck disable=SC2034
OLD_SET=$-

# shellcheck disable=SC2034
SAVEIFS=$IFS

# shellcheck disable=SC2034
[ -z "$DEBUG" ] && DEBUG=False

# shellcheck disable=SC2034
[ -z "$DEBUGGABLE" ] && DEBUGGABLE=False

# shellcheck disable=SC2034
[ -z "$SHELLSCRIPTS_PATH" ] && export SHELLSCRIPTS_PATH="${HOME}/.local/share/shellscripts"

# shellcheck disable=SC2034
[ -z "$SCRIPT" ] && SCRIPT=$(basename "$0" .sh)

# shellcheck disable=SC2034
[ -z "$CURRENT_DIR" ] && CURRENT_DIR=$(pwd)

# shellcheck disable=SC2034
[ -z "$PARENT_DIR" ] && PARENT_DIR=$(realpath "${CURRENT_DIR}/../")

# shellcheck disable=SC2034
# CURRENT_SHELL=$(printf "$SHELL" | awk -F'/' '{ print $NF }')
# CURRENT_SHELL=$(ps -p $$ -oargs= | awk -F'/' '{ print $NF }')
[ -z "$CURRENT_SHELL" ] && CURRENT_SHELL="$(ps -p $$ -ocomm=)"

# shellcheck disable=SC2034
[ -z "$TEMP_DIR" ] && TEMP_DIR="/tmp/${Self:-$SCRIPT}-tmp"

# shellcheck disable=SC2034
[ -z "$ENABLE_OUTFILE" ] && ENABLE_OUTFILE=False

# shellcheck disable=SC2034
[ -z "$OUT_FILE" ] && OUT_FILE="${TEMP_DIR}/${Self:-$SCRIPT}.log"

# shellcheck disable=SC2034
[ -z "$SOURCED_SCRIPTS_LIST" ] && SOURCED_SCRIPTS_LIST=

# shellcheck disable=SC2034
[ -z "$CURRENT_YEAR" ] && CURRENT_YEAR=$(date +"%Y")

# shellcheck disable=SC2034
[ -z "$MISSING_DEPENDENCY" ] && MISSING_DEPENDENCY=False

# shellcheck disable=SC2034
[ -z "$QUESTION_CHOICE" ] && QUESTION_CHOICE=

# shellcheck disable=SC2034
[ -z "$EMPTY_STR" ] && EMPTY_STR=''

#
# endregion Mutables

# region Functions
#

# Exit
#
# Custom exit function, it integrates AskDeleteTempFiles for managing script
# created files and folders.
#
# $1: Message
# $2: Code
#
Exit() {
  local Argc=$#

  local Message="${1:-}"
  [ $# -gt 1 ] && shift

  local Code="${1:-0}"
  [ $# -gt 1 ] && shift

  IFS=$SAVEIFS

  [ $Argc -gt 1 ] && printf "$Message" "$@" >&2
  [ $Argc -eq 0 ] && exit

  exit "$Code"
}

# IsDebuggable
#
IsDebuggable() {
  [ -n "$DEBUG" ] && {
    [ $DEBUG = 1 ] || [ $DEBUG = True ] || [ $DEBUG = true ] ||
      [ $DEBUG = On ] || [ $DEBUG = ON ] || [ $DEBUG = on ] && return 0
  } || [ -n "$DEBUGGABLE" ] && {
      [ $DEBUGGABLE = 1 ] || [ $DEBUGGABLE = True ] || [ $DEBUGGABLE = true ] ||
    [ $DEBUGGABLE = On ] || [ $DEBUGGABLE = ON ] || [ $DEBUGGABLE = on ] && return 0
  }

  return 1
}

# Timestamp
#
Timestamp() {
  command date +"%s"
}

# WriteToFile
#
# If the appropriate variables are set, this function writes output to file.
#
WriteToFile() {
  [ -n "$ENABLE_OUTFILE" ] && {
    [ $ENABLE_OUTFILE = 1 ] || [ $ENABLE_OUTFILE = True ] || [ $ENABLE_OUTFILE = true ] && {
      [ -n "$OUT_FILE" ] && [ -n "$1" ] && {
        printf '%s\n' "$1" >>"$OUT_FILE"
      }
    }
  }

  return 0
}

# IsAsh
#
IsAsh() {
  [ "$(command ps -p $$ -ocomm=)" = "ash" ] || [ "${0##*/}" = ash ]
}

# IsDash
#
IsDash() {
  [ "$(command ps -p $$ -ocomm=)" = "dash" ] || [ "${0##*/}" = dash ]
}

# IsBash
#
IsBash() {
  [ "$(command ps -p $$ -ocomm=)" = "bash" ] || [ -n "${BASH}" ] || [ -n "${BASH_VERSION}" ]
}

# IsZsh
#
IsZsh() {
  [ "$(command ps -p $$ -ocomm=)" = "zsh" ] || [ -n "${ZSH_NAME}" ] || [ -n "${ZSH_VERSION}" ]
}

# IsKsh
#
IsKsh() {
  [ "$(command ps -p $$ -ocomm=)" = "ksh" ] || [ -n "${TMOUT}" ]
}

# IsCsh
#
IsCsh() {
  [ "$(command ps -p $$ -ocomm=)" = "csh" ]
}

# IsTcsh
#
IsTcsh() {
  [ "$(command ps -p $$ -ocomm=)" = "tcsh" ]
}

# IsUrl
#
IsUrl() {
  local UrlRegex='(https?|ftp|file)://[-A-Za-z0-9\+&@#/%?=~_|!:,.;]*[-A-Za-z0-9\+&@#/%=~_|]'
  (printf '%s\n' "${1:-}" | command grep -Eie "$UrlRegex" -o) 1>/dev/null 2>&1
  return $?
}

# IsEmpty
#
# Checks if the given value is empty.
#
# $1: Value
#
IsEmpty() {
  local -r Value="${1:-}"

  [ -n "$Value" ] && {
    [ "$Value" = '' ] || [ -z "$Value" ]
  }
}

# IsInteger
#
# Checks if the given value is an integer.
#
# $1: Value
#
IsInteger() {
  local -r Value="${1#[-+]}"

  case $Value in
  *[!0123456789]*) return 1 ;;
  '') return 1 ;;
  *) return 0 ;;
  esac

  # command expr "X$Value" : "X-\{0,1\}[0-9][0-9]*$" > /dev/null
}

# IsUnsignedInteger
#
# Checks if the given value is an unsigned integer.
#
# $1: Value
#
IsUnsignedInteger() {
  local -r Value="${1:-}"

  case $Value in
  *[!0123456789]*) return 1 ;;
  '') return 1 ;;
  *) return 0 ;;
  esac
}

# IsNumber
#
# Checks if the given value is number.
#
# $1: Value
#
IsNumber() {
  local -r Value="${1#[-+]}"

  case $Value in
  '' | . | *[!0123456789.]* | *.*.*) return 1 ;;
  *) return 0 ;;
  esac
}

# IsTrue
#
IsTrue() {
  if [ "$1" ]; then
    case ${1:-} in
    1 | [Yy][Ee][Ss] | [Tt][Rr][Uu][Ee] | [Oo][Nn]) return 0 ;;
    *) return 1 ;;
    esac
  fi
}

# IsFalse
#
IsFalse() {
  if [ "$1" ]; then
    case ${1:-} in
    0 | [No][Oo] | [Ff][Aa][Ll][Ss][Ee] | [Oo][Ff][Ff]) return 0 ;;
    *) return 1 ;;
    esac
  fi
}

# IsBool
#
IsBool() {
  [ "$1" ] && {
    (IsTrue "$1") || (IsFalse "$1")
  }
}

# IsFile
#
# $1: Path.
#
IsFile() {
  [ "$1" ] && [ -f "$1" ]
}

# IsDir
#
# $1: Path.
#
IsDir() {
  [ "$1" ] && [ -d "$1" ]
}

# IsReadable
#
# $1: Path.
#
IsReadable() {
  [ "$1" ] && [ -r "$1" ]
}

# IsWritable
#
# $1: Path.
#
IsWritable() {
  [ "$1" ] && [ -w "$1" ]
}

# IsExecutable
#
# $1: Path.
#
IsExecutable() {
  [ "$1" ] && (IsFile "$1") && [ -x "$1" ]
}

# IsLink
#
# $1: Path.
#
IsLink() {
  [ "$1" ] && [ -L "$1" ]
}

# IsArchive
#
# Checks if the file given as argument is an elegible archive.
#
# $1: File
#
IsArchive() {
  local -r File="${1:-}"
  (command file "$File" | command grep 'compressed\|archive') 1>/dev/null 2>&1
  return $?
}

# IsValidIdentifier
#
# Checks if the given value is a valid identifier.
#
# $1: Value
#
IsValidIdentifier() {
  local -r Value="${1:-}"

  case $Value in
  *[!a-zA-Z0-9_]*)
    return 1
    ;;
  esac

  return 0
}

# IsCommand
#
# Checks if the given name is a command.
#
# $1: Name
#
IsCommand() {
  local -r Name="${1:-}"
  (command -v "$Name") 1>/dev/null 2>&1
}

# IsAlias
#
# Checks if the given name is an alias.
#
# $1: Name
#
IsAlias() {
  local -r Name="${1:-}"
  (command alias "$Name") 1>/dev/null 2>&1
}

# IsBuiltin
#
# Checks if the given name is a builtin command.
#
# $1: Name
#
IsBuiltin() {
  local -r Name="${1:-}"
  (builtin "$Name") 1>/dev/null 2>&1
}

# IsCallable
#
# Checks if the given name is a command, function, or alias.
#
# $1: Name
#
IsCallable() {
  local -r Name="${1:-}"
  (IsCommand "$Name") || (IsAlias "$Name") || (IsBuiltin "$Name") || return 1
}

# IsNull
#
# Checks if the given value is null.
#
# $1: Value
#
IsNull() {
  local -r Value="${1:-}"
  [ "$Value" = '' ] || [ -z "$Value" ]
}

# SedEscape
#
# Escape a value for use in sed
#
SedEscape() {
  # printf "%s" "$1" | sed -e 's/[\/&]/\\&/g';
  printf '%s\n' "${1:-}" | command sed -Ere 's~[\/&]~\\&~g'
}

# Sequence
#
# $1: Start
# $2: End
#
Sequence() {
  local Start="${1:-}"
  local -r End="${2:-}"

  while [ "$Start" -le "$End" ]; do
    printf "$Start\n"
    Start=$((Start + 1))
  done
}

# Range
#
# $1: Start
# $2: End
#
Range() {
  local Start="${1:-}"
  local -r End="${2:-}"

  while [ "$Start" -le "$End" ]; do
    printf '%d\n' "${Start}"
    Start=$((Start + 1))
  done
}

# ShellVersion
#
# Get the current shell version.
#
ShellVersion() {
  local Version=

  if IsBash; then
    Version=$BASH_VERSION
  elif IsZsh; then
    Version=$ZSH_VERSION
  else
    Version='Unknown'
  fi

  printf '%s\n' "$Version"
}

# ShellName
#
ShellName() {
  [ -n "$BASH" ] && printf "${BASH##/*/}" && return
  [ -n "$ZSH_NAME" ] && printf "$ZSH_NAME" && return
  printf '%s\n' "${0##/*/}"
}

# SignalHandler
#
# $1: Message
#
SignalHandler() {
  local -r Message="${1:-}"

  # printf -- 'Interrupted.'
  # Log -i "Interrupted."

  [ -z "$Message" ] && {
    printf '%s\n' "Interrupted."
  } || printf '%s\n' "$Message"

  command trap - 2
  command kill -2 $$

  # Die 130
  exit 130
}

# OperationCanceled
#
# $1: Message
#
OperationCanceled() {
  local -r Message="${1:-}"

  clear

  # Log -i "\n Operation canceled by user, Bye!"

  [ -z "$Message" ] && {
    printf '%s\n' "\n Operation canceled by user, Bye!"
  } || printf '%s\n' "$Message"

  # Die 1

  exit 1
}

# CurrentScriptPath
#
CurrentScriptPath() {
  printf '%s\n' "$(eval 'echo $(readlink -f $0)')"
}

# Source
#
# $1: Script
# $2: Flag
#
Source() {
  local Script="${1:-}"
  local -r Flag="${2:-}"

  if [ -f "$Script" ] || [ -e "$Script" ]; then
    Script=$(realpath "$Script")
  else
    return 1
  fi

  # shellcheck disable=SC1090 disable=SC1091
  . "$Script" 1>/dev/null 2>&1

  SOURCED_SCRIPTS_LIST="$(printf "${SOURCED_SCRIPTS_LIST}\n${Script}")"

  if [ "$Flag" = "--debug" ] || [ "$Flag" = "-debug" ] || [ "$Flag" = "-d" ]; then
      IsDebuggable && {
        # Log -i "Script ${Faint}'${Reset}${FgBrightBlue}${Script}${Reset}${Faint}'${Reset} is sourced successfully!"
        printf '%s\n' "Script '${Script}' is sourced successfully!"
      }
  fi

  return 0
}

# GetSourcedScripts
#
GetSourcedScripts() {
  printf '%s\n' "$SOURCED_SCRIPTS_LIST"
}

# # IsSourced
# #
# IsSourced() {
# 	local Sourced=0

# 	if (IsBash); then
#     (return 0 2>/dev/null) && Sourced=1
# 	elif (IsKsh); then
#     local Lhs=
#     local Rhs=

#     # shellcheck disable=SC2296 disable=SC2046
#     Lhs="$(cd $(dirname -- "$0") && pwd -P)/$(basename -- "$0")"

#     # shellcheck disable=SC2296 disable=SC2046
#     Rhs="$(cd "$(dirname -- "${.sh.file}")" && pwd -P)/$(basename -- "${.sh.file}")"

#     ! [ "$Lhs" = "$Rhs" ] && Sourced=1

# 	elif (IsZsh); then
#     # shellcheck disable=SC2296
#     case $ZSH_EVAL_CONTEXT in
#       *:file) Sourced=1 ;;
#     esac

#     # shellcheck disable=SC3010
#     [[ $ZSH_EVAL_CONTEXT =~ :file$ ]] && Sourced=1
# 	else
#     # Detects `sh` and `dash`; add additional shell filenames as needed.
#     case ${0##*/} in
#       sh|dash) Sourced=1 ;;
#     esac
# 	fi

#   unset Lhs Rhs

#   return $Sourced
# }

# # IsSourced
# #
# IsSourced() {
# 	local Script=

# 	if (IsBash); then
#     # shellcheck disable=SC3028 disable=SC2128
#     Script=$BASH_SOURCE
# 	elif (IsKsh); then
#     # shellcheck disable=SC2296
#     Script=${.sh.file}
# 	elif (IsZsh); then
#     # shellcheck disable=SC2296
#     Script=${(%):-%x}
# 	elif (IsDash) || (IsAsh); then
#     local Temp=
#     Temp=$(command lsof -p $$ -Fn0 | command tail -1)
#     Script=${Temp#n}
# 	else
#     Script=$0
# 	fi

#   unset Script
#   unset Temp
# }

# GetCurrentScript
#
GetCurrentScript() {
  local RetVal=

  # shellcheck disable=SC3054 disable=SC3028
  case $(ShellName) in
  bash) RetVal=${BASH_SOURCE[0]} ;;
  zsh)
    emulate -L zsh
    # shellcheck disable=SC3054 disable=SC3028 disable=SC2154
    RetVal=${funcfiletrace[1]%:*}
    ;;
  dash | sh | ash)
    RetVal=$(
      command lsof -p $$ -Fn | command tail --lines=1 |
        command xargs --max-args=2 | command cut --delimiter=' ' --fields=2
    )
    RetVal=${RetVal#n}
    ;;
  *) RetVal=$0 ;;
  esac

  # shellcheck disable=SC2005
  printf "$(realpath "$RetVal")"
}

# UniqueId
#
# Adapted from https://unix.stackexchange.com/a/230676/215969
#
UniqueId() {
  local -r Length="${1:-32}"

  command head /dev/urandom |
    command tr -dc A-Za-z0-9 |
    command head -c "$Length"

  printf %b '\n'
}

#
# endregion Functions

# endregion Global

# region Aliases
#

alias Seq=Sequence
alias IsUInt=IsUnsignedInteger
alias IsValidID=IsValidIdentifier
alias UID=UniqueId

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
