#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./lib/logger.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/os.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/terminal.sh"

#
# endregion Imports

# region Logging
#

# shellcheck disable=SC2034
[ -z "$LOG_ENABLE_DATETIME" ] && LOG_ENABLE_DATETIME=False

# shellcheck disable=SC2034
[ -z "$LOG_ENABLE_FULL_NAME" ] && LOG_ENABLE_FULL_NAME=True

# shellcheck disable=SC2034
[ -z "$LOG_ENABLE_TITLE" ] && LOG_ENABLE_TITLE=True

# shellcheck disable=SC2034
[ -z "$LOG_IS_DECORATED" ] && LOG_IS_DECORATED=True

# shellcheck disable=SC2034
[ -z "$LOG_ENABLE_BOLD_TITLE" ] && LOG_ENABLE_BOLD_TITLE=True

# shellcheck disable=SC2034
[ -z "$LOG_ENABLE_FULL_PATH" ] && LOG_ENABLE_FULL_PATH=False

# shellcheck disable=SC2034
[ -z "$LOG_ENABLE_FILE_NAME" ] && LOG_ENABLE_FILE_NAME=False

# shellcheck disable=SC2034
[ -z "$LOG_ENABLE_SHORT_LABEL" ] && LOG_ENABLE_SHORT_LABEL=True

# shellcheck disable=SC2034
[ -z "$LOG_ENABLE_SHORT_LABEL_BACKGROUND" ] && LOG_ENABLE_SHORT_LABEL_BACKGROUND=False

# shellcheck disable=SC2034
[ -z "$LOGGING_DEVICE" ] && LOGGING_DEVICE="/dev/stdout"

#
# endregion Logging

# shellcheck disable=SC2034
[ -z "$ENABLE_OUTFILE" ] && ENABLE_OUTFILE=False

# region Logger
#

# IsLogTitleEnabled
#
IsLogTitleEnabled() {
  [ -n "$LOG_ENABLE_TITLE" ] && {
    [ "$LOG_ENABLE_TITLE" = True ] || [ "$LOG_ENABLE_TITLE" = 1 ] || [ "$LOG_ENABLE_TITLE" = true ]
  }
}

# IsLogBoldTitleEnabled
#
IsLogBoldTitleEnabled() {
  [ -n "$LOG_ENABLE_BOLD_TITLE" ] && {
    [ "$LOG_ENABLE_BOLD_TITLE" = True ] || [ "$LOG_ENABLE_BOLD_TITLE" = 1 ] || [ "$LOG_ENABLE_BOLD_TITLE" = true ]
  }
}

# IsLogShortLabelEnabled
#
IsLogShortLabelEnabled() {
  [ -n "$LOG_ENABLE_SHORT_LABEL" ] && {
    [ "$LOG_ENABLE_SHORT_LABEL" = True ] || [ "$LOG_ENABLE_SHORT_LABEL" = 1 ] || [ "$LOG_ENABLE_SHORT_LABEL" = true ]
  }
}

# IsLogShortLabelBackgroundEnabled
#
IsLogShortLabelBackgroundEnabled() {
  [ -n "$LOG_ENABLE_SHORT_LABEL_BACKGROUND" ] && {
    [ "$LOG_ENABLE_SHORT_LABEL_BACKGROUND" = True ] || [ "$LOG_ENABLE_SHORT_LABEL_BACKGROUND" = 1 ] || [ "$LOG_ENABLE_SHORT_LABEL_BACKGROUND" = true ]
  }
}

# IsLogFileNameEnabled
#
IsLogFileNameEnabled() {
  [ -n "$LOG_ENABLE_FILE_NAME" ] && {
    [ "$LOG_ENABLE_FILE_NAME" = True ] || [ "$LOG_ENABLE_FILE_NAME" = 1 ] || [ "$LOG_ENABLE_FILE_NAME" = true ]
  }
}

# IsLogFullPathEnabled
#
IsLogFullPathEnabled() {
  [ -n "$LOG_ENABLE_FULL_PATH" ] && {
    [ "$LOG_ENABLE_FULL_PATH" = True ] || [ "$LOG_ENABLE_FULL_PATH" = 1 ] || [ "$LOG_ENABLE_FULL_PATH" = true ]
  }
}

# IsLogDeocrated
#
IsLogDeocrated() {
  [ -n "$LOG_IS_DECORATED" ] && {
    [ "$LOG_IS_DECORATED" = True ] || [ "$LOG_IS_DECORATED" = 1 ] || [ "$LOG_IS_DECORATED" = true ]
  }
}

# IsLogFullNameEnabled
#
IsLogFullNameEnabled() {
  [ -n "$LOG_ENABLE_FULL_NAME" ] && {
    [ "$LOG_ENABLE_FULL_NAME" = True ] || [ "$LOG_ENABLE_FULL_NAME" = 1 ] || [ "$LOG_ENABLE_FULL_NAME" = true ]
  }
}

# IsLogDateTimeEnabled
#
IsLogDateTimeEnabled() {
  [ -n "$LOG_ENABLE_DATETIME" ] && {
    [ "$LOG_ENABLE_DATETIME" = True ] || [ "$LOG_ENABLE_DATETIME" = 1 ] || [ "$LOG_ENABLE_DATETIME" = true ]
  }
}

# EchoStdErr
#
# Echo to stderr. Useful for printing script usage information.
#
EchoStdErr() {
  printf >&2 "$@"
}

# SimpleLog
#
# Log the given message at the given level. All logs are written to stderr with a timestamp.
#
SimpleLog() {
  local -r Level="$1"
  local -r Message="$2"
  local -r Timestamp=
  local -r ScriptName="$(basename "$0")"

  Timestamp=$(date +"%Y-%m-%d %H:%M:%S")

  EchoStdErr "${Timestamp} [${Level}] [$ScriptName] ${Message}"
}

# SimpleLogInfo
#
# Log the given message at INFO level. All logs are written to stderr with a timestamp.
#
SimpleLogInfo() {
  local -r Message="$1"
  SimpleLog "INFO" "$Message"
}

# SimpleLogWarn
#
# Log the given message at WARN level. All logs are written to stderr with a timestamp.
#
SimpleLogWarn() {
  local -r Message="$1"
  SimpleLog "WARN" "$Message"
}

# SimpleLogError
#
# Log the given message at ERROR level. All logs are written to stderr with a timestamp.
#
SimpleLogError() {
  local -r Message="$1"
  SimpleLog "ERROR" "$Message"
}

# LogMessage
#
# Prints Message.
#
# $1: Message    Log message.
# $2: Color      Message color.
#
LogMessage() {
  local Color="${2:-$FgCyan}"

  printf "${Color}$*${Reset}"

  ! [ "$LOGGING_DEVICE" = '/dev/stdout' ] && printf "$1" >>"$LOGGING_DEVICE"
}

# LogSuccess
#
# Prints Success Message.
#
# $1: Message    Log message.
#
LogSuccess() {
  [ -n "$ENABLE_OUTFILE" ] && {
    [ $ENABLE_OUTFILE = 1 ] || [ $ENABLE_OUTFILE = True ] || [ $ENABLE_OUTFILE = true ] && {
      [ -n "$OUT_FILE" ] && return
    }
  }

  local LeftPart=
  local RightPart=
  local Label=
  local FileName=
  local LogTitle=

  local Message="${1:-}"
  [ $# -gt 1 ] && shift

  LeftPart="${Faint}${FgBrightBlack}[${Reset}"
  RightPart="${Faint}${FgBrightBlack}]${Reset}"

  # IsLogShortLabelEnabled && Label="✓" || Label="OK"
  IsLogShortLabelEnabled && Label="✔" || Label="OK"

  Label="${FgBrightGreen}${Label}${Reset}"

  IsLogShortLabelBackgroundEnabled && {
    Label="${BgBlack}${LeftPart}${Reset}${BgBlack}${Label}${Reset}${BgBlack}${RightPart}${Reset}"
  } || Label="${LeftPart}${Label}${RightPart}"

  IsLogFileNameEnabled && {
    FileName="$(FormatLink "$(basename "$(CurrentScriptPath)")" "file://$(CurrentScriptPath)")"
  }

  IsLogTitleEnabled && {
    local Title=
    IsLogBoldTitleEnabled && Title="${Bold}"
    LogTitle="${FgBrightMagenta}${Title}${Self:-$SCRIPT}${Reset}${FgBrightBlack}${Faint}:${Reset}"
    unset Title
  }

  ! [ "$FileName" = "" ] && FileName="${Faint}${FgBrightBlack}[${Reset}${Faint}${FileName}${Reset}${Faint}${FgBrightBlack}]${Reset} "
  ! [ "$LogTitle" = "" ] && LogTitle="${LogTitle} " || LogTitle="${LogTitle} "

  printf "${Label} ${LogTitle}${FgGreen}${Message}${Normal}" "$@"

  unset LeftPart
  unset RightPart
  unset Message
  unset Label
  unset FileName
  unset LogTitle

  # printf %b "\n" 2>&1
  # printf %b "\n" >&2
  printf %b "\n" >&1
}

# LogInfo
#
# Prints Information Message.
#
# $1: Message    Log message.
#
LogInfo() {
  [ -n "$ENABLE_OUTFILE" ] && {
    [ $ENABLE_OUTFILE = 1 ] || [ $ENABLE_OUTFILE = True ] || [ $ENABLE_OUTFILE = true ] && {
      [ -n "$OUT_FILE" ] && return
    }
  }

  local LeftPart=
  local RightPart=
  local Label=
  local FileName=
  local LogTitle=

  local Message="${1:-}"
  [ $# -gt 1 ] && shift

  LeftPart="${Faint}${FgBrightBlack}[${Reset}"
  RightPart="${Faint}${FgBrightBlack}]${Reset}"

  # IsLogShortLabelEnabled && Label="🛈" || Label="INF"
  IsLogShortLabelEnabled && Label="i" || Label="INF"

  Label="${FgBrightBlue}${Label}${Reset}"

  IsLogShortLabelBackgroundEnabled && {
    Label="${BgBlack}${LeftPart}${Reset}${BgBlack}${Label}${Reset}${BgBlack}${RightPart}${Reset}"
  } || Label="${LeftPart}${Label}${RightPart}"

  IsLogFileNameEnabled && {
    FileName="$(FormatLink "$(basename "$(CurrentScriptPath)")" "file://$(CurrentScriptPath)")"
  }

  IsLogTitleEnabled && {
    local Title=
    IsLogBoldTitleEnabled && Title="${Bold}"
    LogTitle="${FgBrightMagenta}${Title}${Self:-$SCRIPT}${Reset}${FgBrightBlack}${Faint}:${Reset}"
    unset Title
  }

  ! [ "$FileName" = "" ] && FileName="${Faint}${FgBrightBlack}[${Reset}${Faint}${FileName}${Reset}${Faint}${FgBrightBlack}]${Reset} "
  ! [ "$LogTitle" = "" ] && LogTitle="${LogTitle} " || LogTitle="${LogTitle} "

  printf "${Label} ${LogTitle}${FgBlue}${Message}${Normal}" "$@"

  unset LeftPart
  unset RightPart
  unset Message
  unset Label
  unset FileName
  unset LogTitle

  # printf %b "\n" 2>&1
  # printf %b "\n" >&2
  printf %b "\n" >&1
}

# LogFailure
#
# Prints Failure Message.
#
# $1: Message    Log message.
#
LogFailure() {
  [ -n "$ENABLE_OUTFILE" ] && {
    [ $ENABLE_OUTFILE = 1 ] || [ $ENABLE_OUTFILE = True ] || [ $ENABLE_OUTFILE = true ] && {
      [ -n "$OUT_FILE" ] && return
    }
  }

  local LeftPart=
  local RightPart=
  local Label=
  local FileName=
  local LogTitle=

  local Message="${1:-}"
  [ $# -gt 1 ] && shift

  LeftPart="${Faint}${FgBrightBlack}[${Reset}"
  RightPart="${Faint}${FgBrightBlack}]${Reset}"

  # IsLogShortLabelEnabled && Label="𐄂" || Label="KO"
  IsLogShortLabelEnabled && Label="✖" || Label="KO"

  Label="${FgBrightRed}${Label}${Reset}"

  IsLogShortLabelBackgroundEnabled && {
    Label="${BgBlack}${LeftPart}${Reset}${BgBlack}${Label}${Reset}${BgBlack}${RightPart}${Reset}"
  } || Label="${LeftPart}${Label}${RightPart}"

  IsLogFileNameEnabled && {
    FileName="$(FormatLink "$(basename "$(CurrentScriptPath)")" "file://$(CurrentScriptPath)")"
  }

  IsLogTitleEnabled && {
    local Title=
    IsLogBoldTitleEnabled && Title="${Bold}"
    LogTitle="${FgBrightMagenta}${Title}${Self:-$SCRIPT}${Reset}${FgBrightBlack}${Faint}:${Reset}"
    unset Title
  }

  ! [ "$FileName" = "" ] && FileName="${Faint}${FgBrightBlack}[${Reset}${Faint}${FileName}${Reset}${Faint}${FgBrightBlack}]${Reset} "
  ! [ "$LogTitle" = "" ] && LogTitle="${LogTitle} " || LogTitle="${LogTitle} "

  printf "${Label} ${LogTitle}${FgRed}${Message}${Normal}" "$@"

  unset LeftPart
  unset RightPart
  unset Message
  unset Label
  unset FileName
  unset LogTitle

  # printf %b "\n" 1>&2
  printf %b "\n" >&2
}

# LogFail
#
# Prints Failure Message.
#
# $1: Message    Log message.
#
LogFail() {
  local LeftPart=
  local RightPart=
  local Label=
  local FileName=
  local LogTitle=

  local Message="${1:-}"
  [ $# -gt 1 ] && shift

  # printf "${Self:-$SCRIPT}: $2(): $1" >&2

  [ -n "$ENABLE_OUTFILE" ] && {
    [ $ENABLE_OUTFILE = 1 ] || [ $ENABLE_OUTFILE = True ] || [ $ENABLE_OUTFILE = true ] && {
      [ -n "$OUT_FILE" ] && return
    }
  }

  LeftPart="${Faint}${FgBrightBlack}[${Reset}"
  RightPart="${Faint}${FgBrightBlack}]${Reset}"

  # IsLogShortLabelEnabled && Label="𐄂" || Label="KO"
  IsLogShortLabelEnabled && Label="✖" || Label="KO"

  Label="${FgBrightRed}${Label}${Reset}"

  IsLogShortLabelBackgroundEnabled && {
    Label="${BgBlack}${LeftPart}${Reset}${BgBlack}${Label}${Reset}${BgBlack}${RightPart}${Reset}"
  } || Label="${LeftPart}${Label}${RightPart}"

  IsLogFileNameEnabled && {
    FileName="$(FormatLink "$(basename "$(CurrentScriptPath)")" "file://$(CurrentScriptPath)")"
  }

  IsLogTitleEnabled && {
    local Title=
    IsLogBoldTitleEnabled && Title="${Bold}"
    LogTitle="${FgBrightMagenta}${Title}${Self:-$SCRIPT}${Reset}${FgBrightBlack}${Faint}:${Reset}"
    unset Title
  }

  ! [ "$FileName" = "" ] && FileName="${Faint}${FgBrightBlack}[${Reset}${Faint}${FileName}${Reset}${Faint}${FgBrightBlack}]${Reset} "
  ! [ "$LogTitle" = "" ] && LogTitle="${LogTitle} " || LogTitle="${LogTitle} "

  printf "${Label} ${LogTitle}${FgRed}${Message}${Normal}" "$@"

  unset LeftPart
  unset RightPart
  unset Message
  unset Label
  unset FileName
  unset LogTitle

  # printf %b "\n" 1>&2
  printf %b "\n" >&2

  exit 1
}

# LogWarn
#
# Prints Warning Message.
#
# $1: Message    Log message.
#
LogWarn() {
  local LeftPart=
  local RightPart=
  local Label=
  local FileName=
  local LogTitle=

  local Message="${1:-}"
  [ $# -gt 1 ] && shift

  # printf "${Self:-$SCRIPT}: $2(): $1" >&2

  [ -n "$ENABLE_OUTFILE" ] && {
    [ $ENABLE_OUTFILE = 1 ] || [ $ENABLE_OUTFILE = True ] || [ $ENABLE_OUTFILE = true ] && {
      [ -n "$OUT_FILE" ] && return
    }
  }

  LeftPart="${Faint}${FgBrightBlack}[${Reset}"
  RightPart="${Faint}${FgBrightBlack}]${Reset}"

  # IsLogShortLabelEnabled && Label="🛈" || Label="!"
  IsLogShortLabelEnabled && Label="⚠" || Label="!"

  Label="${FgBrightYellow}${Label}${Reset}"

  IsLogShortLabelBackgroundEnabled && {
    Label="${BgBlack}${LeftPart}${Reset}${BgBlack}${Label}${Reset}${BgBlack}${RightPart}${Reset}"
  } || Label="${LeftPart}${Label}${RightPart}"

  IsLogFileNameEnabled && {
    FileName="$(FormatLink "$(basename "$(CurrentScriptPath)")" "file://$(CurrentScriptPath)")"
  }

  IsLogTitleEnabled && {
    local Title=
    IsLogBoldTitleEnabled && Title="${Bold}"
    LogTitle="${FgBrightMagenta}${Title}${Self:-$SCRIPT}${Reset}${FgBrightBlack}${Faint}:${Reset}"
    unset Title
  }

  ! [ "$FileName" = "" ] && FileName="${Faint}${FgBrightBlack}[${Reset}${Faint}${FileName}${Reset}${Faint}${FgBrightBlack}]${Reset} "
  ! [ "$LogTitle" = "" ] && LogTitle="${LogTitle} " || LogTitle="${LogTitle} "

  printf "${Label} ${LogTitle}${FgYellow}${Message}${Normal}" "$@"

  unset LeftPart
  unset RightPart
  unset Message
  unset Label
  unset FileName
  unset LogTitle

  # printf %b "\n" 1>&2
  printf %b "\n" >&2

  exit 1
}

# LogError
#
# Prints Error Message.
#
# $1: Message    Log message.
#
LogError() {
  LogFailure "$@"
}

# LogWarning
#
# Prints Warning Message.
#
# $1: Message    Log message.
#
LogWarning() {
  LogFailure "$@"
}

# LogFailWithExit
#
# Prints Error Message.
#
# $1: Place      The file name.
# $2: Message    Log message.
# $3: Where      Function name.
#
LogFailWithExit() {
  local Place="${1:-}"
  local Message="${2:-}"
  local Where="${3:-${Self:-$SCRIPT}}()"

  printf "${FgBrightMagenta}${Place}${Reset}${Faint}:${Reset} ${FgBrightBlue}${Where}${Reset}${Faint}:${Reset} ${FgRed}${Message}${Reset}" >&2
  printf %b "\n" >&2

  unset Place Message Where

  exit 1
}

# Log
#
# Custom print/echo function that handles printing on screen.
#
# $1: Type       Log type.
# $2: Message    Log message.
# $@: Arguments  Log arguments.
#
Log() {
  [ -n "$ENABLE_OUTFILE" ] && {
    [ $ENABLE_OUTFILE = 1 ] || [ $ENABLE_OUTFILE = True ] || [ $ENABLE_OUTFILE = true ] && {
      [ -n "$OUT_FILE" ] && return
    }
  }

  local Type="${1:-raw}"
  [ $# -gt 1 ] && shift

  local Message="${1:-}"
  [ $# -gt 1 ] && shift

  local DateTime=""
  local LogName=""
  local LogTitle=""
  local FullPath=""
  local FileName=""

  Message="${Normal}${Message}${Reset}"

  IsLogDateTimeEnabled && {
    DateTime="$(date +"${FgBrightBlack}${Faint}%Y-%m-%d %H:%M:%S%z${Reset}") "
  }

  IsLogTitleEnabled && {
    local Title=
    IsLogBoldTitleEnabled && Title="${Bold}"
    LogTitle="${FgBrightMagenta}${Title}${Self:-$SCRIPT}${Reset}${FgBrightBlack}${Faint}:${Reset}"
    unset Title
  }

  IsLogFullPathEnabled && {
    FullPath="$(FormatLink "$(CurrentScriptPath)" "file://$(CurrentScriptPath)")"
  }

  IsLogFileNameEnabled && {
    FileName="$(FormatLink "$(basename "$(CurrentScriptPath)")" "file://$(CurrentScriptPath)")"
  }

  if IsLogFullNameEnabled; then
    case $Type in
    -a | -assert | --assert) LogName="${FgGreen}ASSERT${Reset}" ;;
    -d | -debug | --debug) LogName="${FgBrightGreen} DEBUG${Reset}" ;;
    -e | -error | --error) LogName="${FgBrightRed} ERROR${Reset}" ;;
    -F | -failure | --failure) LogName="${FgRed}    KO${Reset}" ;;
    -f | -fatal | --fatal) LogName="${FgRed} FATAL${Reset}" ;;
    -i | -info | --info) LogName="${FgBrightBlue}  INFO${Reset}" ;;
    -r | -raw | --raw) LogName="" ;;
    -s | -success | --success) LogName="${FgBrightCyan}    OK${Reset}" ;;
    -v | -verbose | --verbose) LogName="${FgBrightWhite}   LOG${Reset}" ;;
    -w | -warn | -warning | --warn | --warning) LogName="${FgBrightYellow}  WARN${Reset}" ;;
    *? | *) LogName="" ;;
    esac
  else
    case $Type in
    -a | -assert | --assert) LogName="${FgGreen}A${Reset}" ;;
    -d | -debug | --debug) LogName="${FgGreen}D${Reset}" ;;
    -e | -error | --error) LogName="${FgRed}E${Reset}" ;;
    -F | -failure | --failure) LogName="${FgRed}F${Reset}" ;;
    -f | -fatal | --fatal) LogName="${FgBrightRed}F${Reset}" ;;
    -i | -info | --info) LogName="${FgBlue}I${Reset}" ;;
    -r | -raw | --raw) LogName="" ;;
    -s | -success | --success) LogName="${FgCyan}S${Reset}" ;;
    -v | -verbose | --verbose) LogName="${FgBrightWhite}L${Reset}" ;;
    -w | -warn | -warning | --warn | --warning) LogName="${FgYellow}W${Reset}" ;;
    *? | *) LogName="" ;;
    esac
  fi

  ! [ "$FullPath" = "" ] && FullPath="${Faint}${FgBrightBlack}[${Reset}${FullPath}${Faint}${FgBrightBlack}]${Reset}"
  ! [ "$FileName" = "" ] && FileName="${Faint}${FgBrightBlack}[${Reset}${Faint}${FileName}${Reset}${Faint}${FgBrightBlack}]${Reset} "
  ! [ "$LogName" = "" ] && {
    local LeftPart=
    local RightPart=

    IsLogDeocrated && {
      LeftPart="${Faint}${FgBrightBlack}[${Reset}"
      RightPart="${Faint}${FgBrightBlack}]${Reset}"
    }

    LogName="${LeftPart}${LogName}${RightPart}"
    LogName=$(printf '%-7s' "$LogName")

    unset LeftPart RightPart
  }

  ! [ "$LogTitle" = "" ] && LogTitle=" ${LogTitle} " || LogTitle="${LogTitle} "

  case $Type in
  -d | -debug | --debug | -i | -info | --info | -s | -success | --success | \
    -v | -verbose | --verbose | -w | -warn | -warning | --warn | --warning)
    printf "${DateTime}${FullPath}${FileName}${LogName}${LogTitle}${Message}" "$@" 2>&1
    ;;
  -a | -assert | --assert)
    printf "${DateTime}${FullPath}${FileName}${LogName}${LogTitle}${Message}" "$@" 2>&1
    ;;
  -e | -error | --error | -F | -failure | --failure | -f | -fatal | --fatal)
    printf "${DateTime}${FullPath}${FileName}${LogName}${LogTitle}${Message}" "$@" 1>&2
    ;;
  -r | -raw | --raw)
    # printf "${LogTitle} ${Message}" "$@"
    printf "${Message}" "$@"
    return
    ;;
  *? | *)
    ! [ "$LogTitle" = "" ] && LogTitle=$(printf "$LogTitle" | command cut -c2-) || LogTitle=""
    Message=$Type
    printf "${DateTime}${FullPath}${FileName}${LogName}${LogTitle}${Message}" "$@" 2>&1
    ;;
  esac

  # printf %b "\n" 2>&1
  printf '%b' "\n" >&2

  return 0
}

#
# endregion Logger

#  vim: set ts=2 sw=2 tw=80 noet :
