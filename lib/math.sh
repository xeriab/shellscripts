#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./lib/math.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# # shellcheck disable=SC1090 disable=SC1091
# . "$XDG_DATA_HOME/shellscripts/lib/terminal.sh"

# # shellcheck disable=SC1090 disable=SC1091
# . "$XDG_DATA_HOME/shellscripts/lib/ansi.sh"

# SetupAnsi

# # shellcheck disable=SC1090 disable=SC1091
# . "$XDG_DATA_HOME/shellscripts/lib/string.sh"

#
# endregion Imports

# region Math
#

# IsOdd
#
# Determines if the given number is odd.
#
# $1: Number
#
IsOdd() {
  local -r Number="${1:-}"

  (IsInteger "$Number") && {
    if [ $(( Number % 2 )) = 1 ]; then
      return 0
    else
      return 1
    fi
  }

  return 1
}

# IsEven
#
# Determines if the given number is even.
#
# $1: Number
#
IsEven() {
  local -r Number="${1:-}"

  (IsInteger "$Number") && {
    if [ $(( Number % 2 )) = 0 ]; then
      return 0
    else
      return 1
    fi
  }

  return 1
}

# IsPositive
#
# Determines if the given number is positive.
#
# $1: Number
#
IsPositive() {
  local -r Number="${1:-}"

  (IsInteger "$Number") && {
    if [ "$Number" -gt 0 ]; then
      return 0
    else
      return 1
    fi
  }

  return 1
}

# IsNegative
#
# Determines if the given number is negative.
#
# $1: Number
#
IsNegative() {
  local -r Number="${1:-}"

  (IsInteger "$Number") && {
    if [ "$Number" -lt 0 ]; then
      return 0
    else
      return 1
    fi
  }

  return 1
}

# Max
#
# $1: Lhs
# $2: Rhs
#
Max() {
  local -r Lhs="${1:-}"
  local -r Rhs="${2:-}"
  local RetVal=

  ! (IsInteger "$Lhs") || ! (IsInteger "$Rhs") && return 1

  if [ "$Lhs" -gt "$Rhs" ]; then
    RetVal=$Lhs
  elif [ "$Rhs" -gt "$Lhs" ]; then
    RetVal=$Rhs
  elif [ "$Lhs" -eq "$Rhs" ]; then
    RetVal=$Rhs
  fi

  printf '%d' "$RetVal"
}

# Min
#
# $1: Lhs
# $2: Rhs
#
Min() {
  local -r Lhs="${1:-}"
  local -r Rhs="${2:-}"
  local RetVal=

  ! (IsInteger "$Lhs") || ! (IsInteger "$Rhs") && return 1

  if [ "$Lhs" -lt "$Rhs" ]; then
    RetVal=$Lhs
  elif [ "$Rhs" -lt "$Lhs" ]; then
    RetVal=$Rhs
  elif [ "$Lhs" -eq "$Rhs" ]; then
    RetVal=$Rhs
  fi

  printf '%d' "$RetVal"
}



#
# endregion Math

#  vim: set ts=2 sw=2 tw=80 noet :
