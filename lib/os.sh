#!/usr/bin/env sh
#
# shellcheck disable=SC3043
#
# ./lib/os.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

#
# endregion Imports

# region OS
#

# region Mutables
#

# shellcheck disable=SC2034
[ -z "$PLATFORM" ] && PLATFORM=$(command uname)

# shellcheck disable=SC2034
[ -z "$OS_ARCH" ] && OS_ARCH=$(command uname -m)

# shellcheck disable=SC2034
[ -z "$OS_NAME" ] && OS_NAME=$(command uname -s)

# shellcheck disable=SC2034
[ -z "$OS_TYPE" ] && OS_TYPE=$(command uname -o)

# shellcheck disable=SC2034
[ -z "$OS" ] && OS=

# shellcheck disable=SC2034
[ -z "$PLATFORM_TYPE" ] && PLATFORM_TYPE=$(
  [ "$PLATFORM" = "Linux" ] || [ "$PLATFORM" = "Linux2" ] ||
    [ "$PLATFORM" = "FreeBSD" ] || [ "$PLATFORM" = "OpenBSD" ] ||
    [ "$PLATFORM" = "NetBSD" ] || [ "$PLATFORM" = "Darwin" ] ||
    [ "$PLATFORM" = "SunOS" ] &&
    printf '%s' 'POSIX' || printf '%s' 'WINDOWS'
)

[ "$PLATFORM" = "Linux" ] || [ "$PLATFORM" = "Linux2" ] && OS='Linux'
[ "$PLATFORM" = "Mingw" ] || [ "$PLATFORM" = "Cygwin" ] ||
  [ "$PLATFORM" = "Msys" ] || [ "$PLATFORM" = "WSL" ] && OS='Windows'
[ "$PLATFORM" = "FreeBSD" ] || [ "$PLATFORM" = "OpenBSD" ] ||
  [ "$PLATFORM" = "NetBSD" ] || [ "$PLATFORM" = "Darwin" ] &&
  OS='BSD' || OS=Unknown

# shellcheck disable=SC2034

# shellcheck disable=SC2034
[ -z "$ENV" ] && ENV=$(command env)

# shellcheck disable=SC2034
[ -z "$ENTROPY" ] && ENTROPY=1

# shellcheck disable=SC2034
[ -z "$NL" ] && NL='
'

# NEWLINE
#
# Returns a new line.
#
# shellcheck disable=SC2034
[ -z "$NEWLINE" ] && NEWLINE=$(printf '%s' "\n")

case $OS in
Windows)
  NEWLINE=$(printf '%s' "\n\r")

  # NewLine
  #
  # Returns a new line.
  #
  NewLine() {
    printf '%s' "\n\r"
  }
  ;;
*)
  NEWLINE=$(printf '%s' "\n")

  # NewLine
  #
  # Returns a new line.
  #
  NewLine() {
    printf '%s' "\n"
  }
  ;;
esac

# shellcheck disable=SC2034
[ -z "$AWK_COMMAND" ] && AWK_COMMAND='awk'

# Must use nawk for solaris
# http://cfajohnson.com/shell/cus-faq-2.html#Q24
#
case $PLATFORM in SunOS) AWK_COMMAND=nawk ;; esac

#
# endregion Mutables

# region Functions
#

# IsLinux
#
IsLinux() {
  return 1
}

# IsMac
#
IsMac() {
  return 1
}

# IsWindows
#
IsWindows() {
  return 1
}

# IsWsl
#
IsWsl() {
  return 1
}

# IsPSD
#
IsPSD() {
  return 1
}

# IsUnix
#
IsUnix() {
  return 1
}

# IsAndroid
#
IsAndroid() {
  return 1
}

# IsPosix
#
IsPosix() {
  return 1
}

# IsUbuntu
#
IsUbuntu() {
  return 1
}

# IsDebian
#
IsDebian() {
  return 1
}

# IsArchLinux
#
IsArchLinux() {
  return 1
}

# IsFedora
#
IsFedora() {
  return 1
}

# OsName
#
OsName() {
  local os=

  if [ -z "$os" ]; then
    os=$(
      command cat /etc/*-release |
        command grep '^NAME=' |
        command cut --delimiter='=' --fields=2 |
        command sed -e 's/^"//' -e 's/"$//'
    )
  elif [ -z "$os" ]; then
    os=$(command head -1 /etc/issue |
      command cut --delimiter=' ' --fields=1)
  elif [ -z "$os" ]; then
    os=$(command lsb_release --id --short 2>/dev/null)
  fi

  printf '%s\n' "$os"
}

#
# endregion Functions

# endregion OS

#  vim: set ts=2 sw=2 tw=80 noet :
