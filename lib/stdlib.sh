#!/usr/bin/env sh
#
# ./lib/stdlib.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Standard Library
#

# region Exports
#

# shellcheck disable=SC2034
[ -z "$SHELLSCRIPTS_PATH" ] && export SHELLSCRIPTS_PATH="${HOME}/.local/share/shellscripts"

#
# region Exports

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$SHELLSCRIPTS_PATH"/lib/global.sh

# shellcheck disable=SC1090 disable=SC1091
. "$SHELLSCRIPTS_PATH"/lib/os.sh

# # shellcheck disable=SC1090 disable=SC1091
# . "$SHELLSCRIPTS_PATH"/lib/ansi.sh

# SetupAnsi

# # shellcheck disable=SC1090 disable=SC1091
# . "$SHELLSCRIPTS_PATH"/lib/ansi-functions.sh

# shellcheck disable=SC1090 disable=SC1091
. "$SHELLSCRIPTS_PATH"/lib/terminal.sh

# shellcheck disable=SC1090 disable=SC1091
. "$SHELLSCRIPTS_PATH"/lib/array.sh

# # shellcheck disable=SC1090 disable=SC1091
# . "$SHELLSCRIPTS_PATH"/lib/compat/compat.sh

# shellcheck disable=SC1090 disable=SC1091
. "$SHELLSCRIPTS_PATH"/lib/logger.sh

# shellcheck disable=SC1090 disable=SC1091
. "$SHELLSCRIPTS_PATH"/lib/string.sh

# shellcheck disable=SC1090 disable=SC1091
. "$SHELLSCRIPTS_PATH"/lib/math.sh

# shellcheck disable=SC1090 disable=SC1091
. "$SHELLSCRIPTS_PATH"/lib/system.sh

# shellcheck disable=SC1090 disable=SC1091
. "$SHELLSCRIPTS_PATH"/lib/utils.sh

# shellcheck disable=SC1090 disable=SC1091
. "$SHELLSCRIPTS_PATH"/lib/assert.sh

#
# endregion Imports

#
# endregion Standard Library

#  vim: set ts=2 sw=2 tw=80 noet :
