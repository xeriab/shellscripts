#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./lib/string.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/os.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/array.sh"

# # shellcheck disable=SC1090 disable=SC1091
# . "$XDG_DATA_HOME/shellscripts/lib/compat/compat.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/terminal.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/logger.sh"

#
# endregion Imports

# region String
#

# StrEscape
#
# Escapes the given string.
#
# $1: Mode
# $2: String
#
# shellcheck disable=SC1003
#
StrEscape() {
  local Mode=
  local Chars=
  local String=
  local EscapeChars='\\\\'

  if [ "${1}" = "--mode" ] || [ "${1}" = "-mode" ] || [ "${1}" = "-m" ]; then
    Mode="${2}"
    shift 2
  else
    Mode="quote"
  fi

  case "${Mode}" in
  quote) Chars="'\"" ;;
  regex) Chars=')]$.*+\^?[(' ;;
  *) return 1 ;;
  esac

  if [ -z "${1}" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  else
    String="${1}"
  fi

  printf '%s' "$String" | command sed -Ere "\$!N; s~([${Chars}])+~$EscapeChars\1~g"
}

# StrLen
#
# Returns the length of the given string.
#
# $1: String to get length of.
#
StrLen() {
  local -r String="${1:-}"

  printf '%d' "${#String}"
}

# StrStripAnsi
#
# $1: String
#
StrStripAnsi() {
  local -r String="${1:-}"

  # local RegExp=

  # RegExp="${RegExp}s~("
  # RegExp="${RegExp}[\\\x1b|\\\033|\\\e]\[[0-9;]*[mGKH]"
  # RegExp="${RegExp}|"
  # RegExp="${RegExp}[\\\x1b|\\\033|\\\e]\[[a-zA-Z0-9;]*[maDCGPMBJXKLH]"
  # RegExp="${RegExp})+~~g"

  # printf '%s' "$String" | command sed -Er 's~[\x1b]+\[[0-9\;]+[a-zA-Z]{1}~~g'
  printf '%s' "$String" | command sed -Er 's~([\x1b|\033|\e]\[[0-9;]*[mGKH])+~~g'
}

# StrPadLeft
#
# Pads the given string to left.
#
# $1: String to pad.
# $2: Number of pads.
# $3: Character or string to fill the pad with.
#
StrPadLeft() {
  local String="${1:-}"
  local -r Count="${2:-}"
  local -r Pad="${3:-}"
  local Stripped=
  local Len=
  local PadCount=
  local PadChunk=

  Stripped=$(StrStripAnsi "${String}")
  # shellcheck disable=SC2155
  Len="$(StrLen "${Stripped}")"
  PadCount="$((Count - Len))"

  PadChunk=$(printf '%*s' "${PadCount}" "${Pad}")

  # Pad with `n` characters.
  String="${PadChunk}${String}"
  String=$(printf '%s' "${String}" | command grep -Eo "^.{1,$(StrLen "${String}")}")

  unset PadChunk Len PadCount Stripped

  printf '%s' "${String}"
}

# StrPadRight
#
# Pads the given string to right.
#
# $1: String to pad.
# $2: Number of pads.
# $3: Character or string to fill the pad with.
#
StrPadRight() {
  local String="${1:-}"
  local -r Count="${2:-}"
  local -r Pad="${3:-}"
  local Stripped=
  local Len=
  local PadCount=
  local PadChunk=

  Stripped=$(StrStripAnsi "${String}")
  # shellcheck disable=SC2155
  Len="$(StrLen "${Stripped}")"
  PadCount="$((Count - Len))"

  PadChunk=$(printf '%*s' "${PadCount}" "${Pad}")

  # Pad with `n` characters.
  String="${String}${PadChunk}"
  String=$(printf '%s' "${String}" | command grep -Eo "^.{1,$(StrLen "${String}")}")

  unset PadChunk Len PadCount Stripped

  printf '%s' "${String}"
}

# StrPadCenter
#
# Pads the given string within center.
#
# $1: String to pad.
# $2: Number of pads.
# $3: Character or string to fill the pad with.
#
StrPadCenter() {
  local String="${1:-}"
  local Count="${2:-}"
  local Pad="${3:-}"
  local Stripped=
  local Len=
  local PadCount=
  local PadChunk=

  Stripped=$(StrStripAnsi "${String}")
  # shellcheck disable=SC2155
  Len="$(StrLen "${Stripped}")"
  PadCount="$(((Count - Len) / 2))"

  PadChunk=$(printf '%*s' "${PadCount}" "${Pad}")

  # Pad with `n` characters.
  String="${PadChunk}${String}${PadChunk}"
  String=$(printf '%s' "${String}" | command grep -Eo "^.{1,$(StrLen "${String}")}")

  unset Count Pad PadChunk Len PadCount Stripped

  printf '%s' "${String}"
}

# StrPad
#
# Pads the given string to the given direction.
#
# $1: String to pad.
# $2: Number of pads.
# $3: Character or string to fill the pad with.
# $4: Direction [left|right|center]
#
StrPad() {
  local -r String="${1:-}"
  local -r Count="${2:-}"
  local -r Pad="${3:-}"
  local -r Dir="${4:-right}"
  local RetVal=

  case $Dir in
  [Cc]enter) RetVal=$(StrPadCenter "${String}" "${Count}" "${Pad}") ;;
  [Ll]eft) RetVal=$(StrPadLeft "${String}" "${Count}" "${Pad}") ;;
  [Rr]ight) RetVal=$(StrPadRight "${String}" "${Count}" "${Pad}") ;;
  *) RetVal=$(StrPadRight "${String}" "${Count}" "${Pad}") ;;
  esac

  printf '%s' "$RetVal"
}

# # Replace
# #
# # Replaces
# #
# # $1 Pattern
# # $2 Replace
# #
# Replace() {
#   PATTERN=$1 REPL=$2 command awk '{ gsub(ENVIRON["PATTERN"], ENVIRON["REPL"]); print }'
# }

# # StrReplace
# #
# StrReplace() {
#   # local PATTERN=$1
#   # local REPL=$2
#   # awk '{gsub(ENVIRON["PATTERN"], ENVIRON["REPL"]); print}'

#   local Pattern="${2:-}"
#   # local PATTERN=${2:-",|\\s,|\\s,\\s|\\s"}
#   local Replace="${3:-}"

#   printf "${1:-}" | Replace "$Pattern" "$Replace"
# }

# StrToLower
#
# Convert the given string to lowercase.
#
# $1 String to change case.
#
StrToLower() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  # printf "${1:-}" | command tr '[:upper:]' '[:lower:]'
  # printf '%s' "$String" | command awk '{ print tolower($0) }'
  printf '%s' "$String" | command sed -E 's~(\w)~\l\1~g'
}

# StrToLower
#
# Convert the given string to uppercase.
#
# $1 String to change case.
#
StrToUpper() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  # printf "${1:-}" | command tr '[:lower:]' '[:upper:]'
  # printf '%s' "$String" | command awk '{ print toupper($0) }'
  printf '%s' "$String" | command sed -E 's~(\w)~\u\1~g'
}

# StrSeparatorCamelCase
#
# $1 Separator
# $2 String
#
# @depends StrEscape
#
StrSeparatorCamelCase() {
  local Separator="${1:-}"
  local String="${2:-}"

  if [ -z "${Separator}" ]; then
    return 1
  fi

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  Separator=$(StrEscape --mode regex "${1}" | command sed 's~/~\\/~g')

  printf '%s' "$String" | command sed -E "s~${Separator}(\w)~\u\1~g"
}

# StrCamelCaseSeparator
#
# $1 Separator
# $2 String
#
# @depends StrEscape
#
StrCamelCaseSeparator() {
  local Separator="${1:-}"
  local String="${2:-}"
  local Pattern=

  if [ -z "${Separator}" ]; then
    return 1
  fi

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  Separator=$(StrEscape --mode regex "${1}" | command sed 's~/~\\/~g')
  Pattern="s~([A-Za-z0-9])([A-Z])~\1${Separator}\2~g"

  printf '%s' "$String" | command sed -E "${Pattern}" | command sed -E "${Pattern}"
}

# StrUnderscoreCamelCase
#
# $1 String
#
# @depends StrSeparatorCamelCase
#
StrUnderscoreCamelCase() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  StrSeparatorCamelCase '_' "${String}"
}

# StrDashCamelCase
#
# $1 String
#
# @depends StrSeparatorCamelCase
#
StrDashCamelCase() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  StrSeparatorCamelCase '-' "${String}"
}

# StrCamelCaseDash
#
# $1 String
#
# @depends StrCamelCaseSeparator
#
StrCamelCaseDash() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  StrCamelCaseSeparator '-' "${String}"
}

# StrCamelCaseUnderscore
#
# $1 String
#
# @depends StrCamelCaseSeparator
#
StrCamelCaseUnderscore() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  StrCamelCaseSeparator '_' "${String}"
}

# StrToKebeb
#
# $1 String to change case.
# $2 Delimiter
#
StrToKebeb() {
  local -r String="${1:-}"
  local -r Delim="${2:-_}"
  local Pattern=

  Pattern="${Pattern}s~\([^A-Z+]\)\([A-Z0-9]\)~\1${Delim}\2~g"
  Pattern="${Pattern};s~\([0-9]\)\([A-Z]\)~\1${Delim}\2~g"
  Pattern="${Pattern};s~\([A-Z]\)\([0-9]\)~\1${Delim}\2~g"
  Pattern="${Pattern};s~_~${Delim}~g"
  Pattern="${Pattern};s~-~${Delim}~g"
  Pattern="${Pattern};s~\s~${Delim}~g"
  Pattern="${Pattern};s~^${Delim}~~g"
  Pattern="${Pattern};s~${Delim}$~~g"
  Pattern="${Pattern};s~${Delim}${Delim}~${Delim}~g"
  Pattern="${Pattern};s~\([\/]\)${Delim}~\1~g"

  # printf '%s' "$String" | command sed -e "$Pattern" | command tr '[:upper:]' '[:lower:]'
  printf '%s' "$(StrToLower "$(printf "$String" | command sed -e "$Pattern")")"
}

# StrToSnake
#
# $1 String to change case.
#
StrToSnake() {
  local -r String="${1:-}"

  printf '%s' "$(StrToKebeb "$String" '-')"
}

# StrToCamel
#
# $1 String to change case.
#
StrToCamel() {
  local String="${1:-}"
  local Pattern=""

  Pattern="s~-\([a-z0-9]\)~\U\1~g;s~^\([A-Z0-9]\)~\L\1~g"

  String=$(StrToSnake "$String")

  printf '%s' "$String" | command sed -e "$Pattern" | command sed -e 's~\(\s\+\)~~g'
}

# StrToPascal
#
# $1 String to change case.
#
StrToPascal() {
  local String="${1:-}"
  local Pattern=""

  Pattern="s~-\([a-z0-9]\)~\U\1~g;s~^\([a-z0-9]\)~\U\1~g;s~^\([a-Z0-9]\)~\U\1~g"

  String=$(StrToSnake "$String")

  printf '%s' "$String" | command sed -e "$Pattern" | command sed -e 's~\(\s\+\)~~g'
}

# StrToTitle
#
# $1 String to change case.
#
StrToTitle() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  # printf '%s' "$String" | command awk '{ for (x = 1; x <= NF; x++) { $x=toupper(substr($x,1,1)) substr($x,2) } }1'
  # printf '%s' "$String" | command sed -e 's~\b\(.\)~\u\1~g'
  printf '%s' "$String" | command sed -Ere 's~\b(\w)~\u\1~g'
}

# StrContain
#
# Return True (0) if the first string (haystack) contains the second string
# (needle), and False (1) otherwise.
#
# $1: String
# $2: Search
#
StrContain() {
  local -r String="${1:-}"
  local -r Search="${2:-}"

  if (printf '%s\n' "$String" | command sed -Er "/${Search}/p" -n) 1>/dev/null 2>&1; then
    return 0
  fi

  return 1
}

# StrContains
#
# Return True (0) if the first string (haystack) contains the second string
# (needle), and False (1) otherwise.
#
# $1: String
# $2: Search
# $3: Mode
#
StrContains() {
  # local -r String="${1:-}"
  # local -r Search="${2:-}"
  # local -r Mode="${3:-plain}"
  # local RetVal=0

  # case $Mode in
  # --plain | -plain | -p | plain)
  #   (printf '%s' "$String" | command grep -Fe "${Search}" -i -o) 1>/dev/null 2>&1
  #   RetVal=$?
  #   ;;
  # --regex | -regex | -r | regex)
  #   (printf '%s' "$String" | command grep -Ee "${Search}" -i -o) 1>/dev/null 2>&1
  #   RetVal=$?
  #   ;;
  # *) RetVal=1 ;;
  # esac

  # # (printf "${1:-}" | command grep -e "*${2:-}*" -o) 1>/dev/null 2>&1

  # return $RetVal

  local Mode
  local String
  local Search
  local RetVal=0

  if [ "${1}" = "--mode" ] || [ "${1}" = "-mode" ] || [ "${1}" = "-m" ]; then
    Mode="${2}"
    shift 2
  else
    Mode="plain"
  fi

  if [ -z "${1}" ]; then
    return 1
  fi

  if [ -z "${1}" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  else
    String="${1}"
  fi

  Search="${2:-}"

  case "${Mode}" in
  plain)
    (printf '%s' "$String" | command grep -Fe "${Search}" -i -o) 1>/dev/null 2>&1
    RetVal=$?
    ;;
  regex)
    (printf '%s' "$String" | command grep -Ee "${Search}" -i -o) 1>/dev/null 2>&1
    RetVal=$?
    ;;
  esac

  # echo "Mode: '$Mode'"
  # echo "String: '$String'"
  # echo "Search: '$Search'"

  return $RetVal
}

# StrMultilineContains
#
# $1: HayStack
# $2: Needle
#
StrMultilineContains() {
  local -r HayStack="${1:-}"
  local -r Needle="${2:-}"

  printf '%s' "${HayStack}" | command grep -q "${Needle}"
}

# StrHasPrefix
#
# Determines if the given string has the given prefix.
#
# $1: String
# $2: Prefix
#
StrHasPrefix() {
  local -r String="${1:-}"
  local -r Prefix="${2:-}"

  (printf '%s' "$String" | command grep -Ee "^${Prefix}" -o) 1>/dev/null 2>&1
  # (printf "${1:-}" | command grep -Ee "^${2:-}" -o) 1>/dev/null 2>&1

  return $?
}

# StrHasSuffix
#
# Determines if the given string has the given suffix.
#
# $1: String
# $2: Suffix
#
StrHasSuffix() {
  local -r String="${1:-}"
  local -r Suffix="${2:-}"

  (printf '%s' "$String" | command grep -Ee "${Suffix}$" -o) 1>/dev/null 2>&1
  # (printf "${1:-}" | command grep -Ee "^${2:-}" -o) 1>/dev/null 2>&1

  return $?
}

# StringReplace
#
# $1: Mode
# $2: String
# $3: Search
# $4: Replace
#
StringReplace() {
  local Mode=
  local String=

  if [ "${1}" = "--mode" ] || [ "${1}" = "-mode" ] || [ "${1}" = "-m" ]; then
    Mode="${2}"
    shift 2
  else
    Mode="plain"
  fi

  if [ -z "${1}" ]; then
    return 1
  fi

  if [ -z "${1}" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  else
    String="${1}"
  fi

  local -r Search="${2:-}"
  local -r Replace="${3:-}"

  case "${Mode}" in
  plain)
    printf '%s' "${String}" | command tr "$Search" "$Replace"
    return 0
    ;;
  regex)
    printf '%s' "${String}" | command sed -Ere "\$!N; s~(${Search})+~${Replace}~g"
    return 0
    ;;
  *) return 1 ;;
  esac
}

# StrReplace
#
# $1: Mode
# $2: String
# $3: Search
# $4: Replace
#
# @depends StrEscape
#
StrReplace() {
  local Mode
  local Search
  local Replace
  local String

  if [ "${1}" = "--mode" ] || [ "${1}" = "-mode" ] || [ "${1}" = "-m" ]; then
    Mode="${2}"
    shift 2
  else
    Mode="plain"
  fi

  if [ -z "${1}" ]; then
    return 1
  fi

  case "${Mode}" in
  plain)
    Search=$(StrEscape --mode regex "${1}")
    Replace=$(StrEscape --mode regex "${2}")
    ;;
  regex)
    Search="${1}"
    Replace="${2}"
    ;;
  *)
    return 2
    ;;
  esac

  if [ -z "${3}" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  else
    String="${3}"
  fi

  # printf '%s' "${String}" | command sed -Er "s~${Search}~${Replace}~g;"
  printf '%s' "${String}" | command sed -Ere "\$!N; s~${Search}~${Replace}~g;"
}

# StrReplaceAll
#
# @depends StrEscape
# @depends StrReplace
#
StrReplaceAll() {
  local Replacement="${1}"
  local String="${2}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  StrReplace --mode regex "." "$(printf "${Replacement}" | StrEscape --mode regex)" "${String}"
}

# StrSplit
#
# $1: String
# $2: Delim
#
StrSplit() {
  local -r String="${1:-}"
  local -r Delim="${2:-([[:space:]|,|[:space:],|,[:space:]|[:space:],[:space:]|\n])}"

  printf '%s' "$(printf "$String" | command sed -Ere "\$!N; s~${Delim}+~\n~g")"
}

# StrTrimEnd
#
# $1: String
# $2: Pattern
#
# shellcheck disable=SC2120
#
StrTrimEnd() {
  local String=
  local Pattern=
  local Temp=

  if [ -z "${1}" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  else
    String="${1}"
  fi

  if [ -z "${2}" ]; then
    Pattern="([\r\0\t\s\n\x0B\xC2\xA0|[:blank:]|[:space:]])"
  else
    Pattern="${2:-([\r\0\t\s\n\x0B\xC2\xA0|[:blank:]|[:space:]])}"
  fi

  Temp=$(printf '%s; s~%s+$~~' '$!N' "${Pattern}")

  printf '%s' "$String" | command sed -Er "$Temp"
}

# StrTrimStart
#
# $1: String
# $2: Pattern
#
# shellcheck disable=SC2120
#
StrTrimStart() {
  local String=
  local Pattern=
  local Temp=

  if [ -z "${1}" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  else
    String="${1}"
  fi

  if [ -z "${2}" ]; then
    Pattern="([\r\0\t\s\n\x0B\xC2\xA0|[:blank:]|[:space:]])"
  else
    Pattern="${2:-([\r\0\t\s\n\x0B\xC2\xA0|[:blank:]|[:space:]])}"
  fi

  Temp=$(printf '%s; s~^%s+~~' '$!N' "${Pattern}")

  printf '%s' "$String" | command sed -Ere "$Temp"
}

# StrTrim
#
# $1: String
# $2: Pattern
#
StrTrim() {
  local String=
  local Pattern=
  local RetVal=

  if [ -z "${1}" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  else
    String="${1}"
  fi

  if [ -z "${2}" ]; then
    Pattern="([\r\0\t\s\n\x0B\xC2\xA0|[:blank:]|[:space:]])"
  else
    Pattern="${2:-([\r\0\t\s\n\x0B\xC2\xA0|[:blank:]|[:space:]])}"
  fi

  RetVal=$(printf '%s' "${String}")
  RetVal=$(StrTrimStart "${RetVal}" "${Pattern}")
  RetVal=$(StrTrimEnd "${RetVal}" "${Pattern}")

  unset String Pattern

  # printf "$String" | xargs
  # printf '%s' "$String" | command sed 's~^[[:blank:]]*~~' | command sed 's~[[:blank:]]*$~~'

  printf '%s' "${RetVal}"
}

# StrTrimSpaces
#
# $1: String
#
StrTrimSpaces() {
  local -r String="${1:-}"
  printf '%s' "$String" | xargs printf '%b\n'
}

# StrTrimSpaces
#
# $1: String
#
StrTrimLines() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  # printf "$String" | command sed -Er '$!N; s~\n+$~~g; s~^\n\s+~~;' | command sed -Er 's~\s+~~g'
  # printf '%s' "${String}" | command sed -e :a -e '$!N; s~^\n~~; s~\n$~~; ta'
  printf '%s' "${String}" | command sed -e :a -e '$!N; s~^\n~~; s~\n$~~; ta'
}

# StrStripLines
#
# $1: String
#
StrStripLines() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  # printf "$String" | xargs
  # printf '%s' "${String}" | command sed 's~[[:blank:]]*~~g' | command sed -Er '$!N; s~\n+$~~g; s~^\n+~~;'
  printf '%s' "${String}" | command sed -e :a -e '$!N; s~\n~~; ta'
}

# StrNormalizeSpaces
#
# $1: String
#
StrNormalizeSpaces() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  printf '%s' "${String}" | command sed -Er 's~\s+~ ~g'
}

# StrNormalize
#
# $1: String
#
StrNormalize() {
  local String="${1:-}"

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  String=$(StrTrim "${String}")
  # String=$(printf '%s' "$String" | command sed -Er '$!N; s~\s+$~~g; s~^\s+~~;')
  # String=$(printf '%s' "$String" | command sed -Er '$!N; s~\n+$~~g; s~^\n+~~;')
  String=$(StrNormalizeSpaces "${String}")

  printf '%s' "${String}"
}

# StrRegExpReplace
#
# Replace the given String with the given RegExp
#
#
StrRegExpReplace() {
  local String="${1:-}"
  local RegExp="${2:-}"
  local -r Replacement="${3:-}"

  # String=$(printf '%s' "$String" | command grep -E -o "$RegExp")
  String=$(printf '%s' "$String" | command sed -Er "s~${RegExp}~${Replacement}~g")

  printf '%s' "${String}"
}

# StrRepeat
#
# Repeats the given string the given times.
#
# $1 String to repeat.
# $2 Depth to repeat.
#
StrRepeat() {
  local String="${1:-}"
  local Depth="${2:-0}"

  if [ "${Depth}" -lt 1 ]; then
    return 1
  fi

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  # shellcheck disable=SC2034
  # printf "%${Depth}s" | tr " " "$String"
  # printf "$(for each in $(Seq 1 "$Depth"); do printf "$String"; done)"

  for Each in $(Seq 1 "$Depth"); do
    printf '%b' "${String}"
  done

  unset Each
}

# StrStripPrefix
#
# Strip the prefix from the given string. Supports wildcards.
#
# Example:
#
# StrStripPrefix "foo=bar" "foo="  ===> "bar"
# StrStripPrefix "foo=bar" "*="    ===> "bar"
#
# http://stackoverflow.com/a/16623897/483528
#
StrStripPrefix() {
  local -r String="$1"
  local -r Prefix="$2"

  printf "$String" | command sed -e "s~^$Prefix~~"
}

# StrStripSuffix
#
# Strip the suffix from the given string. Supports wildcards.
#
# Example:
#
# StrStripSuffix "foo=bar" "=bar"  ===> "foo"
# StrStripSuffix "foo=bar" "=*"    ===> "foo"
#
# http://stackoverflow.com/a/16623897/483528
#
StrStripSuffix() {
  local -r String="$1"
  local -r Suffix="$2"

  printf "$String" | command sed -e "s~^$Suffix$~~"
}

# StrSubStr
#
# Given a string $String, return the substring beginning at index $Start and
# ending at index $End.
#
# Example:
#
# StrSubStr "Hello World!" 0 5
#   Returns "Hello"
#
StrSubStr() {
  local -r String="$1"
  local Start="$2"
  local End="$3"
  # local Temp=
  # local Out=""
  # local Count=1

  [ -n "$Start" ] && [ "$Start" = 0 ] && Start=1

  if [ -z "$End" ]; then
    End="${#String}"
  fi

  if [ "$Start" -lt 0 ] || [ "$End" -lt 0 ]; then
    Log -e "In the StrSubStr function, each of \$Start and \$End must be >= 0."
    exit 1
  fi

  if [ "$Start" -gt "$End" ]; then
    Log -e "In the StrSubStr function, \$Start must be < \$End."
    exit 1
  fi

  # Temp=$(printf '%s\n' "$String" | command sed -Ere '$!N; s~(.)~\1\n~g; s~(\s)~\1\n~g;')
  # # Temp=$(printf '%s\n' "$String" | command sed -e '$!N; s~\(.\)~\1 ~g')

  # # echo "Temp: '$Temp' "

  # # OLDIFS=$IFS
  # # IFS=$(printf ' ')

  # for Each in $Temp; do
  #   if [ $Count -lt "$Start" ]; then
  #     : # Do Nothing.
  #   elif [ $Count -gt "$End" ]; then
  #     break
  #   else
  #     Out="${Out}${Each}"
  #   fi

  #   Count=$((Count + 1))
  # done

  # unset Each

  # # IFS=$OLDIFS

  # # echo "${String:$Start:$End}"
  # # printf '%s\n' "$Out"

  printf '%s\n' "$(command expr substr "$String" $Start "$End")"
}

# StrSub
#
StrSub() {
  local Replace
  local With

  Replace="$(printf "$1" | command sed 's~/~\\/~g')"
  With="$(printf "$2" | command sed 's~/~\\/~g')"

  command sed 's~'"$Replace"'~'"$With"'~'"${Flags-}"
}

# StrGsub
#
# @depends StrSub
#
StrGsub() {
  Flags=g StrSub "$@"
}

# StrIsEmptyOrNull
#
# Return True if the given response is empty or "null" (the latter is from jq parsing).
#
StrIsEmptyOrNull() {
  local -r Response="$1"
  [ -z "$Response" ] || [ "$Response" = "null" ]
}

# StrCountWords
#
# Count words in the given string.
#
# $1: String
#
StrCountWords() {
  local String="${1:-}"
  local Count=0

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  String=$(printf '%s\n' "${String}" |
    command sed -e :a -e '$!N; s~(\w)+~\n~g; ta')

  for Each in $String; do
    Count=$((Count + 1))
  done

  unset Each

  # printf "$(printf "${1:-}" | command wc --word)"

  printf '%d\n' "$Count"
}

# StrCountLines
#
# Count lines in the given string.
#
# $1: String
#
StrCountLines() {
  local String="${1:-}"
  local Count=0

  if [ -z "$String" ] && ! [ -t 0 ]; then
    String=$(command cat <&0)
  fi

  # String=$(printf '%s' "$String" | command sed -Ere '$!N; s~(\n)+~ ~g')
  String=$(printf '%s\n' "${String}" |
    command sed -e :a -e '$!N; s~\\n~ ~g; ta')

  for Each in $String; do
    Count=$((Count + 1))
  done

  unset Each

  # printf "$(printf "${1:-}" | command wc -l)"

  printf '%d\n' "$Count"
}

# CommaSeparated
#
CommaSeparated() {
  local RetVal=

  for Arg in "$@"; do
    if [ -z "$RetVal" ]; then
      RetVal=$1
    else
      RetVal="$RetVal, $Arg"
    fi
  done

  unset Arg

  printf '%s' "$RetVal"
}

# CharAt
#
# $1: String
# $2: Position
#
CharAt() {
  local -r String="${1:-}"
  local -r Position="${2:-}"

  [ -z "${String}" ] && {
    # shellcheck disable=SC3028
    printf "%s\n" "Usage: ${FUNCNAME:-${Self:-${SCRIPT}}} <string> <position to extract string>." && {
      return 1
    }
  }

  {
    [ "${Position}" -eq 0 ] && printf '%c\n' "${String}" && return 0
  } || {
    [ "${Position}" -gt ${#String} ] && printf '%s\n' "" && return 0
  }

  (
    # All the variables delcared here will get lost after this subshell finsih
    # executing

    local Temp="${String}"
    local CutFirstChar=
    local i=0

    while [ -n "${Temp}" ]; do
      CutFirstChar=$(printf '%c' "${Temp}")
      Temp=${Temp#*"$CutFirstChar"}
      [ "$i" = "$Position" ] && printf '%s\n' "${CutFirstChar}"
      # if [ "$Position" = "$i" ]; then
      # 	printf '%c'
      # fi
      i=$((i + 1))
    done

    unset Temp CutFirstChar i
  )
}

# StrSpread
#
StrSpread() {
  local String="$*"
  local StringCount=
  local i=0

  [ -z "${String}" ] && {
    # shellcheck disable=SC3028
    printf "%s\n" "Usage: ${FUNCNAME:-${Self:-${SCRIPT}}} <string>."
    return 1
  }

  if [ $# -eq 1 ]; then
    StringCount=${#String}
    while [ $i -lt "$StringCount" ] || [ $i -eq "$StringCount" ]; do
      while [ -n "${String}" ]; do
        printf "%c\n" "${String}"
        String=${String#*?}
      done
      i=$((i + 1))
    done
  fi

  unset String StringCount i
}

# # StrRemoveEmptyLines
# #
# StrRemoveEmptyLines() {
#   local -r Content="${1}"
#   printf '%s' "${Content}" | command sed '/^\s*$/d'
# }

# IsString
#
# $1: Value
#
IsString() {
  local -r Value="${1:-}"

  case $Value in
  *[!-+=~@%/:.,_0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ]*)
    return 1
    ;;
  *) return 0 ;;
  esac
}

#
# endregion String

# region Aliases
#

alias StrStartsWith=StrHasPrefix
alias StrEndsWith=StrHasSuffix

#
# endregion Aliases

#  vim: set ts=2 sw=2 tw=80 noet :
