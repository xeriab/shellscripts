#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./lib/system.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/terminal.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/logger.sh"

#
# endregion Imports

# region System
#

# region Mutables
#



#
# endregion Mutables

# region Functions
#



#
# endregion Functions

# endregion System

#  vim: set ts=2 sw=2 tw=80 noet :
