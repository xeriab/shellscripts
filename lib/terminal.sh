#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2154
#
# ./lib/terminal.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/os.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/ansi.sh"

SetupAnsi

# # shellcheck disable=SC1090 disable=SC1091
# . "$XDG_DATA_HOME/shellscripts/lib/ansi-functions.sh"

#
# endregion Imports

# region Terminal
#

# region Mutables
#

# shellcheck disable=SC2034
[ -z "$CONSOLE_FORCE_HYPERLINK" ] && CONSOLE_FORCE_HYPERLINK=False

# shellcheck disable=SC2034
[ -z "$FORCE_HYPERLINK" ] && FORCE_HYPERLINK=${FORCE_HYPERLINK:-$CONSOLE_FORCE_HYPERLINK}

#
# endregion Mutables


# IsInteractive
#
IsInteractive() {
  ! [ "$(command tty)" = "not a tty" ] || (command tty -s)
}

# TerminalWidth
#
TerminalSize() {
  local -r Type="${1:-}"
  local Size=
  local Columns=
  local Rows=
  local RetVal=

  # Columns=$(command stty -a <"$Terminal" | command grep -Po '(?<=columns )\d+')
  # Rows=$(command stty -a <"$terminal" | command grep -Po '(?<=rows )\d+')

  # Size=$(command stty size | command awk "{ print \$0 }")
  # Rows=$(printf "$Size" | command awk -F' ' "{ print \$1 }")
  # Columns=$(printf "$Size" | command awk -F' ' "{ print \$2 }")

  if [ -n "$COLUMNS" ] && [ -n "$LINES" ]; then
    Rows=$(printf "$LINES")
    Columns=$(printf "$COLUMNS")
  else
    Size=$(command stty size | command awk "{ print \$0 }")
    Rows=$(printf "$Size" | command awk -F' ' "{ print \$1 }")
    Columns=$(printf "$Size" | command awk -F' ' "{ print \$2 }")
  fi

  case $Type in
  -j | -json | --json)
    RetVal=$(printf '{ "%s": %d, "%s": %d }' "columns" "$Columns" "rows" "$Rows")
    ;;
  *? | *)
    RetVal=$(printf '%s %d %s %d' "columns" "$Columns" "rows" "$Rows")
    ;;
  esac

  unset Size Columns Rows

  printf '%s' "$RetVal"
}

# TerminalWidth
#
TerminalWidth() {
  local Columns=

  if [ -n "$COLUMNS" ]; then
    Columns="$COLUMNS"
  else
    Columns=$(TerminalSize -json | command grep -Po '(?<= "columns": )\d+')
  fi

  printf '%d' "$Columns"
}

# TerminalHeight
#
TerminalHeight() {
  local Rows=

  if [ -n "$LINES" ]; then
    Rows=$LINES
  else
    Rows=$(TerminalSize -json | command grep -Po '(?<= "rows": )\d+')
  fi

  printf '%d' "$Rows"
}

# # DetectTerminalColor
# #
# DetectTerminalColor() {
#   local OsRelease=
#   OsRelease=$(command cat /proc/sys/kernel/osrelease)
#   if [ -n "$OsRelease" ] && ! [ "$OsRelease" = "" ]; then
#   fi
# }

# TerminalColors
#
TerminalColors() {
  printf '%d' "$(tput colors)"
}

# SupportsHyperlinks
#
# This function uses the logic from supports-hyperlinks[1][2], which is
# made by Kat Marchán (@zkat) and licensed under the Apache License 2.0.
# [1] https://github.com/zkat/supports-hyperlinks
# [2] https://crates.io/crates/supports-hyperlinks
#
# Copyright (c) 2021 Kat Marchán
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
SupportsHyperlinks() {
  # $CONSOLE_FORCE_HYPERLINK must be set and be non-zero (this acts as a logic bypass)
  if [ -n "$CONSOLE_FORCE_HYPERLINK" ]; then
    ! [ "$CONSOLE_FORCE_HYPERLINK" = 0 ] || ! [ "$CONSOLE_FORCE_HYPERLINK" = False ] || ! [ "$CONSOLE_FORCE_HYPERLINK" = false ]
    return $?
  fi

  # $FORCE_HYPERLINK must be set and be non-zero (this acts as a logic bypass)
  if [ -n "$FORCE_HYPERLINK" ]; then
    ! [ "$FORCE_HYPERLINK" = 0 ] || ! [ "$FORCE_HYPERLINK" = False ] || ! [ "$FORCE_HYPERLINK" = false ]
    return $?
  fi

  # If stdout is not a tty, it doesn't support hyperlinks
  IsTTY || return 1

  # DomTerm terminal emulator (domterm.org)
  if [ -n "$DOMTERM" ]; then
    return 0
  fi

  # VTE-based terminals above v0.50 (Gnome Terminal, Guake, ROXTerm, etc)
  if [ -n "$VTE_VERSION" ]; then
    [ "$VTE_VERSION" -ge 5000 ]
    return $?
  fi

  # If $TERM_PROGRAM is set, these terminals support hyperlinks
  case "$TERM_PROGRAM" in
  Hyper | iTerm.app | terminology | WezTerm) return 0 ;;
  esac

  # If $TERMINAL is set, these terminals support hyperlinks
  case "$TERMINAL" in
  Hyper | iTerm.app | terminology | WezTerm | tilix) return 0 ;;
  esac

  # kitty supports hyperlinks
  if [ "$TERM" = xterm-kitty ]; then
    return 0
  fi

  # Windows Terminal or Konsole also support hyperlinks
  if [ -n "$WT_SESSION" ] || [ -n "$KONSOLE_VERSION" ]; then
    return 0
  fi

  return 1
}

# ParseFgColor
#
# Parses Foreground Color.
#
# $1: ColorName
#
ParseFgColor() {
  local -r ColorName="${1:-}"
  local RetVal=""

  case $ColorName in
  # Foreground Colors
  Black | FgBlack) RetVal=$FgBlack ;;
  Red | FgRed) RetVal=$FgRed ;;
  Green | FgGreen) RetVal=$FgGreen ;;
  Yellow | FgYellow) RetVal=$FgYellow ;;
  Blue | FgBlue) RetVal=$FgBlue ;;
  Magenta | FgMagenta) RetVal=$FgMagenta ;;
  Cyan | FgCyan) RetVal=$FgCyan ;;
  White | FgWhite) RetVal=$FgWhite ;;
  # Foreground Bright Colors
  BrightBlack | FgBrightBlack) RetVal=$FgBrightBlack ;;
  BrightRed | FgBrightRed) RetVal=$FgBrightRed ;;
  BrightGreen | FgBrightGreen) RetVal=$FgBrightGreen ;;
  BrightYellow | FgBrightYellow) RetVal=$FgBrightYellow ;;
  BrightBlue | FgBrightBlue) RetVal=$FgBrightBlue ;;
  BrightMagenta | FgBrightMagenta) RetVal=$FgBrightMagenta ;;
  BrightCyan | FgBrightCyan) RetVal=$FgBrightCyan ;;
  BrightWhite | FgBrightWhite) RetVal=$FgBrightWhite ;;
  None) RetVal='' ;;
  *) RetVal=$ColorName ;;
  esac

  printf '%s' "$RetVal"
}

# ParseBgColor
#
# Parses Background Color.
#
# $1: ColorName
#
ParseBgColor() {
  local -r ColorName="${1:-}"
  local RetVal=""

  case $ColorName in
  # Background Colors
  Black | BgBlack) RetVal=$BgBlack ;;
  Red | BgRed) RetVal=$BgRed ;;
  Green | BgGreen) RetVal=$BgGreen ;;
  Yellow | BgYellow) RetVal=$BgYellow ;;
  Blue | BgBlue) RetVal=$BgBlue ;;
  Magenta | BgMagenta) RetVal=$BgMagenta ;;
  Cyan | BgCyan) RetVal=$BgCyan ;;
  White | BgWhite) RetVal=$BgWhite ;;
  # Background Bright Colors
  BrightBlack | BgBrightBlack) RetVal=$BgBrightBlack ;;
  BrightRed | BgBrightRed) RetVal=$BgBrightRed ;;
  BrightGreen | BgBrightGreen) RetVal=$BgBrightGreen ;;
  BrightYellow | BgBrightYellow) RetVal=$BgBrightYellow ;;
  BrightBlue | BgBrightBlue) RetVal=$BgBrightBlue ;;
  BrightMagenta | BgBrightMagenta) RetVal=$BgBrightMagenta ;;
  BrightCyan | BgBrightCyan) RetVal=$BgBrightCyan ;;
  BrightWhite | BgBrightWhite) RetVal=$BgBrightWhite ;;
  None) RetVal='' ;;
  *) RetVal=$ColorName ;;
  esac

  printf '%s' "$RetVal"
}

# ParseColor
#
# Parses Color.
#
# $1: ColorName
# $2: Foreground
#
ParseColor() {
  local -r ColorName="${1:-}"
  local -r Foreground="${2:-False}"

  [ "$Foreground" = 1 ] || [ "$Foreground" = True ] || [ "$Foreground" = true ] && ParseFgColor "$ColorName"
  [ "$Foreground" = 0 ] || [ "$Foreground" = False ] || [ "$Foreground" = false ] && ParseBgColor "$ColorName"

  return 0
}

# FormatLink
#
# Formats the given link/url.
#
# $1: Text
# $2: URL
# $3: Fallback mode
#
FormatLink() {
  if SupportsHyperlinks; then
    printf "${AnsiOsc}8;;%s\a%s${AnsiOsc}8;;\a\n" "$2" "$1"
    return
  fi

  case "$3" in
  --text) printf '%s\n' "$1" ;;
  --url | *) FormatUnderline "$2" ;;
  esac
}

# FormatUnderline
#
FormatUnderline() {
  IsTTY && printf "${Underline}%s${Reset}\n" "$*" || printf '%s\n' "$*"
}

# FormatCode
#
# Backtick in single-quote
#
# shellcheck disable=SC2016
FormatCode() {
  IsTTY && printf "\`${Faint}%s${Reset}\`\n" "$*" || printf '`%s`\n' "$*"
}

# FormatError
#
FormatError() {
  printf '%sError: %s%s\n' "${Bold}${FgRed}" "$*" "${Reset}" >&2
}

# StripAnsiEscapes
#
# Returns the stripped string.
#
# $1: String strip ANSI from.
#
StripAnsiEscapes() {
  local -r String="${1:-}"
  printf "${String}" | command sed 's~\x1b\[[0-9;]*[a-zA-Z]~~g'
  # printf %b '\n'
}

#
# endregion Terminal

#  vim: set ts=2 sw=2 tw=80 noet :
