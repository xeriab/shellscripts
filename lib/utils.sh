#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2059 disable=SC2034
# shellcheck disable=SC2154 disable=SC2155
#
# ./lib/utils.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Imports
#

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/global.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/os.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/array.sh"

# # shellcheck disable=SC1090 disable=SC1091
# . "$XDG_DATA_HOME/shellscripts/lib/compat/compat.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/terminal.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/logger.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/string.sh"

# shellcheck disable=SC1090 disable=SC1091
. "$XDG_DATA_HOME/shellscripts/lib/math.sh"

#
# endregion Imports

# region Utils
#

# region Mutables
#

# shellcheck disable=SC2034
[ -z "$CONSOLE_BOX_CONFIG" ] && CONSOLE_BOX_CONFIG='{ "Py": 1, "Px": 2, "Type": "Classic", "Color": "Cyan", "ContentAlign": "Center", "TitlePos": "Inside", "TitleAlign": "Left", "MaxWidth": 80 }'

# shellcheck disable=SC2034
[ -z "$CONSOLE_BOX_CONFIG_PX" ] && CONSOLE_BOX_CONFIG_PX=1

# shellcheck disable=SC2034
[ -z "$CONSOLE_BOX_CONFIG_PY" ] && CONSOLE_BOX_CONFIG_PY=2

# shellcheck disable=SC2034
[ -z "$CONSOLE_BOX_CONFIG_TYPE" ] && CONSOLE_BOX_CONFIG_TYPE=Classic

# shellcheck disable=SC2034
[ -z "$CONSOLE_BOX_CONFIG_COLOR" ] && CONSOLE_BOX_CONFIG_COLOR=Cyan

# shellcheck disable=SC2034
[ -z "$CONSOLE_BOX_CONFIG_CONTENT_ALIGN" ] && CONSOLE_BOX_CONFIG_CONTENT_ALIGN=Center

# shellcheck disable=SC2034
[ -z "$CONSOLE_BOX_CONFIG_TITLE_POSITION" ] && CONSOLE_BOX_CONFIG_TITLE_POSITION=Inside

# shellcheck disable=SC2034
[ -z "$CONSOLE_BOX_CONFIG_TITLE_ALIGN" ] && CONSOLE_BOX_CONFIG_TITLE_ALIGN=Left

# shellcheck disable=SC2034
[ -z "$CONSOLE_BOX_CONFIG_MAX_WIDTH" ] && CONSOLE_BOX_CONFIG_MAX_WIDTH=80

#
# endregion Mutables

# Question
#
# Asks the user.
#
# $1: Type            Question type, One of ['yes|no', 'yes|no|always|never', 'y|n'].
# $2: Message         Question message.
# $3: ResultVariable  Question result variable name.
#
Question() {
  local -r Type="${1:-'Yes|No'}"
  local Message="${2:-}"
  local -r ResultVariable="${3:-Choice}"
  local Choices=

  case $Type in
  # 'Yes|No' | 'yes|no')
  [Yy][Ee][Ss]'|'[Nn][Oo])
    Choices="${FgBrightBlack}(${Reset}${Faint}${FgBrightBlack}[${Reset}Y${Faint}${FgBrightBlack}]${Reset}es${FgBrightBlack}/${Reset}${Faint}${FgBrightBlack}[${Reset}N${Faint}${FgBrightBlack}]${Reset}o${FgBrightBlack})${Reset}"
    ;;
    # 'Yes|No|Always|Never' | 'yes|no|always|never')
  [Yy][Ee][Ss]'|'[Nn][Oo]'|'[Aa][Ll][Ww][Aa][Ss]'|'[Nn][Ee][Vv][Ee][Rr])
    Choices="${FgBrightBlack}(${Reset}${Faint}${FgBrightBlack}[${Reset}Y${Faint}${FgBrightBlack}]${Reset}es${FgBrightBlack}/${Reset}${Faint}${FgBrightBlack}[${Reset}N${Faint}${FgBrightBlack}]${Reset}o${FgBrightBlack}/${Reset}${Faint}${FgBrightBlack}[${Reset}A${Faint}${FgBrightBlack}]${Reset}lways${FgBrightBlack}/${Reset}N${Faint}${FgBrightBlack}[${Reset}e${Faint}${FgBrightBlack}]${Reset}ver${FgBrightBlack})${Reset}"
    ;;
  [Yy]'|'[Nn])
    Choices="${FgBrightBlack}(${Reset}Y${Faint}${FgBrightBlack}[${Reset}y${Faint}${FgBrightBlack}]${Reset}${FgBrightBlack}/${Reset}N${Faint}${FgBrightBlack}[${Reset}n${Faint}${FgBrightBlack}]${Reset}${FgBrightBlack})${Reset}"
    ;;
  *)
    Choices="${FgBrightBlack}(${Reset}Y${Faint}${FgBrightBlack}[${Reset}y${Faint}${FgBrightBlack}]${Reset}${FgBrightBlack}/${Reset}N${Faint}${FgBrightBlack}[${Reset}n${Faint}${FgBrightBlack}]${Reset}${FgBrightBlack})${Reset}"
    ;;
  esac

  # if [ "$Type" = "yes|no" ]; then
  #   Choices="${FgBrightBlack}(${Reset}${Faint}${FgBrightBlack}[${Reset}Y${Faint}${FgBrightBlack}]${Reset}es${FgBrightBlack}/${Reset}${Faint}${FgBrightBlack}[${Reset}N${Faint}${FgBrightBlack}]${Reset}o${FgBrightBlack})${Reset}"
  # elif [ "$Type" = "yes|no|always|never" ]; then
  #   Choices="${FgBrightBlack}(${Reset}${Faint}${FgBrightBlack}[${Reset}Y${Faint}${FgBrightBlack}]${Reset}es${FgBrightBlack}/${Reset}${Faint}${FgBrightBlack}[${Reset}N${Faint}${FgBrightBlack}]${Reset}o${FgBrightBlack}/${Reset}${Faint}${FgBrightBlack}[${Reset}A${Faint}${FgBrightBlack}]${Reset}lways${FgBrightBlack}/${Reset}N${Faint}${FgBrightBlack}[${Reset}e${Faint}${FgBrightBlack}]${Reset}ver${FgBrightBlack})${Reset}"
  # fi

  # # shellcheck disable=SC2034
  # local QuestionPrefix="${FgBrightYellow}Question${Reset}${FgBrightBlack}:${Reset} "

  # # shellcheck disable=SC2034
  # local ChoicePrefix="${FgBrightYellow}Choice${Reset}${FgBrightBlack}:${Reset} "

  # Message="${Faint}${FgBrightBlack}[${Reset}${Bold}${FgBlue}🛈${Reset}${Faint}${FgBrightBlack}]${Reset} ${Message} ${Choices}"
  Message="${Faint}${FgBrightBlack}[${Reset}${Bold}${FgBlue}?${Reset}${Faint}${FgBrightBlack}]${Reset} ${Message} ${Choices}"

  printf "%b" "${Message}${FgBrightBlack}:${Reset} "

  # eval "read $ResultVariable"
  eval "read -r $ResultVariable" || :
  # eval "export QUESTION_CHOICE=$ResultVariable"
  eval "QUESTION_CHOICE=\$$ResultVariable" || :

  [ "\$$ResultVariable" = "$NEWLINE" ] && return 1

  # printf "%b" "${FgBrightYellow}Choice${Reset}${FgBrightBlack}${Faint}:${Reset} "

  # eval "read $Choice"

  # # Check input
  # #
  # # shellcheck disable=SC2154
  # case "$QUESTION_CHOICE" in
  # # "" | \$'\n' | [\\n] | "no" | "NO" | "No" | [nN])
  # # "" | "$nl" | [\\n] | "no" | "NO" | "No" | [nN])
  # "" | "$NEWLINE" | "no" | "NO" | "No" | [nN])
  # 	return 1
  # 	;;
  # "yes" | "YES" | "Yes" | [yY])
  # 	# eval "QUESTION_CHOICE=\$$input"
  # 	return 0
  # 	;;
  # *)
  # 	return 1
  # 	;; # Interpret anything else as a yes
  # esac

  unset Message Choices

  return 0
}

# AskDeleteTempFiles
#
# Ask user if they wants to delete temporary files generated by this script.
#
AskDeleteTempFiles() {
  local RetVal=0

  Question 'yes|no' "${Bold}Delete temporary files${Normal}${Bold}?${Reset} ${FgBrightBlack}(${Reset}${Faint}${TEMP_DIR}${Reset}${FgBrightBlack})${Reset}" 'Choice'

  # Check input
  #
  # shellcheck disable=SC2154
  case "$Choice" in
  # "" | "$NEWLINE" | [yY])
  "$NEWLINE" | 1 | [Yy][Ee][Ss] | [Yy])
    command rm -rf "$TEMP_DIR"
    Log -i "Temporary files have been deleted."
    RetVal=0
    ;;
  "" | 0 | [Nn][Oo] | [Nn])
    Log -i "Temporary files saved in ${Faint}'${Reset}${TEMP_DIR}${Faint}'${Reset}"
    RetVal=1
    ;;
  *)
    Log -i "Temporary files saved in ${Faint}'${Reset}${TEMP_DIR}${Faint}'${Reset}"
    RetVal=1
    ;;
  esac

  unset Choice

  return $RetVal
}

# Die
#
# Custom exit function, it integrates AskDeleteTempFiles for managing script
# created files and folders.
#
# @depends AskDeleteTempFiles
#
Die() {
  local Argc=$#

  local Code="${1:-0}"
  [ $# -gt 1 ] && shift

  local Message="${1:-}"
  [ $# -gt 1 ] && shift

  IFS=$SAVEIFS

  (IsDir "${TEMP_DIR}") && AskDeleteTempFiles 1>/dev/null 2>&1

  [ $Argc -gt 1 ] && Log -e "$Message" "$@" >&2
  [ $Argc -eq 0 ] && exit

  exit "$Code"
}

# Abort
#
# @depends Log
# @depends Die
#
Abort() {
  Log -e "$@" >&2
  Die 1
}

# Panic
#
# Prints Error Message.
#
# $1: Message
#
# @depends Log
# @depends Die
#
Panic() {
  local -r Message="${1:-}"
  local Place="${CurrentFile:-}"
  local -r Where="${Self:-$SCRIPT}()"
  [ -n "$Place" ] && Place="${FgBrightMagenta}${Place}${Reset}${Faint}:${Reset} "
  printf "${Place}${FgBrightBlue}${Where}${Reset}${Faint}:${Reset} ${FgRed}${Message}${Reset}" >&2
  printf %b "\n" >&2
  Die 1
}

# EvalLike
#
EvalLike() {
  Pattern=$1

  Variables=$(set | command grep "^$Pattern.*=" | command cut -f 1 -d '=')

  for Variable in $Variables; do
    eval value="\$$Variable" || :
    printf "$value"
  done

  unset Variable
}

# EchoValue
#
EchoValue() {
  local -r Value="${1:-}"
  eval printf %b "\$${Value}" || :
}

# RequireVar
#
# Require variable(s) or Die
#
RequireVar() {
  for Variable; do
    if [ "$(EchoValue "$Variable")" ]; then
      Log -i "Environment variable '%s' is set to: '%s'" "$Variable" "$(EchoValue "$Variable")"
    else
      Die 1 "Required environment variable '%s' is not set!" "$Variable"
    fi
  done
}

# SetDefault
#
# Set default value for environment variable
#
SetDefault() {
  local Variable="${1:-}"
  shift

  if [ "$(EchoValue "$Variable")" ]; then
    Log -i "Environment variable '%s' is set to: '%s'" "$Variable" "$(EchoValue "$Variable")"
  else
    eval "$Variable=$*" || :
    Log -i "Setting environment variable '%s' to default value: '%s'" "$Variable" "$(EchoValue "$Variable")"
    export "${Variable?}"
  fi
}

# SetKey
#
# Update key in Java property file / INI file
#
SetKey() {
  local File="${1:-}"
  local Key="${2:-}"
  local Value="${3:-}"
  local Esc=

  Esc=$(SedEscape "${3:-}")

  # Check if file exists
  if [ ! -f "$File" ]; then
    Log -e "File '%s' doesn't exist!" "$File"
    return 1
  fi

  Log -i "Setting property '%s' in file '%s' to: '%s'" "$Key" "$File" "$Value"

  # Replace using sed
  command sed -i --follow-symlinks -e "/^${Key}[ ]*=/{h;s/\(=[ ]*\).*/\1$Esc/};\${x;/^\$/{s//$Key=$Esc/;H};x}" "$File"

  # Show file if in debug mode
  IsDebuggable && command tail -vn+1 "$File"
}

#
# Key-value
#

# GetValue
#
# [Key] [DB]
#
GetValue() {
  local Key="${1:-}"
  local DB="${2:-}"

  if [ -f "$DB" ]; then
    command sed --quiet "s~^$Key \(.*\)~\1~p" "$DB"
  fi
}

# SetValue
#
# [Key] [Value] [DB]
#
SetValue() {
  local Key="${1:-}"
  local Value="${2:-}"
  local DB="${3:-}"

  if command grep --quiet --no-messages "^$Key " "$DB"; then
    command sed --in-place "s/^\($Key \)\(.*\)/\1$(SedEscape "$Value")/" "$DB" &>>"$LOGGING_DEVICE"
  else
    printf "$Key $Value" >>"$DB"
  fi
}

# GetEnv
#
# Get envireonment variable
#
# $1: Name    Key name.
# $2: Default Default value.
#
GetEnv() {
  local -r Name="${1:-}"
  local -r Default="${2:-}"

  if [ "$(EchoValue "${Name}")" ]; then
    EchoValue "${Name}"
  else
    printf "${Default}"
  fi
}

# SetEnv
#
# Set envireonment variable
#
# $1: Name    Key name.
# $2: Default Default value.
# $3: Strict  Strict mode.
#
SetEnv() {
  local -r Name="${1:-}"
  local -r Value="${2:-}"
  local -r Strict="${3:-True}"

  local Key=

  Self='SetEnv'

  [ "$Strict" = 1 ] || [ "$Strict" = True ] || [ "$Strict" = true ] && {
    Key=$(printf "$Name" | tr '[:lower:]' '[:upper:]')
  }

  if [ "$(EchoValue "$Key")" ]; then
    IsDebuggable && {
      Log -i "Environment variable ${Faint}'${Reset}%s${Faint}'${Reset} is set to: ${Faint}'${Reset}%s${Faint}'${Reset}" "$Key" "$Value"
    }
  else
    eval "$Key=$Value" || :
    IsDebuggable && {
      Log -i "Setting environment variable ${Faint}'${Reset}%s${Faint}'${Reset} to default value: ${Faint}'${Reset}%s${Faint}'${Reset}" "$Key" "$Value"
    }
    # export "\$${Key?}"
    export "${Key?}"
  fi

  unset Key
}

# FileFromTemplate
#
# Populate file(s) from template
#
FileFromTemplate() {
  local Template="${1:-}"
  local File="${2:-}"

  # Check if file exists
  if ! [ -f "$Template" ]; then
    Log -e "File $Template doesn't exist!"
    false
  fi

  Log -i "Creating file '%s' from template '%s'" "$File" "$Template"

  # Populate from template
  command envsubst <"$Template" >"$File"

  # Show file if in debug mode
  IsDebuggable && command tail -vn+1 "$File"
}

# Check
#
Check() {
  local CheckCommand=

  if (IsZsh) || (IsBash); then
    CheckCommand=which
  elif (IsAsh) || (IsDash); then
    # shellcheck disable=SC2209
    CheckCommand=hash
  else
    CheckCommand='command -v'
  fi

  $CheckCommand "$@"
}

# HasCommand
#
# Check command availability
#
HasCommand() {
  Check "${1:-}" 1>/dev/null 2>&1
}

# CheckFor
#
CheckFor() {
  # # shellcheck disable=SC2034
  # Self="${Self:-CheckFor}"

  # shellcheck disable=SC2034
  Self='CheckFor'

  local Dependency="${1:-}"

  if (Check "$Dependency" >/dev/null 2>&1); then
    MISSING_DEPENDENCY=True

    if IsDebuggable; then
      Log -i "Command ${Faint}'${Reset}${Bold}${Dependency}${Reset}${Faint}'${Reset} is found!" >&1
    fi

    return 0
  else
    MISSING_DEPENDENCY=False
    Log -e "Command ${Faint}'${Reset}${Bold}${Dependency}${Reset}${Faint}'${Reset} is required!" >&2
    return 1
    # False
  fi

  # for Command; do
  #   CommandPath=$(command -v "$Command")
  #   case "$CommandPath" in
  #   /*) continue ;;
  #   *)
  #     Log -e "Missing dependency '[$Command]' is not found"
  #     MISSING_DEPENDENCY=True
  #     ;;
  #   esac
  # done

  # unset Command

  unset Self

  return 0
}

# CheckCommand
#
CheckCommand() {
  if [ $# -lt 1 ]; then
    Log -e "CheckCommand() command argument should be provided" >&2
    return 1
  fi

  HasCommand "$1"

  # printf "%b\n" $? 2>&1
  return $?
}

# GetCommand
#
GetCommand() {
  if [ $# -lt 1 ]; then
    Log -e "GetCommand() command argument should be provided" >&2
    return 1
  fi

  local Command=

  if (CheckCommand "$1"); then
    Command=$(Check "$1")
  fi

  printf "$Command"
}

# ProgramExists
#
ProgramExists() {
  if [ $# -lt 1 ]; then
    Log -e "ProgramExists() command argument should be provided" >&2
    return 1
  fi

  local Program="${1:-}"
  local Message="${2:-"Program ${Faint}'${Reset}${Program}${Faint}'${Reset} does not exist."}"
  local RetVal=

  CheckCommand "${Program}"
  RetVal=$?

  ! [ $RetVal = 0 ] && {
    # Log -e "Program ${Faint}'${Reset}${Program}${Faint}'${Reset} does not exist." >&2
    Log -e "${Message}" >&2
    Die 1
  } || return 0

}

# Todo
#
# $1: Function
# $2: Message
# $3: Exit
#
# @depends Log
# @depends Die
#
# shellcheck disable=SC2120
#
Todo() {
  local Function="${1:-}"
  local Message="${2:-}"
  local -r Flag="${3:-}"

  [ -n "$Function" ] && {
    Function="${Faint}'${Reset}${Bold}${Function}${Reset}${FgBrightBlack}()${Reset}${Faint}'${Reset}"
    Function="${Bold}${FgBrightYellow}Function${Reset} ${Function} "
  } || Function=

  [ -z "$Message" ] && Message="${Bold}${FgBrightYellow}Is not implemented yet!${Reset}"

  Log -f "${Function}${Message}" >&2
  # LogFailure "${Function}${Message}" >&2

  if [ "$Flag" = "--exit" ] || [ "$Flag" = "-exit" ] || [ "$Flag" = "-e" ]; then
    Die 1
  fi

  return 1
}

# RandomInt
#
RandomInt() {
  local -r Max="${1:-16}"

  # A portable PRNG:
  # the initial seed is the size of our environment vars
  # on each iteration, we add user-entropy

  if [ -z "$SEED" ]; then
    SEED=$(set)
    SEED=${#SEED}
    ENTROPY=1
  else
    SEED=$((SEED * SEED * ENTROPY))
  fi

  unset ENTROPY

  # Greater then $FFFF?
  while [ ${#SEED} -gt 5 ]; do
    SEED=$((SEED / 256))
  done

  printf '%d\n' $(((SEED % Max) + 1))
}

# region Misc
#

# CheckSudo
#
CheckSudo() {
  Todo
  return 0
}

# Push
#
Push() {
  local PushOne=
  local PushTwo=
  local PushThree=
  local PushFour=
  local PushFive=
  local PushSix=

  case $1 in
  -c)
    PushOne=
    shift
    ;;
  *)
    eval PushOne=\$"$1" || :
    ;;
  esac

  PushTwo="$1"

  shift

  for PushFive; do
    [ -z "${PushOne:++}" ] || PushOne="$PushOne "

    unset PushSix

    case ${PushFive:-=} in
    [=~]*)
      PushSix=False
      ;;
    esac

    PushThree=$PushFive

    while PushFour=${PushThree%%\'*}; do
      if ${PushSix-:} && case $PushFour in
      *[!-+=~@%/:.,_0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ]*)
        false
        ;;
      esac then
        PushOne=$PushOne$PushFour
      else
        PushOne="$PushOne'$PushFour'"
        unset PushSix
      fi

      # [ x"$PushFour" = x"$PushThree" ] && {
      #   break
      # } || PushOne=$PushOne\\\'

      [ "$PushFour" = x"$PushThree" ] && {
        break
      } || PushOne=$PushOne\\\'

      PushThree=${PushThree#*\'}
    done
  done

  eval "$PushTwo=\$PushOne || :
	unset PushOne PushTwo PushThree PushFour PushFive
	[ -n \"\${$PushTwo:++}\" ]" || return 1
}

# LogHeader
#
# Prints Message.
#
# $1: Message
#
LogHeader() {
  local Message="${1:-}"
  shift

  local Len=
  local TermWidth=
  local HalfLen=
  local LeftDecorator=
  local LeftDecoratorLen=
  local RightDecorator=
  local LeftSep="${Faint}${FgBrightCyan}[${Reset} "
  local RightSep=" ${Faint}${FgBrightCyan}]${Reset}"
  local Title=
  local TitleLen=

  # Len="$(StrLen "$Message")"
  Len="$(StrLen "$(StrStripAnsi "$Message")")"
  TermWidth=$(TerminalWidth)
  Len=$((TermWidth - Len - 4))
  HalfLen=$((Len / 2))
  LeftDecoratorLen=$((HalfLen - 6))
  RightDecorator="$(StrRepeat "${Faint}${FgBrightCyan}-${Reset}" $((LeftDecoratorLen + HalfLen)))"
  LeftDecorator="$(StrRepeat "${Faint}${FgBrightCyan}-${Reset}" 6)"
  Title="${LeftDecorator}${LeftSep}${FgBrightBlue}${Message}${Reset}${RightSep}${RightDecorator}"
  TitleLen="$(StrLen "$(StrStripAnsi "$Title")")"

  ! [ "$TermWidth" -eq "$TitleLen" ] && Title="${Title}${Faint}${FgBrightCyan}-${Reset}"

  printf '%s\n' "$Title" >&2
}

# LogFooter
#
# Prints Message.
#
LogFooter() {
  printf '%s\n' "$(StrRepeat "${Faint}${FgBrightCyan}-${Reset}" "$(TerminalWidth)")"
}

# BoxSetConfig
#
# Set Box Config.
#
# $1: Config
#
# return Nothing
#
BoxSetConfig() {
  # Box Config
  local Config="${1:-${CONSOLE_BOX_CONFIG:-"{ "Py": 1, "Px": 2, "Type": "Classic", "Color": "Cyan", "ContentAlign": "Center", "TitlePos": "Inside", "TitleAlign": "Left", "MaxWidth": 80 }"}}"

  # Vertical Padding
  local Px=

  # Horizontal Padding
  local Py=

  # Type of Box
  local Type=

  # Color of Box
  local Color=

  # Content Alignment inside the Box
  local ContentAlign=

  # Title Position
  local TitlePos=

  # Title Alignment
  local TitleAlign=

  # Maximum terminal width
  local MaxWidth=

  Config=$(printf "$Config" | command grep -E -o '^[^//]*' | StrStripLines | StrNormalizeSpaces)

  Px=$(printf "$Config" | command grep -Po '(?<=\s"Px":\s|"Px":\s|"Px":|\sPx:\s|Px:\s|Px:)\d+')
  Py=$(printf "$Config" | command grep -Po '(?<=\s"Py":\s|"Py":\s|"Py":|\sPy:\s|Py:\s|Py:)\d+')
  Type=$(printf "$Config" | command grep -Po '(?<=\s"Type":\s|"Type":\s|"Type":|\sType:\s|Type:\s|Type:)(\"+\w+\"|\w+)+')
  Color=$(printf "$Config" | command grep -Po '(?<=\s"Color":\s|"Color":\s|"Color":|\sColor:\s|Color:\s|Color:)(\"+([\e|\\033|\\x1b]+\[+[\d\;]*[mGKHF]|\w)+\")+')
  ContentAlign=$(printf "$Config" | command grep -Po '(?<=\s"ContentAlign":\s|"ContentAlign":\s|"ContentAlign":|\sContentAlign:\s|ContentAlign:\s|ContentAlign:)(\"+\w+\"|\w+)+')
  TitlePos=$(printf "$Config" | command grep -Po '(?<=\s"TitlePos":\s|"TitlePos":\s|"TitlePos":|\sTitlePos:\s|TitlePos:\s|TitlePos:)(\"+\w+\"|\w+)+')
  TitleAlign=$(printf "$Config" | command grep -Po '(?<=\s"TitleAlign":\s|"TitleAlign":\s|"TitleAlign":|\sTitleAlign:\s|TitleAlign:\s|TitleAlign:)(\"+\w+\"|\w+)+')
  MaxWidth=$(printf "$Config" | command grep -Po '(?<=\s"MaxWidth":\s|"MaxWidth":\s|"MaxWidth":|\sMaxWidth:\s|MaxWidth:\s|MaxWidth:)\d+')

  Type=$(StrReplace '"' '' "$Type")
  Color=$(StrReplace '"' '' "$Color")
  ContentAlign=$(StrReplace '"' '' "$ContentAlign")
  TitlePos=$(StrReplace '"' '' "$TitlePos")
  TitleAlign=$(StrReplace '"' '' "$TitleAlign")
  Color=$(ParseColor "$Color" True)

  [ "$MaxWidth" -eq 0 ] || [ "$MaxWidth" = 0 ] && MaxWidth=$(TerminalWidth)

  CONSOLE_BOX_CONFIG_PX=$Px
  CONSOLE_BOX_CONFIG_PY=$Py
  CONSOLE_BOX_CONFIG_TYPE=$Type
  CONSOLE_BOX_CONFIG_COLOR=$Color
  CONSOLE_BOX_CONFIG_CONTENT_ALIGN=$ContentAlign
  CONSOLE_BOX_CONFIG_TITLE_POSITION=$TitlePos
  CONSOLE_BOX_CONFIG_TITLE_ALIGN=$TitleAlign
  CONSOLE_BOX_CONFIG_MAX_WIDTH=$MaxWidth

  unset Config Px Py Type Color ContentAlign TitlePos TitleAlign MaxWidth

  return 0
}

# Box
#
# Prints Box.
#
# $1: Title
# $2: Message
#
Box() {
  Self=Box
  CurrentFile='utils.sh'

  # Box Types
  local -r Types="$(Array 'Bold' 'Classic' 'Double' 'DoubleSingle' 'Hidden' 'Round' 'Single' 'SingleDouble')"

  # Title Positions
  local -r TitlePositions="$(Array 'Inside' 'Bottom' 'Top')"

  # Content Alignments
  local -r ContentAligns="$(Array 'Left' 'Right' 'Center')"

  # Title Alignments
  local -r TitleAligns="$(Array 'Left' 'Right' 'Center')"

  # # 1 = Separator
  # # 2 = Spacing
  # # 3 = Line
  # # 4 = OddSpace
  # # 5 = Space
  # # 6 = SideMargin
  # #
  # local -r CenterAlign='%[1]s%[2]s%[3]s%[4]s%[2]s%[1]s'
  # local -r LeftAlign='%[1]s%[6]s%[3]s%[4]s%[2]s%[5]s%[1]s'
  # local -r RightAlign='%[1]s%[2]s%[4]s%[5]s%[3]s%[6]s%[1]s'

  local Px="${CONSOLE_BOX_CONFIG_PX:-1}"
  local Py="${CONSOLE_BOX_CONFIG_PY:-2}"
  local Type="${CONSOLE_BOX_CONFIG_TYPE:-Classic}"
  local Color="${CONSOLE_BOX_CONFIG_COLOR:-}"
  local TitlePos="${CONSOLE_BOX_CONFIG_TITLE_POSITION:-Inside}"
  local TitleAlign="${CONSOLE_BOX_CONFIG_TITLE_ALIGN:-Left}"
  local ContentAlign="${CONSOLE_BOX_CONFIG_CONTENT_ALIGN:-Center}"
  local MaxWidth="${CONSOLE_BOX_CONFIG_MAX_WIDTH:-80}"

  # Symbols used for TopRight Corner
  local TopRight=

  # Symbols used for TopLeft Corner
  local TopLeft=

  # Symbols used for BottomRight Corner
  local BottomRight=

  # Symbols used for BottomRight Corner
  local BottomLeft=

  # Symbols used for Horizontal Bars
  local Horizontal=

  # Symbols used for Vertical Bars
  local Vertical=

  if (InArray "$Type" "$Types"); then
    case $Type in
    Classic)
      TopRight='+'
      TopLeft='+'
      BottomRight='+'
      BottomLeft='+'
      Horizontal='-'
      Vertical='|'
      ;;
    Single)
      TopRight='┐'
      TopLeft='┌'
      BottomRight='┘'
      BottomLeft='└'
      Horizontal='─'
      Vertical='│'
      ;;
    Double)
      TopRight='╗'
      TopLeft='╔'
      BottomRight='╝'
      BottomLeft='╚'
      Horizontal='═'
      Vertical='║'
      ;;
    Round)
      TopRight='╮'
      TopLeft='╭'
      BottomRight='╯'
      BottomLeft='╰'
      Horizontal='─'
      Vertical='│'
      ;;
    Bold)
      TopRight='┓'
      TopLeft='┏'
      BottomRight='┛'
      BottomLeft='┗'
      Horizontal='━'
      Vertical='┃'
      ;;
    SingleDouble)
      TopRight='╖'
      TopLeft='╓'
      BottomRight='╜'
      BottomLeft='╙'
      Horizontal='─'
      Vertical='║'
      ;;
    DoubleSingle)
      TopRight='╕'
      TopLeft='╒'
      BottomRight='╛'
      BottomLeft='╘'
      Horizontal='═'
      Vertical='│'
      ;;
    Hidden)
      TopRight='+'
      TopLeft='+'
      BottomRight='+'
      BottomLeft='+'
      Horizontal=' '
      Vertical=' '
      ;;
    esac
  else
    Panic "Unknown Type. value must be one of [Bold, Classic, Double, DoubleSingle, Hidden, Round, Single, SingleDouble]."
  fi

  local Title="${1:-}"
  local Message="${2:-}"
  local Stripped=

  Title=" $(StrNormalize "${Title}") "
  Stripped=$(StrStripAnsi "${Title}")
  TitleLen=$(StrLen "${Stripped}")

  Message=" $(StrNormalize "${Message}") "
  Stripped=$(StrStripAnsi "${Message}")
  MessageLen=$(StrLen "${Stripped}")

  local BoxHead=
  local BoxBody=
  local BoxFoot=

  local Spacer
  local TermWidth=
  local RepeatWidth=
  local LeftRepeat=

  if (IsInteger "$Px") && [ "$Px" -lt "$MaxWidth" ] && [ "$Px" -gt 0 ]; then
    (IsOdd "$Px") && Px=$((Px + 1))
    TermWidth="$((MaxWidth - Px))"
  else
    TermWidth=$((MaxWidth - 10))
  fi

  RepeatWidth=$((TermWidth - 2))

  # (IsOdd "$Py") && Py="$((Py + 1))"

  if ! (InArray "$ContentAlign" "$ContentAligns"); then
    Panic "Unknown ContentAlign. value must be one of [Left, Right, Center]."
  fi

  if ! (InArray "$TitleAlign" "$TitleAligns"); then
    Panic "Unknown TitleAlign. value must be one of [Left, Right, Center]."
  fi

  if (InArray "$TitlePos" "$TitlePositions"); then
    case $TitlePos in
    Bottom)
      BoxHead="${Color}${TopLeft}$(StrRepeat "${Horizontal}" $RepeatWidth)${TopRight}${Reset}"

      LeftRepeat=$RepeatWidth
      (IsOdd "$LeftRepeat") && LeftRepeat=$((LeftRepeat - 1))

      if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
        for Times in $(Range 1 "$Py"); do
          BoxBody=$(printf "${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}\n${BoxBody}")
        done
      fi

      Spacer=''
      RepeatWidth=$((RepeatWidth - MessageLen))
      RepeatWidth=$((RepeatWidth / 2))
      LeftRepeat=$RepeatWidth

      (IsOdd "$MessageLen") && {
        Spacer=' '
        # LeftRepeat=$((LeftRepeat + 1))
      }

      if [ "$ContentAlign" = "Left" ]; then
        if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}${Message}${Spacer}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $RepeatWidth)${Color}${Vertical}${Reset}")
        elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
          BoxBody=$(printf "${Color}${Vertical}${Reset}${Spacer}${Message}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
        fi
      elif [ "$ContentAlign" = "Right" ]; then
        if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $RepeatWidth)${Message}${Spacer}${Color}${Vertical}${Reset}")
        elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
          BoxBody=$(printf "${Color}${Vertical}${Reset}${Spacer}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $LeftRepeat)${Message}${Color}${Vertical}${Reset}")
        fi
      elif [ "$ContentAlign" = "Center" ]; then
        if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Message}${Spacer}$(StrRepeat ' ' $RepeatWidth)${Color}${Vertical}${Reset}")
        elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
          BoxBody=$(printf "${Color}${Vertical}${Reset}${Spacer}$(StrRepeat ' ' $LeftRepeat)${Message}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
        fi
      fi

      RepeatWidth=$((TermWidth - 2))
      LeftRepeat=$RepeatWidth

      (IsOdd "$LeftRepeat") && LeftRepeat=$((LeftRepeat - 1))

      if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
        for Times in $(Range 1 "$Py"); do
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
        done
      elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
        BoxBody=$(printf "${BoxBody}")
      fi

      RepeatWidth=$((TermWidth - 2))
      RepeatWidth=$((RepeatWidth - TitleLen))
      LeftRepeat=$RepeatWidth

      (IsOdd "$LeftRepeat") && LeftRepeat=$((LeftRepeat - 1)) || LeftRepeat=$((LeftRepeat - 1))

      BoxFoot="${Color}${BottomLeft}${Horizontal}${Reset}${Title}${Color}$(StrRepeat "${Horizontal}" $LeftRepeat)${BottomRight}${Reset}"
      ;;
    Inside)
      BoxHead="${Color}${TopLeft}$(StrRepeat "${Horizontal}" $RepeatWidth)${TopRight}${Reset}"

      LeftRepeat=$RepeatWidth
      (IsOdd "$LeftRepeat") && LeftRepeat=$((LeftRepeat - 1))

      if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
        for Times in $(Range 1 "$Py"); do
          BoxBody=$(printf "${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}\n${BoxBody}")
        done
      fi

      Spacer=''
      RepeatWidth=$((RepeatWidth - TitleLen))
      RepeatWidth=$((RepeatWidth / 2))
      LeftRepeat=$RepeatWidth

      (IsOdd "$TitleLen") && {
        Spacer=' '
        # LeftRepeat=$((LeftRepeat + 1))
      }

      if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
        BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Title}${Spacer}$(StrRepeat ' ' $RepeatWidth)${Color}${Vertical}${Reset}")
      elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
        BoxBody=$(printf "${Color}${Vertical}${Reset}${Spacer}$(StrRepeat ' ' $LeftRepeat)${Title}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
      fi

      RepeatWidth=$((TermWidth - 2))

      if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
        LeftRepeat=$RepeatWidth
        (IsOdd "$LeftRepeat") && LeftRepeat=$((LeftRepeat - 1))
        BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
      fi

      Spacer=''
      RepeatWidth=$((RepeatWidth - MessageLen))
      RepeatWidth=$((RepeatWidth / 2))
      LeftRepeat=$RepeatWidth

      (IsOdd "$MessageLen") && {
        Spacer=' '
        # LeftRepeat=$((LeftRepeat + 1))
      }

      if [ "$ContentAlign" = "Left" ]; then
        if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}${Message}${Spacer}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $RepeatWidth)${Color}${Vertical}${Reset}")
        elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}${Spacer}${Message}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
        fi
      elif [ "$ContentAlign" = "Right" ]; then
        if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $RepeatWidth)${Message}${Spacer}${Color}${Vertical}${Reset}")
        elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}${Spacer}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $LeftRepeat)${Message}${Color}${Vertical}${Reset}")
        fi
      elif [ "$ContentAlign" = "Center" ]; then
        if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Message}${Spacer}$(StrRepeat ' ' $RepeatWidth)${Color}${Vertical}${Reset}")
        elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}${Spacer}$(StrRepeat ' ' $LeftRepeat)${Message}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
        fi
      fi

      RepeatWidth=$((TermWidth - 2))
      LeftRepeat=$RepeatWidth

      (IsOdd "$LeftRepeat") && LeftRepeat=$((LeftRepeat - 1))

      if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
        for Times in $(Range 1 "$Py"); do
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
        done
      elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
        BoxBody=$(printf "${BoxBody}")
      fi

      BoxFoot="${Color}${BottomLeft}$(StrRepeat "${Horizontal}" $RepeatWidth)${BottomRight}${Reset}"
      ;;
    Top)
      RepeatWidth=$((TermWidth - 2))
      RepeatWidth=$((RepeatWidth - TitleLen))
      LeftRepeat=$RepeatWidth

      (IsOdd "$LeftRepeat") && LeftRepeat=$((LeftRepeat - 1))

      BoxHead="${Color}${TopLeft}${Horizontal}${Reset}${Title}${Color}$(StrRepeat "${Horizontal}" $LeftRepeat)${TopRight}${Reset}"

      RepeatWidth=$((TermWidth - 2))
      LeftRepeat=$RepeatWidth
      (IsOdd "$LeftRepeat") && LeftRepeat=$((LeftRepeat - 1))

      if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
        for Times in $(Range 1 "$Py"); do
          BoxBody=$(printf "${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}\n${BoxBody}")
        done
      fi

      Spacer=''
      RepeatWidth=$((RepeatWidth - MessageLen))
      RepeatWidth=$((RepeatWidth / 2))
      LeftRepeat=$RepeatWidth

      (IsOdd "$MessageLen") && {
        Spacer=' '
        # LeftRepeat=$((LeftRepeat + 1))
      }

      if [ "$ContentAlign" = "Left" ]; then
        if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}${Message}${Spacer}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $RepeatWidth)${Color}${Vertical}${Reset}")
        elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
          BoxBody=$(printf "${Color}${Vertical}${Reset}${Spacer}${Message}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
        fi
      elif [ "$ContentAlign" = "Right" ]; then
        if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $RepeatWidth)${Message}${Spacer}${Color}${Vertical}${Reset}")
        elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
          BoxBody=$(printf "${Color}${Vertical}${Reset}${Spacer}$(StrRepeat ' ' $LeftRepeat)$(StrRepeat ' ' $LeftRepeat)${Message}${Color}${Vertical}${Reset}")
        fi
      elif [ "$ContentAlign" = "Center" ]; then
        if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Message}${Spacer}$(StrRepeat ' ' $RepeatWidth)${Color}${Vertical}${Reset}")
        elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
          BoxBody=$(printf "${Color}${Vertical}${Reset}${Spacer}$(StrRepeat ' ' $LeftRepeat)${Message}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
        fi
      fi

      RepeatWidth=$((TermWidth - 2))
      LeftRepeat=$RepeatWidth

      (IsOdd "$LeftRepeat") && LeftRepeat=$((LeftRepeat - 1))

      if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
        for Times in $(Range 1 "$Py"); do
          BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
        done
      elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
        BoxBody=$(printf "${BoxBody}")
      fi

      BoxFoot="${Color}${BottomLeft}$(StrRepeat "${Horizontal}" $RepeatWidth)${BottomRight}${Reset}"
      ;;
    esac
  else
    Panic "Unknown TitlePos. value must be one of [Inside, Bottom, Top]."
  fi

  TermWidth=$(TerminalWidth)

  printf "$(StrPadCenter "${BoxHead}" "$TermWidth" ' ')\n"
  printf '%s\n' "$BoxBody" | while IFS="$(printf '\n')" read -r Line; do
    if ! [ "$Line" = "" ]; then
      printf "$(StrPadCenter "$Line" "$TermWidth" ' ')\n"
    fi
  done
  printf "$(StrPadCenter "${BoxFoot}" "$TermWidth" ' ')\n"

  # printf '%s\n%s\n%s\n' "$BoxHead" "$BoxBody" "$BoxFoot"

  unset TopRight TopLeft BottomRight BottomLeft Horizontal Vertical
  unset Type Px Py Color TitlePos ContentAlign MaxWidth
  unset Spacer TermWidth RepeatWidth BoxHead BoxBody BoxFoot
  unset Self CurrentFile

  return 0
}

# HorizontalLine
#
# Prints Horizontal Line.
#
# $1: Type
#
HorizontalLine() {
  local -r Type="${1:-Classic}"
  Self=Box
  CurrentFile='utils.sh'

  # Box Types
  local -r Types="$(Array 'Bold' 'Classic' 'Double' 'DoubleSingle' 'Hidden' 'Round' 'Single' 'SingleDouble')"

  # Symbols used for Horizontal Bars
  local Horizontal=

  if (InArray "$Type" "$Types"); then
    case $Type in
    Classic)
      Horizontal='-'
      ;;
    Single)
      Horizontal='─'
      ;;
    Double)
      Horizontal='═'
      ;;
    Round)
      Horizontal='─'
      ;;
    Bold)
      Horizontal='━'
      ;;
    SingleDouble)
      Horizontal='─'
      ;;
    DoubleSingle)
      Horizontal='═'
      ;;
    Hidden)
      Horizontal=' '
      ;;
    esac
  else
    Panic "Unknown Type. value must be one of [Bold, Classic, Double, DoubleSingle, Hidden, Round, Single, SingleDouble]."
  fi

  TermWidth=$(TerminalWidth)

  RepeatWidth=$((TermWidth - 14))
  # LeftRepeat=$RepeatWidth

  # (IsOdd "$LeftRepeat") && LeftRepeat=$((LeftRepeat - 1))

  # if (IsInteger "$Py") && [ "$Py" -lt "$(TerminalHeight)" ] && [ "$Py" -gt 0 ]; then
  #   for Times in $(Range 1 "$Py"); do
  #     BoxBody=$(printf "${BoxBody}\n${Color}${Vertical}${Reset}$(StrRepeat ' ' $LeftRepeat)${Color}${Vertical}${Reset}")
  #   done
  # elif (IsInteger "$Py") && [ "$Py" -eq 0 ]; then
  #   BoxBody=$(printf "${BoxBody}")
  # fi

  Line="$(StrRepeat "${Horizontal}" $RepeatWidth)"

  printf "$(StrPadCenter "${CONSOLE_BOX_CONFIG_COLOR}${Line}${Reset}" "$TermWidth" ' ')\n"

  # printf '%s\n%s\n%s\n' "$BoxHead" "$BoxBody" "$BoxFoot"

  unset Horizontal TermWidth RepeatWidth Self CurrentFile

  return 0
}

# GitRepoTimestamp
#
GitRepoTimestamp() {
  command git log --max-count=1 --date=short --pretty=format:%cr
}

# GitRepoID
#
GitRepoID() {
  command git log --max-count=1 --pretty=format:%H
}

#
# endregion Utils

#  vim: set ts=2 sw=2 tw=80 noet :
