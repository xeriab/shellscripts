#!/usr/bin/env zsh
#
# shellcheck disable=SC3043
#
# ./plugins/zsh-common/plugin-options.zsh
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

zpm_plugin_source=false
zpm_plugin_bin_path=false
zpm_plugin_functions_path=true
zpm_plugin_autoload=appendfpath:appendpath:check-if:debug:fpath:hyperlink:hyperlink-file:hyperlink-file-pr:hyperlink-pr:is-callable:is-recursive-exist:is-true:is-false:p:path:prependfpath:prependpath
