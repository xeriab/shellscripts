#!/usr/bin/env zsh
#
# shellcheck disable=SC3043
#
# ./plugins/zsh-common/zsh-helpers.plugin.zsh
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# Standarized ZSH polyfills, following:
# https://github.com/zdharma/Zsh-100-Commits-Club/blob/master/Zsh-Plugin-Standard.adoc
0="${${ZERO:-${0:#$ZSH_ARGZERO}}:-${(%):-%N}}"
0="${${(M)0:#/*}:-$PWD/$0}"

if [[ $PMSPEC != *f* ]] {
  fpath+=( "${0:h}/../../functions/zsh" )
}

autoload -Uz                      \
  path fpath p appendpath         \
  appendfpath prependpath         \
  prependfpath is-recursive-exist \
  check-if debug                  \
  is-callable is-true is-false    \
  hyperlink hyperlink-pr          \
  hyperlink-file hyperlink-file-pr

alias is=check-if
