#!/usr/bin/env zsh
#
# shellcheck disable=SC3043
#
# ./plugins/zsh-common/zsh-pyenv.plugin.zsh
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# This plugin loads pyenv into the current shell and provides prompt info via
# the 'pyenv_prompt_info' function. Also loads pyenv-virtualenv if available.

# Load pyenv only if command not already available
if command -v pyenv 1>/dev/null 2>&1 && [[ "$(uname -r)" != *icrosoft* ]]; then
  FOUND_PYENV=1
else
  FOUND_PYENV=0
fi

if [ $FOUND_PYENV -ne 1 ]; then
  pyenvdirs=(
    "$HOME/.local/share/pyenv"
    "$HOME/.pyenv"
    "/usr/local/pyenv"
    "/opt/programs/pyenv"
    "/opt/pyenv"
    "/usr/local/opt/pyenv"
  )

  for dir in $pyenvdirs; do
    if [ -d $dir/bin ]; then
      export PATH="$PATH:$dir/bin"
      FOUND_PYENV=1
      break
    fi
  done
fi

if [ $FOUND_PYENV -ne 1 ]; then
  if (($ + commands[brew])) && dir=$(brew --prefix pyenv 1>/dev/null 2>&1); then
    if [ -d $dir/bin ]; then
      export PATH="$PATH:$dir/bin"
      FOUND_PYENV=1
    fi
  fi
fi

if [ $FOUND_PYENV -eq 1 ]; then
  # eval "$(pyenv init --no-rehash zsh)"
  eval "$(pyenv init -)"

  if (($ + commands[pyenv - virtualenv - init])); then
    # eval "$(pyenv virtualenv-init zsh)"
    eval "$(pyenv virtualenv-init -)"
  fi

  function pyenv_prompt_info() {
    echo "$(pyenv version-name)"
  }
else
  # Fallback to system python
  function pyenv_prompt_info() {
    echo "system: $(python -V 2>&1 | cut -f 2 -d ' ')"
  }
fi

unset FOUND_PYENV pyenvdirs dir
