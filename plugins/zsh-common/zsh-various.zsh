#!/usr/bin/env zsh
#
# shellcheck disable=SC3043
#
# ./plugins/zsh-common/zsh-various.zsh
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

eval "$(dircolors)"
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
