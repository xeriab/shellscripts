#!/usr/bin/env zsh
#
# shellcheck disable=SC3043
#
# ./plugins/zsh-common/zsh-vscodium.plugin.zsh
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# Use the stable VS Code release, unless the Insiders version is the only
# available installation
if ! command -v codium >/dev/null && command -v code-insiders >/dev/null; then
  # : ${VSCODIUM:=code-insiders}
  : ${VSCODIUM:=$(command -v code-insiders)}
else
  # : ${VSCODIUM:=codium}
  : ${VSCODIUM:=$(command -v codium)}
fi

# Define aliases
alias vsc="$VSCODIUM ."
alias vsca="$VSCODIUM --add"
alias vscd="$VSCODIUM --diff"
alias vscg="$VSCODIUM --goto"
alias vscn="$VSCODIUM --new-window"
alias vscr="$VSCODIUM --reuse-window"
alias vscw="$VSCODIUM --wait"
alias vscu="$VSCODIUM --user-data-dir"
alias vsced="$VSCODIUM --extensions-dir"
alias vscie="$VSCODIUM --install-extension"
alias vscue="$VSCODIUM --uninstall-extension"
alias vscv="$VSCODIUM --verbose"
alias vscl="$VSCODIUM --log"
alias vscde="$VSCODIUM --disable-extensions"

# alias code="$VSCODIUM"

# Define sst only if sudo exists
(($ + commands[sudo])) && alias svsc="sudo '$VSCODIUM'"

alias code="'$VSCODIUM'"

(($ + commands[sudo])) && alias svsc="sudo '$VSCODIUM'"
