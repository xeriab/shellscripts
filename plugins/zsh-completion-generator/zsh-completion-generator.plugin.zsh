#!/usr/bin/env zsh
#
# shellcheck disable=SC3043
#
# ./plugins/zsh-completion-generator/zsh-completion-generator.plugin.zsh
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

0=${(%):-%N}
source ${0:A:h}/zsh-completion-generator.zsh 1>/dev/null 2>&1
