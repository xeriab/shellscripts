#!/usr/bin/env zsh
#
# shellcheck disable=SC3043
#
# ./plugins/zsh-mouse/zsh-mouse.plugin.zsh
#
# -*- tab-width: 2; encoding: utf-8; mode: zsh; -*-
#
# Copyright (c) 2019-2022 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

[ -n $ZSH_USE_MOUSE ] && [ "$ZSH_USE_MOUSE" -eq 1 ] || [ "$ZSH_USE_MOUSE" = true ] && {
  0=${(%):-%N}
  source ${0:A:h}/zsh-mouse.zsh 1>/dev/null 2>&1
  zle-mouse-toggle
}
