#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2154 disable=SC2181
#
# ./test.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Global
#

# TEMP_DIR=$(mktemp -d)
# OUT_FILE="${TEMP_DIR}/Log.Log"
# SAVEIFS=$IFS

# # shellcheck disable=SC2034
# Self=$(eval basename "$0" .sh)

# shellcheck disable=SC2034
Self='Test'

# shellcheck disable=SC2034
DEBUG=True

# shellcheck disable=SC2034
DEBUGGABLE=True

# shellcheck disable=SC2034
LOG_ENABLE_FULL_NAME=True

# shellcheck disable=SC2034
LOG_ENABLE_DATETIME=False

# shellcheck disable=SC2034
LOG_ENABLE_TITLE=True

# shellcheck disable=SC2034
LOG_ENABLE_FULL_PATH=False

# shellcheck disable=SC2034
LOG_ENABLE_FILE_NAME=False

# shellcheck disable=SC2034
LOG_ENABLE_BOLD_TITLE=False

# shellcheck disable=SC2034
LOG_IS_DECORATED=True

# shellcheck disable=SC2034
TEMP_DIR="/tmp/${Self}-tmp.XXXXXXXXXX"
# TEMP_DIR=$(mktemp -d "$TEMP_DIR")

# touch "${TEMP_DIR}/script.Log"

# shellcheck disable=SC2034
OUT_FILE="${TEMP_DIR}/script.Log"

#
# endregion Global

# region Imports
#

# load loader.sh
#
if [ "$(command ps -p $$ -ocomm=)" = "bash" ] || [ -n "$BASH" ]; then
  # shellcheck disable=SC1090 disable=SC1091
  . "./lib/compat/bash/loader.bash"
elif [ "$(command ps -p $$ -ocomm=)" = "zsh" ] || [ -n "$ZSH_NAME" ]; then
  # shellcheck disable=SC1090 disable=SC1091
  . "./lib/compat/zsh/loader.zsh"
elif [ "$(command ps -p $$ -ocomm=)" = "ksh" ] || [ -n "$TMOUT" ]; then
  # shellcheck disable=SC1090 disable=SC1091
  . "./lib/compat/ksh/loader.ksh"
else
  # shellcheck disable=SC1090 disable=SC1091
  . "./lib/compat/default/loader.sh"
fi

# Add directories to search path.
#
loader_addpath "lib"

# Load main script.
#
# load global.sh
# load os.sh
# load ansi.sh
# load ansi-functions.sh
# load terminal.sh
# load logger.sh
# load array.sh
# load string.sh
# load math.sh
# load utils.sh
# load assert.sh
load stdlib.sh

#
# endregion Imports

# region Functions
#

# Main
#
Main() {
  for Each in ./tests/*_test.sh; do
    local TestName=

    TestName=$(basename "$Each" "_test.sh" | command sed -Ere 's~\b(\w)~\u\1~g')
    # TestName="${TestName}Test"

    Each=$(realpath "$Each")

    if [ "$TestName" = "Common" ]; then
      printf '\n'

      LogHeader "${FgBrightWhite}Test${Reset} ${FgBrightBlack}'${Reset}${FgCyan}${TestName}${Reset}${FgBrightBlack}'${Reset}"

      printf '\n'

      Log "Test: %s\n" One

      # shellcheck disable=SC1090 disable=SC1091
      . "$Each"

      printf '\n'
    fi

    # LogFooter
  done

  unset Each

  return 0
}

#
# endregion Functions

# region Main Login
#

# Remove loader from shellspace since we no longer need it.
#
loader_finish

trap 'rm -rf $TEMP_DIR; exit 0' INT TERM EXIT

Main "$@"
exit 0

#
# endregion Main Login

#  vim: set ts=2 sw=2 tw=80 noet :
