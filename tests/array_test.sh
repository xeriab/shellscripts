#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2154 disable=SC2181
#
# ./tests/array_test.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Global
#

# # shellcheck disable=SC2034
# Self=$(eval basename "$0" .sh)

# shellcheck disable=SC2034
Self='ArrayTest'

# shellcheck disable=SC2034
TEMP_DIR="/tmp/${Self}-tmp.XXXXXXXXXX"

# shellcheck disable=SC2034
OUT_FILE="${TEMP_DIR}/script.Log"

#
# endregion Global

# region Imports
#



#
# endregion Imports

# region Tests
#

# TestArray
#
TestArray() {
  local Arr=
  local Index=
  local Val=
  local Len=
  local E=
  local A1=
  local Joined=
  local ExpectEncodedE=
  local ReallyEncodedE=
  local ReallyDecodedE=

  # TestArrayDefine
  #
  TestArrayDefine() {
    LogHeader "TestArray::Define"

    # Arr=$(Array -n ' bob' ross '' khan ross '/\#$^' ro)

    ArrayDefine Arr

    Arr=$(ArrayAppend Foo Bar Baz)

    local Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array${Reset}${Faint}:${Reset} ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Len=$(printf '%s\n' "$Arr" | ArrayLen) || {
      LogFailure "ArrayDefine Index failed" >&2
    }

    if [ "$Len" -ne 3 ]; then
      LogFailure "ArrayDefine expected 3, got $Len" >&2
    else
      LogSuccess "ArrayDefine got 3."
    fi

    LogFooter
  }

  # TestArrayIndexOf
  #
  TestArrayIndexOf() {
    LogHeader "TestArray::IndexOf"

    Arr=$(Array -n ' bob' ross '' khan ross '/\#$^' ro)

    local Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array${Reset}${Faint}:${Reset} ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf -n) || {
      LogFailure "ArrayIndexOf Index of -n failed" >&2
    }

    if [ "$Index" -ne 0 ]; then
      LogFailure "ArrayIndexOf Index of -n, expected 0, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of -n passed."
    fi

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf khan) || {
      LogFailure "ArrayIndexOf Index of khan failed" >&2
    }

    if [ "$Index" -ne 4 ]; then
      LogFailure "ArrayIndexOf Index of khan, expected 4, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of khan passed."
    fi

    if (printf '%s\n' "$Arr" | ArrayIndexOf tim) 1>/dev/null 2>&1; then
      LogFailure "ArrayIndexOf Index of tim should fail, got $(printf '%s\n' "$Arr" | ArrayIndexOf tim)" >&2
    fi

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf '') || {
      LogFailure "ArrayIndexOf Index of '' failed" >&2
    }

    if [ "$Index" -ne 3 ]; then
      LogFailure "ArrayIndexOf Index of '', expected 3, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of '' passed."
    fi

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf ross) || {
      LogFailure "ArrayIndexOf Index of ross failed" >&2
    }

    if [ "$Index" -ne 2 ]; then
      LogFailure "ArrayIndexOf Index of ross, expected 2, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of ross passed."
    fi

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf '/\#$^') || {
      LogFailure "ArrayIndexOf Index of '/\#$^' failed" >&2
    }

    if [ "$Index" -ne 6 ]; then
      LogFailure "ArrayIndexOf Index of '/\#$^', expected 6, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of '/\#$^' passed."
    fi

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf ro) || {
      LogFailure "ArrayIndexOf Index of ro failed" >&2
    }

    if [ "$Index" -ne 7 ]; then
      LogFailure "ArrayIndexOf Index of ro, expected 7, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of ro passed."
    fi

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf ' bob') || {
      LogFailure "ArrayIndexOf Index of ' bob' failed" >&2
    }

    if [ "$Index" -ne 1 ]; then
      LogFailure "ArrayIndexOf Index of ' bob', expected 1, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of ' bob' passed."
    fi

    LogFooter
  }

  # TestArrayNth
  #
  TestArrayNth() {
    LogHeader "TestArray::Nth"

    local Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array Before${Reset}${Faint}:${Reset} ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Arr=$(Array -e ' bob' ross '' khan ross '/\#$^' ro)

    Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array After${Reset}${Faint}:${Reset}  ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Val=$(printf '%s\n' "$Arr" | ArrayNth 3) || {
      LogFailure "ArrayNth 3 failed" >&2
    }

    if ! [ "$Val" = '' ]; then
      LogFailure "ArrayNth that should return the empty string, didn't." >&2
    else
      LogSuccess "ArrayNth returns non empty string."
    fi

    LogFooter
  }

  # TestArrayElementEncodeAndArrayElementDecode
  #
  TestArrayElementEncodeAndArrayElementDecode() {
    LogHeader "TestArray::ElementEncodeAndElementDecode"

    E='look
ma
three lines and a %'
    ExpectEncodedE='look%0Ama%0Athree lines and a %25'
    ReallyEncodedE=$(printf '%s\n' "$E" | ArrayElementEncode)

    if ! [ "$ExpectEncodedE" = "$ReallyEncodedE" ]; then
      LogFailure "ArrayElementEncode doesn't do the right thing" >&2
      Log -e "  wanted: $ExpectEncodedE" >&2
      Log -e " but got: $ReallyEncodedE" >&2
    fi

    # ReallyDecodedE=$(echo "$ReallyEncodedE" | ArrayElementDecode)
    ReallyDecodedE=$(printf '%s\n' "$ReallyEncodedE" | ArrayElementDecode)

    if ! [ "$E" = "$ReallyDecodedE" ]; then
      LogFailure "ArrayElementDecode doesn't do the right thing" >&2
      Log -e "  wanted: $E" >&2
      Log -e " but got: $ReallyDecodedE" >&2
    fi

    A1=$(Array "$E")

    local Array=

    for Each in $A1; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array${Reset}${Faint}:${Reset} ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    if ! [ "$(printf '%s\n' "$A1" | ArrayNth 0)" = "$E" ]; then
      LogFailure "ArrayNth cannot handle encoded elements" >&2
    else
      LogSuccess "ArrayNth can handle encoded elements."
    fi

    if [ "$(printf '%s\n' "$A1" | ArrayIndexOf "$E")" -ne 0 ]; then
      LogFailure "ArrayIndexOf cannot handle encoded elements" >&2
    else
      LogSuccess "ArrayIndexOf can handle encoded elements."
    fi

    LogFooter
  }

  # TestArrayAppend
  #
  TestArrayAppend() {
    LogHeader "TestArray::Append"

    local Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array Before${Reset}${Faint}:${Reset} ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Arr=$(ArrayAppend "$Arr" next)

    Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array After${Reset}${Faint}:${Reset}  ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf next) || {
      LogFailure "ArrayIndexOf Index of next failed" >&2
    }

    if [ "$Index" -ne 8 ]; then
      LogFailure "ArrayIndexOf Index of next, expected 8, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of next passed."
    fi

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf ' bob') || {
      LogFailure "ArrayIndexOf Index of ' bob' failed" >&2
    }

    if [ "$Index" -ne 1 ]; then
      LogFailure "ArrayIndexOf Index of ' bob', expected 1, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of ' bob' passed."
    fi

    LogFooter
  }

  # TestArrayPop
  #
  TestArrayPop() {
    LogHeader "TestArray::Pop"

    local Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array Before${Reset}${Faint}:${Reset} ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Arr=$(ArrayPop "$Arr")

    Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array After${Reset}${Faint}:${Reset}  ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf ro) || {
      LogFailure "ArrayIndexOf Index of ro failed" >&2
    }

    if [ "$Index" -ne 7 ]; then
      LogFailure "ArrayIndexOf Index of ro, expected 7, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of ro passed."
    fi

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf ' bob') || {
      LogFailure "ArrayIndexOf Index of ' bob' failed" >&2
    }

    if [ "$Index" -ne 1 ]; then
      LogFailure "ArrayIndexOf Index of ' bob', expected 1, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of ' bob' passed."
    fi

    LogFooter
  }

  # TestArrayPrepend
  #
  TestArrayPrepend() {
    LogHeader "TestArray::Prepend"

    local Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array Before${Reset}${Faint}:${Reset} ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Arr=$(ArrayPrepend "$Arr" previous)

    Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array After${Reset}${Faint}:${Reset}  ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf previous) || {
      LogFailure "ArrayIndexOf Index of previous failed" >&2
    }

    if [ "$Index" -ne 0 ]; then
      LogFailure "ArrayIndexOf Index of previous, expected 0, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of previous passed."
    fi

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf ' bob') || {
      LogFailure "ArrayIndexOf Index of ' bob' failed" >&2
    }

    if [ "$Index" -ne 2 ]; then
      LogFailure "ArrayIndexOf Index of ' bob', expected 2, got $Index" >&2
    else
      LogSuccess "ArrayIndexOf Index of ' bob' passed."
    fi

    LogFooter
  }

  # TestArrayLen
  #
  TestArrayLen() {
    LogHeader "TestArray::Len"

    local Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array${Reset}${Faint}:${Reset} ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Len=$(printf '%s\n' "$Arr" | ArrayLen) || {
      LogFailure "ArrayLen Index failed" >&2
    }

    if [ "$Len" -ne 8 ]; then
      LogFailure "ArrayLen expected 8, got $Len" >&2
    else
      LogSuccess "ArrayLen got 8."
    fi

    LogFooter
  }

  # TestArrayRemove
  #
  TestArrayRemove() {
    LogHeader "TestArray::Remove"

    local Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array Before${Reset}${Faint}:${Reset} ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Arr=$(printf '%s\n' "$Arr" | ArrayRemove previous)

    # shellcheck disable=SC2016
    LogInfo 'ArrayRemove $(printf "%s\\n" "$Arr" | ArrayRemove previous)'

    Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array After${Reset}${Faint}:${Reset}  ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    if (printf '%s\n' "$Arr" | ArrayIndexOf previous) 1>/dev/null 2>&1; then
      LogFailure "ArrayRemove Index of previous should fail, got $(printf '%s\n' "$Arr" | ArrayIndexOf previous)" >&2
    fi

    # shellcheck disable=SC2016
    LogInfo 'ArrayRemove $(Array first second third first)'

    Arr=$(Array first second third first)

    Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array After${Reset}${Faint}:${Reset}  ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    # shellcheck disable=SC2016
    LogInfo 'ArrayRemove $(printf "%s\\n" "$Arr" | ArrayRemove first)'

    Arr=$(printf '%s\n' "$Arr" | ArrayRemove first)

    Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array After${Reset}${Faint}:${Reset}  ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Index=$(printf '%s\n' "$Arr" | ArrayIndexOf first) || {
      LogFailure "ArrayRemove Index of first failed" >&2
    }

    if [ "$Index" -ne 2 ]; then
      LogFailure "ArrayRemove expected 2, got $Index" >&2
    else
      LogSuccess "ArrayRemove got 2."
    fi

    Arr=$(printf '%s\n' "$Arr" | ArrayRemove first)

    # shellcheck disable=SC2016
    LogInfo 'ArrayRemove $(printf "%s\\n" "$Arr" | ArrayRemove first)'

    Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array After${Reset}${Faint}:${Reset}  ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    if (printf '%s\n' "$Arr" | ArrayIndexOf first) 1>/dev/null 2>&1; then
      LogFailure "ArrayRemove Index of first should fail, got '%s'." "$(printf '%s\n' "$Arr" | ArrayIndexOf first)" >&2
    fi

    LogFooter
  }

  # TestArrayMisc
  #
  TestArrayMisc() {
    LogHeader "TestArray::Misc"

    Arr=$(Array Foo Bar Baz)

    local Array=

    for Each in $Arr; do
      Array="${Array}${Faint}'${Reset}${Each}${Faint}'${Reset}, "
    done
    unset Each

    Array=$(StrTrimEnd "$Array" ', ')
    Array=$(StrReplace -m plain ',' "${FgBrightBlack},${Reset}" "$Array")

    Log -r "${Bold}Array${Reset}${Faint}:${Reset} ${FgBrightBlack}[${Reset}%s${FgBrightBlack}]${Reset}\n" "$Array"

    Joined=$(ArrayJoin -s ',' "$Arr")
    Log "Joined: '${Joined}'"

    LogFooter
  }

  # Test Calls

  TestArrayDefine
  TestArrayIndexOf
  TestArrayNth
  TestArrayElementEncodeAndArrayElementDecode
  TestArrayAppend
  TestArrayPop
  TestArrayPrepend
  TestArrayLen
  TestArrayRemove
  TestArrayMisc

  # Misc

  # Joined=$(ArrayJoin -s ',' "$Arr")

  # Log "Joined: '${Joined}'"

  # Log "ArrayGet: '$(ArrayGet 0 "$Arr")'"
  # Log "ArraySize: '$(ArraySize "$Arr")'"
  # Log "ArrayShow: '$(ArrayShow ',' "$Arr")'"

  # LogHeader "TestArray::Misc"
  # ArrayDefine TestArray
  # ArrayAppend "$TestArray" 0 1 2 3
  # printf '%s\n' "$TestArray" | ArrayElementDecode
  # LogFooter

  unset Arr Index Val Len E A1 Joined \
    ExpectEncodedE ReallyEncodedE ReallyDecodedE

  printf '%s\n' "${Bold}${FgBrightBlue}Tests successful!${Reset}"

  return 0
}

#
# endregion Tests

# region Functions
#

# Main
#
Main() {
  TestArray
  return 0
}

#
# endregion Functions

# region Main Login
#

# # Remove loader from shellspace since we no longer need it.
# #
# loader_finish

trap 'rm -rf $TEMP_DIR; exit 0' INT TERM EXIT

Main "$@"
# exit 0

#
# endregion Main Login

#  vim: set ts=2 sw=2 tw=80 noet :
