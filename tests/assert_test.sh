#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2154 disable=SC2181
#
# ./tests/assert_test.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Global
#

# # shellcheck disable=SC2034
# Self=$(eval basename "$0" .sh)

# shellcheck disable=SC2034
Self='AssertTest'

# shellcheck disable=SC2034
TEMP_DIR="/tmp/${Self}-tmp.XXXXXXXXXX"

# shellcheck disable=SC2034
OUT_FILE="${TEMP_DIR}/script.Log"

#
# endregion Global

# region Imports
#



#
# endregion Imports

# region Tests
#

# TestAssert
#
TestAssert() {
  TestAssertEq() {
    LogHeader "TestAssert::AssertEq"

    AssertEq "Hello" "Hello"

    if [ $? -eq 0 ]; then
      LogSuccess "AssertEq returns 0 if two words are equal"
    else
      LogFailure "AssertEq should return 0"
    fi

    AssertEq "Hello" "World"

    if [ $? -eq 1 ]; then
      LogSuccess "AssertEq returns 1 if two words are not equal"
    else
      LogFailure "AssertEq should return 1"
    fi

    LogFooter
  }

  TestAssertNotEq() {
    LogHeader "TestAssert::AssertNotEq"

    AssertNotEq "Hello" "Hello"

    if [ $? = 1 ]; then
      LogSuccess "AssertNotEq returns 1 if two words are equivalent"
    else
      LogFailure "AssertNotEq should return 1"
    fi

    AssertNotEq "Hello" "World"

    if [ $? = 0 ]; then
      LogSuccess "AssertNotEq returns 0 if two params are not equal"
    else
      LogFailure "AssertNotEq should return 0"
    fi

    LogFooter
  }

  TestAssertTrue() {
    LogHeader "TestAssert::AssertTrue"

    AssertTrue True

    if [ $? = 0 ]; then
      LogSuccess "AssertTrue returns 0 if param is True"
    else
      LogFailure "AssertTrue does not work"
    fi

    AssertTrue False

    if [ $? = 1 ]; then
      LogSuccess "AssertTrue returns 1 if param is False"
    else
      LogFailure "AssertTrue does not work"
    fi

    LogFooter
  }

  TestAssertFalse() {
    LogHeader "TestAssert::AssertFalse"

    AssertFalse False

    if [ $? = 0 ]; then
      LogSuccess "AssertFalse returns 0 if param is False"
    else
      LogFailure "AssertFalse does not work"
    fi

    AssertFalse True

    if [ $? = 1 ]; then
      LogSuccess "AssertFalse returns 1 if param is True"
    else
      LogFailure "AssertFalse does not work"
    fi

    LogFooter
  }

  # TestAssertArrayEq() {
  #   LogHeader "TestAssert::AssertArrayEq"

  #   declare -a exp
  #   declare -a act

  #   exp=("one" "tw oo" "333")
  #   act=("one" "tw oo" "333")
  #   AssertArrayEq exp[@] act[@] "Should not be equal"
  #   if [ $? = 0 ]; then
  #     LogSuccess "AssertArrayEq returns 0 if two arrays are equal by values"
  #   else
  #     LogFailure "AssertArrayEq should return 0"
  #   fi

  #   exp=("one")
  #   act=("one" "tw oo" "333")
  #   AssertArrayEq exp[@] act[@] # it can be an issue on other implementation of shell
  #   if [ $? = 1 ]; then
  #     LogSuccess "AssertArrayEq returns 1 if the lengths of the two arrays are not equal"
  #   else
  #     LogFailure "AssertArrayEq should return 1"
  #   fi

  #   exp=("one" "222" "333")
  #   act=("one" "tw oo" "333")
  #   AssertArrayEq exp[@] act[@]
  #   if [ $? = 1 ]; then
  #     LogSuccess "AssertArrayEq returns 1 if two arrays are not equal"
  #   else
  #     LogFailure "AssertArrayEq should return 1"
  #   fi

  #   exp=()
  #   act=("one" "tw oo" "333")
  #   AssertArrayEq exp[@] act[@]
  #   if [ $? = 1 ]; then
  #     LogSuccess "AssertArrayEq returns 1 if one array is empty"
  #   else
  #     LogFailure "AssertArrayEq should return 1"
  #   fi

  # }

  # TestAssertArrayNotEq() {
  #   LogHeader "TestAssert::AssertArrayNotEq"

  #   declare -a exp
  #   declare -a act

  #   exp=("one" "tw oo" "333")
  #   act=("one" "tw oo" "333")
  #   AssertArrayNotEq exp[@] act[@]
  #   if [ $? = 1 ]; then
  #     LogSuccess "AssertArrayNotEq returns 1 if two arrays are equal by values"
  #   else
  #     LogFailure "AssertArrayNotEq should return 1"
  #   fi

  #   exp=("one")
  #   act=("one" "tw oo" "333")
  #   AssertArrayNotEq exp[@] act[@] # it can be an issue on other implementation of shell
  #   if [ $? = 0 ]; then
  #     LogSuccess "AssertArrayNotEq returns 0 if the lengths of the two arrays are not equal"
  #   else
  #     LogFailure "AssertArrayNotEq should return 0"
  #   fi

  #   exp=("one" "222" "333")
  #   act=("one" "tw oo" "333")
  #   AssertArrayNotEq exp[@] act[@]
  #   if [ $? = 0 ]; then
  #     LogSuccess "AssertArrayNotEq returns 0 if two arrays are not equal"
  #   else
  #     LogFailure "AssertArrayNotEq should return 0"
  #   fi

  #   exp=()
  #   act=("one" "tw oo" "333")
  #   AssertArrayNotEq exp[@] act[@]
  #   if [ $? = 0 ]; then
  #     LogSuccess "AssertArrayNotEq returns 0 if one array is empty"
  #   else
  #     LogFailure "AssertArrayNotEq should return 0"
  #   fi

  # }

  TestAssertEmpty() {
    LogHeader "TestAssert::AssertEmpty"

    AssertEmpty ""

    if [ $? = 0 ]; then
      LogSuccess "AssertEmpty returns 0 if param is empty string"
    else
      LogFailure "AssertEmpty does not work"
    fi

    AssertEmpty "some text"

    if [ $? = 1 ]; then
      LogSuccess "AssertEmpty returns 1 if param is some text string"
    else
      LogFailure "AssertEmpty does not work"
    fi

    AssertEmpty "\n"

    if [ $? = 1 ]; then
      LogSuccess "AssertEmpty returns 1 if param is some white space"
    else
      LogFailure "AssertEmpty does not work"
    fi

    LogFooter
  }

  TestAssertNotEmpty() {
    LogHeader "TestAssert::AssertNotEmpty"

    AssertNotEmpty "some text"

    if [ $? = 0 ]; then
      LogSuccess "AssertNotEmpty returns 0 if param is some text string"
    else
      LogFailure "AssertNotEmpty does not work"
    fi

    AssertNotEmpty "\n"

    if [ $? = 0 ]; then
      LogSuccess "AssertNotEmpty returns 0 if param is some white space"
    else
      LogFailure "AssertNotEmpty does not work"
    fi

    AssertNotEmpty ""

    if [ $? = 1 ]; then
      LogSuccess "AssertNotEmpty returns 1 if param is empty string"
    else
      LogFailure "AssertNotEmpty does not work"
    fi

    LogFooter
  }

  TestAssertContain() {
    LogHeader "TestAssert::AssertContain"

    AssertContain "haystack" "needle"

    if [ $? = 1 ]; then
      LogSuccess "AssertContain returns 1 if the needle is not in the haystack"
    else
      LogFailure "AssertContain does not work"
    fi

    AssertContain "haystack"

    if [ $? = 0 ]; then
      LogSuccess "AssertContain returns 0 if no needle is given"
    else
      LogFailure "AssertContain does not work"
    fi

    AssertContain "haystack" "stack"

    if [ $? = 0 ]; then
      LogSuccess "AssertContain returns 0 if the haystack ends in the needle"
    else
      LogFailure "AssertContain does not work"
    fi

    AssertContain "haystack" "hay"

    if [ $? = 0 ]; then
      LogSuccess "AssertContain returns 0 if the haystack starts with the needle"
    else
      LogFailure "AssertContain does not work"
    fi

    AssertContain "haystack" "aysta"

    if [ $? = 0 ]; then
      LogSuccess "AssertContain returns 0 if the needle is somewhere in the middle of the haystack"
    else
      LogFailure "AssertContain does not work"
    fi

    AssertContain "foo\nbar\nhello\nworld" "foo"

    if [ $? = 0 ]; then
      LogSuccess "AssertContain returns 0 if the needle matches the first haystack line"
    else
      LogFailure "AssertContain does not work"
    fi

    AssertContain "foo\nbar\nhello\nworld" "bar"

    if [ $? = 0 ]; then
      LogSuccess "AssertContain returns 0 if the needle matches the second haystack line"
    else
      LogFailure "AssertContain does not work"
    fi

    AssertContain "foo\nbar\nhello\nworld" "ell"

    if [ $? = 0 ]; then
      LogSuccess "AssertContain returns 0 if the needle is on the third haystack line"
    else
      LogFailure "AssertContain does not work"
    fi

    AssertContain "foo\nbar\nhello\nworld" "barbecue"

    if [ $? = 1 ]; then
      LogSuccess "AssertContain returns 1 if the needle is not in a multi-line haystack"
    else
      LogFailure "AssertContain does not work"
    fi

    LogFooter
  }

  TestAssertNotContain() {
    LogHeader "TestAssert::AssertNotContain"

    AssertNotContain "haystack" "needle"

    if [ $? = 0 ]; then
      LogSuccess "AssertNotContain returns 0 if the needle is not in the haystack"
    else
      LogFailure "AssertNotContain does not work"
    fi

    AssertNotContain "haystack"

    if [ $? = 0 ]; then
      LogSuccess "AssertNotContain returns 0 if no needle is given"
    else
      LogFailure "AssertNotContain does not work"
    fi

    AssertNotContain "haystack" "stack"

    if [ $? = 1 ]; then
      LogSuccess "AssertNotContain returns 1 if the haystack ends in the needle"
    else
      LogFailure "AssertNotContain does not work"
    fi

    AssertNotContain "haystack" "hay"

    if [ $? = 1 ]; then
      LogSuccess "AssertNotContain returns 1 if the haystack starts with the needle"
    else
      LogFailure "AssertNotContain does not work"
    fi

    AssertNotContain "haystack" "aysta"

    if [ $? = 1 ]; then
      LogSuccess "AssertNotContain returns 1 if the needle is somewhere in the middle of the haystack"
    else
      LogFailure "AssertNotContain does not work"
    fi

    AssertNotContain "foo\nbar\nhello\nworld" "foo"

    if [ $? = 1 ]; then
      LogSuccess "AssertNotContain returns 1 if the needle matches the first haystack line"
    else
      LogFailure "AssertNotContain does not work"
    fi

    AssertNotContain "foo\nbar\nhello\nworld" "bar"

    if [ $? = 1 ]; then
      LogSuccess "AssertNotContain returns 1 if the needle matches the second haystack line"
    else
      LogFailure "AssertNotContain does not work"
    fi

    AssertNotContain "foo\nbar\nhello\nworld" "ell"

    if [ $? = 1 ]; then
      LogSuccess "AssertNotContain returns 1 if the needle is on the third haystack line"
    else
      LogFailure "AssertNotContain does not work"
    fi

    AssertNotContain "foo\nbar\nhello\nworld" "barbecue"

    if [ $? = 0 ]; then
      LogSuccess "AssertNotContain returns 0 if the needle is not in a multi-line haystack"
    else
      LogFailure "AssertNotContain does not work"
    fi

    LogFooter
  }

  TestAssertGt() {
    LogHeader "TestAssert::AssertGt"

    AssertGt 1 2

    if [ $? = 1 ]; then
      LogSuccess "AssertGt returns 1 if second param is greater than first param"
    else
      LogFailure "AssertGt does not work"
    fi

    AssertGt 2 -1

    if [ $? = 0 ]; then
      LogSuccess "AssertGt returns 0 if first param is greater than second param"
    else
      LogFailure "AssertGt does not work"
    fi

    AssertGt 2 2

    if [ $? = 1 ]; then
      LogSuccess "AssertGt returns 1 if two params are equal"
    else
      LogFailure "AssertGt does not work"
    fi

    LogFooter
  }

  TestAssertGe() {
    LogHeader "TestAssert::AssertGe"

    AssertGe 1 1

    if [ $? = 0 ]; then
      LogSuccess "AssertGe returns 0 if two params are equal"
    else
      LogFailure "AssertGe does not work"
    fi

    AssertGe 1 2

    if [ $? = 1 ]; then
      LogSuccess "AssertGe returns 1 if second param is greater than first param"
    else
      LogFailure "AssertGe does not work"
    fi

    AssertGe 2 -1

    if [ $? = 0 ]; then
      LogSuccess "AssertGe returns 0 if first param is greater than second param"
    else
      LogFailure "AssertGe does not work"
    fi

    LogFooter
  }

  TestAssertLt() {
    LogHeader "TestAssert::AssertLt"

    AssertLt 2 1

    if [ $? = 1 ]; then
      LogSuccess "AssertLt returns 1 if second param is less than first param"
    else
      LogFailure "AssertLt does not work"
    fi

    AssertLt -2 1

    if [ $? = 0 ]; then
      LogSuccess "AssertLt returns 0 if first param is less than second param"
    else
      LogFailure "AssertLt does not work"
    fi

    AssertLt 2 2

    if [ $? = 1 ]; then
      LogSuccess "AssertLt returns 1 if two params are equal"
    else
      LogFailure "AssertLt does not work"
    fi

    LogFooter
  }

  TestAssertLe() {
    LogHeader "TestAssert::AssertLe"

    AssertLe 1 1

    if [ $? = 0 ]; then
      LogSuccess "AssertLe returns 0 if two params are equal"
    else
      LogFailure "AssertLe does not work"
    fi

    AssertLe 2 1

    if [ $? = 1 ]; then
      LogSuccess "AssertLe returns 1 if second param is less than first param"
    else
      LogFailure "AssertLe does not work"
    fi

    AssertLe -2 1

    if [ $? = 0 ]; then
      LogSuccess "AssertLe returns 0 if first param is less than second param"
    else
      LogFailure "AssertLe does not work"
    fi

    LogFooter
  }

  # Test Calls

  TestAssertEq
  TestAssertNotEq
  TestAssertTrue
  TestAssertFalse
  # TestAssertArrayEq
  # TestAssertArrayNotEq
  TestAssertEmpty
  TestAssertNotEmpty
  TestAssertContain
  TestAssertNotContain
  TestAssertGt
  TestAssertGe
  TestAssertLt
  TestAssertLe

  return 0
}

#
# endregion Tests

# region Functions
#

# Main
#
Main() {
  TestAssert
  return 0
}

#
# endregion Functions

# region Main Login
#

# # Remove loader from shellspace since we no longer need it.
# #
# loader_finish

trap 'rm -rf $TEMP_DIR; exit 0' INT TERM EXIT

Main "$@"
# exit 0

#
# endregion Main Login

#  vim: set ts=2 sw=2 tw=80 noet :
