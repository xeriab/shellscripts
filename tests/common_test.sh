#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2154 disable=SC2181
#
# ./tests/common_test.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Global
#

# # shellcheck disable=SC2034
# Self=$(eval basename "$0" .sh)

# shellcheck disable=SC2034
Self='CommonTest'

# shellcheck disable=SC2034
TEMP_DIR="/tmp/${Self}-tmp.XXXXXXXXXX"

# shellcheck disable=SC2034
OUT_FILE="${TEMP_DIR}/script.Log"

#
# endregion Global

# region Imports
#



#
# endregion Imports

# region Tests
#

# CommonTest
#
CommonTest() {
  # TestMisc
  #
  TestMisc() {
    LogHeader "TestCommon::Misc"

    Todo TestMisc

    sleep 0.1
    Log -e "Test 1"
    sleep 0.1
    Log -d "Test 2"
    sleep 0.1
    Log -i "Test 3"
    sleep 0.1
    Log -w "Test 4"
    sleep 0.1
    Log -v "Test 5"
    sleep 0.1
    Log -s "Test 6"
    sleep 0.1
    Log -f "Test 7"
    sleep 0.1
    Log -r "Test 8"
    sleep 0.1

    printf '\n'

    Log "Test 9"
    Log -r "Test %s\n" 10
    Log -v "$(OsName)"

    TEST=$(GetEnv JETBRAINS_CLION_BINa False)

    Log "$TEST"

    Log "$SOURCED_SCRIPTS_LIST"

    # Source './lib/math.sh'
    # Source './lib/ansi.sh'

    # GetSourcedScripts

    # IsSourced

    # LogSuccess "Test"

    # LogFailure "Test"

    # SetDefault EXEN 1
    # RequireVar EXEN

    # Log "Random Integer: $(RandomInt)"

    SetEnv Test1 0

    CheckFor "exen"

    # LogFailWithExit "tests.sh" "Test" TestCommon

    LogFooter
  }

  # Test Calls

  TestMisc

  return 0
}

#
# endregion Tests

# region Functions
#

# Main
#
Main() {
  CommonTest
  return 0
}

#
# endregion Functions

# region Main Login
#

# # Remove loader from shellspace since we no longer need it.
# #
# loader_finish

trap 'rm -rf $TEMP_DIR; exit 0' INT TERM EXIT

Main "$@"
# exit 0

#
# endregion Main Login

#  vim: set ts=2 sw=2 tw=80 noet :
