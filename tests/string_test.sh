#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2154 disable=SC2181
#
# ./tests/string_test.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Global
#

# # shellcheck disable=SC2034
# Self=$(eval basename "$0" .sh)

# shellcheck disable=SC2034
Self='StringTest'

# shellcheck disable=SC2034
TEMP_DIR="/tmp/${Self}-tmp.XXXXXXXXXX"

# shellcheck disable=SC2034
OUT_FILE="${TEMP_DIR}/script.Log"

#
# endregion Global

# region Imports
#



#
# endregion Imports

# region Tests
#

# TestString
#
TestString() {
  Self='StringTest'

  # TestStrSplit
  #
  TestStrSplit() {
    LogHeader "TestString::StrSplit"
    Log -v "$(StrSplit "Test, One")" # Test One
    LogFooter
  }

  # TestStrTrim
  #
  TestStrTrim() {
    LogHeader "TestString::StrTrim"
    Log -v "$(StrTrim " That's my boy! ")" # That\'s my boy!
    LogFooter
  }

  # TestStrEscape
  #
  TestStrEscape() {
    LogHeader "TestString::StrEscape"
    Log -v "$(StrEscape "That's my boy!")"                        # That\'s my boy!
    Log -v "$(StrEscape 'We have some "rules"')"                  # We have some \"rules\"
    Log -v "$(StrEscape -mode regex "He (Moody) loves to code.")" # He \(Moody\) loves to code\.
    LogFooter
  }

  # TestStrReplace
  #
  TestStrReplace() {
    LogHeader "TestString::StrReplace"
    Log -v "$(StrReplace "boy" "girl" "That's my boy.")"            # That's my girl.
    Log -v "$(StrReplace "John Doe" "John" "Jane")"                 # Jane Doe
    Log -v "$(StrReplace -mode regex "[^a-z]" "" "That's my boy.")" # hatsmyboy
    LogFooter
  }

  # TestStrReplaceAll
  #
  TestStrReplaceAll() {
    LogHeader "TestString::StrReplaceAll"
    Log -v "$(StrReplaceAll "*" "p4ssw0rd")" # ********
    LogFooter
  }

  # TestStrDashCamelCase
  #
  TestStrDashCamelCase() {
    LogHeader "TestString::StrDashCamelCase"
    Log -v "$(StrDashCamelCase "This-is-a-string")" # ThisIsAString
    LogFooter
  }

  # TestStrSeparatorCamelCase
  #
  TestStrSeparatorCamelCase() {
    LogHeader "TestString::StrSeparatorCamelCase"
    Log -v "$(StrSeparatorCamelCase " " "This is a string")" # ThisIsAString
    LogFooter
  }

  # TestStrUnderscoreCamelCase
  #
  TestStrUnderscoreCamelCase() {
    LogHeader "TestString::StrUnderscoreCamelCase"
    Log -v "$(StrUnderscoreCamelCase "This_is_a_string")" # ThisIsAString
    LogFooter
  }

  # TestStrCamelCaseDash
  #
  TestStrCamelCaseDash() {
    LogHeader "TestString::StrCamelCaseDash"
    Log -v "$(StrCamelCaseDash "ThisIsAString")" # This-Is-A-String
    LogFooter
  }

  # TestStrCamelCaseSeparator
  #
  TestStrCamelCaseSeparator() {
    LogHeader "TestString::StrCamelCaseSeparator"
    Log -v "$(StrCamelCaseSeparator " " "ThisIsAString")" # This Is A String
    LogFooter
  }

  # TestStrCamelCaseUnderscore
  #
  TestStrCamelCaseUnderscore() {
    LogHeader "TestString::StrCamelCaseUnderscore"
    Log -v "$(StrCamelCaseUnderscore "thisIsAString")" # this_Is_A_String
    LogFooter
  }

  # TestStrToCamel
  #
  TestStrToCamel() {
    LogHeader "TestString::StrToCamel"
    Log -v "$(StrToCamel "abc_def")"
    Log -v "$(StrToCamel "abc-def")"
    Log -v "$(StrToCamel "abc def")"
    Log -v "$(StrToCamel "Abc Def")"
    Log -v "$(StrToCamel "ABC_DEF")"
    Log -v "$(StrToCamel "ABC-DEF")"
    Log -v "$(StrToCamel "ABC DEF")"
    Log -v "$(StrToCamel "my-test-file")"
    Log -v "$(StrToCamel "My-Test-File")"
    Log -v "$(StrToCamel "MY-TEST-FILE")"
    LogFooter
  }

  # TestStrToPascal
  #
  TestStrToPascal() {
    LogHeader "TestString::StrToPascal"
    Log -v "$(StrToPascal "abc_def")"
    Log -v "$(StrToPascal "abc-def")"
    Log -v "$(StrToPascal "abc def")"
    Log -v "$(StrToPascal "Abc Def")"
    Log -v "$(StrToPascal "ABC_DEF")"
    Log -v "$(StrToPascal "ABC-DEF")"
    Log -v "$(StrToPascal "ABC DEF")"
    Log -v "$(StrToPascal "my-test-file")"
    Log -v "$(StrToPascal "My-Test-File")"
    Log -v "$(StrToPascal "MY-TEST-FILE")"
    LogFooter
  }

  # TestStrToSnake
  #
  TestStrToSnake() {
    LogHeader "TestString::StrToSnake"
    Log -v "$(StrToSnake "abc_def")"
    Log -v "$(StrToSnake "abc-def")"
    Log -v "$(StrToSnake "abc def")"
    Log -v "$(StrToSnake "Abc Def")"
    Log -v "$(StrToSnake "ABC_DEF")"
    Log -v "$(StrToSnake "ABC-DEF")"
    Log -v "$(StrToSnake "ABC DEF")"
    Log -v "$(StrToSnake "MyTestFile")"
    Log -v "$(StrToSnake "My-Test-File")"
    Log -v "$(StrToSnake "MY-TEST-FILE")"
    LogFooter
  }

  # TestStrToKebeb
  #
  TestStrToKebeb() {
    LogHeader "TestString::StrToKebeb"
    Log -v "$(StrToKebeb "abc_def")"
    Log -v "$(StrToKebeb "abc-def")"
    Log -v "$(StrToKebeb "abc def")"
    Log -v "$(StrToKebeb "Abc Def")"
    Log -v "$(StrToKebeb "ABC_DEF")"
    Log -v "$(StrToKebeb "ABC-DEF")"
    Log -v "$(StrToKebeb "ABC DEF")"
    Log -v "$(StrToKebeb "MyTestFile")"
    Log -v "$(StrToKebeb "My-Test-File")"
    Log -v "$(StrToKebeb "MY-TEST-FILE")"
    LogFooter
  }

  # TestStrToLower
  #
  TestStrToLower() {
    LogHeader "TestString::StrToLower"
    Log -v "$(StrToLower "Some STRING")" # some string
    LogFooter
  }

  # TestStrToUpper
  #
  TestStrToUpper() {
    LogHeader "TestString::StrToUpper"
    Log -v "$(StrToUpper "Some string")" # SOME STRING
    LogFooter
  }

  # TestStrToTitle
  #
  TestStrToTitle() {
    LogHeader "TestString::StrToTitle"
    Log -v "$(StrToTitle "some string")" # Some String
    LogFooter
  }

  # TestStrCountWords
  #
  TestStrCountWords() {
    LogHeader "TestString::StrCountWords"
    Words=$(StrCountWords "$(printf '%s' "Foo Bar Baz")")
    Log -v "Words: $Words"
    Words=$(StrCountWords "$(printf '%s' "Foo Bar")")
    Log -v "Words: $Words"
    LogFooter
  }

  # TestStrCountLines
  #
  TestStrCountLines() {
    LogHeader "TestString::StrCountLines"
    Lines=$(StrCountLines "$(printf '%s' "Foo\nBar\nBaz")")
    Log -v "Lines: $Lines"
    Lines=$(StrCountLines "$(printf '%s' "Foo\nBar")")
    Log -v "Lines: $Lines"
    LogFooter
  }

  # TestMisc
  #
  TestMisc() {
    LogHeader "TestString::Misc"
    Log -v "|$(StrPad "Test" 10 " " "right")|"
    Log -v "|$(StrPad "Test" 10 " " "left")|"
    Log -v "|$(StrPad "Test" 10 " " "center")|"
    Log -v "|$(StrPad "Name" 10 " " "left")|"
    Log -v "|$(StrPad "Name" $(($(TerminalWidth) - 21)) ' ' "right")|"
    Log -v "$(StrRepeat "Test" 10)"
    Log -v "$(StrSubStr "hello world" 0 5)"
    Log -v "$(StrSubStr "hello world" 4)"
    Log -v "$(StrSubStr "hello world" 6)"
    Log -v "$(CharAt "Hello" 0)"
    Log -v "$(CharAt)"
    Log -v "$(StrSpread "Hello")"
    Log -v "$(StrContains "Hello" 'X')"

    for Str in $(StrSplit "Test, One"); do
      Log -v "$Str"
    done

    unset Str

    Log -v "$(StringReplace -mode 'regex' "Test, Two" 'Two' 'One')"
    Log -v "$(StrStripAnsi "${FgBrightBlue}Test${Reset}")"

    LogFooter
  }

  # Test Calls

  TestStrSplit
  TestStrTrim
  TestStrEscape
  TestStrReplace
  TestStrReplaceAll
  TestStrDashCamelCase
  TestStrSeparatorCamelCase
  TestStrUnderscoreCamelCase
  TestStrCamelCaseDash
  TestStrCamelCaseSeparator
  TestStrCamelCaseUnderscore
  TestStrToCamel
  TestStrToPascal
  TestStrToSnake
  TestStrToKebeb
  TestStrToLower
  TestStrToUpper
  TestStrToTitle
  # TestStrCountWords
  # TestStrCountLines
  TestMisc

  printf '%s\n' "${Bold}${FgBrightBlue}Tests successful!${Reset}"

  return 0
}

#
# endregion Tests

# region Functions
#

# Main
#
Main() {
  TestString
  return 0
}

#
# endregion Functions

# region Main Login
#

# # Remove loader from shellspace since we no longer need it.
# #
# loader_finish

trap 'rm -rf $TEMP_DIR; exit 0' INT TERM EXIT

Main "$@"
# exit 0

#
# endregion Main Login

#  vim: set ts=2 sw=2 tw=80 noet :
