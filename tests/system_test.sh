#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2154 disable=SC2181
#
# ./tests/system_test.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Global
#

# # shellcheck disable=SC2034
# Self=$(eval basename "$0" .sh)

# shellcheck disable=SC2034
Self='SystemTest'

# shellcheck disable=SC2034
TEMP_DIR="/tmp/${Self}-tmp.XXXXXXXXXX"

# shellcheck disable=SC2034
OUT_FILE="${TEMP_DIR}/script.Log"

#
# endregion Global

# region Imports
#



#
# endregion Imports

# region Tests
#

# SystemTest
#
SystemTest() {
  # TestSystem
  #
  TestSystem() {
    LogHeader "SystemTest::System"

    LogFooter
  }

  # Test Calls

  TestSystem

  return 0
}

#
# endregion Tests

# region Functions
#

# Main
#
Main() {
  SystemTest
  return 0
}

#
# endregion Functions

# region Main Login
#

# # Remove loader from shellspace since we no longer need it.
# #
# loader_finish

trap 'rm -rf $TEMP_DIR; exit 0' INT TERM EXIT

Main "$@"
# exit 0

#
# endregion Main Login

#  vim: set ts=2 sw=2 tw=80 noet :
