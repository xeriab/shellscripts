#!/usr/bin/env sh
#
# shellcheck disable=SC3043 disable=SC2154 disable=SC2181
#
# ./tests/utils_test.sh
#
# -*- tab-width: 2; encoding: utf-8; mode: sh; -*-
#
# Copyright (c) 2019-2021 Xeriab Nabil <xeriab@tuta.io>
#
# SPDX-License-Identifier: MIT
#

# region Global
#

# # shellcheck disable=SC2034
# Self=$(eval basename "$0" .sh)

# shellcheck disable=SC2034
Self='UtilsTest'

# shellcheck disable=SC2034
TEMP_DIR="/tmp/${Self}-tmp.XXXXXXXXXX"

# shellcheck disable=SC2034
OUT_FILE="${TEMP_DIR}/script.Log"

#
# endregion Global

# region Imports
#



#
# endregion Imports

# region Tests
#

# TestUtils
#
TestUtils() {
  # TestBox
  #
  TestBox() {
    LogHeader "TestUtils::Box"

    BoxSetConfig '{
      "Px": 1,
      "Py": 1,

      // Values: Bold, Classic, Double, DoubleSingle, Hidden, Round, Single, SingleDouble
      "Type": "Round",

      // Border Color
      // "Color": "\\033[2m\\033[36m",
      "Color": "BrightBlack",

      // Values: Left, Right, Center
      "ContentAlign": "Center",

      // Values: Inside, Bottom, Top
      "TitlePos": "Top",

      // Values: Left, Right, Center
      "TitleAlign": "Left",

      // Maximum Terminal Width
      "MaxWidth": 0
    }'

    Box "${Bold}${FgBlue}Box Title${Reset}" "${FgBrightWhite}Box Message Body${Reset}"

    # HorizontalLine 'SingleDouble'

    # BoxSetConfig '{
    #   "Px": 1,
    #   "Py": 0,

    #   // Values: Bold, Classic, Double, DoubleSingle, Hidden, Round, Single, SingleDouble
    #   "Type": "Classic",

    #   // Border Color
    #   // "Color": "\033[2m\033[36m",
    #   "Color": "BrightBlack",

    #   // Values: Left, Right, Center
    #   "ContentAlign": "Center",

    #   // Values: Inside, Bottom, Top
    #   "TitlePos": "Top",

    #   // Values: Left, Right, Center
    #   "TitleAlign": "Left",

    #   // Maximum Terminal Width
    #   "MaxWidth": 0
    # }'

    # Box "${Bold}${FgBlue}Box Title${Reset}" "${FgBrightWhite}Box Message Body${Reset}"

    LogFooter

    return 0
  }

  # Test Calls

  TestBox

  return 0
}

#
# endregion Tests

# region Functions
#

# Main
#
Main() {
  TestUtils
  return 0
}

#
# endregion Functions

# region Main Login
#

# # Remove loader from shellspace since we no longer need it.
# #
# loader_finish

trap 'rm -rf $TEMP_DIR; exit 0' INT TERM EXIT

Main "$@"
# exit 0

#
# endregion Main Login

#  vim: set ts=2 sw=2 tw=80 noet :
